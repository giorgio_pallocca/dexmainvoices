<?php
set_include_path(".:/usr/share/php:/usr/share/pear:/home/dexma/DexmaCommons:../include:../include/Controllers:../templates");
define('APPLICATION_ENV', 'development');

require_once('/home/dexma/DexmaCommons/Zend/Loader/Autoloader.php');
$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('App_');
$loader->setFallbackAutoloader(true);


# APPLICATION CONFIG
$config = new Zend_Config_Ini('../../conf/config.ini', APPLICATION_ENV);
Zend_Registry::set('config', $config);

# USER CONFIG
$userConfig = new Zend_Config_Ini('../../conf/userSettings.ini', APPLICATION_ENV, array('allowModifications'=>true));
Zend_Registry::set('customerUid', $userConfig->options->application_uid);

# DB
Zend_Db_Table::setDefaultAdapter(Zend_Db::factory($userConfig->db->merge($config->db)));

$params = array('host' => $userConfig->database->hostname,
    'username' => $userConfig->database->username,
    'password' => $userConfig->database->password,
    'dbname' => $userConfig->database->database,
    'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8;')
);

# DB
$db = Zend_Db::factory($userConfig->db);
Zend_Registry::set('db', $db);
?>