<?php
require_once 'GrantType/IGrantType.php';
require_once 'GrantType/AuthorizationCode.php';
require_once 'Client.php';
require_once("common.php");

//get these values from the FreeAgent developer dashboard
$identifier = 'vElvr9CkKZAvtrckFGduvA';
$secret = 'RGW-qUzFShqZHgtpS3ks6g';
 
//the URL of this script. doesn't have to be publicly accessible.
$script_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
 
//the base URL of the API. shouldn't need to change this.
$base_url = 'https://api.freeagent.com/v2';
 
//create the OAuth client
$client = new OAuth2\Client($identifier, $secret);
 
//check what stage we're at
if (empty($_GET['code']) && empty($_GET['token'])) {
 
    //no code and no token so redirect user to FreeAgent to log in
    $auth_url = $client->getAuthenticationUrl($base_url . '/approve_app', $script_url);
    header('Location: ' . $auth_url);
 
} elseif (isset($_GET['code'])) {
 
    //we have a code so use it to get an access token
    $response = $client->getAccessToken(
        $base_url . '/token_endpoint',
        'authorization_code',
        array('code' => $_GET['code'], 'redirect_uri' => $script_url)
    );
 
    //normally you would store the token for use in future requests
    $token = $response['result']['access_token'];
    header('Location: ' . $script_url . '?token=' . $token);
 
} elseif (isset($_GET['token'])) {
 
    //when we have a token, just set up the client
    $client->setAccessToken($_GET['token']);
    $client->setAccessTokenType(OAuth2\Client::ACCESS_TOKEN_BEARER);
	echo "<pre>";
 
	$sql = "INSERT INTO payments (date,description,amount,uniqueid) VALUES (?,?,?,UUID())";
	$sql_explanation = "INSERT INTO explanations (invoice_id,payment_id,amount) VALUES (?,?,?)";
	
	
	for ($si=1;$si<9;$si++)
	{	
    $response = $client->fetch($base_url . "/bank_transactions?bank_account=65463&per_page=100&page=$si", array(), OAuth2\Client::HTTP_METHOD_GET, array('User-Agent' => 'Ea'));
		
		for ($i=0;$i<100;$i++)
		{
				$bank_transaction = $response['result']['bank_transactions'][$i];
				$btd = $client->fetch($bank_transaction['url'], array(), OAuth2\Client::HTTP_METHOD_GET, array('User-Agent' => 'Ea'));
				$btd = $btd['result']['bank_transaction'];
				
				if ($btd['amount'] > 0)
				{
					$db->query($sql,array($btd['dated_on'],$btd['description'],$btd['amount']));
					$lastID = $db->lastInsertId();
					echo "Inserito pagamento " . $btd['description']  . $btd['amount'] . "\n";
					
					foreach ($btd['bank_transaction_explanations'] as $item)
					{
						if  ($item['paid_invoice'] != '')
						{
							$related_invoice = $client->fetch($item['paid_invoice'], array(), OAuth2\Client::HTTP_METHOD_GET, array('User-Agent' => 'Ea'));
							$invoice_reference = $related_invoice['result']['invoice']['reference'];
							
							// Calcoliamo l'anno delle fatture
							$cyear=2011;
							
							$invoice_reference = $related_invoice['result']['invoice']['reference'];
							if (strpos($invoice_reference,"2012-") !== false) {
								$cyear=2012;
							}
							
							$invoice_reference = str_replace("2012-", "", $invoice_reference);
							
							$invoiceID = $db->fetchOne("SELECT id FROM invoices WHERE serialNumber = ? AND year(date) = ?",array($invoice_reference,$cyear));
							$db->query($sql_explanation,array($invoiceID,$lastID,$item['gross_value']));
							echo "Inserita spiegazione pagamento " . $invoiceID  . " - " . $item['gross_value'] . "\n";
						}
					}
				}
		}
	}
	echo "<pre>";
}

?>