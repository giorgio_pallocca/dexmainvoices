<?php
require_once 'GrantType/IGrantType.php';
require_once 'GrantType/AuthorizationCode.php';
require_once 'Client.php';
require_once("common.php");

//get these values from the FreeAgent developer dashboard
$identifier = 'vElvr9CkKZAvtrckFGduvA';
$secret = 'RGW-qUzFShqZHgtpS3ks6g';
 
//the URL of this script. doesn't have to be publicly accessible.
$script_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
 
//the base URL of the API. shouldn't need to change this.
$base_url = 'https://api.freeagent.com/v2';
 
//create the OAuth client
$client = new OAuth2\Client($identifier, $secret);
 
//check what stage we're at
if (empty($_GET['code']) && empty($_GET['token'])) {
 
    //no code and no token so redirect user to FreeAgent to log in
    $auth_url = $client->getAuthenticationUrl($base_url . '/approve_app', $script_url);
    header('Location: ' . $auth_url);
 
} elseif (isset($_GET['code'])) {
 
    //we have a code so use it to get an access token
    $response = $client->getAccessToken(
        $base_url . '/token_endpoint',
        'authorization_code',
        array('code' => $_GET['code'], 'redirect_uri' => $script_url)
    );
 
    //normally you would store the token for use in future requests
    $token = $response['result']['access_token'];
    header('Location: ' . $script_url . '?token=' . $token);
 
} elseif (isset($_GET['token'])) {
 
    //when we have a token, just set up the client
    $client->setAccessToken($_GET['token']);
    $client->setAccessTokenType(OAuth2\Client::ACCESS_TOKEN_BEARER);
 
    //and make the request to the API
	echo "<pre>";
	for ($si=1;$si<4;$si++)
	{	
    $response = $client->fetch(
        $base_url . "/invoices/?per_page=100&nested_invoice_items=true&page=$si", //API path
        array(), //request parameters
        OAuth2\Client::HTTP_METHOD_GET, //GET, PUT, POST, DELETE
        array('User-Agent' => 'Example app') //API requires UA header
    );
	
	$sql = "INSERT INTO invoices (date,serialNumber,seller,buyer,amount,notes,expireDate,vat_amount,theme_id,taxable,net_amount,paid_amount,payments_term_description,customer_id,customer_name,uniqueid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,UUID())";
	$sql_movement = "INSERT INTO movements (uniqueid,invoice_id,quantity,description,price,vat,position) VALUES (UUID(),?,?,?,?,?,?)";
	
	echo "<pre>";
	for ($i=0;$i<100;$i++)
	{
			// Reperisco i dati del cliente
			
			$invoice = $response['result']['invoices'][$i];
			$customer_data = $client->fetch(
				$invoice['contact'], //API path
				array(), //request parameters
				OAuth2\Client::HTTP_METHOD_GET, //GET, PUT, POST, DELETE
				array('User-Agent' => 'Example app') //API requires UA header
			);
			
			// Reperisco l'ide del cliente
			$customer=$customer_data['result']['contact'];
			$customer_id = $db->fetchOne("SELECT id FROM customers WHERE company = ?",array($customer['organisation_name']));
			
			
			print_r($invoice);
			print_r($customer_data);
			$buyer = implode(PHP_EOL, array(
				$customer['organisation_name'],
				$customer['address1'],
				$customer['town'] . ', ' . $customer['postcode'] . ' (' . $customer['region'] . ')',
				'IVA: ' . $customer['sales_tax_registration_number']
			));

			$seller = array(
				"Dexma Srl",
				"Via di Ponente, 83",
				"Velletri",
				"RM" . ' ' . "00049",
				'IVA: ' . "07463841002",
				"info@dexma.it",
				"069630367",
				"http://www.dexma.it"
			);

			$seller = implode(PHP_EOL, array_filter($seller));			
			if ($invoice['sales_tax_value'] == '') {$invoice['sales_tax_value'] = 0;}
			$invoice['reference'] = str_replace("2012-", "", $invoice['reference']);
			$db->query($sql,array($invoice['dated_on'],$invoice['reference'],$seller,$buyer,$invoice['total_value'],$invoice['comments'],$invoice['due_on'],$invoice['sales_tax_value'],0,$invoice['net_value'],$invoice['net_value'],$invoice['paid_value'],'',$customer_id,$customer['organisation_name']));
			$lastID = $db->lastInsertId();
			echo "<pre>$lastID</pre>";
			foreach ($invoice['invoice_items'] as $item)
			{
				if ($item['sales_tax_rate'] == '') {$item['sales_tax_rate']=0;}
				$db->query($sql_movement,array($lastID,$item['quantity'],$item['description'],$item['price'],$item['sales_tax_rate'],$item['position']));
			}
	}
	}
	echo "<pre>";
}

?>