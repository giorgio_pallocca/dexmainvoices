ALTER TABLE `movements` ADD INDEX `uniqueid`(`uniqueid`);
ALTER TABLE `products` ADD INDEX `uniqueid_idx`(`uniqueid`); 
ALTER TABLE `products` ADD INDEX `description_idx`(`description`(500)), ADD INDEX `amount_idx`(`amount`), ADD INDEX `vat_idx`(`vat`);
ALTER TABLE `products` ADD INDEX `uniqueid_idx`(`uniqueid`);
ALTER TABLE `invoices` MODIFY COLUMN `serialNumber` INTEGER UNSIGNED NOT NULL;
ALTER TABLE `invoices` ADD INDEX `date`(`date`), ADD INDEX `serialNumber`(`serialNumber`);
ALTER TABLE `users` ADD INDEX `email`(`email`);
ALTER TABLE `users` MODIFY COLUMN `role` VARCHAR(20)  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `users` ADD INDEX `role`(`role`);

ALTER TABLE `customers`
	ADD COLUMN `fiscalCode` VARCHAR(16)  DEFAULT NULL AFTER `vatNumber`,
	MODIFY COLUMN `debt_amount` DECIMAL(10,2) NOT NULL,
	ADD INDEX `debt_amount`(`debt_amount`),
	ADD INDEX `vatNumber`(`vatNumber`),
	ADD INDEX `email`(`email`),
	ADD INDEX `company`(`company`);

ALTER TABLE `explanations`
	MODIFY COLUMN `amount` DECIMAL(10,2) NOT NULL,
	CHARACTER SET utf8 COLLATE utf8_general_ci; 

ALTER TABLE `messages` ADD COLUMN `sender_id` INTEGER UNSIGNED NOT NULL AFTER `text`,
	ADD COLUMN `customer_email` VARCHAR(255)  NOT NULL AFTER `sender_id`,
	ADD COLUMN `creation_date` DATETIME  NOT NULL AFTER `customer_email`;

CREATE TABLE `activations` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INTEGER UNSIGNED NOT NULL,
  `uniqueid` VARCHAR(255)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM;

CREATE TABLE `quotations` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `serialNumber` INTEGER UNSIGNED NOT NULL,
  `seller` TEXT  NOT NULL,
  `buyer` TEXT  NOT NULL,
  `amount` DECIMAL(10,2) NOT NULL,
  `notes` TEXT  DEFAULT NULL,
  `expireDate` DATE NOT NULL,
  `vat_amount` DECIMAL(10,2) NOT NULL,
  `pdf` VARCHAR(255)  NOT NULL,
  `theme_id` INTEGER UNSIGNED NOT NULL,
  `uniqueid` VARCHAR(255)  NOT NULL,
  `taxable` DECIMAL(10,2)  NOT NULL,
  `customer_id` INTEGER UNSIGNED NOT NULL,
  `customer_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM;

CREATE TABLE `additionalnotes` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255)  NOT NULL,
  `description` TEXT  NOT NULL,
  `referredto` INTEGER  NOT NULL COMMENT '0:invoices; 1:quotations; 2:ddt; 3:creditnotes',
  `uniqueid` VARCHAR(255)  NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = MyISAM;

CREATE TABLE `movementsquotations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text,
  `price` decimal(10,2) NOT NULL,
  `vat` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `quotation_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `invoices`
	MODIFY COLUMN `expireDate` DATE  DEFAULT NULL,
	MODIFY COLUMN `date` DATE NOT NULL,
	MODIFY COLUMN `vat_amount` DECIMAL(10,2)  NOT NULL,
	MODIFY COLUMN `taxable` DECIMAL(10,2) NOT NULL,
	DROP COLUMN `net_amount`;

ALTER TABLE `quotations`
	MODIFY COLUMN `date` DATE  NOT NULL,
	MODIFY COLUMN `expireDate` DATE  DEFAULT NULL, 
	MODIFY COLUMN `vat_amount` DECIMAL(10,2)  NOT NULL,
	MODIFY COLUMN `taxable` DECIMAL(10,2) NOT NULL;

CREATE TABLE `transportdocuments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `serialNumber` int(10) unsigned NOT NULL,
  `seller` text NOT NULL,
  `buyer` text NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `notes` text,
  `expireDate` date DEFAULT NULL,
  `vat_amount` decimal(10,2) NOT NULL,
  `pdf` varchar(255) NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `uniqueid` varchar(255) NOT NULL,
  `taxable` decimal(10,2) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniqueid` (`uniqueid`),
  KEY `serialNumber` (`serialNumber`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8

CREATE TABLE `movementstransportdocuments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `vat` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `transportdocument_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `creditnotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `serialNumber` int(10) unsigned NOT NULL,
  `seller` text NOT NULL,
  `buyer` text NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `notes` text DEFAULT NULL,
  `expireDate` date DEFAULT NULL,
  `vat_amount` decimal(10,2) NOT NULL,
  `pdf` varchar(255) NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `uniqueid` varchar(255) NOT NULL,
  `taxable` decimal(10,2) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `movementscreditnotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text DEFAULT ,
  `price` decimal(10,2) NOT NULL,
  `vat` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `creditnote_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `additionalnotes` ADD COLUMN `isdefault` INTEGER  DEFAULT 0 AFTER `referredto`;

ALTER TABLE `payments` ADD COLUMN `fit_id` VARCHAR(18)  DEFAULT NULL AFTER `uniqueid`;
 
ALTER TABLE `invoices`
	ADD COLUMN `is_external` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `customer_name`,
	ADD INDEX `customer_name`(`customer_name`),
	ADD INDEX `is_external`(`is_external`);

ALTER TABLE `creditnotes` MODIFY COLUMN `pdf` VARCHAR(255)  CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `invoices` MODIFY COLUMN `pdf` VARCHAR(255)  CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `quotations` MODIFY COLUMN `pdf` VARCHAR(255)  CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `transportdocuments` MODIFY COLUMN `pdf` VARCHAR(255)  CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;

ALTER TABLE `payments` ADD INDEX `fit_id`(`fit_id`);
ALTER TABLE `customers` ADD COLUMN `distanceKm` DECIMAL(10,2) NOT NULL AFTER `debt_amount`;


CREATE TABLE `vehicles` (
  `id` INTEGER  NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255)  NOT NULL,
  `plate` VARCHAR(255)  NOT NULL,
  `aciCoefficient` DECIMAL(10,3)  NOT NULL,
  `uniqueid` VARCHAR(255)  NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `uniqueid_idx`(`uniqueid`),
  INDEX `plate_idx`(`plate`)
) 
ENGINE = MyISAM DEFAULT CHARSET=utf8;

 
CREATE TABLE `transferts` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE  NOT NULL,
  `customerName` VARCHAR(255)  DEFAULT NULL,
  `customerAddress` VARCHAR(255)  DEFAULT NULL,
  `customerZipcode` VARCHAR(255)  DEFAULT NULL,
  `customerCity` VARCHAR(255)  DEFAULT NULL,
  `vehicle_id` INTEGER UNSIGNED NOT NULL,
  `user_id` INTEGER UNSIGNED NOT NULL,
  `kmTot` DECIMAL(10,2)  NOT NULL, 
  `uniqueid` VARCHAR(255)  NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `uniqueid_idx`(`uniqueid`)
)
ENGINE = MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `explanations` MODIFY COLUMN `invoice_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
						   ADD COLUMN `quick_explanation_id` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `amount`,
						   ADD INDEX `quick_explanation_id`(`quick_explanation_id`);

INSERT INTO `parameters` (`parameter_name`, `parameter_value`) VALUES
	('company_tax1', ''), ('company_tax2', '0');

ALTER TABLE `invoices`
	ADD COLUMN `tax1` VARCHAR(255) DEFAULT NULL AFTER `vat_amount`,
	ADD COLUMN `tax2` TINYINT(1)  NOT NULL AFTER `tax1`;

ALTER TABLE `payments` MODIFY COLUMN `type` TINYINT(1) UNSIGNED NOT NULL COMMENT '0 = Incasso(+); 1 = Spesa(-);';

DROP TABLE IF EXISTS `quick_explanations`;
CREATE TABLE `quick_explanations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0 = Pre spesa; 1 = Per incasso;',
  `uniqueid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniqueid` (`uniqueid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
LOCK TABLES `quick_explanations` WRITE;
INSERT INTO `quick_explanations` VALUES (1,'Interessi',0,'13BB28B9560-113372543150D18C2E69D00'),(3,'Rimborsi',0,'C54F975F0F2-92203161050D18C3C3A34E'),(4,'Finanziamento soci',0,'C54F9770CB2-42859782750D18C437A483'),(5,'Altri incassi',0,'C54F977F0E0-3611086350D18C4954D41'),(7,'Interessi passivi',1,'C54F97A10FB-153873526250D18C5742065'),(8,'Spese bancarie',1,'C54F97B090E-94250382350D18C5D97D85'),(9,'Tasse',1,'C553A64A3BD-2780006650D335C6B7E0C'),(10,'Affitto',1,'C553A5B8EE6-115859780650D3358B39BD7'),(11,'Mutuo',1,'C553A5DCF4B-138632939450D33599F24AB'),(12,'Leasing',1,'C553A602F7C-108125104650D335A989477'),(13,'Stipendi',1,'C553A61BB65-35628178850D335B3AA4D7'),(14,'Collaborazioni occasionali',1,'C553A62B827-124566925250D335BA29095'),(15,'Altre spese',1,'C553A63A926-153952466350D335C0526EE');
UNLOCK TABLES;

ALTER TABLE `transportdocuments`
	ADD COLUMN `transportBy` VARCHAR(255)  DEFAULT NULL AFTER `customer_name`,
	ADD COLUMN `appearance` VARCHAR(255)  DEFAULT NULL AFTER `transportBy`,
	ADD COLUMN `numberPackage` INTEGER  DEFAULT NULL AFTER `appearance`,
	ADD COLUMN `weight` VARCHAR(255)  DEFAULT NULL AFTER `numberPackage`,
	ADD COLUMN `vectorData` VARCHAR(255)  DEFAULT NULL AFTER `weight`,
	ADD COLUMN `dateTimeTransport` DATETIME  DEFAULT NULL AFTER `vectorData`;
 
ALTER TABLE `transferts`
	ADD COLUMN `diaria` TINYINT  NOT NULL DEFAULT 0 COMMENT '0: no; 1:si' AFTER `kmTot`;

ALTER TABLE `payments` ADD COLUMN `typology` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 = Banca; 1 = Cassa;' AFTER `fit_id`;
ALTER TABLE `customers` ADD COLUMN `type` TINYINT(1) UNSIGNED NOT NULL DEFAULT 2 COMMENT '0:cliente; 1:fornitore; 2:cliente/fornitore' AFTER `distanceKm`;

ALTER TABLE `invoices`
	ADD COLUMN `tax1amount` DECIMAL(10,2) AFTER `status`,
	ADD COLUMN `tax2amount` DECIMAL(10,2) AFTER `tax1amount`;

ALTER TABLE `invoices`
	MODIFY COLUMN `tax1amount` DECIMAL(10,2)  NOT NULL,
	MODIFY COLUMN `tax2amount` DECIMAL(10,2)  NOT NULL;

ALTER TABLE `creditnotes`
	ADD COLUMN `tax1` VARCHAR(255)  AFTER `vat_amount`,
	ADD COLUMN `tax2` DECIMAL(5,2)  NOT NULL AFTER `tax1`,
	ADD COLUMN `tax1amount` DECIMAL(10,2)  NOT NULL,
	ADD COLUMN `tax2amount` DECIMAL(10,2)  NOT NULL;

ALTER TABLE `quotations`
	ADD COLUMN `tax1` VARCHAR(255)  AFTER `vat_amount`,
	ADD COLUMN `tax2` DECIMAL(5,2)  NOT NULL AFTER `tax1`,
	ADD COLUMN `tax1amount` DECIMAL(10,2)  NOT NULL,
	ADD COLUMN `tax2amount` DECIMAL(10,2)  NOT NULL;

ALTER TABLE `transportdocuments`
	ADD COLUMN `tax1amount` DECIMAL(10,2)  NOT NULL,
	ADD COLUMN `tax2amount` DECIMAL(10,2)  NOT NULL;

ALTER TABLE `transportdocuments`
	DROP COLUMN `tax1amount`,
	DROP COLUMN `tax2amount`;

/* CREARE NEL PROPRIO LOCALE IL DB pi_shared che deve contenere la tabella languages */
DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lc` varchar(100) NOT NULL,
  `description_it` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `description_de` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39;

LOCK TABLES `languages` WRITE;
INSERT INTO `languages` VALUES (1,'view','Panoramica','Home','Home'),(2,'best_customers','Migliori clienti','Best customers','Besten kunden'),(3,'shortcuts','Scelte rapide','Shortcuts','Verknüpfungen'),(4,'turnover','Fatturato','Turnover','Umsatz'),(5,'taxable','Imponibile','Taxable','Steuerpflichtig'),(6,'total','Totale','Total','Gesamt'),(7,'vat','Iva','Vat','Mehrwertsteuer'),(8,'unpaid','Scoperto','Unpaid','Unbezahlt'),(9,'paid','Incassato','Paid','Bezahlt'),(10,'economic_perf','Andamento economico','Economic performance','Wirtschaftliche Leistungsfähigkeit'),(11,'new_customer','Nuovo cliente','New customer','Neukunden'),(12,'new_invoice','Nuova fattura','New invoice','Neue Rechnung'),(13,'new_estimate','Nuovo preventivo','New estimate','Neue Schätzung'),(14,'new_ddt','Nuovo DDT','New DDT','Neue DDT'),(15,'new_creditnote','Nuova nota di credito','New credit note','Neue Gutschrift'),(16,'new_product','Nuovo prodotto','New product','Neue Produkt'),(17,'to_payments','Vai ai pagamenti','Go to payments','Zum Zahlungen'),(18,'contacts','Contatti','Contacts','Kontakte'),(19,'invoices','Fatture','Invoices','Rechnungen'),(20,'documents','Documenti','Documents','Dokumente'),(21,'payments','Pagamenti','Payments','Zahlungen'),(22,'transferts','Trasferte','Transferts','übertragene'),(23,'estimates','Preventivi','Estimates','Schätzungen'),(24,'creditnotes','Note di credito','Credit notes','Gutschriften'),(25,'ddt','DDT','DDT','DDT'),(26,'options','Opzioni','Options','Optionen'),(27,'users','Utenti','Users','Nutzer'),(28,'invoices','Fatture emesse','Invoices','Rechnungen'),(29,'external_invoices','Fatture ricevute','Received invoices','Eingangsrechnungen'),(30,'company_data','Dati azienda','Company data','Unternehmensdaten'),(31,'products','Prodotti','Products','Produkte'),(32,'vehicles','Veicoli','Vehicles','Fahrzeuge'),(33,'settings','Impostazioni','Settings','Einstellungen'),(34,'customize_notes','Personalizza note','Customize notes','Anpassen Notizen'),(35,'show_all','Mostra tutte','Show all','Alle zeigen'),(36,'search','Cerca','Search','Suche'),(37,'unpaid_a','Da pagare','Unpaid','Unbezahlt'),(38,'customer','Cliente','Customer','Kunde');
UNLOCK TABLES;

CREATE VIEW languages AS SELECT * FROM pi_shared.languages;
INSERT INTO `parameters` (`parameter_name`, `parameter_value`) VALUES ('language', 'it');

ALTER TABLE`users` ADD COLUMN `dashboard` INTEGER  DEFAULT 1 COMMENT '0:disabilitata; 1:abilitata' AFTER `uniqueid`;

ALTER TABLE `invoices` ADD COLUMN `private` INTEGER  DEFAULT 0 COMMENT '0:visibile a tutti; 1:visibile solo agli amministratori ' AFTER `tax2amount`;

ALTER TABLE `quotations` ADD COLUMN `private` INTEGER  DEFAULT 0 COMMENT '0:visibile a tutti; 1:visibile solo agli amministratori ' AFTER `tax2amount`;

ALTER TABLE `transportdocuments` ADD COLUMN `private` INTEGER  DEFAULT 0 COMMENT '0:visibile a tutti; 1:visibile solo agli amministratori ' AFTER `dateTimeTransport`;

ALTER TABLE `creditnotes` ADD COLUMN `private` INTEGER  DEFAULT 0 COMMENT '0:visibile a tutti; 1:visibile solo agli amministratori ' AFTER `tax2amount`;



ALTER TABLE `movements` ADD COLUMN `unit_id` INTEGER  DEFAULT NULL AFTER `quantity`;
ALTER TABLE `movementstransportdocuments` ADD COLUMN `unit_id` INTEGER  DEFAULT NULL AFTER `quantity`;
ALTER TABLE `movementscreditnotes` ADD COLUMN `unit_id` INTEGER  DEFAULT NULL AFTER `quantity`;
ALTER TABLE `movementsquotations` ADD COLUMN `unit_id` INTEGER  DEFAULT NULL AFTER `quantity`;

DROP TABLE IF EXISTS `units`;
CREATE TABLE `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `plural` varchar(255) NOT NULL,
  `uniqueid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `description_idx` (`description`),
  KEY `uniqueid_idx` (`uniqueid`)
) ENGINE=MyISAM AUTO_INCREMENT=11;

INSERT INTO `units` VALUES (1,'Ora','Ore','C5C39C0B8E6-704348513510111A194FCF'),
(2,'Giorno','Giorni','C5C39E51A2E-557699155510112900761F'),(3,'Mese','Mesi','13C6C3341D5-1285844779510113440CF58'),
(4,'Anno','Anni','C5C3A09CBDE-1772264939510113807C343'),(5,'Settimana','Settimane','C5C3A0D03B8-14788149635101139592519'),
(6,'Prodotto','Prodotti','C5C3A112A0B-1600089180510113B0C20A6'),(7,'Servizio','Servizi','C5C3A12349D-129245587510113B7971C7'),
(8,'Spesa','Spese','C5C3A18328D-1710981207510113DED8BD7'),(9,'Sconto','Sconti','C5C3A23DF51-9404325315101142B61B9E'),
(10,'Credito','Crediti','C5C3A285393-5282726045101144891633');

CREATE TABLE `recurringinvoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `frequency` VARCHAR(20) NOT NULL,
  `expireDate` date DEFAULT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `payments_term_description` varchar(255) DEFAULT NULL,
  `notes` text,
  `seller` text NOT NULL,
  `buyer` text NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `private` int(1) DEFAULT '0' COMMENT '0:visibile a tutti; 1:visibile solo agli amministratori',
  `tax1` varchar(255) DEFAULT NULL,
  `tax2` tinyint(1) NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `tax1amount` decimal(10,2) NOT NULL,
  `tax2amount` decimal(10,2) NOT NULL,
  `taxable` decimal(10,2) NOT NULL,
  `vat_amount` decimal(10,2) NOT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'draft',
  `payments_term` INTEGER  DEFAULT NULL,
  `last_recurred` DATE  DEFAULT NULL,
  `next_recurring` DATE  DEFAULT NULL,
  `endDate` DATE  DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `amount_idx` (`amount`),
  KEY `date_idx` (`date`),
  KEY `customer_name_idx` (`customer_name`)
) ENGINE=MyISAM;

CREATE TABLE `movementsrecurringinvoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(255) NOT NULL,
  `unit_id` tinyint DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `description` text,
  `price` decimal(10,2) NOT NULL,
  `vat` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `recurringinvoice_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;

ALTER TABLE `users` ADD COLUMN `menu` VARCHAR(20)  NOT NULL DEFAULT 'default' AFTER `dashboard`;
ALTER TABLE `users` MODIFY COLUMN `menu` INTEGER  DEFAULT 0 COMMENT '0:default, 1:professionista';

ALTER TABLE `invoices` ENGINE=InnoDB;
ALTER TABLE `creditnotes` ENGINE=InnoDB;
ALTER TABLE `quotations` ENGINE=InnoDB;
ALTER TABLE `transportdocuments` ENGINE=InnoDB;
ALTER TABLE `recurringinvoices` ENGINE=InnoDB;
ALTER TABLE `movements` ENGINE=InnoDB;
ALTER TABLE `movementscreditnotes` ENGINE=InnoDB;
ALTER TABLE `movementsquotations` ENGINE=InnoDB;
ALTER TABLE `movementsrecurringinvoices` ENGINE=InnoDB;
ALTER TABLE `movementstransportdocuments` ENGINE=InnoDB;
ALTER TABLE `users` ENGINE=InnoDB;
ALTER TABLE `parameters` ENGINE=InnoDB;
ALTER TABLE `activations` ENGINE=InnoDB;
ALTER TABLE `units` ENGINE=InnoDB;

ALTER TABLE `invoices` ADD COLUMN `sent` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:non inviato, 1:inviato' AFTER `private`;
ALTER TABLE `quotations` ADD COLUMN `sent` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:non inviato, 1:inviato' AFTER `private`;
ALTER TABLE `transportdocuments` ADD COLUMN `sent` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:non inviato, 1:inviato' AFTER `private`;
ALTER TABLE `creditnotes` ADD COLUMN `sent` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:non inviato, 1:inviato' AFTER `private`;

ALTER TABLE `invoices` ADD COLUMN `privatenotes` TEXT  DEFAULT NULL AFTER `sent`;
ALTER TABLE `quotations` ADD COLUMN `privatenotes` TEXT  DEFAULT NULL AFTER `sent`;
ALTER TABLE `transportdocuments` ADD COLUMN `privatenotes` TEXT  DEFAULT NULL AFTER `sent`;
ALTER TABLE `creditnotes` ADD COLUMN `privatenotes` TEXT  DEFAULT NULL AFTER `sent`;

ALTER TABLE `messages` ADD COLUMN `document_type` INTEGER  NOT NULL COMMENT '0:invoices; 1:quotations; 2:ddt; 3:creditnotes' AFTER `creation_date`;
ALTER TABLE `messages` ADD COLUMN `document_id` INTEGER  NOT NULL AFTER `document_type`;

ALTER TABLE `customers` ADD COLUMN `notes` TEXT  DEFAULT NULL AFTER `type`;


ALTER TABLE `movements` ADD COLUMN `free` TINYINT  NOT NULL DEFAULT 0 COMMENT '0:movimento con IVA; 1:spesa esente' AFTER `invoice_id`;
ALTER TABLE `movements` MODIFY COLUMN `free` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:movimento con IVA; 1:spesa esente';
ALTER TABLE `movementscreditnotes` ADD COLUMN `free` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:movimento con IVA; 1:spesa esente' AFTER `creditnote_id`;
ALTER TABLE `movementsquotations` ADD COLUMN `free` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:movimento con IVA; 1:spesa esente' AFTER `quotation_id`;
ALTER TABLE `movementsrecurringinvoices` ADD COLUMN `free` TINYINT(1)  NOT NULL DEFAULT 0 COMMENT '0:movimento con IVA; 1:spesa esente' AFTER `recurringinvoice_id`;

ALTER TABLE `invoices` ADD COLUMN `attachment` VARCHAR(255)  DEFAULT NULL AFTER `privatenotes`;

ALTER TABLE `invoices` ADD COLUMN `taxableFree` DECIMAL(10,2)  NOT NULL AFTER `taxable`;
ALTER TABLE `recurringinvoices` ADD COLUMN `taxableFree` DECIMAL(10,2)  NOT NULL AFTER `taxable`;
ALTER TABLE `quotations` ADD COLUMN `taxableFree` DECIMAL(10,2)  NOT NULL AFTER `taxable`;
ALTER TABLE `creditnotes` ADD COLUMN `taxableFree` DECIMAL(10,2)  NOT NULL AFTER `taxable`;
ALTER TABLE `dexmainvoices`.`recurringinvoices` ADD COLUMN `code` VARCHAR(45)  DEFAULT NULL AFTER `endDate`;

ALTER TABLE `movements` ADD COLUMN `product_id` INTEGER  DEFAULT NULL AFTER `free`;
ALTER TABLE `movementscreditnotes` ADD COLUMN `product_id` INTEGER  DEFAULT NULL AFTER `free`;
ALTER TABLE `movementsquotations` ADD COLUMN `product_id` INTEGER  DEFAULT NULL AFTER `free`;
ALTER TABLE `movementsrecurringinvoices` ADD COLUMN `product_id` INTEGER  DEFAULT NULL AFTER `free`;
ALTER TABLE `movementstransportdocuments` ADD COLUMN `product_id` INTEGER  DEFAULT NULL AFTER `transportdocument_id`;

CREATE TABLE  `productmovements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `load` decimal(10,0) NOT NULL,
  `unload` decimal(10,0) NOT NULL,
  `prbuyunitary` decimal(10,2) NOT NULL,
  `prsaleunitary` decimal(10,2) NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `date` date NOT NULL,
  `uniqueid_invoices` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `uniqueid_movements` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8


ALTER TABLE `products` ADD COLUMN `quantity` DECIMAL(5,0)  NOT NULL AFTER `uniqueid`,
 ADD COLUMN `product_name` VARCHAR(100)  CHARACTER SET latin1 NOT NULL AFTER `quantity`,
 ADD COLUMN `amountsale` DECIMAL(10,2)  NOT NULL AFTER `product_name`;

ALTER TABLE `invoices` MODIFY COLUMN `tax2` DECIMAL(5,2)  NOT NULL;
ALTER TABLE `recurringinvoices` MODIFY COLUMN `tax2` DECIMAL(5,2)  NOT NULL;

CREATE TABLE `dexmainvoices`.`list` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_type` VARCHAR(30)  CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

CREATE TABLE `dexmainvoices`.`product_prices` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` INTEGER UNSIGNED NOT NULL,
  `id_list` INTEGER UNSIGNED NOT NULL,
  `amount` DECIMAL(10,2)  NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `dexmainvoices`.`customers` ADD COLUMN `id_list` INTEGER UNSIGNED NOT NULL AFTER `notes`;

/* Gli script in alto sono già stati eseguiti sul template */