<?php
define('APPLICATION_PATH', dirname(dirname($_SERVER['DOCUMENT_ROOT']))); # TRUNK
define('DEXMA_COMMONS', trim(file_get_contents(APPLICATION_PATH . '/conf/dexmacommons.txt')));
define('APPLICATION_ENV', 'development');


if (isset($_SERVER['HOME_DIRECTORY'])) {
	define('HOME_DIRECTORY', $_SERVER['HOME_DIRECTORY']);
}
else {
	$subdomain = array_shift((explode(".",$_SERVER['HTTP_HOST'])));
	define('HOME_DIRECTORY', "/home/pi" . $subdomain . "/dexmainvoices");
}

# INCLUDE PATH
set_include_path(implode(PATH_SEPARATOR, array(
	DEXMA_COMMONS,
	APPLICATION_PATH . '/Software/include',
	APPLICATION_PATH . '/Software/include/Controllers'
	)
));

# AUTOLOADER
require_once DEXMA_COMMONS . '/Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()
	->pushAutoloader(NULL, 'Smarty_')
	->setFallbackAutoloader(TRUE);

# APPLICATION CONFIG
$config = new Zend_Config_Ini(APPLICATION_PATH . '/conf/config.ini', APPLICATION_ENV);
Zend_Registry::set('config', $config);

# USER CONFIG
$userConfig = new Zend_Config_Ini(HOME_DIRECTORY . '/conf/settings.ini', APPLICATION_ENV, array('allowModifications'=>TRUE));
Zend_Registry::set('customerUid', $userConfig->options->application_uid);

# DB
Zend_Db_Table::setDefaultAdapter(Zend_Db::factory($userConfig->db->merge($config->db)));

# AUTH STORAGE
$authStorage = new Zend_Auth_Storage_Session($config->auth->session, $config->auth->member);
Zend_Auth::getInstance()->setStorage($authStorage);

# VIEW
$vr = new Zend_Controller_Action_Helper_ViewRenderer();
$vr	->setView(new DexmaZend_View_Smarty(APPLICATION_PATH))
	->setViewSuffix('tpl');
Zend_Controller_Action_HelperBroker::addHelper($vr);

Zend_Controller_Front::getInstance()
	->registerPlugin(new DexmaZend_Controller_Plugin_Acl(new Zend_Config_Ini(APPLICATION_PATH . '/conf/acl.ini'))) # ACL
	->run(APPLICATION_PATH . '/Software/include/Controllers');
