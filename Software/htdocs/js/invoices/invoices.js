Handlebars.registerHelper('amount', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('vat_amount', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.vat_amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('unpaid_amount', function()
{
	var warning = '';
	if (this.unpaid_amount > 0){
		warning = ' class="text-error"';
	}
	return new Handlebars.SafeString('<div class="pagination-right"><span' + warning + '>' + number_format(this.unpaid_amount, 2, ',', '.') + '</span></div>');
});

Handlebars.registerHelper('status', function()
{
	var status = label = '';

	if (this.status == 0) {
		status = 'Pagato';
		label = ' label-success';
	}
	else if (this.status == 1) {
		status = 'Da pagare';
		label = ' label-important';
	}

	return new Handlebars.SafeString('<div class="pagination-right"><span class="label' + label + '">' + status + '</span></div>');
});

Handlebars.registerHelper('sent', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + ((this.sent == 1)? 'Si' : 'No') + '</div>');
});

//function updateListData(data)
//{
//	$('#referencesGrid table tbody').append(
//			'<tr><td colspan=3><div class="pagination-right"><b>Totali</b></div></td>'
//		+	'<td><div class="pagination-right">€ ' + number_format(data.totals.amount.toString(), 2, ',', '.') + '</div></td>'
//		+	'<td><div class="pagination-right text-error">€ ' + number_format(data.totals.unpaid_amount.toString(), 2, ',', '.') + '</div></td>'
//		+	'<td><div class="pagination-right">€ ' + number_format(data.totals.vat_amount.toString(), 2, ',', '.') + '</div></td>'
//		+	'<td colspan=3></td></tr>'
//	);
//	updateListDataPopover(data);
//}

function printListInvoices()
{
	location.href = '/invoices/printlist?' + $('#referenceSearch').serialize();
}

!function(){
	var invoicesDIV = $('#referencesGrid'),
		pageSize = $('body').data('pagesize');
	window.invoicesGrid = invoicesDIV.simplePagingGrid({
		columnNames: ['Data', 'Progr.', 'Cliente', 'Importo (€)', 'Scoperto', 'IVA', 'Stato', 'Inviata', ''],
		columnKeys: ['date', 'serialNumber', 'customer_name', 'amount', 'unpaid_amount', 'vat_amount', 'status', 'sent', ''],
		columnWidths: ['10%', '10%', '19%', '14%', '13%', '10%', '9%', '11%', '4%'],
		data: invoicesDIV.data('list'),
		dataUrl: '/invoices/list',
		sortable: [true, true, true, true, true, true, true, true, false],
		currentPage: invoicesDIV.data('activepage'),
		initialSortColumn: invoicesDIV.data('activesortfield'),
		sortOrder: invoicesDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, null],
		cellTemplates:[null, null, null, '{{amount}}', '{{unpaid_amount}}', '{{vat_amount}}', '{{status}}', '{{sent}}', '{{privatenotes}}'],
		rowClickHandler: function(row){location.href = '/' + $('body').data('controller') + '/view?uid=' + row.uniqueid},
		searchFormName: 'referenceSearch',
		searchButtonName: 'referenceSearchButton',
		showAllButtonName: 'referenceShowAllButton',
		tableClass: 'table table-bordered table-hover',
		pageRenderedEvent: updateListData
	});
	invoicesGrid.settings.data = null;
	invoicesDIV.attr('data-list', null);
	invoicesDIV.attr('data-activepage', null);
	invoicesDIV.attr('data-activesortfield', null);
	invoicesDIV.attr('data-activesortdirection', null);
}();

function generateZip(year){
	var $modal = $('#generateZipModal');
	$('#generateYear').text(year);

	$modal.modal();

	$.getJSON('/invoices/generatezip?year=' + year, function(data){
		if (data && data.path) {
			$('#generatingZipBox').addClass('hide');
			$('#resultZipBox').removeClass('hide');
			$('#resultZipBox').find('a:first').attr('href', '/invoices/getzip?href=' + data.path + '&filename=' + data.filename);
		}
	});

	setTimeout(function(){getPercentage(year)}, 500);
}

function getPercentage(year){
	var bar = $('#generatingBar'),
		current_perc = 0;

	$.getJSON('/invoices/getzipstatus', {year:year}, function(data){
		if (bar.is(':visible'))
		{
			current_perc = parseInt((data.tot - data.remain) * 100 / data.tot, 10);
			bar.css('width', current_perc + '%');
			bar.text(current_perc + '%');

			if (!data.remain) {
				return;
			}

			setTimeout(function(){
				if ($('#generateZipModal').is(':visible')) {
					getPercentage(year);
				}
			}, 700);
		}
	});
}

$(function(){
	createCalendar('startDate');
	createCalendar('endDate');

	var startDate = $('#startDate');
	var endDate = $('#endDate');

	if(startDate.val() == '')
	{
		endDate.prop('disabled', true);
		$('#substractDayEndButton').prop('disabled', true);
		$('#addDayEndButton').prop('disabled', true);
	}

	startDate.change(function()
	{
		endDate.prop('disabled', false);
		$('#substractDayEndButton').prop('disabled', false);
		$('#addDayEndButton').prop('disabled', false);
		updateMinEndDate('startDate','endDate');
	});

	$('#popoverPrivatenotes').on('show shown hide hidden', function(e){
		e.stopPropagation();
	});

	$('#generateZipModal').on('hidden', function () {
		$('#generateYear').text('');
		$('#generatingBar').css('width', 0);
		$('#generatingZipBox').removeClass('hide');
		$('#resultZipBox').addClass('hide');
		$('#resultZipBox').find('a:first').attr('href', '#');
		$('#errorZipBox').addClass('hide');
	});
	
	$('#getZipBtn').on('click', function(e){
		var $target = $(e.target);
		var href = $target.attr('data-href');
		$.post('/invoices/getzip', {href: href}, function(result){
			
		});
	});
});