function validateVat()
{
	var name = 'company_vat';
	return checkField(name, 'Partita IVA obbligatoria') && checkNumber(name, 'Partita IVA non conforme', /^[0-9]{11}$/);
}

function validateDefaultVat()
{
	var name = 'company_default_vat',
		input = $('#' + name),
		msg = 'Iva predefinita obbligatoria, sono validi i valori da 0 a 100';

	if (!checkField(name, msg) || input.val() < 0 || input.val() > 100)
	{
			enableErrorClass(name, msg);
			return false;
	}

	return true;
}

function removeLogo()
{
	$.post('/default/company/removelogo', function(data)
	{
		if (data == true)
		{
			$('#logo-preview').addClass('hide');
			$('#company_logo').removeClass('hide');
		}
	});
}

function checkUserFirstname() {
	return checkField('firstname', 'Nome obbligatorio');
}

function checkUserSurname() {
	return checkField('surname', 'Cognome obbligatorio');
}

function checkUserEmail() {
	return checkEmail('email', 'Email obbligatoria', null, null, null, false);
}

function checkUserPassword() {
	return compareFields('password', true, 5, 'Password obbligatoria');
}

function checkFirstUser() {
	return $('body').data('action') != 'createfirstuser' || (
		checkUserFirstname() && checkUserSurname() && checkUserEmail() && checkUserPassword()
	);
}

function checkCompanyEmail() {
	return $('body').data('action') == 'createfirstuser' || checkEmail('company_email', 'Email obbligatoria', null, null, null, false);
}

function checkCompanyAddress() {
	return checkField('company_address', 'Indirizzo obbligatorio');
}

function checkCompanyCAP() {
	return checkField('company_cap', 'CAP obbligatorio');
}

function checkCompanyCity() {
	return checkField('company_city', 'Città obbligatoria');
}

function checkCompanyState() {
	return checkField('company_state', 'Provincia obbligatoria');
}

function saveCompany()
{
	fieldId = 'companyForm';

	var saveButton = $('#saveCompanyBtn').prop('disabled', true);

	if (checkFirstUser() && checkCompanyAddress() && checkCompanyCAP() && checkCompanyCity() && checkCompanyState()
			&& validateVat() && checkCompanyEmail() && validateDefaultVat())
	{
		$('#companyForm').submit();
	}

	saveButton.prop('disabled',false);
}