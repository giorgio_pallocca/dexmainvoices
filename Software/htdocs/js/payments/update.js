var invoiceType = '';

function paymentOpenMethodInsert(data)
{
	insertForDialog(data.list);
	managePaymentTypeLabel(data.paymentType);
	$('#deletePaymentButton').addClass('hide');
}

function paymentOpenMethodUpdate(data)
{
	uploadForDialog(data.list);
	managePaymentTypeLabel(data.paymentType);
	$('#deletePaymentButton').removeClass('hide');
}

function managePaymentTypeLabel(paymentType)
{
	if(paymentType === '0')
	{
		$('#paymentDirectionLabel').html("<span class='label label-success'>Incasso</span>");
		invoiceType = 'invoices';
	}
	else if(paymentType === '1')
	{
		$('#paymentDirectionLabel').html("<span class='label label-important'>Spesa</span>");
		invoiceType = 'externalinvoices';
	}
	$('#type').val(paymentType);
}

function paymentCloseMethod()
{
	disableErrorClass('description');
	disableErrorClass('amount');
	closeForDialog();
}

function validateDescription()
{
	return checkField('description', '');
}

function validatePaymentDescription()
{
	return checkField('paymentDescription', '');
}

function validateAmount()
{
	var result = false;
	if(checkField('amount', 'Importo obbligatorio'))
	{
		if(isNumber('amount', 'Importo non numerico'))
		{
			result = true;
		}
	}
	return result;
}

function deletePayment()
{
	if(confirm('Eliminare definitivamente il pagamento?')) 
	{ 
		var params = {uniqueid: $('#uniqueid').val()};
		$.post('/' + controllerName + '/delete', params, function(data) 
		{		
			if(data != null && data >= 0)
			{
				$('#paymentDIV').modal('hide');
				gridObject[0].refreshDataRemote(data);
			}
		});
	}
}

function savePayment(mode, invoiceUid)
{
	var saveButton = $('#savePaymentButton').prop('disabled', true);

	if(validateDescription() && validateAmount() && validateDetail())
	{
		if (mode == 0)
		{
			preparePaymentDetail();
		}
		else if (mode == 1)
		{
			var paymentDetail = $('#paymentDetail').val() + ',' + $('#amount').val();
			$('#paymentDetail').val(paymentDetail);
		}
		
		$.post('/' + controllerName + '/save', $('#paymentForm').serialize(), function(data) 
		{
			if(data != null && data != false)
			{
				$('#paymentDIV').modal('hide');
				saveButton.prop('disabled', false);
				if (mode == 0)
				{
					gridObject[0].refreshData();
				}
				else if (mode == 1)
				{
					location.href = '/invoices/view?uid=' + invoiceUid;
				}
    		}
    	}, 'json');
    } 
    else
    {
    	saveButton.prop('disabled', false);
    }
}

function showQuickInvoiceTab()
{
	$.post('/' + invoiceType + '/quickinvoiceadd', function(data)
	{
		if(data != null && data != false)
		{
			$('#dateInvoice').val(data.dateInvoice);
			$('#year').text('/' + data.year);
			$.each(['payment-tabs-2Tab', 'vatQuickContainer'], function(){
				$('#' + this).removeClass('hide');
			});
			$('#payment-tabs-1Tab').addClass('hide');
			if(invoiceType == 'externalinvoices')
			{
				$('#serialNumber').attr('onblur', 'checkSerialNumber()');
				$('#dateInvoice').attr('onblur', 'checkDateInvoice()');
				$('#customer_idLabel').text('Fornitore');
				$.each(['tax1Container', 'tax2Container'], function(){
					$('#' + this).removeClass('hide');
				});
			}
			else if(invoiceType == 'invoices')
			{
				$('#serialNumber').val(data.serialNumber);
				$('#serialNumber').attr('onblur', 'checkReferenceFields(\'dateInvoice\', \'' + invoiceType + '\')');
				$('#dateInvoice').attr('onchange', 'checkReferenceFields(\'dateInvoice\', \'' + invoiceType + '\')');
				$('#customer_idLabel').text('Cliente');
				if (data.defaultTax1 != 0){
					$('#tax1Container').removeClass('hide');
					$('#tax1 option[value="' + data.defaultTax1 + '"]').prop('selected', true);
				}
				if (data.defaultTax2 != 0){
					$('#tax2Container').removeClass('hide');
					$('#tax2').prop('checked', true);
				}
			}
			$('#paymentTabs a[href="#payment-tabs-2"]').tab('show');
			$('#serialNumber').focus();
		}
	}, 'json');
}

function hideQuickInvoiceTab()
{
	resetFormData('quickInvoiceForm');
	$.each(['serialNumber', 'descriptionMovementQuick', 'priceQuick', 'vatQuick', 'dateInvoice'], function(){
		disableErrorClass(this);
	});
	$('#payment-tabs-2Tab').addClass('hide');
	$('#payment-tabs-1Tab').removeClass('hide');
	$('#paymentTabs a[href="#payment-tabs-1"]').tab('show');
}

function checkDateInvoice()
{
	return checkField('dateInvoice', 'Data obbligatoria');
}

function checkTaxable()
{
	return checkField('priceQuick', 'Imponibile obbligatorio');
}

function checkFields()
{
	var dateName = 'dateInvoice';
	var saveBtn = $('#saveQuickInvoiceBtn').prop('disabled', true);
	var params = $('#quickInvoiceForm').serializeArray();

	if (checkSerialNumber() && checkDateInvoice() && checkDescription('descriptionMovementQuick') && checkTaxable() && checkVat('vatQuick'))
	{
		if(invoiceType == 'invoices')
		{
			params.push({name: 'date', value: $('#dateInvoice').val()});
			$.post('/invoices/checkreferencefields', params, function(data) {
				if (data != null) {
					if (data.errors == false) {
						saveQuickInvoice(invoiceType, params);
					}
					else {
						enableReferenceErrors(data, dateName);
						saveBtn.prop('disabled', false);
					}
				}
			}, 'json');
		}
		else {
			saveQuickInvoice(invoiceType, params);
		}
	}
	else saveBtn.prop('disabled', false);
}

function saveQuickInvoice(invoiceType, params)
{
	var saveBtn = $('#saveQuickInvoiceBtn');
	var fees = $('#fees');
	var primaryOptgroup = $('#primaryOptgroup');

	$.post('/' + invoiceType + '/quickinvoicesave', params, function(data) {
		$('#payment-tabs-2Tab').addClass('hide');
		$('#payment-tabs-1Tab').removeClass('hide');
		drawOption(primaryOptgroup, data.uniqueid, data.customer_name, data.serialNumber, data.unpaid_amount, data.type);
		fees.val(data.uniqueid);
		hideQuickInvoiceTab();
		saveBtn.prop('disabled', false);
		addPayment();
	}, 'json');
}

function selectFromProduct(description, amount, vat){
	$.each(['descriptionMovementQuick', 'priceQuick', 'vatQuick'], function(){
		disableErrorClass(this);
	});
	$('#descriptionMovementQuick').val(description);
	$('#priceQuick').val(amount);
	$('#vatQuick').val(vat);
}

$(function()
{
	$('#paymentTabs a:first').tab('show');
	createCalendar('date');
	createCalendar('dateInvoice');

	var dateInvoice = $('#dateInvoice');
	dateInvoice.change(function(){
		var year = dateInvoice.datepicker('getDate').getFullYear();
		$('#year').text('/' + year);
	});
	
	var paymentDIV = $('#paymentDIV');
	paymentDIV.on('shown', function ()
	{
		$('#description').focus();
	});
	
	paymentDIV.on('hide', function()
	{
		$('#goBackButton').focus();
	});
	
	paymentDIV.on('hidden', function() 
	{
		closeDialog('paymentForm', paymentCloseMethod);
	});
	
	paymentsOnReady();
});