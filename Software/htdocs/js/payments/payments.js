var controllerName = 'payments';
var gridObject = null;

Handlebars.registerHelper('amount', function()
{
	var highlight = new String();

	if (this.amount < 0)
	{
		highlight = ' text-error';
	}
	return new Handlebars.SafeString('<div class="pagination-right' + highlight + '">&euro; ' + number_format(this.amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('rest', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">&euro; ' + number_format(new String() + this.rest, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('status', function() 
{
	if(this.status == 'Open')
	{
		return new Handlebars.SafeString('<div class="pagination-right"><span class="label label-important">Aperto</span></div>');
	}
	else if(this.status == 'Close')
	{
		return new Handlebars.SafeString('<div class="pagination-right"><span class="label label-success">Chiuso</span></div>');
	}
});

function printPayments()
{
	location.href = '/payments/printlist?' + $('#paymentsSearch').serialize();
}

function updateListData(data)
{
	$('#paymentsGrid table tbody').append(
			'<tr><td colspan=3><div class="pagination-right"><b>Totali</b></div></td>'
		+	'<td><div class="pagination-right">€ ' + number_format(data.totals.sumAmount.toString(), 2, ',', '.') + '</div></td>'
		+	'<td><div class="pagination-right">€ ' + number_format(data.totals.sumRest.toString(), 2, ',', '.') + '</div></td>'
		+	'<td></td></tr>'
	);
}

!function()
{
	var paymentsDIV = $('#paymentsGrid'),
		pageSize = $('body').data('pagesize');
	gridObject = paymentsDIV.simplePagingGrid({
		columnNames: ['', 'Descrizione', 'Data', 'Importo', 'Rimanenza', 'Stato'],
		columnKeys: ['typology', 'description', 'dateLabel', 'amount', 'rest', 'status'],
		columnWidths: ['10px', '50%', '10%', '15%', '15%', '10%'],
		data: paymentsDIV.data('list'),
		dataUrl: '/' + controllerName + '/list',
		sortable: [false, true, true, true, true, true],
		currentPage: paymentsDIV.data('activepage'),
		initialSortColumn: paymentsDIV.data('activesortfield'),
		sortOrder: paymentsDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		disableAutoSubmitForm: true,
		headerTemplates: [null, null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, '{{amount}}', '{{rest}}', '{{status}}'],
		rowClickHandler: function(row){paymentDetail(row.uniqueid, row.paymentType, 0)},
		searchFormName: 'paymentsSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		pageRenderedEvent: updateListData
	});
	gridObject.settings.data = null;
	paymentsDIV.attr('data-list', null);
	paymentsDIV.attr('data-activepage', null);
	paymentsDIV.attr('data-activesortfield', null);
	paymentsDIV.attr('data-activesortdirection', null);
}();

function paymentDetail(uniqueid, paymentType, paymentMode)
{
	if (paymentMode == 0){
		openDialog('paymentDIV', uniqueid, '/' + controllerName + '/getrowdata', null, paymentOpenMethodUpdate, paymentOpenMethodInsert, {paymentType: paymentType});
	}
	else if (paymentMode == 1){
		openDialog('paymentDIV', uniqueid, '/' + controllerName + '/getrowdata', null, null, null, {paymentType: paymentType});
	}
}

$(function()
{
	createCalendar('startDateSearch');
	createCalendar('endDateSearch');

	var startDateSearch = $('#startDateSearch');
	var endDateSearch = $('#endDateSearch');

	if(startDateSearch.val() == '')
	{
		endDateSearch.prop('disabled', true);
		$('#substractDayEndButton').prop('disabled', true);
		$('#addDayEndButton').prop('disabled', true);
	}

	startDateSearch.change(function()
	{
		endDateSearch.prop('disabled', false);
		$('#substractDayEndButton').prop('disabled', false);
		$('#addDayEndButton').prop('disabled', false);
		updateMinEndDate('startDateSearch','endDateSearch');
	});
});