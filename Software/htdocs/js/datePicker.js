function createCalendar(calendarName, minDate)
{
	$("#" + calendarName).datepicker({dateFormat: "dd/mm/yy",
									  showButtonPanel: true,
									  changeMonth: true,
									  changeYear: true,
									  showOtherMonths: true,
									  selectOtherMonths: true,
									  showAnim: "slideDown"/*,
								 	  numberOfMonths: 2*/});
	if (minDate != null) {
		$('#'+calendarName).datepicker('option','minDate',minDate);
	}
}

/**
 * @param {Boolean} add, imposta la data al giorno successivo (true) o precedente (false)
 * @param {String} id,  attributo id dell'input (datepicker)
 * @param {Boolean} change, scatena l'evento change sull'elemento
 */
function addSubstractDay(add, id, change)
{
	var dateField = $('#' + id);
	
	var	currentVal = dateField.datepicker('getDate');
	
	if(!currentVal)
	{
		dateField.datepicker('setDate', '+0');
		currentVal = dateField.datepicker('getDate');
	}

    var	date = new Date(Date.parse(currentVal));

	if (add)
	{
		date.setDate( date.getDate() + 1);
	}
	else
	{
		date.setDate( date.getDate() - 1);
	}

	var newDate = date.toDateString(); 
	newDate = new Date(Date.parse(newDate));

	dateField.datepicker('setDate', newDate);

	if (change) dateField.change();
}