function deleteProduct()
{
	if (confirm('Eliminare definitivamente il prodotto ?')) {
		location.href = '/products/delete?uid=' + $('#uniqueid').val();
	}
}

function validateDescription()
{
	return checkField('description', 'Descrizione obbligatoria');
}

function validateQuantity()
{
	return checkField('quantity', 'Inserire una quantità') 
	&& checkNumber('quantity', 'Quantità in formato NON numerico', /^[0-9]+$/) 
	&& checkNumber('quantity', 'Massima quantità 99999', /^[0-9]{1,5}$/);
}

function validateProduct_name()
{
	return checkField('product_name', 'Nome Prodotto obbligatorio');
}

function validateAmount()
{
	return checkNumber('amount', 'Importo in formato NON numerico', /^[0-9]+(\.?)([0-9]*)$/);
}

function validateAmountSale()
{
	return checkNumber('amountsale', 'Importo in formato NON numerico', /^[0-9]+(\.?)([0-9]*)$/);
}

function validateVat()
{
	return checkNumber('vat', 'IVA in formato NON numerico', /^[0-9]+(\.?)([0-9]*)$/);
}

function saveProduct()
{
	var saveButton = $('#saveProductBtn').prop('disabled', true);

	if(validateDescription() && validateAmount() && validateVat() && validateProduct_name() && validateQuantity())
	{
		saveButton.prop('disabled', false);
		$('#productsForm').submit();
	}
	else
	{
		saveButton.prop('disabled', false);
	}
}

function selectFromCustomertype(customer_type, id){
	
	
	$('#amountsaleAddContainer').append([
			'<div id="amountsale'+id+'container" class="control-group">',
				'<label class="control-label" for="amountsale'+id+'">'+customer_type+'</label>',
				'<div class="controls">',
					'<input class="input-mini" id="'+id+'" name="'+id+'" type="text" value="" onblur="validateAmountSale()">',
					'<span id="amountsaleError" class="help-inline"></span>',
					'<button type="button" class="btn btn-mini btn-danger delete-movement-btn"><i class="icon-remove icon-white" style="vertical-align:middle" onclick="deletePrice(\'amountsale'+id+'container\'\,\''+id+'\')"></i></button>',
				'</div>',
			'</div>'
			].join(''));
	
	$('#list'+id).addClass('hide');
}

function deletePrice(divRemove, id){
	console.log(divRemove);
	$('#'+divRemove).remove();
	$('#list'+id).removeClass('hide');
}