Handlebars.registerHelper('amount', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('amountsale', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.amountsale, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('vat', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + this.vat + '</div>');
});

Handlebars.registerHelper('quantity', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + this.quantity + '</div>');
});

!function(){
	var productsDIV = $('#productsGrid'),
		body = $('body');
	window.productsGrid = productsDIV.simplePagingGrid({
		columnNames: ['Nome Prodotto', 'Descrizione', 'Quantità', 'IVA', 'Importo Acquisto', 'Importo Vendita'],
		columnKeys: ['product_name','description','quantity', 'vat', 'amount', 'amountsale'],
		columnWidths: ['20%', '30%', '10%', '10%', '15%', '15%'],
		data: productsDIV.data('list'),
		dataUrl: '/products/list',
		sortable: [true, true, true, true, true, true],
		currentPage: productsDIV.data('activepage'),
		initialSortColumn: productsDIV.data('activesortfield'),
		sortOrder: productsDIV.data('activesortdirection'),
		pageSize: body.data('pagesize'),
		minimumVisibleRows: body.data('pagesize'),
		headerTemplates: [null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, '{{quantity}}', '{{vat}}', '{{amount}}', '{{amountsale}}'],
		rowClickHandler: function(row){location.href = '/products/view?uid=' + row.uniqueid;},
		searchFormName: 'productsSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		disableAutoSubmitForm: true
	});
	productsGrid.settings.data = null;
	productsDIV.attr('data-list', null);
	productsDIV.attr('data-activepage', null);
	productsDIV.attr('data-activesortfield', null);
	productsDIV.attr('data-activesortdirection', null);
}();