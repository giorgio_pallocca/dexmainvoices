$(function()
{
	if($('#tab-name').val() == 'movements'){
		$('#productTabs a[href="#movements"]').tab('show');
	}else $('#productTabs a:first').tab('show');

	//inizio calendario
	
	$('.jqueryCalendar').each(function(){
		var elem = $(this);

		elem.datepicker({
			dateFormat: 'dd/mm/yy',
			showButtonPanel: true,
			changeMonth: true,
			changeYear: true,
			constrainInput: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			yearRange: elem.data('jquery-yearrange') || 'c-10:c+10'
		})
		.change(function(){
			try {
				$.datepicker.parseDate(elem.datepicker('option', 'dateFormat'), this.value);
				elem.data('lastdate', this.value);
			}
			catch(ex) {
				elem.datepicker('setDate', elem.data('lastdate'));
			}
		});

		elem.closest('.input-group').find('.trigger-calendar:first').click(function(){elem.focus()});
		elem.attr('data-jquery-yearrange', null);
	});
	
	
	//fine calendario
	
	
	$('.modify-movement-btn').click( function(e){
		var $target = $(e.target);
		//console.log($target.closest(".product-row"));
		
		$target.closest(".product-row").find(".pro-enab").attr("disabled",false);
		$target.closest(".product-row").find(".modify-movement-btn").addClass('hide');
		$target.closest(".product-row").find(".apply-movement-btn").removeClass('hide');
		$target.closest(".product-row").find(".jqueryCalendar").removeClass('pro-enab');
	});
	
	$('.apply-movement-btn').click( function(e){
		var $target = $(e.target);
		//console.log($target.closest(".product-row"));
		
		
		
		var $target = $(e.target);
		var id = $target.closest(".product-row").data("id");
		var load = $target.closest(".product-row").find(".product-load").val();
		var unload = $target.closest(".product-row").find(".product-unload").val();
		var prbuyunitary = $target.closest(".product-row").find(".product-prbuyunitary").val();
		var prsaleunitary = $target.closest(".product-row").find(".product-prsaleunitary").val();
console.log(prbuyunitary);		
		if(validateNumberBtn(load) && validateNumberBtn(unload) && validateNumberEurBtn(prbuyunitary) && validateNumberEurBtn(prsaleunitary)){
			$target.closest(".product-row").find(".pro-enab").attr("disabled",true);
			$target.closest(".product-row").find(".modify-movement-btn").removeClass('hide');
			$target.closest(".product-row").find(".apply-movement-btn").addClass('hide');
			$target.closest(".product-row").find(".jqueryCalendar").addClass('pro-enab');
			var description = $target.closest(".product-row").find(".product-description").val();
			var date = $target.closest(".product-row").find(".product-date").val();
console.log(prbuyunitary);	
			$.post('/products/update',
					{id: id, load: load, unload: unload, prbuyunitary: prbuyunitary, prsaleunitary: prsaleunitary, description: description, date: date}, 
					function(uniqueid){
						//$target.closest(".product-row").remove();
						//da inserire controllo tramite json di ritorno
						location.href='/products/view?movimento&uid='+uniqueid;
					});
		}
		
	});
	
	$('.view-invoice-btn').click(function(e){
		var tipoFattura = $(e.target).closest(".view-invoice-btn").data("tipe");
		if(tipoFattura == "Fattura out"){
			var $uniqueid = $(e.target).closest(".view-invoice-btn").val();
			location.href='/invoices/view?uid='+$uniqueid;
		}
		else{
			var $uniqueid = $(e.target).closest(".view-invoice-btn").val();
			location.href='/externalinvoices/view?uid='+$uniqueid;
		}
	});
	
	$('.add-movement-btn').click( function(e){
		
		var $target = $(e.target);
		var load = $target.closest(".product-row-up").find(".product-load").val();
		var unload = $target.closest(".product-row-up").find(".product-unload").val();
		var prbuyunitary = $target.closest(".product-row-up").find(".product-prbuyunitary").val();
		console.log(prbuyunitary);
		var prsaleunitary = $target.closest(".product-row-up").find(".product-prsaleunitary").val();
		console.log(prsaleunitary);
		if(validateNumberBtn(load) && validateNumberBtn(unload) && validateNumberEurBtn(prbuyunitary) && validateNumberEurBtn(prsaleunitary)){
			var description = $target.closest(".product-row-up").find(".product-description").val();
			var date = $target.closest(".product-row-up").find(".product-date").val();
			var id_prodotto = $target.closest(".product-row-up").data("id");
			//console.log(load, unload, prbuyunitary, prsaleunitary, description, date, id_prodotto);
			//var id_prodotto = $product.id_prodotto;
			//console.log(id_prodotto);
			$.post('/products/savemovement',
					{id_prodotto: id_prodotto, load: load, unload: unload, prbuyunitary: prbuyunitary, prsaleunitary: prsaleunitary, description: description, date: date}, 
					function(uniqueid){
						//$target.closest(".product-row").remove();
						//console.log(uniqueid);
						location.href='/products/view?movimento&uid='+uniqueid;
					});
		}
	});
	
	$('.delete-movement-btn').click( function(e){
		if(confirm('Eliminare il movimento del magazzino?')){
			var $target = $(e.target);
			var id = $target.closest(".product-row").data("id");
			$.post('/products/delete',{id: id}, function(uniqueid){
					location.href='/products/view?movimento&uid='+uniqueid;
					//$target.closest(".product-row").remove();
			});
		}
	});
	
	$('.product-load-err').blur(function(e){
		validateNumber(e);
	});
	
	$('.product-unload-err').blur(function(e){
		validateNumber(e);
	});
	
	$('.product-prbuyunitary-err').blur(function(e){
		validateNumberEur(e);
	});
	
	$('.product-prsaleunitary-err').blur(function(e){
		validateNumberEur(e);
	});
	
});

function validateNumber(e){
	var value = $(e.target).val();
	var expression = /^[0-9]{1,5}$/;
	if(value != '' && !expression.test(value)){
		$(e.target).closest(".control-group").addClass('error');
	}
	else{
		$(e.target).closest(".control-group").removeClass('error');
	}
}

function validateNumberEur(e){
	var value = $(e.target).val();
	var expression = /^[0-9]+(\,?)([0-9]?)([0-9]?)$/;
	if(value != '' && !expression.test(value)){
		$(e.target).closest(".control-group").addClass('error');
	}
	else{
		$(e.target).closest(".control-group").removeClass('error');
	}
}

function validateNumberBtn(value){
	var expression = /^[0-9]{1,5}$/;
	if(value != '' && !expression.test(value)){
		return false;
	}
	else{
		return true;
	}
}

function validateNumberEurBtn(value){
	var expression = /^[0-9]+(\,?)([0-9]?)([0-9]?)$/;
	if(value != '' && !expression.test(value)){
		return false;
	}
	else{
		return true;
	}
}