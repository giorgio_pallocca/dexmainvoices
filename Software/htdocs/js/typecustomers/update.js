function typecustomersDetail(id)
{
	console.log(id);
	
	openDialog('typecustomersModal', id, '/typecustomers/getrowdata', null, typecustomersUpdate, null, null);
}

function typecustomersUpdate()
{
	$('#deleteTypecustomersBtn').removeClass('hide');
}

function typecustomersClose()
{
	$('#deleteTypecustomersBtn').addClass('hide');
}

function saveTypecustomers()
{
	var form = $('#typecustomersForm'),
		saveBtn = $('#saveTypecustomersBtn').prop('disabled', true);
console.log(form[0]);
	$.post(form[0].action, form.serialize(), function(data)
	{
		if (data != null && data.error == false)
		{
			typecustomersGrid[0].refreshData();
			$('#typecustomersModal').modal('hide');
		}

		saveBtn.prop('disabled', false);
	}, 'json');
}

function deleteTypecustomers()
{
	if (confirm("Eliminare il tipo cliente '" + $('#typecustomersCustomer_type').val() + "' ?"))
	{
		$.post('/typecustomers/delete', {id: $('#typecustomersId').val()}, function(data)
		{
			if (data != null && data >= 0)
			{
				typecustomersGrid[0].refreshDataRemote(data);
				$('#typecustomersModal').modal('hide');
			}
		}, 'json');
	}
}

$(function()
{
	$('#typecustomersModal').on('hide', function()
	{
			$('#typecustomersCancelButton').focus();
	})
	.on('hidden', function()
	{
		closeDialog('typecustomersForm', typecustomersClose);
	})
	.on('shown', function()
	{
		$('#typecustomersName').focus();
	});
});