!function(){
	var typecustomersDIV = $('#typecustomersGrid'),
		body = $('body');

	window.typecustomersGrid = typecustomersDIV.simplePagingGrid({
		columnNames: ['Tipo Cliente'],
		columnKeys: ['customer_type'],
		columnWidths: ['100%'],
		data: typecustomersDIV.data('list'),
		dataUrl: '/typecustomers/list',
		sortable: [true],
		currentPage: typecustomersDIV.data('activepage'),
		initialSortColumn: typecustomersDIV.data('activesortfield'),
		sortOrder: typecustomersDIV.data('activesortdirection'),
		pageSize: body.data('pagesize'),
		minimumVisibleRows: body.data('pagesize'),
		headerTemplates: [null],
		cellTemplates:[null],
		rowClickHandler: function(row){typecustomersDetail(row.id)},
		cellTemplates:[null],
		searchFormName: 'typecustomersSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		disableAutoSubmitForm: true
	});
	typecustomersGrid.settings.data = null;
	typecustomersDIV.attr('data-list', null);
	typecustomersDIV.attr('data-activepage', null);
	typecustomersDIV.attr('data-activesortfield', null);
	typecustomersDIV.attr('data-activesortdirection', null);
}();