function searchIndex()
{
	$("#indexFilter").submit();
}

PIUIVA.morris = {
	dateFormat: function(x){
		var date = new Date(x);
		return PIUIVA.morris.getMonthName(date.getMonth()) + ' ' + date.getFullYear();
	},
	xLabelFormat: function(x){
		var date = new Date(x);
		return [PIUIVA.morris.pad(date.getDate()), PIUIVA.morris.pad(date.getMonth() +1), date.getFullYear()].join('-');
	},
	pad: function(n){
		return n<10 ? '0'+n : n;
	},
	getMonthName: function(month){
		return ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'][month];
	}
}