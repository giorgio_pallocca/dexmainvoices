Handlebars.registerHelper('przacq', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.prbuyunitary, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('przven', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.prsaleunitary, 2, ',', '.') + '</div>');
});

function updateListData(data)
{
	$('#productmovementsGrid table tbody').append(
			'<tr><td colspan=5><div class="pagination-right"><b>Totale</b></div></td>'
		+	'<td><div class="pagination-right">€ ' + number_format(data.total.toString(), 2, ',', '.') + '</div></td>'
	);
}

!function(){
	var productmovementsDIV = $('#productmovementsGrid'),
		pageSize = $('body').data('pagesize');
	window.productmovementsGrid = productmovementsDIV.simplePagingGrid({
		columnNames: ['Data', 'Nome', 'Carico', 'Scarico', 'Pz. Acquisto', 'Pz. Vendita', 'Descrizione'],
		columnKeys: ['date', 'product_name', 'load', 'unload', 'prbuyunitary', 'prsaleunitary', 'description'],
		columnWidths: ['15%', '15%', '10%', '10%', '15%', '15%', '20%'],
		data: productmovementsDIV.data('list'),
		dataUrl: '/productmovements/list',
		sortable: [true, true, true, true, true, true, true],
		currentPage: productmovementsDIV.data('activepage'),
		initialSortColumn: productmovementsDIV.data('activesortfield'),
		sortOrder: productmovementsDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null, null, null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, null, '{{przacq}}', '{{przven}}', null],
		searchFormName: 'productmovementsSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover'
	});
	productmovementsGrid.settings.data = null;
	productmovementsDIV.attr('data-list', null);
	productmovementsDIV.attr('data-activepage', null);
	productmovementsDIV.attr('data-activesortfield', null);
	productmovementsDIV.attr('data-activesortdirection', null);
}();

function printProductmovements()
{
	if (productmovementsGrid[0].currentData.length > 0)
	{
		location.href = '/productmovements/printlist?' + $('#productmovementsSearch').serialize();
	}
	else
	{
		alert('Non è presente alcun prodotto in magazzino');
	}
}

$(function()
{
	createCalendar('startDateSearch');
	createCalendar('endDateSearch');

	var startDateSearch = $('#startDateSearch');
	var endDateSearch = $('#endDateSearch');

	if(startDateSearch.val() == '')
	{
		endDateSearch.prop('disabled', true);
		$('#substractDayEndButton').prop('disabled', true);
		$('#addDayEndButton').prop('disabled', true);
	}

	startDateSearch.change(function()
	{
		endDateSearch.prop('disabled', false);
		$('#substractDayEndButton').prop('disabled', false);
		$('#addDayEndButton').prop('disabled', false);
		updateMinEndDate('startDateSearch','endDateSearch');
	});
});