function validateCompany()
{
	return checkField('company', 'Denominazione obbligatoria');
}

function validateEmail()
{
	var result = false;
	var email = $('#email');
	var validationEmailExpression = /^[a-z0-9-_]+(\.[a-z0-9-_]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/;

	if(email.val() != '' && !validationEmailExpression.test(email.val()))
	{
		enableErrorClass('email', 'Email NON conforme');
	}
	else
	{
		disableErrorClass('email');
		result = true;
	}
	return result;
}

function validateVatNumberFiscalCode()
{
	if(checkField('vatNumber', 'Inserire Partita IVA') || checkField('fiscalCode', 'o Codice Fiscale'))
	{
		disableErrorClass('vatNumber');
		disableErrorClass('fiscalCode');
		return (checkNumber('vatNumber', 'Partita IVA NON conforme', /^[0-9]{11}$/)
				&& checkNumber('fiscalCode', 'Codice Fiscale NON conforme', /^[A-Za-z0-9]{11,16}$/));
	}
}

function saveCustomer(addFromReference)
{
	var saveButton = $('#saveCustomerBtn').prop('disabled', true);
	var uniqueid = $('#uniqueid').val();

	if(addFromReference)
	{
		var params = $('#addCustomerForm').serializeArray();
		params.push({name: 'addCustomer', value:'addCustomer'});

		if(validateCompany() && validateVatNumberFiscalCode())
		{
			$.post('/customers/save', params, function(data) 
			{
				if(data.errors != false)
				{
					if(data.errors.email)
					{
						enableErrorClass('email', 'Email già utilizzata');
					}
					saveButton.prop('disabled', false);
				}
				else
				{
					if(data.newCustomer != null && data.newCustomer != false)
					{
						disableErrorClass('customer_id');
						sortCombo('customer_id');
						$('#customer_id')
							.append("<option value='" + data.newCustomer.id + "'>" + data.newCustomer.company + "</option>")
							.val(data.newCustomer.id);
						saveButton.prop('disabled', false);
						$('#addCustomerDIV').modal('hide');
					}
				}
			}, 'json');
		}
		else
		{
			saveButton.prop('disabled', false);
		}
	}
	else
	{
		if(validateCompany() && checkNumber('vatNumber', 'Partita IVA NON conforme', /^[0-9]{11}$/)
							&& checkNumber('fiscalCode', 'Codice Fiscale NON conforme', /^[A-Za-z0-9]{11,16}$/))
		{
			$.post('/customers/save', $('#customersForm').serialize(), function(data)
			{
				if(data.errors != false)
				{
					if(data.errors.email)
					{
						enableErrorClass('email', 'Email già utilizzata');
					}
					saveButton.prop('disabled', false);
				}
				else
				{
					location.href = '/customers/view?uid=' + data.newCustomer.uniqueid;
				}
			}, 'json');
		}
		else
		{
			saveButton.prop('disabled', false);
		}
	}
}

function addCustomer()
{
	displayDialog('addCustomerDIV');
}

$(function()
{
	$('#addCustomerDIV').on('show', function () 
	{
		disableErrorClass('company');
		disableErrorClass('vatNumber');
		disableErrorClass('fiscalCode');
	});
	
	$('#addCustomerDIV').on('hidden', function () 
	{
		resetFormData('addCustomerDIV');
	});
	
	$('.customerType').change(function(e){
		var $target = $(e.target);
		var value = $target.val();
		if($target.val() == "privato"){
			$('#company').val($('#firstname').val()+" "+$('#surname').val());
		}
		else{
			$('#company').val("");
		}
	});
});

function deleteCustomer(){
	if (confirm('Eliminare definitivamente il contatto "' + $('#company').val() + '" ?'))
		location.href = '/customers/delete?uid=' + $('#uniqueid').val();
}