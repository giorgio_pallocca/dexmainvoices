Handlebars.registerHelper('unpaid_amount', function(){
	var warning = '';
	if (this.unpaid_amount > 0){
		warning = ' class="text-error"';
	}
	return new Handlebars.SafeString('<div class="pagination-right"><span' + warning + '>' + number_format(this.unpaid_amount, 2, ',', '.') + '</span></div>');
});

!function(){
	var customersDIV = $('#customersGrid'),
		body = $('body');
	window.customersGrid = customersDIV.simplePagingGrid({
		columnNames: ['Denominazione', 'Nome', 'Cognome', 'Email', 'Tipologia', 'Scoperto (€)'],
		columnKeys: ['company', 'firstname', 'surname', 'email', 'type', 'unpaid_amount'],
		columnWidths: ['20%', '20%', '20%', '10%', '15%', '15%'],
		data: customersDIV.data('list'),
		dataUrl: '/customers/list',
		sortable: [true, true, true, true, true, true],
		currentPage: customersDIV.data('activepage'),
		initialSortColumn: customersDIV.data('activesortfield'),
		sortOrder: customersDIV.data('activesortdirection'),
		pageSize: body.data('pagesize'),
		minimumVisibleRows: body.data('pagesize'),
		headerTemplates: [null, null, null, null, null, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, null, null, '{{unpaid_amount}}'],
		rowClickHandler: function(row){location.href = '/customers/view?uid=' + row.uniqueid;},
		searchFormName: 'customersSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		disableAutoSubmitForm: true
	});
	customersGrid.settings.data = null;
	customersDIV.attr('data-list', null);
	customersDIV.attr('data-activepage', null);
	customersDIV.attr('data-activesortfield', null);
	customersDIV.attr('data-activesortdirection', null);
}();