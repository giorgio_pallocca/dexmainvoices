$(function(){
	var contacts = $('#contactsPreviewTable').data('contacts'),
		$title = $(document.createDocumentFragment()),
		$fieldsOptions = $(document.createDocumentFragment()),
		$contactsExample = $(document.createDocumentFragment()),
		selectValues = {
			'company': 'Denominazione',
			'firstname': 'Nome',
			'surname': 'Cognome',
			'email': 'Email',
			'vatNumber': 'Partita IVA',
			'fiscalCode': 'Codice Fiscale',
			'address': 'Indirizzo',
			'zipcode': 'CAP',
			'city': 'Città',
			'state': 'Provincia',
			'phone': 'Telefono',
			'fax': 'Fax',
			'distanceKm': 'Distanza in Km'
		};

	if(contacts){
		for(var k in selectValues) {
			$fieldsOptions.append('<option value="' + k + '">' + selectValues[k] + '</option>');
		}

		$('#contactsPreview').removeClass('hide');
		for($i=0; $i<contacts[0].length; $i++) {
			var title = contacts[0];
			$title.append(
				'<th>' +
					title[$i] + '<br><br>' +
					'<select class="fields-select" name=' + $i + '></select><br>' +
				'</th>'
			);
		}

		$('#titleRow').append($title);

		$('.fields-select').each(function() {
			$(this).append($fieldsOptions.clone());
		});

		for($j=1; $j<=4; $j++) {
			var row = contacts[$j];

			$contactsExample.append('<tr>');

			for ($t=0; $t<contacts[0].length; $t++) {
				var content = row[$t];
				if (!content){
					content = '';
				}
				$contactsExample.append(
					'<td>' + content + '</td>'
				);
			}

			$contactsExample.append('</tr>');
		}

		$('.preview-body').append($contactsExample);
	}

	$('.fields-select').change(function(){
		$(this).removeClass('error');
	});

	$('#confirmBtn').click(function(){
		var selectValues = [];
		$('.fields-select').each(function() {
			$value = $(this).find(':selected').text();
			if (jQuery.inArray($value, selectValues) == -1){
				selectValues.push($value);
				$(this).removeClass('error');
			}
			else $(this).addClass('error');
		});

		if($('.fields-select').hasClass('error') || $('.fields-select option[value="company"]').prop('selected') != true){
			$('#errorInSelect').removeClass('hide');
		}
		else $('#importContactsForm').submit();
	});
})