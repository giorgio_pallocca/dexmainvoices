var PIUIVA = {
	grids: {
		thRightNotSortable: '<th class="pagination-right" width="{{width}}">{{title}}</th>',
		thRightSortable: '<th class="pagination-right" width="{{width}}">{{title}}<ul class="sort"><li class="sort-ascending"/><li class="sort-descending"/></ul></th>'
	}
}

function checkVat(fieldName){
	if($('#free').is(':checked'))
		return true;
	else
		return checkField(fieldName, 'IVA obbligatoria');
}

function checkDescription(fieldName){
	return checkField(fieldName, 'Descrizione obbligatoria');
}

function checkSerialNumber()
{
	if($('body').data('controller') != 'recurringinvoices')
	{
		var id = 'serialNumber', msg = 'N° progressivo obbligatorio';
		var num = $('#' + id);
		num.val(num.val().replace(/^[0]+/g,''));
		return checkField(id, msg) && checkNumber(id, msg, /^[1-9][0-9]*$/);
	}
}

function enableReferenceErrors(data, inputDateName)
{
	if (data.errors.serialNumber){
		enableErrorClass('serialNumber', 'Il N° progressivo ' + data.serialNumber + ' è già utilizzato per l\'anno ' + data.year);
	}
	if (data.errors.dates){
		enableErrorClass(inputDateName, 'Date valide nel ' + data.year + ' per il N° progressivo ' + data.serialNumber  + ': dal ' + data.previousDate + ' al ' + data.nextDate);
	}
}

function checkReferenceFields(inputDateName, controllerName)
{
	disableErrorClass('serialNumber');
	disableErrorClass(inputDateName);

	var date = $('#' + inputDateName), serialNumber = $('#serialNumber');

	if (!checkSerialNumber()) return;

	var params = {uniqueid: $('#uniqueid').val(), date: date.val(), serialNumber: serialNumber.val()};
	$.post('/' + controllerName + '/checkreferencefields', params, function(data)
	{
		if (data != null)
		{
			enableReferenceErrors(data, inputDateName);
		}
	}, 'json');
}

function isFree(vatFieldName){
	if($('#free').is(':checked'))
		$('#' + vatFieldName + 'Container').addClass('hide');
	else
		$('#' + vatFieldName + 'Container').removeClass('hide');
}

$(function()
{
	if (!document.createElement('input').autofocus)
	{
		$('[autofocus]:visible:first').focus();
	}

	$('a[data-toggle="tab"]').on('show shown' , function(e)
	{
		e.stopPropagation();
	});
});