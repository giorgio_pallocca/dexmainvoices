function deleteTransfert()
{
	if (confirm('Eliminare definitivamente la trasferta ?')) {
		location.href = '/transferts/delete?uid=' + $('#uniqueid').val();
	}
}

function selectFromCustomers(company, address, zipcode, city, distanceKm){
	$('#customerName').val(company);
	$('#customerAddress').val(address);
	$('#customerZipcode').val(zipcode);
	$('#customerCity').val(city);
	$('#kmTot').val(distanceKm);
}

function validateKmTot()
{
	return checkField('kmTot', 'Km totali obbligatori');
}

function saveTransfert()
{
	var saveButton = $('#saveTransfertBtn').prop('disabled', true);

	if(validateKmTot() && checkField('vehicle_id', 'Veicolo obbligatorio') && checkField('user_id', 'Utente obbligatorio'))
	{
		saveButton.prop('disabled', false);
		$('#transfertsForm').submit();
	}
	else
	{
		saveButton.prop('disabled', false);
	}
}

$(function(){
	createCalendar('date');
});