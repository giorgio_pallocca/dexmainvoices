Handlebars.registerHelper('kmTot', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.kmTot, 2, '.', ',') + '</div>');
});

Handlebars.registerHelper('cost', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.cost, 2, ',', '.') + '</div>');
});

function updateListData(data)
{
	$('#transfertsGrid table tbody').append(
			'<tr><td colspan=5><div class="pagination-right"><b>Totale</b></div></td>'
		+	'<td><div class="pagination-right">€ ' + number_format(data.total.toString(), 2, ',', '.') + '</div></td>'
	);
}

!function(){
	var transfertsDIV = $('#transfertsGrid'),
		pageSize = $('body').data('pagesize');
	window.transfertsGrid = transfertsDIV.simplePagingGrid({
		columnNames: ['Data', 'Cliente', 'Veicolo', 'Utente', 'Km Tot', 'Costo'],
		columnKeys: ['date', 'customerName', 'plate', 'fullname', 'kmTot', 'cost'],
		columnWidths: ['10%', '28%', '11%', '30%', '11%', '10%'],
		data: transfertsDIV.data('list'),
		dataUrl: '/transferts/list',
		sortable: [true, true, true, true, true, true],
		currentPage: transfertsDIV.data('activepage'),
		initialSortColumn: transfertsDIV.data('activesortfield'),
		sortOrder: transfertsDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null, null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, null, '{{kmTot}}', '{{cost}}'],
		rowClickHandler: function(row){location.href = '/transferts/update?uid=' + row.uniqueid},
		searchFormName: 'transfertsSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		pageRenderedEvent: updateListData
	});
	transfertsGrid.settings.data = null;
	transfertsDIV.attr('data-list', null);
	transfertsDIV.attr('data-activepage', null);
	transfertsDIV.attr('data-activesortfield', null);
	transfertsDIV.attr('data-activesortdirection', null);
}();

function printTransferts()
{
	if (transfertsGrid[0].currentData.length > 0)
	{
		location.href = '/transferts/printlist?' + $('#transfertsSearch').serialize();
	}
	else
	{
		alert('Non è presente nessuna trasferta da stampare');
	}
}

$(function()
{
	createCalendar('startDateSearch');
	createCalendar('endDateSearch');

	var startDateSearch = $('#startDateSearch');
	var endDateSearch = $('#endDateSearch');

	if(startDateSearch.val() == '')
	{
		endDateSearch.prop('disabled', true);
		$('#substractDayEndButton').prop('disabled', true);
		$('#addDayEndButton').prop('disabled', true);
	}

	startDateSearch.change(function()
	{
		endDateSearch.prop('disabled', false);
		$('#substractDayEndButton').prop('disabled', false);
		$('#addDayEndButton').prop('disabled', false);
		updateMinEndDate('startDateSearch','endDateSearch');
	});
});