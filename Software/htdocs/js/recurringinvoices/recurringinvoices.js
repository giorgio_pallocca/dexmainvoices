Handlebars.registerHelper('amount', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('frequency', function(){
	var text;

	switch (this.frequency) {
		case '1_week':
			text = 'Settimanale';
			break;
		case '2_week':
			text = 'Quindicinale';
			break;
		case '1_month':
			text = 'Mensile';
			break;
		case '2_month':
			text = 'Bimestrale';
			break;
		case '3_month':
			text = 'Trimestrale';
			break;
		case '4_month':
			text = 'Quadrimestrale';
			break;
		case '6_month':
			text = 'Semestrale';
			break;
		case '1_year':
			text = 'Annuale';
			break;
		case '2_year':
			text = 'Biennale';
	}

	return new Handlebars.SafeString('<div class="pagination-right">' + text + '</div>');
});

Handlebars.registerHelper('status', function()
{
	var status = label = '';

	if (this.status == 'activated') {
		status = 'Attiva';
		label = ' label-success';
	}
	else if (this.status == 'draft') {
		status = 'Non attiva';
		label = ' label-important';
	}

	return new Handlebars.SafeString('<span class="label' + label + '">' + status + '</span>');
});

!function(){
	var recurringinvoicesDIV = $('#recurringinvoicesGrid'),
		body = $('body'),
		controller = body.data('controller');

	window.recurringinvoiceGrid = recurringinvoicesDIV.simplePagingGrid({
		columnNames: ['Cliente', 'Data inizio', 'Ultima emissione', 'Frequenza', 'Prossima emissione', 'Codice', 'Stato', 'Importo'],
		columnKeys: ['customer_name', 'date', 'last_recurred', 'frequency', 'next_recurring', 'code', 'status', 'amount'],
		columnWidths: ['15%', '13%', '18%', '13%', '10%', '10%', '9%', '12%'],
		data: recurringinvoicesDIV.data('list'),
		dataUrl: '/recurringinvoices/list',
		sortable: [true, true, true, true, true, true, true, true],
		currentPage: recurringinvoicesDIV.data('activepage'),
		initialSortColumn: recurringinvoicesDIV.data('activesortfield'),
		sortOrder: recurringinvoicesDIV.data('activesortdirection'),
		pageSize: body.data('pagesize'),
		minimumVisibleRows: body.data('pagesize'),
		headerTemplates: [null, null, null, PIUIVA.grids.thRightSortable, null, null, null, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, '{{frequency}}', null, null, '{{status}}', '{{amount}}'],
		rowClickHandler: function(row){location.href = '/' + controller + '/view?uid=' + row.uniqueid},
		searchFormName: 'recurringinvoiceSearch',
		searchButtonName: 'recurringinvoiceSearchButton',
		showAllButtonName: 'recurringinvoiceShowAllButton',
		tableClass: 'table table-bordered table-hover'
	});
	recurringinvoiceGrid.settings.data = null;
	recurringinvoicesDIV.attr('data-list', null);
	recurringinvoicesDIV.attr('data-activepage', null);
	recurringinvoicesDIV.attr('data-activesortfield', null);
	recurringinvoicesDIV.attr('data-activesortdirection', null);
}();

$(function(){
	createCalendar('startDate');
	createCalendar('endDate');

	var startDateSearch = $('#startDate');
	var endDateSearch = $('#endDate');

	if(startDateSearch.val() == '')
	{
		endDateSearch.prop('disabled', true);
		$('#substractDayEndButton').prop('disabled', true);
		$('#addDayEndButton').prop('disabled', true);
	}

	startDateSearch.change(function()
	{
		endDateSearch.prop('disabled', false);
		$('#substractDayEndButton').prop('disabled', false);
		$('#addDayEndButton').prop('disabled', false);
		updateMinEndDate('startDate','endDate');
	});
});