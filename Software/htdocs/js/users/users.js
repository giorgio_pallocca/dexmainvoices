Handlebars.registerHelper('role', function()
{
	var role = label = '';

	if (this.role == 'administrator') {
		status = 'Amministratore';
		label = ' label-info';
	}
	else if (this.role == 'user') {
		status = 'Utente';
		label = ' label-success';
	}

	return new Handlebars.SafeString('<div class="pagination-right"><span class="label' + label + '">' + status + '</span></div>');
});

!function(){
	var usersDIV = $('#usersGrid'),
		pageSize = $('body').data('pagesize');
	window.usersGrid = usersDIV.simplePagingGrid({
		columnNames: ['Nome', 'Email', 'Telefono', 'Ruolo'],
		columnKeys: ['fullname', 'email', 'phone', 'role'],
		columnWidths: ['30%', '35%', '20%', '15%'],
		data: usersDIV.data('list'),
		dataUrl: '/users/list',
		sortable: [true, true, false, true],
		currentPage: usersDIV.data('activepage'),
		initialSortColumn: usersDIV.data('activesortfield'),
		sortOrder: usersDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null, null, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, '{{role}}'],
		rowClickHandler:function(row){userDetail(row.uniqueid)},
		searchFormName: 'userSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover'
	});
	usersGrid.settings.data = null;
	usersDIV.attr('data-list', null);
	usersDIV.attr('data-activepage', null);
	usersDIV.attr('data-activesortfield', null);
	usersDIV.attr('data-activesortdirection', null);
}();

function userDetail(uniqueid)
{
	if (uniqueid == '') {
		location.href='/users/update';
	}
	else {
		location.href='/users/update?uid=' + uniqueid;
	}
}