function deleteUser(){
	if (confirm('Eliminare definitivamente l\'utente "' + $('#surname').val() + ' ' + $('#firstname').val() + '" ?')) {
		location.href = '/users/delete?uid=' + $('#uniqueid').val();
	}
}

function validateEmail(){
	return checkEmail('email', 'Email obbligatoria', 'uniqueid', '/users', 'saveUserBtn', true);
}

function validatePassword(){
	var mandatory = true;

	if($('#uniqueid').val() != ''){
		mandatory = false;
	}
	return compareFields('password', mandatory, 5, 'Password obbligatoria');
}

function saveUser()
{
	fieldId = 'userForm';

	var saveButton = $('#saveUserBtn').prop('disabled',true);

	if(checkField('firstname','Nome obbligatorio') && checkField('surname','Cognome obbligatorio') && validatePassword()){
		toBeSubmitted = true;

		if(!validateEmail()){
			saveButton.prop('disabled',false);
		}
	}
	else{
		saveButton.prop('disabled',false);
	}
}