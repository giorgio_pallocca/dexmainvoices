!function(){
	var unitsDIV = $('#unitsGrid'),
		pageSize = $('body').data('pagesize');
	window.unitsGrid = unitsDIV.simplePagingGrid({
		columnNames: ['Descrizione singolare', 'Descrizione plurale'],
		columnKeys: ['description', 'plural'],
		columnWidths: ['50%', '50%'],
		data: unitsDIV.data('list'),
		dataUrl: '/units/list',
		sortable: [true, false],
		currentPage: unitsDIV.data('activepage'),
		initialSortColumn: unitsDIV.data('activesortfield'),
		sortOrder: unitsDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null],
		cellTemplates:[null, null],
		rowClickHandler: function(row){location.href = '/units/update?uid=' + row.uniqueid},
		searchFormName: 'unitsSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		disableAutoSubmitForm: true
	});
	unitsGrid.settings.data = null;
	unitsDIV.attr('data-list', null);
	unitsDIV.attr('data-activepage', null);
	unitsDIV.attr('data-activesortfield', null);
	unitsDIV.attr('data-activesortdirection', null);
}();