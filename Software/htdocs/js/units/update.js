function validateDescription() {
	var result = false;
	if (checkField('description','Descrizione singolare obbligatoria')) {
		checkData('description', 'uniqueid', '/units', 'saveUnitBtn', 'Descrizione singolare già utilizzata');
		result = true;
	} else {
		toBeSubmitted = false;
	}
	return result;
}

function validatePlural() {
	return checkField('plural', 'Descrizione plurale obbligatoria');
}

function saveUnit() {
    fieldId = 'unitsForm';
    var saveButton = $('#saveUnitBtn').prop('disabled', true);
    toBeSubmitted = true;

    if(validatePlural())
    {
	    if (!validateDescription()) {
	    	saveButton.prop('disabled', false);
	    }
    }
    else
    {
    	saveButton.prop('disabled', false);
    }
}

function deleteUnit()
{
	if (confirm('Eliminare definitivamente l\'unità di misura ?')) {
		location.href = '/units/delete?uid=' + $('#uniqueid').val();
	}
}