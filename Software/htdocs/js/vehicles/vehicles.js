!function(){
	var vehiclesDIV = $('#vehiclesGrid'),
		pageSize = $('body').data('pagesize');
	window.vehiclesGrid = vehiclesDIV.simplePagingGrid({
		columnNames: ['Descrizione', 'Targa', 'Coefficiente ACI'],
		columnKeys: ['description', 'plate', 'aciCoefficient'],
		columnWidths: ['50%', '25%', '25%'],
		data: vehiclesDIV.data('list'),
		dataUrl: '/vehicles/list',
		sortable: [true, true, true],
		currentPage: vehiclesDIV.data('activepage'),
		initialSortColumn: vehiclesDIV.data('activesortfield'),
		sortOrder: vehiclesDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null, null],
		cellTemplates:[null, null, null],
		rowClickHandler: function(row){location.href = '/vehicles/update?uid=' + row.uniqueid},
		searchFormName: 'vehiclesSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		disableAutoSubmitForm: true
	});
	vehiclesGrid.settings.data = null;
	vehiclesDIV.attr('data-list', null);
	vehiclesDIV.attr('data-activepage', null);
	vehiclesDIV.attr('data-activesortfield', null);
	vehiclesDIV.attr('data-activesortdirection', null);
}();