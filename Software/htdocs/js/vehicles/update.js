function deleteVehicle()
{
	if (confirm('Eliminare definitivamente il veicolo ?')) {
		location.href = '/vehicles/delete?uid=' + $('#uniqueid').val();
	}
}

function validateDescription()
{
	return checkField('description', 'Descrizione obbligatoria');
}

function validatePlate()
{
	var result = false;
	if(checkField('plate','Targa obbligatoria')) 
	{
		checkData('plate', 'uniqueid', '/vehicles', 'saveVehicleBtn', 'Targa gi&agrave; utilizzata');
		result = true;
    } 
	else 
	{
    	toBeSubmitted = false;
    }
    return result;
}

function validateAciCoefficient()
{
	return checkField('aciCoefficient', 'Coefficiente ACI obbligatorio');
}

function saveVehicle()
{
	fieldId = 'vehiclesForm';
	var saveButton = $('#saveVehicleBtn').prop('disabled', true);

	if(validateDescription() && validateAciCoefficient())
	{
		toBeSubmitted = true;
		if(!validatePlate())
		{	
			saveButton.prop('disabled', false);
		}
	}
	else
	{
		saveButton.prop('disabled', false);
	}
}