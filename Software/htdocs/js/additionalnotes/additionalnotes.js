Handlebars.registerHelper('referredto', function(){
	var text;

	switch(this.referredto) {
		case 0:
			text = 'Fatture';
			break;
		case 1:
			text = 'Preventivi';
			break;
		case 2:
			text = 'DDT';
			break;
		case 3:
			text = 'Note di credito';
	}

	return new Handlebars.SafeString('<div class="pagination-right">' + text + '</div>');
});

!function(){
	var additionalnotesDIV = $('#additionalnotesGrid'),
		pageSize = $('body').data('pagesize');
	window.additionalnotesGrid = additionalnotesDIV.simplePagingGrid({
		columnNames: ['Titolo', 'Descrizione', 'Riferimento'],
		columnKeys: ['title', 'description', 'referredto'],
		columnWidths: ['30%', '55%', '15%'],
		data: additionalnotesDIV.data('list'),
		dataUrl: '/additionalnotes/list',
		sortable: [true, true, true],
		currentPage: additionalnotesDIV.data('activepage'),
		initialSortColumn: additionalnotesDIV.data('activesortfield'),
		sortOrder: additionalnotesDIV.data('activesortdirection'),
		pageSize: pageSize,
		minimumVisibleRows: pageSize,
		headerTemplates: [null, null, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, '{{referredto}}'],
		rowClickHandler: function(row){location.href = '/additionalnotes/update?uid=' + row.uniqueid;},
		searchFormName: 'additionalnotesSearch',
		searchButtonName: 'searchButton',
		showAllButtonName: 'showAllButton',
		tableClass: 'table table-bordered table-hover',
		disableAutoSubmitForm: true
	});
	additionalnotesGrid.settings.data = null;
	additionalnotesDIV.attr('data-list', null);
	additionalnotesDIV.attr('data-activepage', null);
	additionalnotesDIV.attr('data-activesortfield', null);
	additionalnotesDIV.attr('data-activesortdirection', null);
}();