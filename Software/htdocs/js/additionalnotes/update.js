function deleteNote()
{
	if (confirm('Eliminare definitivamente la nota aggiuntiva ?')) {
		location.href = '/additionalnotes/delete?uid=' + $('#uniqueid').val();
	}
}

function validateTitle()
{
	return checkField('title', 'Titolo obbligatorio');
}

function validateDescription()
{
	return checkField('description', 'Descrizione obbligatoria');
}

function saveNote()
{
	var saveButton = $('#saveNoteBtn').prop('disabled', true);

	if(validateTitle() && validateDescription())
	{
		saveButton.prop('disabled', false);
		$('#additionalnotesForm').submit();
	}
	else
	{
		saveButton.prop('disabled', false);
	}
}