Handlebars.registerHelper('amount', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('vat_amount', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.vat_amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('status', function()
{
	var status = label = '';

	if (this.status == 0) {
		status = 'Pagato';
		label = ' label-success';
	}
	else if (this.status == 1) {
		status = 'Da pagare';
		label = ' label-important';
	}

	return new Handlebars.SafeString('<div class="pagination-right"><span class="label' + label + '">' + status + '</span></div>');
});

Handlebars.registerHelper('unpaid_amount', function(){
	var warning = '';
	if (this.unpaid_amount > 0){
		warning = ' class="text-error"';
	}
	return new Handlebars.SafeString('<div class="pagination-right"><span' + warning + '>' + number_format(this.unpaid_amount, 2, ',', '.') + '</span></div>');
});

!function(){
	var referencesDIV = $('#referencesGrid'),
		body = $('body'),
		controller = body.data('controller');

	window.referenceGrid = referencesDIV.simplePagingGrid({
		columnNames: ['Data', 'Progr.', 'Fornitore', 'Importo (€)', 'Scoperto', 'IVA', 'Stato'],
		columnKeys: ['date', 'serialNumber', 'customer_name', 'amount', 'unpaid_amount', 'vat_amount', 'status'],
		columnWidths: ['10%', '10%', '33%', '14%', '14%', '10%', '9%'],
		data: referencesDIV.data('list'),
		dataUrl: '/' + controller + '/list',
		sortable: [true, true, true, true, true, true, true],
		currentPage: referencesDIV.data('activepage'),
		initialSortColumn: referencesDIV.data('activesortfield'),
		sortOrder: referencesDIV.data('activesortdirection'),
		pageSize: body.data('pagesize'),
		minimumVisibleRows: body.data('pagesize'),
		headerTemplates: [null, null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable],
		cellTemplates:[null, null, null, '{{amount}}', '{{unpaid_amount}}', '{{vat_amount}}', '{{status}}'],
		rowClickHandler: function(row){location.href = '/' + controller + '/view?uid=' + row.uniqueid},
		searchFormName: 'referenceSearch',
		searchButtonName: 'referenceSearchButton',
		showAllButtonName: 'referenceShowAllButton',
		tableClass: 'table table-bordered table-hover',
		pageRenderedEvent: updateListData
	});
	referenceGrid.settings.data = null;
	referencesDIV.attr('data-list', null);
	referencesDIV.attr('data-activepage', null);
	referencesDIV.attr('data-activesortfield', null);
	referencesDIV.attr('data-activesortdirection', null);
}();

$(function(){
	createCalendar('startDate');
	createCalendar('endDate');

	var startDate = $('#startDate'),
		endDate = $('#endDate');

	updateMinEndDate('startDate','endDate');
	startDate.change(function(){
		endDate.prop('disabled', false);
		updateMinEndDate('startDate','endDate');
	});

	if (startDate.val() == '') endDate.prop('disabled', true);
});