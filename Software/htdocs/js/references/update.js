function checkCustomer()
{
	var text = 'Cliente obbligatorio';
	if ($('body').data('controller') == 'externalinvoices') {
		text = 'Fornitore obbligatorio';
	}

	return checkField('customer_id', text);
}

function saveReference()
{
	var saveBtn = $('#saveReferencesBtn').prop('disabled', true);
	var dateName = 'date';

	var serialNumber = $('#serialNumber'),
		date = $('#date'),
		params = {serialNumber: serialNumber.val(), uniqueid: $('#uniqueid').val(), date: date.val()};

	var controller = $('body').data('controller');
	if(controller == 'recurringinvoices')
	{
		$('#referencesForm').submit();
	}

	if (checkCustomer() && checkSerialNumber())
	{
		if (controller == 'externalinvoices') {
			$('#referencesForm').submit();
		}
		else {
			$.post('/' + controller + '/checkreferencefields', params, function(data) {
				if (data != null) {
					if (data.errors == false) {
						$('#referencesForm').submit();
					}
					else {
						enableReferenceErrors(data, dateName);
						saveBtn.prop('disabled', false);
					}
				}
			}, 'json');
		}
	}
	else saveBtn.prop('disabled', false);
}

function selectFromAdditionalNotes(description){
	var notes = $('#notes'),
		separator = (notes.val() == '')? '':'\n';

	notes.val(notes.val() + separator + description);
}

function showOptionsDiv()
{
	$('#otherOptions').show(); 
	$('#showOptions').addClass('hide'); 
	$('#hideOptions').removeClass('hide');
}

function hideOptionsDiv()
{
	$('#otherOptions').hide(); 
	$('#showOptions').removeClass('hide'); 
	$('#hideOptions').addClass('hide');
}

function removeAttachment(fileName)
{
	var params = {fileName: fileName, uniqueid: $('#uniqueid').val()};
	$.post('/default/externalinvoices/removeattachment', params, function(data)
	{
		if (data == true)
		{
			$('#attachmentName').addClass('hide');
			$('#attachment').removeClass('hide');
		}
	});
}

$(function(){
	createCalendar('date');
	createCalendar('expireDate');
	createCalendar('endDate');

	var controller = $('body').data('controller');
	if (controller == 'transportdocuments') {
		createCalendar('dateTimeTransport');
	}

	var date = $('#date');
	date.change(function(){
		if (controller != 'recurringinvoices') {
			var year = $('#date').datepicker('getDate').getFullYear();
			$('#year').text('/' + year);
		}
		$('#expireDate').datepicker('option', 'minDate', date.datepicker('getDate'));
		$('#endDate').datepicker('option', 'minDate', date.datepicker('getDate'));
	})
	.change();

	$('#hideOptions').addClass('hide');
});