Handlebars.registerHelper('price', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.price, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('vat', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + this.vat + '</div>');
});

Handlebars.registerHelper('taxable', function(){
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.taxable, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('actions', function(){
	return new Handlebars.SafeString('<div class="pagination-right">'
		+ '<button class="btn btn-mini modify-movement-btn" data-uniqueid="'+this.uniqueid+'">Modifica</button> '
		+ '<button class="btn btn-mini btn-danger" onclick="deleteMovement(\'' + this.uniqueid + '\')"><i class="icon-remove icon-white" style="vertical-align:middle"></i></button>'
		+ '</div>');
});

!function(){
	var referenceMovementsDIV = $('#referenceMovementsGrid'),
		thRightAligned = '<th class="pagination-right" width="{{width}}">{{title}}</th>';

	window.referenceGrid = referenceMovementsDIV.simplePagingGrid({
		columnNames: ['Quantità', 'Descrizione', 'Prezzo', 'IVA', 'Importo', null],
		columnKeys: ['tagQuantity', 'description', 'price', 'vat', 'taxable', 'actions'],
		columnWidths: ['10%', '37%', '13%', '8%', '16%', '16%'],
		dataUrl: '/' + $('body').data('controller') + '/getmovements?uid=' + referenceMovementsDIV.data('uid'),
		minimumVisibleRows: 0,
		headerTemplates: [null, null, thRightAligned, thRightAligned, thRightAligned],
		cellTemplates: [null, null, '{{price}}', '{{vat}}', '{{taxable}}', '{{actions}}'],
		templates: {buttonBarTemplate: null},
		showInfoBar: false,
		tableClass: 'table table-condensed cursor-default',
		pageRenderedEvent: updateMovementsTable
	});
}();

function deleteReference(uid){
	if (confirm('Si vuole procedere con l\'eliminazione?')){
		location.href = '/' + $('body').data('controller') + '/delete?uid=' + uid;
	}
}

function movementDetail(uid){
	$('#nomeProdotto').addClass('hide');
	$('#selectContainer').addClass('hide');
	openDialog('editMovementDIV', uid, '/' + $('body').data('controller') + '/getmovementdata', null, movementUpdate, null, null);
}

function updateMovementsTable(data){
	var table = $('#referenceMovementsGrid table tbody'),
		fragment = $(document.createDocumentFragment());

	if (data.totalRows == 0)
		fragment.append('<tr><td colspan="6">Non è stato ancora aggiunto nessun movimento</td></tr>');

	var tax1amount = 0;
	var taxable = parseFloat(data.reference.taxable);

	var taxableFree = parseFloat(data.reference.taxableFree);

	var imponibile = number_format(taxable.toString(), 2, ',', '.');

	fragment.append('<tr><td colspan=4><div class="pagination-right">Tot. Importi</div></td><td><div class="pagination-right">' + imponibile + '</div></td><td></td></tr>');

	if (data.reference.tax1 && data.reference.tax1 != 0)
	{
		var tax1 = data.reference.tax1.split('---');
		tax1amount = number_format(data.reference.tax1amount.toString(), 2, ',', '.');
		fragment.append('<tr><td colspan=4><div class="pagination-right">' + tax1[0] + ' ' + tax1[1] + '% </div></td><td><div class="pagination-right">' + tax1amount + '</div></td><td></td></tr>');
	}

	var TotImponibile = number_format(data.reference.totImponibile.toString(), 2, ',', '.');

	fragment.append('<tr><td colspan=4><div class="pagination-right">Tot. Imponibile</div></td><td><div class="pagination-right">' + TotImponibile + '</div></td><td></td></tr>');

	var nonImponibile = number_format(taxableFree.toString(), 2, ',', '.');

	fragment.append('<tr><td colspan=4><div class="pagination-right">Tot. Non imponibile</div></td><td><div class="pagination-right">' + nonImponibile + '</div></td><td></td></tr>');

	var vat = parseFloat(data.reference.vat_amount);
	var extra = totTax1 = 0;

	fragment.append('<tr><td colspan=4><div class="pagination-right">IVA</div></td><td><div class="pagination-right">' + number_format(vat.toString(), 2, ',', '.') + '</div></td><td></td></tr>');
	if (data.reference.tax2 && data.reference.tax2 != 0) {
		var percentualeIrpef = '';
		if(data.reference.tax2 == 20){
			percentualeIrpef = data.reference.tax2/2*2;
		}else{
			percentualeIrpef = data.reference.tax2*2 + '%' + ' su 50';
		}
		fragment.append('<tr><td colspan=4><div class="pagination-right">Ritenuta d\'acconto IRPEF ' + percentualeIrpef + '% </div></td><td><div class="pagination-right">- ' + number_format(data.reference.tax2amount.toString(), 2, ',', '.') + '</div></td><td></td></tr>');
	}

	fragment.append('<tr><td colspan=4><div class="pagination-right"><b>Totale fattura</b></div></td><td><div class="pagination-right"><b>€ ' + number_format(data.reference.amount.toString(), 2, ',', '.') + '</b></div></td><td></td></tr>');
	table.append(fragment);
}

function toggleSentStatus(uniqueid)
{
	$.post('/' + $('body').data('controller') + '/togglesentstatus', {uniqueid:uniqueid, sentStatus:$('#sentStatusToggler').val()} , function(result){
		if (result !== null)
		{
			$('#sentStatusToggler').val(result);
			$('#sentStatus').html(result? '<i class="icon-ok"></i>' : '');
		}
	});
}

$(function()
{
	$('#sentStatusToggler').click(function(e)
	{
		e.stopPropagation();
	});

	$('#referenceTabs a:first').tab('show');
	
	$('#referenceMovementsGrid').on('click', '.modify-movement-btn', function(e){
		e.preventDefault();
		e.stopPropagation();
		var $target = $(e.target);
		var uniqueid = $target.data('uniqueid');
		movementDetail(uniqueid);
	});
});