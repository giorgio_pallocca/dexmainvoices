Handlebars.registerHelper('amount', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + number_format(this.amount, 2, ',', '.') + '</div>');
});

Handlebars.registerHelper('sent', function()
{
	return new Handlebars.SafeString('<div class="pagination-right">' + ((this.sent == 1)? 'Si' : 'No') + '</div>');
});

!function(){
	var referencesDIV = $('#referencesGrid'),
		body = $('body'),
		controller = body.data('controller');

	var columnNames = ['Data', 'Progr.', 'Cliente', 'Importo (€)'];

	if (controller == 'creditnotes') {
		columnNames.push('Inviata', '');
	}
	else {
		columnNames.push('Inviato', '');
	}

	window.referenceGrid = referencesDIV.simplePagingGrid({
		columnNames: columnNames,
		columnKeys: ['date', 'serialNumber', 'customer_name', 'amount', 'sent', 'privatenotes'],
		columnWidths: ['10%', '10%', '49%', '14%', '11%', '5%'],
		data: referencesDIV.data('list'),
		dataUrl: '/' + controller + '/list',
		sortable: [true, true, true, true, true, false],
		currentPage: referencesDIV.data('activepage'),
		initialSortColumn: referencesDIV.data('activesortfield'),
		sortOrder: referencesDIV.data('activesortdirection'),
		pageSize: body.data('pagesize'),
		minimumVisibleRows: body.data('pagesize'),
		headerTemplates: [null, null, null, PIUIVA.grids.thRightSortable, PIUIVA.grids.thRightSortable, null],
		cellTemplates:[null, null, null, '{{amount}}', '{{sent}}', '{{privatenotes}}'],
		rowClickHandler: function(row){location.href = '/' + controller + '/view?uid=' + row.uniqueid},
		searchFormName: 'referenceSearch',
		searchButtonName: 'referenceSearchButton',
		showAllButtonName: 'referenceShowAllButton',
		tableClass: 'table table-bordered table-hover',
		pageRenderedEvent: updateListDataPopover
	});
	referenceGrid.settings.data = null;
	referencesDIV.attr('data-list', null);
	referencesDIV.attr('data-activepage', null);
	referencesDIV.attr('data-activesortfield', null);
	referencesDIV.attr('data-activesortdirection', null);
}();

$(function(){
	createCalendar('startDate');
	createCalendar('endDate');

	var startDate = $('#startDate');
	var endDate = $('#endDate');

	if(startDate.val() == '')
	{
		endDate.prop('disabled', true);
		$('#substractDayEndButton').prop('disabled', true);
		$('#addDayEndButton').prop('disabled', true);
	}

	startDate.change(function()
	{
		endDate.prop('disabled', false);
		$('#substractDayEndButton').prop('disabled', false);
		$('#addDayEndButton').prop('disabled', false);
		updateMinEndDate('startDate','endDate');
	});

	$('#popoverPrivatenotes').on('show shown hide hidden', function(e){
		e.stopPropagation();
	});
});