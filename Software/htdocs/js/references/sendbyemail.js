function sendByEmail(uid, type)
{
	var params = $('#sendbyemailForm').serializeArray();
	params.push({'name':'toSend','value':true});
	params.push({'name':'type','value':type});

	var saveButton = $('#sendbyemailButton').prop('disabled', true);

	if(checkField('receiver', 'Inserire almeno un destinatario'))
	{
		var controller = '/' + $('body').data('controller');
		$.post(controller + '/print', params, function(data)
		{
			saveButton.prop('disabled', false);
			if (data == 0){
				location.href = controller + '/view?uid=' + uid;
			}
			else{
				$('#errorMsg').removeClass('hide');
			}
		}, 'json');
	}
	else
	{
		saveButton.prop('disabled', false);
	}
}

function closeMsg(){
	$('#errorMsg').addClass('hide');
}