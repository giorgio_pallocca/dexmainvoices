Handlebars.registerHelper('privatenotes', function()
{
	if (this.privatenotes) {
		return new Handlebars.SafeString('<div id="popoverPrivatenotes'+this.id+'" class="pagination-right"><i id="popoverPrivatenotesIcon'+this.id+'" class="icon-edit"></i></div>');
	}
});

function showPopover(elementId, content)
{
	$('#popoverPrivatenotes'+elementId).popover({
		content: content,
		placement: 'left',
		title: 'Note private',
		trigger: 'hover'
	});
	$('#popoverPrivatenotes'+elementId).popover('show');
}

function updateListData(data)
{
	$('#referencesGrid table tbody').append(
			'<tr><td colspan=3><div class="pagination-right"><b>Totali</b></div></td>'
		+	'<td><div class="pagination-right">€ ' + number_format(data.totals.amount.toString(), 2, ',', '.') + '</div></td>'
		+	'<td><div class="pagination-right text-error">€ ' + number_format(data.totals.unpaid_amount.toString(), 2, ',', '.') + '</div></td>'
		+	'<td><div class="pagination-right">€ ' + number_format(data.totals.vat_amount.toString(), 2, ',', '.') + '</div></td>'
		+	'<td colspan=3></td></tr>'
	);
	updateListDataPopover(data);
}

function updateListDataPopover(data)
{
	var rows = data.currentPage;

	for (var i=0; i<rows.length; i++) {
		var row = rows[i];

		if (row.privatenotes) {
			var data = {id: row.id, note: row.privatenotes};

			$('#popoverPrivatenotesIcon' + row.id).mouseover(function(data)
			{
				return function()
				{
					showPopover(data.id, data.note);
				};
			}(data));
		}
	}
}