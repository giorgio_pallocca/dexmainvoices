function movementUpdate(data){
	$('#nomeProdotto').val(data['product_name_by_id']);
	$('#nomeProdotto').attr('data-uniqueid', data['uniqueid_product']);
	$('#id_prodotto').val(data['product_id']);
	$('#amount').val(data['amount']);
	if(data['product_name_by_id'] != ""){
		$('#nomeProdotto').removeClass('hide');
		$('#selectContainer').removeClass('hide');
	}
	$('#deleteMovementBtn').removeClass('hide');
}

function movementClose(){
	$('#nomeProdotto').val('');
	$('#nomeProdotto').attr('data-uniqueid', null);
	$('#id_prodotto').attr('value',null);
	$('#amount').attr('value',null);
	$('#deleteMovementBtn').addClass('hide');
	$.each(['quantity', 'descriptionMovement', 'price', 'vat'], function(){
		disableErrorClass(this);
	});
}

function schedaProdotto(){
	var uid = $('#nomeProdotto').data('uniqueid');
	location.href='/products/view?uid='+uid;
}

function deleteMovement(uid){
	var closeModal = false;
	if (confirm('Eliminare il movimento dalla fattura?')){
		if (!uid){
			uid = $('#movementUniqueid').val();
			closeModal = true;
		}

		$.post('/' + $('body').data('controller') + '/deletemovement', {uid: uid, referenceUid: $('#referenceMovementsGrid').data('uid')}, function(data){
			if (data.errors == false){
				referenceGrid[0].refreshData();
				if (closeModal) $('#editMovementDIV').modal('hide');
			}
			else if (data.wrongTotal)
			{
				alert('Attenzione, il totale è inferiore alla somma dei pagamenti relativi alla fattura');
			}
		}, 'json');
	}
}

function saveMovement(saveAndContinue){
	var saveMovementBtn = $('#saveMovementBtn').prop('disabled', true);

	if (checkQuantity() && checkDescription('descriptionMovement') && checkPrice() && checkVat('vat')){
		$.post('/' + $('body').data('controller') + '/savemovement', $('#movementForm').serialize(), function(data){
			if (data.errors == false){
				referenceGrid[0].refreshData();
				if (saveAndContinue){
					$('#quantity').val(1).focus();
					$('#deleteMovementBtn').addClass('hide');
					$('#movementUniqueid').val('')
					$('#descriptionMovement').val('');
					$('#price').val(0);
				}
				else{
					$('#editMovementDIV').modal('hide');
				}
			}
			else if (data.wrongTotal)
			{
				alert('Attenzione, il totale è inferiore alla somma dei pagamenti relativi alla fattura');
			}
			saveMovementBtn.prop('disabled', false);
			console.log(data);
			if(data.quantityRes != undefined){
				if(data.quantityRes !='empty' && data.quantityRes <= 3){
					alert('Il prodotto inserito nella fattura sta per terminare in magazzino !!!');
				}
			}
		}, 'json');
	}
	else{
		saveMovementBtn.prop('disabled', false);
	}
}

function checkQuantity(){
	return checkField('quantity', 'Quantità obbligatoria');
}

function checkPrice(){
	return checkField('price', 'Prezzo obbligatorio') && isNumber('price', 'Il prezzo deve essere in formato numerico. Esempio: 100.00');
}

function selectFromProduct(description, amountsale, vat, id, amount_pro, uniqueid, customer_id){
	
	$.post('/' + $('body').data('controller') + '/customerproductprices', { customer_id: customer_id, id_product: id }, function(data){
		console.log(data);
		if(data !=-1){
			$('#price').val(data);
		}else{
			$('#price').val(amountsale);
		}
		}, 'json');
	
	console.log(customer_id);
	disableErrorClass('descriptionMovement');
	disableErrorClass('price');
	disableErrorClass('vat');
	$('#descriptionMovement').val(description);
	$('#vat').val(vat);
	$('#id_prodotto').val(id);
	$('#amount').val(amount_pro);
	$('#nomeProdotto').val(description);
	$('#nomeProdotto').attr('data-uniqueid', uniqueid);
	$('#nomeProdotto').removeClass('hide');
	$('#selectContainer').removeClass('hide');
}

function selectFromCustomertype(idlist){
	console.log($('#id_prodotto').val());
	console.log(idlist);
	
	var idprodotto = $('#id_prodotto').val();
	
	$.post('/' + $('body').data('controller') + '/productprices', { id_list: idlist, id_product: idprodotto }, function(data){
		console.log(data);
		if(data !=-1){
			$('#price').val(data);
		}
		}, 'json');
}

$(function(){
	var editMovementDIV = $('#editMovementDIV');

	editMovementDIV.on('hide', function(){
		$('#cancelButton').focus();
		disableErrorClass('insertUnit');
	});

	editMovementDIV.on('hidden', function(){
		closeDialog('movementForm', movementClose);
	});

	editMovementDIV.on('shown', function(){
		$('#quantity').focus();
		$('#vatContainer').removeClass('hide');
		isFree('vat');
	});
});