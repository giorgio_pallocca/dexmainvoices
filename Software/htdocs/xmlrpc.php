<?php
        // Definiamo dove si trova l'applicazione sul server e dove le Dexma Commons
        define('HOME_DIRECTORY', $_SERVER['HOME_DIRECTORY']);
        define("SITE_ROOT", dirname($_SERVER['DOCUMENT_ROOT']));
        define("DEXMA_COMMONS", chop(file_get_contents(SITE_ROOT."/../conf/dexmacommons.txt")));

        set_include_path(".:/usr/share/php:/usr/share/pear:".DEXMA_COMMONS);
        require_once(DEXMA_COMMONS.'/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();
        $loader->registerNamespace('App_');
        $loader->setFallbackAutoloader(true)->pushAutoloader(NULL, 'Smarty_' );

        $config = new Zend_Config_Ini(HOME_DIRECTORY."/conf/settings.ini",'development');
        $sRoot = str_replace("Software", "", SITE_ROOT);
        $config2 = new Zend_Config_Ini($sRoot."conf/settings.ini",'development');

        set_include_path(".:/usr/share/php:/usr/share/pear:".SITE_ROOT."/include:".DEXMA_COMMONS.":".SITE_ROOT."/include/Controllers");

        Zend_Registry::set('config', $config);
        Zend_Registry::set('config2', $config2);

        $loggingFile = $config2->paths->data."/logs/debug.log";
        $applogginFile = $config2->paths->data."/logs/application.log";
        $apploggingLevel = $config2->applogging->level;

        $logger = new Zend_Log(new Zend_Log_Writer_Stream($loggingFile));
        Zend_Registry::set('logger',$logger);

        $applogger = new Zend_Log(new Zend_Log_Writer_Stream($applogginFile));
        //Imposta il livello di log.
        $filter = new Zend_Log_Filter_Priority((int)$apploggingLevel);
        //Aggiunge il filtro al logger
        $applogger->addFilter($filter);

        Zend_Registry::set('applogger',$applogger);

        $params = array('host' => $config->db->params->hostname,
                'username' => $config->db->params->username,
                'password' => $config->db->params->password,
                'dbname' => $config->db->params->dbname,
                'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8;')
        );


        $db = Zend_Db::factory("pdo_mysql", $params);
        Zend_Registry::set('db',$db);

        $xmlRequest = file_get_contents("php://input");
        handleRequest($xmlRequest);


        function getUid() {          
                $uid = base_convert(microtime(true), 10, 16) . "-" . uniqid(rand());
                return strtoupper($uid);
        }

        function addContact_func($method_name, $params, $app_data) {
                global $db;

                $username = $params[1];
                $password = md5($params[2]);
                $userData = $params[3];
                $sql = "SELECT id FROM users WHERE email=? AND password=?";
                $id = $db->fetchOne($sql,array($username,$password));

                if (!empty($id)) {                        
                        //$sql = "SELECT email,name AS last_name,'' AS first_name FROM users ORDER by email";
                        //$results = $db->fetchAll($sql);
                        $uid = getUid();
                        $userData["uniqueid"]=$uid;

                        $sql = "SELECT id FROM customers WHERE email = ?";
                        $id = $db->fetchOne($sql,array($userData["email"]));

                        if (empty($id)) {
                                $db->insert('customers',$userData);
                                return "ok";
                        }
                        else {
                                return "ko";
                        }

//                        $results = $userData;
                        return $results;
                }
                else {
                        return null;
                }
        }

        function handleRequest($xmlRequest) {
                /* * This creates a server and sets a handle for the * server in the variable $xmlrpc_server */
                $xmlrpc_server = xmlrpc_server_create();

                /* * xmlrpc_server_register_method() registers a PHP * function as an XML-RPC method. It takes three * parameters: * The first is the handle of a server created with * xmlrpc_server_create(), the second is the name to * register the server under (this is what needs to * be in the <methodName> of a request for this * method), and the third is the name of the PHP * function to register. */
                xmlrpc_server_register_method($xmlrpc_server, "addContact", "addContact_func");

                /* * The xmlrpc_server_call_method() sends a request to * the server and returns the response XML. In this case, * it sends the raw post data we got before. It requires * 3 arguments: * The first is the handle of a server created with * xmlrpc_server_create(), the second is a string containing * an XML-RPC request, and the third is for application data. * Whatever is passed into the third parameter of this function * is passed as the third paramater of the PHP function that the * request is asking for. */

                $response = xmlrpc_server_call_method($xmlrpc_server, $xmlRequest, '');

                //$response = str_replace("iso-8859-1","UTF-8",$response);
                // Now we print the response for the client to read. 
                print $response;


                /* * This method frees the resources for the server specified * It takes one argument, a handle of a server created with * xmlrpc_server_create(). */

                xmlrpc_server_destroy($xmlrpc_server);
        }



?>
                                                                      