<?php
abstract class CustomControllerAction extends DexmaControllerAction
{
	protected static $ACCOUNT = 'account';
	const DEFAULT_PAGE_SIZE = 8;
	const UPLOAD_FOLDER = '/uploads/';

	function init()
	{
		setlocale(LC_ALL, 'it_IT.UTF-8');
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->db->query('SET lc_time_names="it_IT"');

		$config = Zend_Registry::get('config');
		$this->applogger = Zend_Log::factory($config->applogger);

		$this->nameSpaceName = $config->session->namespace; // per clearSession() nel DexmaControllerAction
		$this->nameSpace = new Zend_Session_Namespace($this->nameSpaceName);

		require_once('DexmaCommonsNew.php');
		$this->dexmaCommons = new DexmaCommons($this->db, $this->applogger);
	}

	function preDispatch()
	{
		$request = $this->getRequest();

		if (!$request->isXmlHttpRequest())
		{
			$auth = Zend_Auth::getInstance();

			if ($auth->hasIdentity())
			{
				$this->view->loggedUser = $auth->getIdentity();
				$this->view->account = parent::getAttribute(self::$ACCOUNT);
			}

			$this->view->request = array(
				'module' => $request->getModuleName(),
				'controller' => $request->getControllerName(),
				'action' => $request->getActionName()
			);

			$this->view->appVersion = Zend_Registry::get('config')->parameters->version;

			$language = parent::getParameter('language');
			$selectWords = "SELECT lc, description_$language AS description
							FROM languages";
			$results = $this->db->fetchAll($selectWords);

			$words = array();
			foreach($results as $item)
			{
				$words[$item['lc']] = $item['description'];
			}
			$this->view->words = $words;
		}
		else $this->_helper->viewRenderer->setNoRender();
	}

	protected function index($activePageKey, $activeSortFieldKey, $sortFieldDefault, $activeSortDirectionKey, $sortDirectionDefault,
							 $pageSize = self::DEFAULT_PAGE_SIZE, $activePageTag='activePage', $activeSortFieldTag='activeSortField',
							 $activeSortDirectionTag='activeSortDirection', $pageSizeTag='pageSize')
	{
		if ($this->getParameter('lists_rows')) {
			$pageSize = $this->getParameter('lists_rows');
		}
		$activePage = $this->getAttribute($activePageKey);
		if (empty($activePage)) {
			$activePage = 0;
		}
		$this->view->$activePageTag = $activePage;

		$activeSortField = $this->getAttribute($activeSortFieldKey);
		if (empty($activeSortField)) {
			$activeSortField = $sortFieldDefault;
		}
		$this->view->$activeSortFieldTag = $activeSortField;

		$activeSortDirection = $this->getAttribute($activeSortDirectionKey);
		if (empty($activeSortDirection)) {
			$activeSortDirection = $sortDirectionDefault;
		}
		$this->view->$activeSortDirectionTag = $activeSortDirection;

		$this->view->$pageSizeTag = $pageSize;

		return array(
			'page' => $activePage,
			'pageSize' => $pageSize,
			'sortColumn' => $activeSortField,
			'sortOrder' => $activeSortDirection
		);
	}

	protected function getInvoicesYears() {
		$years = array();
		$selectYears = 'SELECT DISTINCT(YEAR(date)) AS year
						FROM invoices';
		$results = $this->db->fetchAll($selectYears);
		foreach($results as $row)
		{
			array_push($years, $row['year']);
		}
		return $years;
	}

	protected function getProductsList() {
		return $this->db->fetchAll('SELECT id, description, amount, vat, quantity, product_name, amountsale, uniqueid FROM products');
	}
	
	protected function getcustomertype() {
		return $this->db->fetchAll('SELECT id, customer_type FROM list');
	}
	
	protected function getlistPrice() {
		return $this->db->fetchAll('SELECT id, id_product, id_list, amount FROM product_prices');
	}

	protected function getCustomers() {
		return $this->db->fetchAll('SELECT id, company FROM customers WHERE type <> 1 ORDER BY company ASC');
	}
	
	protected function initList($activePageKey, $activeSortFieldKey, $sortFieldDefault, $activeSortDirectionKey, $sortDirectionDefault,
			$pageSize = self::DEFAULT_PAGE_SIZE, $activePageTag='activePage', $activeSortFieldTag='activeSortField',
			$activeSortDirectionTag='activeSortDirection', $pageSizeTag='pageSize')
	{
		$activePage = $this->getAttribute($activePageKey);
		if (empty($activePage))
			$activePage = 0;
	
		$this->view->$activePageTag = $activePage;
	
		$activeSortField = $this->getAttribute($activeSortFieldKey);
		if (empty($activeSortField))
			$activeSortField = $sortFieldDefault;
	
		$this->view->$activeSortFieldTag = $activeSortField;
	
		$activeSortDirection = $this->getAttribute($activeSortDirectionKey);
		if (empty($activeSortDirection))
			$activeSortDirection = $sortDirectionDefault;
	
		$this->view->$activeSortDirectionTag = $activeSortDirection;
		$this->view->$pageSizeTag = $pageSize;
	
		return array(
				'page' => $activePage,
				'pageSize' => $pageSize,
				'sortColumn' => $activeSortField,
				'sortOrder' => $activeSortDirection,
		);
	}
}