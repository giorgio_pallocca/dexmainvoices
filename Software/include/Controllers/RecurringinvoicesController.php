<?php
class RecurringinvoicesController extends ReferencesController
{
	function init()
	{
		parent::init();
		parent::setRedirect('/recurringinvoices/recurringinvoices');
		$this->tableName = 'recurringinvoices';
		$this->tableMovements = 'movementsrecurringinvoices';
		$this->columnId = 'recurringinvoice_id';
		$this->referredTo = 0;
	}

	function recurringinvoicesAction() {
		$searchParams['searchStatus'] = parent::getAttribute($this->status);
		$this->reference($searchParams);
	}

	protected function getSelectForList() {
		return 'SELECT DATE_FORMAT(ref.date, "%d/%m/%Y") AS date, ref.customer_name, ref.frequency, ref.amount, ref.uniqueid, ref.status,
				DATE_FORMAT(ref.last_recurred, "%d/%m/%Y") AS last_recurred, DATE_FORMAT(ref.next_recurring, "%d/%m/%Y") AS next_recurring, ref.code,
				DATE_FORMAT(ref.endDate, "%d/%m/%Y") AS endDate';
	}

	function updateAction()
	{
		$this->update();
	}

	function viewAction() {
		$this->view(true);
	}

	protected function update($invoiceON = false, $ddtON = false)
	{
		$this->view->customers = parent::getCustomers();
		$this->view->additionalnotes = $this->getAdditionalNotes();

		$defaultPayment = parent::getParameter('company_default_payment');
		if (!empty($_REQUEST['uid']))
		{
			$uid = $_REQUEST['uid'];

			$referenceSelect = "SELECT uniqueid, DATE_FORMAT(date, '%d/%m/%Y') AS date, frequency, customer_id, payments_term,
								DATE_FORMAT(expireDate, '%d/%m/%Y') AS expireDate, notes, private, payments_term_description, amount, tax1, tax2, status, code
								FROM $this->tableName
								WHERE uniqueid = ?";
			$reference = $this->db->fetchRow($referenceSelect, $uid);
		}
		else
		{
			$reference = array(
				'date' => date('d/m/Y')
			);

			$reference['payments_term_description'] = $defaultPayment;

			$reference['tax1'] = parent::getParameter('company_tax1');
			$reference['tax2'] = parent::getParameter('company_tax2');
			$reference['notes'] = $this->getDefaultNote();
		}
		$this->view->reference = $reference;

		$this->view->defaultTax1 = parent::getParameter('company_tax1');
		$this->view->defaultTax2 = parent::getParameter('company_tax2');
	}

	function saveAction()
	{
		$reference = $_POST;

		if (isset($_POST['status'])){
			$reference['status'] = 'activated';
		}
		else $reference['status'] = 'draft';

		$startDate = date('Y-m-d', strtotime(str_replace('/', '-', $reference['date'])));

		$frequency = str_replace('_', ' ', $reference['frequency']);
		$payments_term = $reference['payments_term'];
		$reference['next_recurring'] = date('Y-m-d', strtotime("+ $frequency" , strtotime($startDate)));
// 		$reference['expireDate'] = date('Y-m-d', strtotime("+ $payments_term day" , strtotime($startDate)));

		$this->save($reference, true);
	}

	protected function getReference($uid)
	{
		$reference="SELECT id, uniqueid, DATE_FORMAT(date, '%d %M %Y') AS date, DATE_FORMAT(expireDate, '%d %M %Y') AS expireDate, payments_term_description,
					payments_term, amount, buyer, seller, customer_id, notes, tax1, tax2, tax1amount, tax2amount, theme_id, taxable, taxableFree, vat_amount,
					taxable + tax1amount AS totImponibile
					FROM $this->tableName
					WHERE uniqueid = ?";
		return $this->db->fetchRow($reference, $uid);
	}

	protected function delete($uniqueid, $invoiceON = false)
	{
		$reference="SELECT id
					FROM $this->tableName
					WHERE uniqueid = ?";
		$reference = $this->db->fetchRow($reference, $uniqueid);

		try {
			$this->db->beginTransaction();
			$this->db->delete($this->tableName, array('id = ?' => $reference['id']));
			$this->db->delete($this->tableMovements, array("$this->columnId = ?" => $reference['id']));
			$this->db->delete('productmovements', array("uniqueid_invoices = ?" => $uniqueid));
			$this->db->commit();
		}
		catch (Exception $e) {
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
		}
	}

	protected function getSelectForMovementData() {
		return parent::getSelectForMovementData()
		. ', free, product_id';
	}
}