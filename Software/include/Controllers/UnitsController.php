<?php
class UnitsController extends CustomControllerAction
{
	function init() {
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('description');
		parent::setDirectionDefault('ASC');
		parent::setSearchKey('searchUnit');
		parent::setRedirect('/units/units');
	}

	public function unitsAction() {
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$search = array('searchUnit' => parent::getAttribute(parent::getSearchKey()));
		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getList($params) {
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		if(!empty($params['searchUnit']))
		{
			$searchUnit = $params['searchUnit'];
			parent::setAttribute(parent::getSearchKey(), $searchUnit);
			$whereCondition .= 'AND (u.description LIKE ? OR u.plural LIKE ?) ';
			array_push($data, "%$searchUnit%", "%$searchUnit%");
		}
		else parent::setAttribute(parent::getSearchKey(), null);

		$sql = "SELECT COUNT(u.id)
				FROM units u
		$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);
		$sql = "SELECT u.id, u.description, u.plural, u.uniqueid
				FROM units u
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function updateAction()
	{
		if(!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$selectUnit = 'SELECT u.id, u.description, u.plural, u.uniqueid
						   FROM units u
						   WHERE u.uniqueid = ?';
			$unit = $this->db->fetchRow($selectUnit, $uniqueid);
			$this->view->unit = array_map('htmlspecialchars', $unit);
		}
	}

	public function saveAction()
	{
		$unit = $_POST;

		if($_GET['remote'] == true)
		{
			$result = false;
			$id = $this->db->fetchOne('SELECT id FROM units WHERE description = ?', $_REQUEST['description']);

			if(empty($id))
			{
				try
				{
					$uniqueid = $this->db->save('units', $unit);
					$result = $this->db->fetchRow('SELECT id, description FROM units WHERE uniqueid = ?', $uniqueid);
				}
				catch(Exception $e)
				{
					$result = false;
				}
			}
			echo json_encode($result);
			$this->_helper->viewRenderer->setNoRender();
		}
		else
		{
			$this->db->save('units', $unit);
			$this->_redirect(parent::getRedirect());
		}
	}

	public function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$where['uniqueid = ?'] = $_REQUEST['uid'];

			$uniqueid = $_REQUEST['uid'];

			$selectId = $this->db->fetchOne("SELECT id FROM units WHERE uniqueid = '$uniqueid'");

			try
			{
				$this->db->beginTransaction();

				$this->db->query('UPDATE movements SET unit_id = null WHERE unit_id = ?', $selectId);
				$this->db->query('UPDATE movementsquotations SET unit_id = null WHERE unit_id = ?', $selectId);
				$this->db->query('UPDATE movementscreditnotes SET unit_id = null WHERE unit_id = ?', $selectId);
				$this->db->query('UPDATE movementstransportdocuments SET unit_id = null WHERE unit_id = ?', $selectId);

				$this->db->delete('units', $where);
				$this->db->commit();
			}
			catch(Exception $e)
			{
				$this->db->rollBack();
				$this->applogger->info($e->getMessage());
			}
			parent::updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}

	public function checkdataAction()
	{
		$key = $_REQUEST['key'];
		$value = $_REQUEST['value'];
		$uid = $_REQUEST['uid'];

		$sql = "SELECT id FROM units WHERE $key = ?";
		$data = array($value);
		if (!empty($uid))
		{
			$sql .= ' AND uniqueid <> ?';
			$data[] = $uid;
		}

		$result = $this->db->fetchOne($sql, $data);
		echo $result != NULL;
	}
}