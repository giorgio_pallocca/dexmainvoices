<?php
class AdditionalnotesController extends CustomControllerAction
{
	function init()
	{
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('title');
		parent::setDirectionDefault('ASC');
		parent::setRedirect('/additionalnotes/additionalnotes');

		$request = $this->getRequest();
		$prefix = $request->getModuleName() . '-' . $request->getControllerName();
		$this->searchNote = $prefix . '-note';
	}

	public function additionalnotesAction() {
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$search = array('searchNote' => parent::getAttribute($this->searchNote));
		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getList($params)
	{
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		if(!empty($params['searchNote']))
		{
			$searchNote = $params['searchNote'];
			parent::setAttribute($this->searchNote, $searchNote);
			$whereCondition .= 'AND (a.title LIKE ? OR a.description LIKE ? OR a.referredto LIKE ?) ';
			array_push($data, "%$searchNote%", "%$searchNote%", "%$searchNote%");
		}
		else parent::setAttribute($this->searchNote, null);

		$sql = "SELECT COUNT(id)
				FROM additionalnotes a
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);
		$sql = "SELECT a.id, a.title, a.description, a.referredto, a.uniqueid
				FROM additionalnotes a
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function updateAction()
	{
		if(!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$selectNote =  'SELECT a.id, a.title, a.description, a.referredto, a.isdefault, a.uniqueid
							FROM additionalnotes a
							WHERE a.uniqueid = ?';
			$note = $this->db->fetchRow($selectNote, $uniqueid);
			$this->view->note = array_map('htmlspecialchars', $note);
		}
	}

	public function saveAction()
	{
		$note = $_POST;
		if (isset($_POST['isdefault']))
		{
			$isDefaultSelect = 'UPDATE additionalnotes
								SET isdefault = 0
								WHERE referredto = ? AND uniqueid <> ?';
			$isDefaultUpdate = $this->db->query($isDefaultSelect, array($note['referredto'], $note['uniqueid']));

			$note['isdefault'] = 1;
		}
		$this->db->save('additionalnotes', $note);
		$this->_redirect(parent::getRedirect());
	}

	public function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$where['uniqueid = ?'] = $_REQUEST['uid'];
			$this->db->delete('additionalnotes', $where);
			parent::updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}
}