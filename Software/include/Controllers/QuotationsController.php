<?php
class QuotationsController extends ReferencesController
{
	function init()
	{
		parent::init();
		parent::setRedirect('/quotations/quotations');
		$this->folderPath = 'quotations/';
		$this->textMessage = 'send_quotation_text';
		$this->tableName = 'quotations';
		$this->tableMovements = 'movementsquotations';
		$this->columnId = 'quotation_id';
		$this->referredTo = 1;
		$this->titleReference = 'Preventivo numero_';
		$this->destinationArray = array(
			'tableName' => 'invoices',
			'tableMovements' => 'movements',
			'columnName' => 'invoice_id'
		);
		$this->destinationArrayDDT = array(
			'tableName' => 'transportdocuments',
			'tableMovements' => 'movementstransportdocuments',
			'columnName' => 'transportdocument_id'
		);
	}

	function quotationsAction() {
		$this->reference();
	}

    protected function getSelectForMovementData() {
		return parent::getSelectForMovementData()
		. ', free, product_id';
	}
}