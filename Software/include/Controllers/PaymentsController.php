<?php
class PaymentsController extends CustomControllerAction
{
	private $startDateSearch = 'startDateSearch',
			$endDateSearch = 'endDateSearch',
			$statusSearch = 'statusSearch',
			$typologySearch = 'tipologySearch';

	public function deleteAction()
	{
		$deleteResult = null;
		try
		{
			if(!empty($_REQUEST['uniqueid']))
			{
				$this->db->beginTransaction();

				$this->getPayment()->removeExplanations($_REQUEST['uniqueid']);

				$where['uniqueid = ?'] = $_REQUEST['uniqueid'];
				$this->db->delete('payments', $where);

				$this->applogger->debug('ELIMINATA RIGA. RIFERIMENTO TABELLA: payments UNIQUEID: ' . $_REQUEST['uniqueid']);

				$this->db->commit();

				parent::updateActivePageAfterDelete();
				$deleteResult = parent::getAttribute(parent::getActivePageKey());
			}
		}
		catch(Exception $e)
		{
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
			$deleteResult = null;
		}

		echo $deleteResult;
		$this->_helper->viewRenderer->setNoRender();
	}

	public function getfeeAction()
	{
		echo json_encode($this->getPayment()->getFee($_REQUEST['feeUniqueid'], $_REQUEST['paymentUniqueid']));
		$this->_helper->viewRenderer->setNoRender();
	}

	public function getrowdataAction()
	{
		$result = array();

		$paymentId = '';
		$paymentType = '1';

		if(!empty($_REQUEST['uId']))
		{
			$selectPayment = "	SELECT p.id, p.description,
									CASE WHEN(p.type = 1) THEN -p.amount ELSE p.amount END AS amount,
									p.type AS paymentType,
									p.typology,
									DATE_FORMAT(p.date, '%d/%m/%Y') AS date, p.uniqueid
								FROM payments p
								WHERE p.uniqueid = ?";
			$payment = $this->db->fetchRow($selectPayment, array($_REQUEST['uId']));

			$payment = $this->getPayment()->getPaymentDetail($payment, $payment['id']);

			$paymentId = $payment['id'];
			$paymentType = $payment['paymentType'];

			unset($payment['id']);
			unset($payment['paymentType']);
		}
		else
		{
			$payment = array();
			$payment['date'] = date('d/m/Y');
			$payment['amount'] = '0.00';

			$paymentType = $_REQUEST['paymentType'];
		}

		$result['paymentType'] = $paymentType;
		$result['form'] = $payment;
		$result['list'] = $this->getPayment()->getFeesList($paymentId, $paymentType, null, null);
		echo json_encode($result);
		$this->_helper->viewRenderer->setNoRender();
	}

	private function getPayment()
	{
		return new PaymentsManager_Payment('serialNumber', 'customer_name', 'amount', 'paid_amount',
										   null, 'is_external', null, 'invoices', 
										   'payments', 'amount', 'invoice_id', 'quick_explanation_id',
										   'payment_id', 'explanations',
										   'description', 'type', 'quick_explanations',
										   $this->db, $this->applogger);
	}

	public function init()
	{
		parent::init();
		parent::setListSessionKeys('payments');
		parent::setFieldDefault('dateLabel');
		parent::setDirectionDefault('DESC');
		parent::setSearchKey('paymentsSearch');
		parent::setRedirect('/payments/payments');
	}

	private function getList($params, $printList = false)
	{
		$fromCondition = 'FROM payments p LEFT JOIN explanations e ON e.payment_id = p.id LEFT JOIN invoices i ON i.id = e.invoice_id ';
		$whereCondition = 'WHERE 1=1 ';
		$groupbyCondition = 'GROUP BY p.id ';
		$havingCondition = 'HAVING 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		$startDate = $params['startDateSearch'];
		parent::setAttribute($this->startDateSearch, $startDate);

		if(!empty($startDate))
		{
			$startDate = parent::getDateInSQLFormat($startDate, '/');

			if(!empty($params['endDateSearch']))
			{
				$endDate = $params['endDateSearch'];
				parent::setAttribute($this->endDateSearch, $endDate);
				$endDate = parent::getDateInSQLFormat($endDate, '/');
				$whereCondition .= 'AND (p.date BETWEEN ? AND ?) ';
				array_push($data, $startDate, $endDate);
			}
			else
			{
				parent::setAttribute($this->endDateSearch, null);
				$whereCondition .= 'AND (p.date = ?) ';
				array_push($data, $startDate);
			}
		}
		else
		{
			parent::setAttribute($this->endDateSearch, null);
		}

		if($params['paymentSearchField'] != null && $params['paymentSearchField'] != '')
		{
			$paymentSearchField = $params['paymentSearchField'];
			parent::setAttribute(parent::getSearchKey(), $paymentSearchField);
			$havingCondition .= 'AND (description LIKE ? OR CAST(p.amount AS nchar(255)) = ? OR CAST(rest AS nchar(255)) = ?) ';
			array_push($data, "%$paymentSearchField%", $paymentSearchField, $paymentSearchField);
		}
		else
		{
			parent::setAttribute(parent::getSearchKey(), null);
		}

		if(isset($params['statusSearch']))
		{
			parent::setAttribute($this->statusSearch, true);
			$havingCondition .= "AND status = 'Open' ";
		}
		else
		{
			parent::setAttribute($this->statusSearch, false);
		}

		$typology = $params['typologySearch'];

		parent::setAttribute($this->typologySearch, $typology);
		if($typology != -1)
		{
			$whereCondition .= 'AND (p.typology = ?) ';
			array_push($data, $typology);
		}

		$sql = "SELECT p.description, p.amount, p.date,
				DATE_FORMAT(p.date, '%d/%m/%Y') AS dateLabel,
				p.uniqueid,
				CASE WHEN(p.amount < 0) THEN -p.amount ELSE p.amount END - CASE WHEN SUM(e.amount) IS NOT NULL THEN SUM(e.amount) ELSE 0 END AS rest,
				CASE WHEN((CASE WHEN(p.amount < 0) THEN -p.amount ELSE p.amount END - (CASE WHEN SUM(e.amount) IS NOT NULL THEN SUM(e.amount) ELSE 0 END)) <> 0) THEN 'Open' ELSE 'Close' END AS status
				$fromCondition
				$whereCondition
				$groupbyCondition
				$havingCondition";

		$totalRows = count($this->db->fetchAll($sql, $data));
		/** PARAMETRI PASSATI DALLA GRIGLIA **/
		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$prefix = 'p.';

		if($sortField == 'rest' || $sortField == 'status')
		{
			$prefix = '';
		}
		else if($sortField == 'dateLabel')
		{
			$sortField = 'date';
		}

		$sql = "SELECT p.id, p.amount, p.date, p.type AS paymentType,
				DATE_FORMAT(p.date, '%d/%m/%Y') AS dateLabel,
				CASE WHEN p.typology = 0 THEN 'B' ELSE 'C' END AS typology,
				p.uniqueid,
				CASE WHEN(p.amount < 0) THEN -p.amount ELSE p.amount END - CASE WHEN SUM(e.amount) IS NOT NULL THEN SUM(e.amount) ELSE 0 END AS rest,
				CASE WHEN((CASE WHEN(p.amount < 0) THEN -p.amount ELSE p.amount END - (CASE WHEN SUM(e.amount) IS NOT NULL THEN SUM(e.amount) ELSE 0 END)) <> 0) THEN 'Open' ELSE 'Close' END AS status,
				CASE WHEN e.invoice_id > 0 THEN CONCAT(p.description, ' - associato alla fattura n° ', i.serialNumber) ELSE p.description END AS description
				$fromCondition
				$whereCondition
				$groupbyCondition
				$havingCondition
				ORDER BY $prefix$sortField $sortDir";

		if(!$printList) {
			$sql .= ' LIMIT ' . $page * $pageSize . ',' . $pageSize;
		}

		$payments = $this->db->fetchAll($sql, $data);

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $payments;

		$sumAmount = 0;
		foreach($payments as $p) {
			$sumAmount = $sumAmount + $p['amount'];
		}

		$selectSumRest = "SELECT IFNULL(SUM(rest), 0) FROM
							($sql) AS total";
		$sumRest = $this->db->fetchOne($selectSumRest, $data);

		$results['totals']['sumAmount'] = $sumAmount;
		$results['totals']['sumRest'] = $sumRest;

		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function paymentsAction()
	{
		$this->view->products = parent::getProductsList();
		$this->view->customers = parent::getCustomers();
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
				parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$startDateReset = date('01/01/Y');
		$endDateReset = date('31/12/Y');

		$search['startDateSearch'] = parent::getAttribute($this->startDateSearch);
		if ($search['startDateSearch'] == NULL){
			$search['startDateSearch'] = $startDateReset;
		}
		$search['endDateSearch'] = parent::getAttribute($this->endDateSearch);
		if ($search['endDateSearch'] == NULL){
			$search['endDateSearch'] = $endDateReset;
		}

		$search['paymentSearchField'] = parent::getAttribute(parent::getSearchKey());
		$search['startDateReset'] = $startDateReset;
		$search['endDateReset'] = $endDateReset;

		if (parent::getAttribute($this->statusSearch) == true) {
			$search['statusSearch'] = true;
		}
		$this->view->search = $search;

		$currentTypology = parent::getAttribute($this->typologySearch);
		if(empty($currentTypology) && $currentTypology !== '0')
		{
			$currentTypology = -1;
		}
		else if($currentTypology != -1)
		{
			$this->view->currentTypology = $currentTypology;
		}
		$search['typologySearch'] = $currentTypology;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	public function saveAction()
	{
		$saveResult = true;
		try
		{
			$this->db->beginTransaction();

			$payment = $_POST;
			if(empty($payment['uniqueid']))
			{
				$paymentType = $payment['type'];
			}
			else
			{
				$selectPayment = 'SELECT type
								  FROM payments
								  WHERE uniqueid = ?';
				$paymentType = $this->db->fetchOne($selectPayment, array($payment['uniqueid']));
				$payment['type'] = $paymentType ;
			}

			unset($payment['fees']);
			$paymentDetailField = $payment['paymentDetail'];
			unset($payment['paymentDetail']);
			$quickDetailField = $payment['quickDetail'];
			unset($payment['quickDetail']);

			$payment['date'] = parent::getDateInSQLFormat($payment['date'], '/');

			if($paymentType == 1)
			{
				$payment['amount'] = -$payment['amount'];
			}

			$paymentUniqueid = $this->db->save('payments', $payment);

			$this->getPayment()->saveExplanations($paymentUniqueid, $paymentDetailField, $quickDetailField);

			$this->db->commit();
		}
		catch(Exception $e)
		{
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
			$saveResult = null;
		}

		echo json_encode($saveResult);
		$this->_helper->viewRenderer->setNoRender();
	}

	private function fixTag($tag, $acceptSpaces = false)
	{
		$tag = strtoupper($tag);
		$this->OFXContent = preg_replace('/<'.$tag.'>([\w0-9\.\:\/\(\)\'\-_\+\,'.(($acceptSpaces)? ' ': '').'])+/i', '$0</'.$tag.'>', $this->OFXContent);
	}

	public function importmovementsAction()
	{
		if (isset($_REQUEST['errorMsg'])) {
			$this->view->errorMsg = 'errorMsg';
		}
		else if (isset($_REQUEST['confirmMsg'])) {
			$this->view->confirmMsg = 'confirmMsg';
			$this->view->countInsert = $_GET['countInsert'];
			$this->view->countSkipped = $_GET['countSkipped'];
		}
	}

	public function loadfromfileAction()
	{
		$OFXFile = $_FILES['bankMovements']['tmp_name'];
		$extension = substr($_FILES['bankMovements']['name'], -4);

		if (is_uploaded_file($OFXFile) && (strcasecmp($extension, '.ofx') == 0) || strcasecmp($extension, '.ofc') == 0) {
			$info = pathinfo($OFXFile);
			if(strcasecmp($extension, '.ofx') == 0) {
				$result = $this->loadFromStringOfx(file_get_contents($OFXFile));
			}
			else if(strcasecmp($extension, '.ofc') == 0) {
				$result = $this->loadFromStringOfc(file_get_contents($OFXFile));
			}

			$this->_redirect('/payments/importmovements?confirmMsg=true&countInsert='.$result['countInsert'].'&countSkipped='.$result['countSkipped']);
		}
		else $this->_redirect('/payments/importmovements?errorMsg=true');
	}

	private function callfixTag()
	{
		$this->fixTag('SEVERITY');
		$this->fixTag('code');
		$this->fixTag('LANGUAGE');
		$this->fixTag('DTSERVER');
		$this->fixTag('ACCTTYPE');
		$this->fixTag('ACCTID');
		$this->fixTag('BANKID');
		$this->fixTag('MEMO', true);
		$this->fixTag('FITID');
		$this->fixTag('TRNAMT');
		$this->fixTag('DTAVAIL', true);
		$this->fixTag('DTPOSTED', true);
		$this->fixTag('TRNTYPE');
		$this->fixTag('DTEND');
		$this->fixTag('DTSTART');
		$this->fixTag('DTASOF');
		$this->fixTag('BALAMT');
		$this->fixTag('CURDEF');
		$this->fixTag('TRNUID');
		$this->fixTag('DTD');
		$this->fixTag('CPAGE');
		$this->fixTag('LEDGER');
	}

	private function loadFromString($OFXContent, $string) {
		$this->OFXContent = $OFXContent;
// 		$this->conf = $this->OFXContent[0]; //l'HEADER del file
		$this->OFXContent = explode($string, $this->OFXContent);//tutto il resto del contenuto del file
		$this->OFXContent = $string . $this->OFXContent[1];

		$this->callfixTag();

		return simplexml_load_string("<?xml version='1.0'?>" . $this->OFXContent);
	}

	private function selectFitId() {
		return 'SELECT fit_id
				FROM payments
				WHERE fit_id = ? AND amount = ?';
	}

	public function loadFromStringOfc($OFXContent)
	{
		$countInsert = 0;
		$countSkipped = 0;
		$result = array();

		$this->ofx = $this->loadFromString($OFXContent, '<OFC>');

		try
		{
			$this->db->beginTransaction();
			foreach($this->ofx->ACCTSTMT->STMTRS->STMTTRN as $mov)
			{
				$resultFitId = $this->db->fetchOne($this->selectFitId(), array($mov->GENTRN->FITID, str_replace(',', '.', $mov->GENTRN->TRNAMT)));

				if ($resultFitId == null) {
					$payments = array(
						'date' => date('Y-m-d H:i:s', strtotime($mov->GENTRN->DTPOSTED)),
						'description' => (string) $mov->GENTRN->MEMO,
						'amount' =>  str_replace(',', '.', $mov->GENTRN->TRNAMT),
						'fit_id' => (string) $mov->GENTRN->FITID,
					);
					if ($mov->GENTRN->TRNAMT < 0) {
						$payments['type'] = 1;
					}
					$this->db->save('payments', $payments);
					$countInsert ++;
				}
				else {
					$countSkipped ++;
				}
			}
			$result['countInsert'] = $countInsert;
			$result['countSkipped'] = $countSkipped;
			$this->db->commit();
			return $result;
		}
		catch(Exception $e)
		{
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
		}
	}

	public function loadFromStringOfx($OFXContent)
	{
		$countInsert = 0;
		$countSkipped = 0;
		$result = array();

		$this->ofx = $this->loadFromString($OFXContent, '<OFX>');

		try
		{
			$this->db->beginTransaction();

			foreach($this->ofx->BANKMSGSRSV1->STMTTRNRS->STMTRS->BANKTRANLIST->STMTTRN as $mov)
			{
				$resultFitId = $this->db->fetchOne($this->selectFitId(), array(($mov->FITID), str_replace(',', '.', $mov->TRNAMT)));

				if ($resultFitId == null) {
					$payments = array(
						'date' => date('Y-m-d H:i:s', strtotime($mov->DTAVAIL)),
						'description' => (string) $mov->MEMO,
						'amount' =>  str_replace(',', '.', $mov->TRNAMT),
						'fit_id' => (string) $mov->FITID,
					);
					if ($mov->TRNAMT < 0) {
						$payments['type'] = 1;
					}
					$this->db->save('payments', $payments);
					$countInsert ++;
				}
				else {
					$countSkipped ++;
				}
			}
			$result['countInsert'] = $countInsert;
			$result['countSkipped'] = $countSkipped;
			$this->db->commit();
			return $result;
		}
		catch(Exception $e)
		{
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
		}
	}

	public function printlistAction()
	{
		$results = array();
		$parameters = Zend_Registry::get('config')->parameters;
		parent::initDomPdf();
		$search = $_REQUEST;
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());
		$params = array_merge($listParams, $search);

		$payments = $this->getList($params, true);

		foreach ($payments['currentPage'] as $payment) {
			$selectExplanations = '
				SELECT e.id, i.serialNumber AS invoice_id, e.payment_id, e.amount, (SELECT qe.description FROM quick_explanations qe WHERE qe.id = e.quick_explanation_id) as description
				FROM explanations e INNER JOIN invoices i ON i.id = e.invoice_id
				WHERE e.payment_id = ?';
			$explanations = $this->db->fetchAll($selectExplanations, $payment['id']);
			$payment['explanations'] = $explanations;
			array_push($results, $payment);
		}
		$this->view->payments = $results;

		$dompdf = new DOMPDF();
		$dompdf->load_html($this->view->render('payments/printlist.tpl'));
		$dompdf->render();

		$pdfFilename = 'Pagamenti.pdf';
		$pdfPath = $parameters->uploads_path . $pdfFilename;
		$dompdf->stream($pdfFilename, array('Attachment' => 1));
		$this->_helper->viewRenderer->setNoRender();
	}
}