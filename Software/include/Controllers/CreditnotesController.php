<?php
class CreditnotesController extends ReferencesController
{
	function init()
	{
		parent::init();
		parent::setRedirect('/creditnotes/creditnotes');
		$this->folderPath = 'creditnotes/';
		$this->textMessage = 'send_creditnote_text';
		$this->tableName = 'creditnotes';
		$this->tableMovements = 'movementscreditnotes';
		$this->columnId = 'creditnote_id';
		$this->referredTo = 3;
		$this->titleReference = 'Nota di credito numero_';
	}

	function creditnotesAction() {
		$this->reference();
	}

    protected function getSelectForMovementData() {
		return parent::getSelectForMovementData()
		. ', free, product_id';
	}

}