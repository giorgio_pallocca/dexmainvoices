<?php
class VehiclesController extends CustomControllerAction
{
	function init()
	{
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('description');
		parent::setDirectionDefault('ASC');
		parent::setSearchKey('searchVehicle');
		parent::setRedirect('/vehicles/vehicles');
	}

	public function vehiclesAction() {
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$search = array('searchVehicle' => parent::getAttribute(parent::getSearchKey()));
		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getList($params)
	{
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		if(!empty($params['searchVehicle']))
		{
			$searchVehicle = $params['searchVehicle'];
			parent::setAttribute(parent::getSearchKey(), $searchVehicle);
			$whereCondition .= 'AND (v.description LIKE ? OR v.plate LIKE ? OR v.aciCoefficient LIKE ?) ';
			array_push($data, "%$searchVehicle%", "%$searchVehicle%", "%$searchVehicle%");
		}
		else parent::setAttribute(parent::getSearchKey(), null);

		$sql = "SELECT COUNT(id)
				FROM vehicles v
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);
		$sql = "SELECT v.id, v.description, v.plate, v.aciCoefficient, v.uniqueid
				FROM vehicles v
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function updateAction()
	{
		if(!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$selectVehicle = 'SELECT v.id, v.description, v.plate, v.aciCoefficient, v.uniqueid
							  FROM vehicles v
							  WHERE v.uniqueid = ?';
			$vehicle = $this->db->fetchRow($selectVehicle, $uniqueid);
			$this->view->vehicle = array_map('htmlspecialchars', $vehicle);
		}
	}

	public function saveAction()
	{
		$vehicle = $_POST;
		$this->db->save('vehicles', $vehicle);
		$this->_redirect(parent::getRedirect());
	}

	public function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$where['uniqueid = ?'] = $_REQUEST['uid'];
			$this->db->delete('vehicles', $where);
			parent::updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}

	public function checkdataAction()
	{
		$key = $_REQUEST['key'];
		$value = $_REQUEST['value'];
		$uid = $_REQUEST['uid'];

		$sql = "SELECT id FROM vehicles WHERE $key = ?";
		$data = array($value);

		if (!empty($uid))
		{
			$sql .= ' AND uniqueid <> ?';
			$data[] = $uid;
		}

		$result = $this->db->fetchOne($sql, $data);
		echo $result != NULL;
	}
}