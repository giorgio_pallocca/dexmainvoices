<?php
class CustomersController extends CustomControllerAction
{
	private $type = 'customersType';

	public function init() {
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('company');
		parent::setDirectionDefault('ASC');
		parent::setRedirect('/customers/customers');

		$this->folderPath = 'contacts';

		$request = $this->getRequest();
		$prefix = $request->getModuleName() . '-' . $request->getControllerName();
		$this->searchCustomer = $prefix . '-customer';
	}

	public function customersAction()
	{
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$search = array('searchCustomer' => parent::getAttribute($this->searchCustomer));

		$type = parent::getAttribute($this->type);
		if (!empty($type) || $type === '0')
		{
			$search['searchCustomerType'] = $type;
		}
		else
		{
			$search['searchCustomerType'] = -1;
		}

		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getList($params)
	{
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		if (!empty($params['searchCustomer'])) {
			$searchCustomer = $params['searchCustomer'];
			parent::setAttribute($this->searchCustomer, $searchCustomer);
			$whereCondition .= ' AND (c.firstname LIKE ? OR c.surname LIKE ? OR c.company LIKE ? OR c.email LIKE ?)';
			array_push($data, "%$searchCustomer%", "%$searchCustomer%" , "%$searchCustomer%" , "%$searchCustomer%");
		}
		else parent::setAttribute($this->searchCustomer, null);

		$searchCustomerType = $params['searchCustomerType'];
		parent::setAttribute($this->type, $searchCustomerType);
		if ($searchCustomerType != -1)
		{
			$whereCondition .= ' AND c.type = ?';
			$data[] = $searchCustomerType;
		}

		$sql = "SELECT COUNT(c.id)
				FROM customers c
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$sql = "SELECT c.id, c.uniqueid, c.firstname, c.surname, c.company, c.email,
				CASE (c.type) WHEN 0 THEN 'Cliente' WHEN 1 THEN 'Fornitore' ELSE 'Cliente/Fornitore' END AS type,
					(SELECT IFNULL(SUM(i.amount - i.paid_amount), 0)
					FROM invoices i
					WHERE i.customer_id = c.id) AS unpaid_amount
				FROM customers c
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function updateAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$selectCustomer =  'SELECT c.company, c.firstname, c.surname, c.email, c.vatNumber, c.uniqueid, c.fiscalCode,
									c.address, c.zipcode, c.city, c.state, c.phone, c.fax, c.type, c.distanceKm, c.notes, c.id_list
								FROM customers c
								WHERE c.uniqueid = ?';
			$customer = $this->db->fetchRow($selectCustomer, $uniqueid);
			$this->view->customer = array_map('htmlspecialchars', $customer);
		}
		$selectList = 'SELECT id, customer_type FROM list';
		$list = $this->db->fetchAll($selectList);
		$this->view->customertypes = $list;
	}

	public function saveAction()
	{
		$customer = $_POST;
		$result = $this->getCheckFieldsResults($customer);

		if($result['errors'] === false)
		{
			unset($customer['addCustomer']);
			unset($customer['inlineRadioOptions']);
			if (!empty($customer['fiscalCode'])) $customer['fiscalCode'] = strtoupper($customer['fiscalCode']);
			$customerUniqueid = $this->db->save('customers', $customer);

			$selectNewCustomer='SELECT id, company, uniqueid
								FROM customers
								WHERE uniqueid = ?';
			$newCustomer = $this->db->fetchRow($selectNewCustomer, $customerUniqueid);

			$result['newCustomer'] = $newCustomer;
		}
		echo json_encode($result);
	}

	private function getCheckFieldsResults($customer)
	{
		$result = array(
				'errors' => array()
		);

		if(!empty($customer['email']))
		{
			$dataEmail = array($customer['email']);
			$selectEmail = 'SELECT id
							FROM customers
							WHERE email = ?';
			if (!empty($customer['uniqueid']))
			{
				$selectEmail .= ' AND uniqueid <> ?';
				$dataEmail[] = $customer['uniqueid'];
			}
			$emailExists = $this->db->fetchOne($selectEmail, $dataEmail);
			$this->applogger->info($selectEmail);
			$this->applogger->info(print_r($dataEmail,1));
		}
		if(!empty($emailExists))
		{
			$result['errors']['email'] = true;
		}
		else
		{
			$result['errors'] = false;
		}
		return $result;
	}

	public function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$where['uniqueid = ?'] = $_REQUEST['uid'];
			$this->db->delete('customers', $where);

			parent::updateActivePageAfterDelete();
		}

		$this->_redirect(parent::getRedirect());
	}

	private function getReferenceData($tableName, $id)
	{
		$select =  "SELECT uniqueid, serialNumber, amount, DATE_FORMAT(date, '%d/%m/%Y') AS date
					FROM $tableName
					WHERE customer_id = ?";
		return $this->db->fetchAll($select, $id);
	}

	public function viewAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$customerSelect =  "SELECT c.id, c.company, c.firstname, c.surname, c.email, c.uniqueid, c.vatNumber, c.fiscalCode,
								c.address, c.zipcode, c.city, c.state, c.phone, c.fax, c.distanceKm, c.notes,
								CASE (c.type) WHEN 0 THEN 'Cliente' WHEN 1 THEN 'Fornitore' ELSE 'Cliente/Fornitore' END AS type
								FROM customers c
								WHERE uniqueid = ?";
			$customer = $this->db->fetchRow($customerSelect, $uniqueid);
			$this->view->customer = array_map('htmlspecialchars', $customer);

			$invoiceSelect =   "SELECT i.uniqueid, i.serialNumber, i.amount, (i.amount - i.paid_amount) AS unpaid_amount,
								CASE (i.is_external) WHEN 0 THEN 'emessa' WHEN 1 THEN 'ricevuta' END AS is_external
								FROM invoices i
								WHERE i.customer_id = ?";
			$invoice = $this->db->fetchAll($invoiceSelect, $customer['id']);
			$this->view->invoice = $invoice;

			$this->view->quotations = $this->getReferenceData('quotations', $customer['id']);
			$this->view->ddt = $this->getReferenceData('transportdocuments', $customer['id']);
			$this->view->creditnotes = $this->getReferenceData('creditnotes', $customer['id']);

			/*** totale fatture e scoperto ***/
			$totalAmount = 'SELECT SUM(amount) AS debtAmount,
								SUM(i.amount - i.paid_amount) AS totalUnpaid,
								COUNT(i.id) AS totalInvoices
							FROM invoices i
							WHERE customer_id = ?';
			$totalAmount = $this->db->fetchAll($totalAmount, $customer['id']);
			$this->view->totalAmount = $totalAmount;

			/*** messaggi utente ***/
			$messagesSelect =  'SELECT subject, text
								FROM messages
								WHERE customer_id = ?';
			$messages = $this->db->fetchAll($messagesSelect, $customer['id']);
			$this->view->messages = $messages;
		}
	}

	public function checkdataAction()
	{
		$key = $_REQUEST['key'];
		$value = $_REQUEST['value'];
		$uid = $_REQUEST['uid'];

		$sql = "SELECT id FROM customers WHERE $key = ?";
		$data = array($value);

		if (!empty($uid))
		{
			$sql .= " AND uniqueid <> ?";
			$data[] = $uid;
		}

		$result = $this->db->fetchOne($sql, $data);
		echo $result != null;
	}

	function importcontactsAction() {
		$contacts = array();

		if (isset($_GET['removeFile'])) {
			$this->setAttribute('uploadedFile', null);
		}
		else if ($this->getAttribute('errorUploadFile') == true) {
			$this->view->errorUploadFile = true;
			$this->setAttribute('errorUploadFile', null);
		}
		else if ($this->getAttribute('uploadedFile') != null) {
			$fileName = $this->getAttribute('uploadedFile');
			$this->view->uploadedFile = $fileName;

			$filePath = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath . '/' . $fileName;

			$fp = fopen($filePath, 'r');
			while (($line = fgetcsv($fp)) !== false) {
				$contacts[] = explode(';', $line[0]);
			}
			fclose($fp);

			$this->view->contactsJSON = json_encode($contacts, JSON_HEX_APOS);
		}
		else if ($this->getAttribute('insertedCustomers')) {
			$this->view->insertedCustomers = true;
			$this->setAttribute('insertedCustomers', null);
		}
	}

	function uploadcontactsAction() {
		$content = array();

		if($_POST) {
			$post = $_POST;
			$fileName = $this->getAttribute('uploadedFile');
			$this->setAttribute('uploadedFile', null);

			$filePath = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath . '/' . $fileName;

			$fp = fopen($filePath, 'r');
			while (($line = fgetcsv($fp)) !== false) {
				$content[] = explode(';', $line[0]);
			}
			fclose($fp);

			try {
				$this->db->beginTransaction();

				for($i=1; $i<count($content); $i++){
					$customer = array();
					for($t=0; $t<count($content[$i]); $t++){
						$customer[$post[$t]] = $content[$i][$t];
						$customer['type'] = 0;
					}
					$this->db->save('customers', $customer);
				}

				$this->db->commit();
				$this->setAttribute('insertedCustomers', true);
				$this->_redirect('/default/customers/importcontacts');
			}
			catch(Exception $e) {
				$this->db->rollBack();
				$this->applogger->info($e->getMessage());
			}
		}
		else {
			$fileTmpName = $_FILES['contacts']['tmp_name'];
			$fileName = $_FILES['contacts']['name'];
			$extension = substr($fileName, -4);

			if (is_uploaded_file($fileTmpName) && strcasecmp($extension, '.csv') == 0) {
				$uploadFolder = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath;
				if(!file_exists($uploadFolder)) {
					mkdir($uploadFolder);
				}

				$filePath = $uploadFolder . '/' . $fileName;
// 				$filePath = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath . $fileName;
				move_uploaded_file($fileTmpName, $filePath);
				$this->setAttribute('uploadedFile', $fileName);
			}
			else $this->setAttribute('errorUploadFile', true);
			$this->_redirect('/default/customers/importcontacts');
		}
	}

}
