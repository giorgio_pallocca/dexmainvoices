<?php
class UsersController extends CustomControllerAction
{
	const ROLE_ADMINISTRATOR = 'administrator';

	private $name = 'usersName',
			$email = 'usersEmail',
			$role = 'usersRole';

	public function init()
	{
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('surname');
		parent::setDirectionDefault('ASC');
		parent::setRedirect('/users/users');
	}

	public function usersAction()
	{
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
				parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$search = array(
			'searchName' => parent::getAttribute($this->name),
			'searchEmail' => parent::getAttribute($this->email)
		);

		$role = parent::getAttribute($this->role);
		if (!empty($role))
			$search['searchUserRole'] = $role;
		else $search['searchUserRole'] = -1;

		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getList($params)
	{
		$whereCondition = 'WHERE 1=1';
		$data = array();
		$params = array_map('trim', $params);

		if (!empty($params['searchName']))
		{
			$searchName = $params['searchName'];
			parent::setAttribute($this->name, $searchName);
			$whereCondition .= " AND (CONCAT(u.firstname, ' ', u.surname) LIKE ? || CONCAT(u.surname, ' ', u.firstname) LIKE ?) ";
			array_push($data, "%$searchName%", "%$searchName%");
		}
		else parent::setAttribute($this->name, null);

		if (!empty($params['searchEmail']))
		{
			$searchEmail = $params['searchEmail'];
			parent::setAttribute($this->email, $searchEmail);
			$whereCondition .= ' AND u.email LIKE ?';
			$data[] = "%$searchEmail%";
		}
		else parent::setAttribute($this->email, null);

		$searchUserRole = $params['searchUserRole'];
		parent::setAttribute($this->role, $searchUserRole);
		if ($searchUserRole != -1)
		{
			$whereCondition .= ' AND u.role = ?';
			$data[] = $searchUserRole;
		}

		$sql = "SELECT COUNT(id)
				FROM users u
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];

		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$sql = "SELECT u.uniqueid, CONCAT(u.surname, ' ', u.firstname) AS fullname, u.email, u.phone, u.role
				FROM users u
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function updateAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$user = 'SELECT uniqueid, firstname, surname, email, phone, role, dashboard, menu FROM users WHERE uniqueid = ?';
			$user = $this->db->fetchRow($user, $_REQUEST['uid']);
			$this->view->user = array_map('htmlspecialchars', $user);
		}
	}

	public function saveAction()
	{
		$auth = Zend_Auth::getInstance();
		$loggedUser = $auth->getIdentity();

		$user = array(
			'firstname' => $_POST['firstname'],
			'surname' => $_POST['surname'],
			'email' => $_POST['email'],
			'phone' => $_POST['phone'],
			'role' => $_POST['role'],
			'menu' => $_POST['menu'],
			'uniqueid' => $_POST['uniqueid']
		);

		if (isset($_POST['dashboard']))
			$user['dashboard'] = 1;
		else
			$user['dashboard'] = 0;

		$config = Zend_Registry::get('config');

		$password = $_POST['password'];
		if (!empty($password) && $password == $_POST['passwordConfirm'])
		{
			$user['password'] = md5($password);

			if (isset($_POST['sendCredentials']))
			{
				$fromEmail = $loggedUser->email;
				$parameters = $config->emails->sendCredentials;
				$this->view->email = $user['email'];
				$this->view->password = $password;
				$this->view->websiteurl = parent::getWebSiteURL();
				$text = $this->view->render('emails/sendcredentials.tpl');
				parent::sendEmailSmtpFromConfig($user['email'], strip_tags($text), $text, $parameters->subject, null, null, null, $fromEmail);
			}
		}
		$this->db->save('users', $user);

		if ($loggedUser->uniqueid == $user['uniqueid']) {
			$login = $config->login;
			$this->db->setFetchMode(Zend_Db::FETCH_OBJ);
			$userRow = $this->db->fetchRow("SELECT $login->toGetColumns FROM $login->table WHERE uniqueid = ?", $user['uniqueid']);
			$auth->getStorage()->write($userRow);
		}

		$this->_redirect(parent::getRedirect());
	}

	public function deleteAction()
	{
		if (!empty($_REQUEST['uid']) && $_REQUEST['uid'] != Zend_Auth::getInstance()->getIdentity()->uniqueid)
		{
			$where['uniqueid = ?'] = $_REQUEST['uid'];
			$this->db->delete('users', $where);
			parent::updateActivePageAfterDelete();
		}

		$this->_redirect(parent::getRedirect());
	}

	public function checkdataAction()
	{
		$key = $_REQUEST['key'];
		$value = $_REQUEST['value'];
		$uid = $_REQUEST['uid'];

		$sql = "SELECT id FROM users WHERE $key = ?";
		$data = array($value);

		if (!empty($uid)) {
			$sql .= ' AND uniqueid <> ?';
			$data[] =$uid;
		}

		$result = $this->db->fetchOne($sql, $data);
		echo $result != null;
	}
}