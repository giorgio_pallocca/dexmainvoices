<?php
class IndexController extends CustomControllerAction
{
	private $yearSelected = 'yearSelected';

	public function init()
	{
		parent::init();
	}

	public function indexAction()
	{
		$loggedUser = Zend_Auth::getInstance()->getIdentity();

		$dashboard = $this->db->fetchOne('SELECT dashboard FROM users WHERE id = ?', $loggedUser->id);
		if ($dashboard == 0)
			$this->_redirect('/invoices/invoices');

		if(!empty($_REQUEST['yearSelected']))
		{
			parent::setAttribute($this->yearSelected, $_REQUEST['yearSelected']);
			$yearSelected = parent::getAttribute($this->yearSelected);
		}
		else
		{
			$yearSelected = date('Y');
		}
		$this->view->yearSelected = $yearSelected;

		$this->view->years = parent::getInvoicesYears();

		$whereCondition = "WHERE YEAR(date) = $yearSelected AND is_external = 0 ";

		if ($loggedUser->role == 'user')
			$whereCondition .= 'AND (private <> 1)';

		$selectTot ="SELECT SUM(amount) AS total,
							SUM(paid_amount) AS paid_amount,
							SUM(amount - paid_amount) AS unpaid_amount,
							SUM(vat_amount) AS vat_amount,
							SUM(taxable) AS taxable
					 FROM invoices
					 $whereCondition";
		$result = $this->db->fetchRow($selectTot);
		$result['total'] = number_format($result['total'], 2 , ',' , '.');
		$result['paid_amount'] = number_format($result['paid_amount'], 2 , ',' , '.');
		$result['unpaid_amount'] = number_format($result['unpaid_amount'], 2 , ',' , '.');
		$result['vat_amount'] = number_format($result['vat_amount'], 2 , ',' , '.');
		$result['taxable'] = number_format($result['taxable'], 2 , ',' , '.');
		$this->view->result = $result;

		$selectGraph1="SELECT DATE_FORMAT(date,'%Y-%m') AS ym, SUM(amount) AS a
					   FROM invoices
					   $whereCondition
					   GROUP BY year(date), month(date)
					   ORDER BY year(date) desc, month(date) desc LIMIT 12";

		$resultGraph1=$this->db->fetchAll($selectGraph1);

		if(!empty($resultGraph1))
		{
			$graph1 = json_encode($resultGraph1);
		}
		else
		{
			$graph1 = array();
		}

		$this->view->graph1 = $graph1;

		$selectGraph2= "SELECT DATE_FORMAT(date,'%Y-%m') AS ym, SUM(vat_amount) as a,SUM(taxable) AS b,SUM(amount - paid_amount) AS c
						FROM invoices
						$whereCondition
						GROUP BY year(date), month(date)
						ORDER BY year(date) desc, month(date) desc LIMIT 12";

		$resultGraph2=$this->db->fetchAll($selectGraph2);

		if(!empty($resultGraph2))
		{
			$graph2 = json_encode($resultGraph2);
		}
		else
		{
			$graph2 = array();
		}

		$this->view->graph2 = $graph2;

		$graph="SELECT customer_name as label,round(sum(amount)) as value FROM invoices
				$whereCondition
				GROUP BY customer_id
				ORDER BY value DESC	LIMIT 10";

		$jsongraph = '';
		$stmt = $this->db->query($graph); 
		while ($row = $stmt->fetch()) {	$jsongraph .= "{label:'" . addslashes($row['label']). "', value:".$row['value']."},\n";	}
		$this->view->graph3 = substr($jsongraph, 0, -2);
	}
        function getlogoAction()
        {
            $pathtofile = HOME_DIRECTORY .'/uploads/logo/' . parent::getParameter('company_logo');
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            header("Content-type:   ".finfo_file($finfo, $pathtofile));
            finfo_close($finfo);
            header('Content-length: '.filesize($filepath));
            header('Content-Disposition: inline; filename="'.$file.'"');
            header("X-Sendfile: $pathtofile");
            $this->_helper->viewRenderer->setNoRender();
        }

}
