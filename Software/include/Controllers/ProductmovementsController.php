<?php
class ProductmovementsController extends CustomControllerAction
{
	private $startDateSearch = 'startDateSearch',
			$endDateSearch = 'endDateSearch';

	function init()
	{
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('date');
		parent::setDirectionDefault('ASC');
		parent::setSearchKey('searchProductmovements');
		parent::setRedirect('/productmovements/productmovements');
	}

	public function productmovementsAction() {
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$startDate = parent::getAttribute($this->startDateSearch);
		if ($startDate == NULL){
			$startDate = date('01/01/Y');
		}
		$endDate = parent::getAttribute($this->endDateSearch);
		if ($endDate == NULL){
			$endDate = date('31/12/Y');
		}

		$search = array(
			'startDateSearch' => $startDate,
			'startDateReset' => date('01/01/Y'),
			'endDateReset' => date('31/12/Y'),
			'endDateSearch' => $endDate,
			'searchProductmovements' => parent::getAttribute(parent::getSearchKey())
		);
		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getFromCondition()
	{
		$fromCondition='FROM productmovements pm
						LEFT JOIN products p ON p.id = pm.id_prodotto';
		return $fromCondition;
	}

	private function getWhereCondition($params)
	{
		$result = array();
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		$startDate = $params['startDateSearch'];
		parent::setAttribute($this->startDateSearch, $startDate);

		if(!empty($startDate))
		{
			$startDate = parent::getDateInSQLFormat($startDate, '/');

			if(!empty($params['endDateSearch']))
			{
				$endDate = $params['endDateSearch'];
				parent::setAttribute($this->endDateSearch, $endDate);
				$endDate = parent::getDateInSQLFormat($endDate, '/');
				$whereCondition .= 'AND (pm.date BETWEEN ? AND ?) ';
				array_push($data, $startDate, $endDate);
			}
			else
			{
				parent::setAttribute($this->endDateSearch, null);
				$whereCondition .= 'AND (pm.date = ?) ';
				array_push($data, $startDate);
			}
		}
		else
		{
			parent::setAttribute($this->endDateSearch, null);
		}

		if(!empty($params['searchProductmovements']))
		{
			$searchProductmovements = $params['searchProductmovements'];
			parent::setAttribute(parent::getSearchKey(), $searchProductmovements);
			$whereCondition .= 'AND (p.product_name LIKE ? OR pm.description LIKE ? OR pm.load LIKE ? OR pm.unload LIKE ? OR pm.prsaleunitary LIKE ?) ';
			array_push($data, "%$searchProductmovements%", "%$searchProductmovements%", "%$searchProductmovements%", "%$searchProductmovements%", "%$searchProductmovements%");
		}
		else parent::setAttribute(parent::getSearchKey(), null);

		$result['whereCondition'] = $whereCondition;
		$result['data'] = $data;
		return $result;
	}

	private function getOrderbyCondition($sortField, $sortDir)
	{
		$prefix = 'pm.';

		if($sortField == 'description')
		{
			$prefix = 'pm.';
		}
		else if($sortField == 'product_name')
		{
			$prefix = 'p.';
		}

		return "ORDER BY $prefix$sortField $sortDir";
	}

	private function getTotal($fromCondition, $whereCondition, $orderbyCondition, $data)
	{
		$selectTotal = "SELECT SUM(prbuyunitary) AS total
		$fromCondition
		$whereCondition
		$orderbyCondition";
		return $this->db->fetchOne($selectTotal, $data);
	}

	private function getList($params)
	{
		$result = $this->getWhereCondition($params);
		$data = $result['data'];
		$whereCondition = $result['whereCondition'];
		$fromCondition = $this->getFromCondition();

		$sql = "SELECT COUNT(pm.id)
				$fromCondition
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$orderbyCondition = $this->getOrderbyCondition($sortField, $sortDir);

		$sql = "SELECT DATE_FORMAT(pm.date, '%d/%m/%Y') AS date, pm.id, pm.load, p.product_name,
				pm.unload, pm.prbuyunitary, pm.prsaleunitary, pm.description
				$fromCondition
				$whereCondition
				$orderbyCondition
				LIMIT " . $page * $pageSize . ',' . $pageSize;
		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);

		$results['total'] = $this->getTotal($fromCondition, $whereCondition, $orderbyCondition, $data);

		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function printlistAction()
	{
		$params = $_REQUEST;
		$fromCondition = $this->getFromCondition();
		$result = $this->getWhereCondition($params);

		$data = $result['data'];
		$whereCondition = $result['whereCondition'];

		$sortField = parent::getAttribute(parent::getActiveSortFieldKey());
		$sortDir = parent::getAttribute(parent::getActiveSortDirectionKey());

		$orderbyCondition = $this->getOrderbyCondition($sortField, $sortDir);

		$sql = "SELECT DATE_FORMAT(pm.date, '%d/%m/%Y') AS date, p.product_name, pm.load,
				pm.unload, pm.prbuyunitary, pm.prsaleunitary, pm.description
				$fromCondition
				$whereCondition
				$orderbyCondition";
		$results = $this->db->fetchAll($sql, $data);

		$total = $this->getTotal($fromCondition, $whereCondition, $orderbyCondition, $data);

		$header = array('Data', 'Nome', 'Carico', 'Scarico', 'Pz. Acquisto', 'Pz. Vendita', 'Descrizione');
		$footer = array('TOTALE', '', '', '', '', $total);
		$this->createCSV($results, $header, $footer, 'Movimenti magazzino');

		$this->_helper->viewRenderer->setNoRender();
	}

	protected function createCSV($results, $header, $footer, $fileName)
	{
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Type: application/octet-stream charset=UTF-8');
		header('Content-Disposition: attachment; filename=' . $fileName . '.csv');
		header('Content-Transfer-Encoding: binary');

		$fp = fopen('php://output', 'w');
		fputcsv($fp, array_values($header), ';');
		foreach($results as $row)
		{
			fputcsv($fp, array_values($row), ';');
		}
		//fputcsv($fp, array_values($footer), ';');
		fclose($fp);
	}
}