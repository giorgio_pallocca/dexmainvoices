<?php
class ExternalinvoicesController extends ReferencesController
{
	function init()
	{
		parent::init();
		parent::setRedirect('/externalinvoices/externalinvoices');
		$this->tableName = 'invoices';
		$this->tableMovements = 'movements';
		$this->columnId = 'invoice_id';
		$this->titleReference = 'Fattura ricevuta numero_';
	}

	function externalinvoicesAction() {
		$this->reference(array('is_external'=>1));
	}

	function listAction() {
		$searchParams = $_REQUEST;
		$searchParams['is_external'] = 1;
		echo htmlspecialchars(json_encode($this->getList($searchParams)), ENT_NOQUOTES);
	}

	protected function getSelectForList() {
		return parent::getSelectForList()
			. ', (ref.amount - ref.paid_amount) AS unpaid_amount,
				CASE WHEN(ref.paid_amount < ref.amount) THEN 1 ELSE 0 END AS status';
	}

	protected function getReference($uid)
	{
		$reference="SELECT id, uniqueid, DATE_FORMAT(date, '%d %M %Y') AS date, DATE_FORMAT(expireDate, '%d %M %Y') AS expireDate, YEAR(date) AS year, serialNumber,
						amount, taxable, taxableFree, vat_amount, buyer, seller, notes, theme_id, customer_id, pdf, tax1, tax2, tax1amount, tax2amount, paid_amount,
						taxable + tax1amount AS totImponibile
					FROM $this->tableName
					WHERE uniqueid = ?";
		return $this->db->fetchRow($reference, $uid);
	}

	protected function getCustomers()
	{
		return $this->db->fetchAll('SELECT id, company FROM customers WHERE type <> 0 ORDER BY company ASC');
	}

	function updateAction()
	{
		$this->view->customers = $this->getCustomers();

		if (!empty($_REQUEST['uid']))
		{
			$uid = $_REQUEST['uid'];

			$select =  "SELECT uniqueid, serialNumber, DATE_FORMAT(date, '%d/%m/%Y') AS date, customer_id,
						DATE_FORMAT(expireDate, '%d/%m/%Y') AS expireDate, tax1, tax2, attachment
						FROM $this->tableName
						WHERE uniqueid = ?";
			$reference = $this->db->fetchRow($select, $uid);
		}
		else {
			$reference = array(
				'date' => date('d/m/Y')
			);
		}

		$this->view->reference = array_map('htmlspecialchars', $reference);

		$this->view->defaultTax1 = parent::getParameter('company_tax1');
		$this->view->defaultTax2 = parent::getParameter('company_tax2');
	}

	function viewAction() {
		$this->view(true);
	}

	function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$reference="SELECT id, serialNumber, YEAR(date) AS year
						FROM $this->tableName
						WHERE uniqueid = ?";
			$reference = $this->db->fetchRow($reference, $_REQUEST['uid']);

			try {
				$this->db->beginTransaction();
				$this->db->delete($this->tableName, array('id = ?' => $reference['id']));
				$this->db->delete($this->tableMovements, array("$this->columnId = ?" => $reference['id']));
				$this->db->delete('explanations', array('invoice_id = ?' => $reference['id']));
				$this->db->delete('productmovements', array("uniqueid_invoices = ?" => $_REQUEST['uid']));
				$this->db->commit();
			} catch (Exception $e) {
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
			}
			$this->updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}

	function quickinvoiceaddAction() {
		$this->quickInvoiceAdd();
	}

	function quickinvoicesaveAction() {
		$this->quickInvoiceSave(1);
	}

	function saveAction() {
		$reference = $_POST;
		$this->applogger->info(print_r($_POST,1));
		$reference['is_external'] = 1;

		$filename = $_FILES['attachment']['name'];
		if(!empty($filename)) {
			$uploadsPath = Zend_Registry::get('config')->parameters->uploads_path;
			$uploadFolder = $uploadsPath . 'externalinvoicesUploads';
			if(!file_exists($uploadFolder))
			{
				mkdir($uploadFolder, 0777, true);
			}

			$filePath = $uploadFolder . '/' . $filename;

			$counter = 1;
			$fileArray = explode('.', $filename);
			$ext = $fileArray[count($fileArray) - 1];
			unset($fileArray[count($fileArray) - 1]);
			$prefix = implode('.', $fileArray);
			while(true)
			{
				if(file_exists($filePath))
				{
					$filename = $prefix . '(' . $counter++ . ').' . $ext;
					$filePath = $uploadFolder . '/' . $filename;
				}
				else
				{
					break;
				}
			}

			move_uploaded_file($_FILES['attachment']['tmp_name'], $filePath);
			$reference['attachment'] = basename($filePath);
		}

		$this->save($reference, true);
	}

	function savemovementAction() {
		if (isset($_POST['free'])) {
			$_POST['free'] = 1;
			$_POST['vat'] = 0;
		}
		else $_POST['free'] = 0;

		echo json_encode($this->savemovement(true));
	}

	function deletemovementAction() {

	    //aggiungere delete
	    $this->applogger->info(print_r($_POST,1));

	    $whereMov['uniqueid_invoices = ?'] = $_POST['referenceUid'];
	    $whereMov['uniqueid_movements = ?'] = $_POST['uid'];
    	$this->db->delete('productmovements', $whereMov);
	    //fine

		echo json_encode($this->deletemovement(true));
	}

	function removeattachmentAction() {
		$fileName = $_REQUEST['fileName'];
		$uniqueid = $_REQUEST['uniqueid'];
		$result = null;
		$parameters = Zend_Registry::get('config')->parameters;
		$attachmentPath = $parameters->uploads_path . 'externalinvoicesUploads/';

		$attachmentPath .= $fileName;

		if (file_exists($attachmentPath) && unlink($attachmentPath))
		{
			$where['uniqueid = ?'] = $uniqueid;
			$this->db->update($this->tableName, array('attachment' => null), $where);
			$result = true;
		}
		echo $result;
	}

	protected function getSelectForMovementData() {
		return parent::getSelectForMovementData()
		. ', free, product_id';
	}
}