<?php
class ProductsController extends CustomControllerAction {

	public function init() {
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('description');
		parent::setDirectionDefault('ASC');
		parent::setRedirect('/products/products');

		$request = $this->getRequest();
		$prefix = $request->getModuleName() . '-' . $request->getControllerName();
		$this->searchProduct = $prefix . '-product';
	}

	public function productsAction()
	{
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
				parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$search = array('searchProduct' => parent::getAttribute($this->searchProduct));
		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getList($params)
	{
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		if(!empty($params['searchProduct']))
		{
			$searchProduct = $params['searchProduct'];
			parent::setAttribute($this->searchProduct, $searchProduct);
			$whereCondition .= 'AND (p.description LIKE ? OR p.amount LIKE ? OR p.vat LIKE ?) ';
			array_push($data, "%$searchProduct%", "%$searchProduct%", "%$searchProduct%");
		}
		else parent::setAttribute($this->searchProduct, null);

		$sql = "SELECT COUNT(id)
				FROM products p
				$whereCondition";
				$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$sql = "SELECT p.description, p.amount, p.vat, p.uniqueid, p.id, p.quantity, p.product_name, p.amountsale
				FROM products p
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function updateAction()
	{
		if(empty($_REQUEST['id'])){
    	    if(!empty($_REQUEST['uid']))
    		{
    			$uniqueid = $_REQUEST['uid'];
    			$selectProduct='SELECT p.description, p.amount, p.amountsale, p.vat, p.id, p.uniqueid, p.quantity, p.product_name
    							FROM products p
    							WHERE p.uniqueid = ?';
    			$product = $this->db->fetchRow($selectProduct, $uniqueid);
    			$this->view->product = array_map('htmlspecialchars', $product);
    		}
    		$this->view->customertypes = $this->db->fetchAll('SELECT l.id, l.customer_type FROM list l');
	    }
	    else{
	    	$_REQUEST['prbuyunitary'] = number_format(str_replace(',', '.', $_REQUEST['prbuyunitary']), 2, '.', '');
	    	$_REQUEST['prsaleunitary'] = number_format(str_replace(',', '.', $_REQUEST['prsaleunitary']), 2, '.', '');
	        $_REQUEST['date'] = $this->getDateInSQLFormat($_REQUEST['date'], '/');
	        $this->db->update('productmovements',$_REQUEST ,'id='.$_REQUEST['id']);
	        $id_prodotto = $this->db->fetchOne('SELECT id_prodotto FROM productmovements WHERE id = ?',$_REQUEST['id']);
	        $uniqueid = $this->db->fetchOne('SELECT uniqueid FROM products WHERE id = ?',$id_prodotto);
	        echo $uniqueid;
	    }
	}

	public function viewAction()
	{
	if (!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$productSelect =  "SELECT p.id, p.description, p.amount, p.vat, p.quantity, p.product_name, p.uniqueid, p.amountsale
								FROM products p
								WHERE uniqueid = ?";
			$product = $this->db->fetchRow($productSelect, $uniqueid);
			$product['amount'] =  number_format($product['amount'], 2, ',', '.');
			$product['amountsale'] =  number_format($product['amountsale'], 2, ',', '.');
			//$this->view->product = array_map('htmlspecialchars', $product);

			$productmovementsSelect = "SELECT pm.id, pm.load, pm.unload, pm.prbuyunitary, pm.prsaleunitary, pm.description, pm.id_prodotto, DATE_FORMAT(pm.date, '%d/%m/%Y') AS date, pm.uniqueid_invoices
			                           FROM productmovements pm
			                           WHERE id_prodotto = ?";
			$productmovements = $this->db->fetchAll($productmovementsSelect, $product['id']);
			$this->view->productmovements = $productmovements;
			$totproducts = 0;
			foreach ($productmovements as $row ){
			    $totproducts = $totproducts + $row['load'];
			}
		    foreach ($productmovements as $row ){
			    $totproducts = $totproducts - $row['unload'];
			}
			$this->view->totproducts = $totproducts;
		    $pezzisale = 0;
		    $totsale = 0;
		    foreach ($productmovements as $row ){
			    $totsale = $totsale + $row['prsaleunitary'];
			    $pezzisale = $pezzisale + 1;
			}
			if(!$pezzisale){
			    $medsale = $totsale;
			}else{
			    $medsale = $totsale/$pezzisale;
			}

			$this->view->medsale = number_format($medsale, 2, ',', '.');
		    $pezzibuy = 0;
		    $totbuy = 0;
		    foreach ($productmovements as $row){
			    $totbuy = $totbuy + $row['prbuyunitary'];
			    $pezzibuy = $pezzibuy +1;
			}
			if(!$pezzibuy){
			    $medbuy = $totbuy;
			}else{
			    $medbuy = $totbuy / $pezzibuy;
			}
			$this->view->medbuy = number_format($medbuy, 2, ',', '.');

			$dataLast = $this->db->fetchOne('SELECT MAX(date) FROM productmovements WHERE id_prodotto = ?', $product['id']);
			$amount = $this->db->fetchOne('SELECT prbuyunitary FROM productmovements WHERE date = ? AND id_prodotto = ? ORDER BY date, id DESC', array($dataLast,$product['id']));
			$amountsale = $this->db->fetchOne('SELECT prsaleunitary FROM productmovements WHERE date = ? AND id_prodotto = ? ORDER BY date, id DESC', array($dataLast,$product['id']));

			$productUpdate = array('quantity' => $totproducts, 'amount' => $amount, 'amountsale' => $amountsale);
			$this->db->update('products',$productUpdate ,'id='.$product['id']);
			$product['quantity'] = $totproducts;
			$product['amount'] = $amount;
			$product['amountsale'] = $amountsale;
			$this->view->product = array_map('htmlspecialchars', $product);
		}
		if(isset($_REQUEST['movimento'])){
		    $this->view->tabname = 'movements';
		}
	}

	public function saveAction()
	{
		$product = array('uniqueid' => $_POST['uniqueid'], 'product_name' => $_POST['product_name'], 'description' => $_POST['description'], 'quantity' => $_POST['quantity'], 'amount' => $_POST['amount'], 'amountsale' => $_POST['amountsale'], 'vat' => $_POST['vat']);
		$amount = $_POST;
		unset($amount['uniqueid']);
		unset($amount['product_name']);
		unset($amount['description']);
		unset($amount['quantity']);
		unset($amount['amount']);
		unset($amount['amountsale']);
		unset($amount['vat']);
		

		//$this->applogger->info(print_r($_POST,1));
		//$this->applogger->info(print_r($amount,1));
		//$this->applogger->info(print_r($product,1));
		$this->db->save('products', $product);
		$productId = $this->db->lastInsertId();

		if($productId==0){
			$productId = $this->db->fetchOne('SELECT id FROM products WHERE uniqueid = ?', $product['uniqueid']);
		}
		
		$this->applogger->info(print_r($product,1));

		$productmovements = array('load' => $product['quantity'], 'unload' => 0, 'prbuyunitary' => $product['amount'], 'prsaleunitary' => $product['amountsale'], 'description' => $product['description'], 'id_prodotto' => $productId, 'date' => date("Y-m-d"));
		//$this->applogger->info(print_r($productmovements,1));

		$this->db->insert('productmovements', $productmovements);
		
		foreach ($amount as $key=>$value ){
			$result=$this->db->fetchOne('SELECT id FROM product_prices WHERE id_product=? AND id_list=? ', array($productId, $key));
			if(empty($result)){
				$productprices = array('id_product'=>$productId, 'id_list'=>$key, 'amount'=>$value);
				$this->db->insert('product_prices', $productprices);
			}
			else{
				$productprices = array('id_product'=>$productId, 'id_list'=>$key, 'amount'=>$value);
				$this->db->update('product_prices',$productprices ,'id='.$result);
			}			
			//$this->applogger->info($result);
			
		}

		$this->_redirect(parent::getRedirect());
	}

	public function savemovementAction(){
	    if(!empty($_REQUEST['id_prodotto'])){
	        $_REQUEST['date'] = $this->getDateInSQLFormat($_REQUEST['date'], '/');
	        $_REQUEST['prbuyunitary'] = number_format(str_replace(',', '.', $_REQUEST['prbuyunitary']), 2, '.', '');
	        $_REQUEST['prsaleunitary'] = number_format(str_replace(',', '.', $_REQUEST['prsaleunitary']), 2, '.', '');
	        $this->db->insert('productmovements', $_REQUEST);
	        $uniqueid = $this->db->fetchOne('SELECT uniqueid FROM products WHERE id = ?',$_REQUEST['id_prodotto']);
	        echo $uniqueid;
	    }
	}

	public function deleteAction()
	{
	    if(empty($_REQUEST['id'])){
    		if (!empty($_REQUEST['uid']))
    		{
    		    $id_prodotto = $this->db->fetchOne('SELECT id FROM products WHERE uniqueid = ?', $_REQUEST['uid']);
    		    $whereMov['id_prodotto = ?'] = $id_prodotto;
    		    $this->db->delete('productmovements', $whereMov);
    		    $where['uniqueid = ?'] = $_REQUEST['uid'];
    			$this->db->delete('products', $where);
    			parent::updateActivePageAfterDelete();
    		}
    		$this->_redirect(parent::getRedirect());
	    }
	    else{
	        $where['id = ?'] = $_REQUEST['id'];
    		$id_prodotto = $this->db->fetchOne('SELECT id_prodotto FROM productmovements WHERE id = ?',$_REQUEST['id']);
	        $uniqueid = $this->db->fetchOne('SELECT uniqueid FROM products WHERE id = ?',$id_prodotto);
	        $this->db->delete('productmovements', $where);
	        echo $uniqueid;
	    }
	}
}