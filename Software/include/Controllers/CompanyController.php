<?php
class CompanyController extends CustomControllerAction
{
	public function init()
	{
		parent::init();
		parent::setRedirect('/company/update');
	}

	public function updateAction()
	{
		if (isset($_REQUEST['saved']))
		{
			$this->view->confirmMessage = 'saved';
		}

		$params = array(
			'company_name', 'company_vat', 'company_address', 'company_cap', 'company_city', 'company_state',
			'company_email', 'company_default_vat', 'company_logo', 'company_website', 'company_phone',
			'company_fax', 'company_default_payment', 'company_tax1', 'company_tax2', 'company_heading'
		);

		foreach($params as &$item) {
			$params[$item] = parent::getParameter($item);
		}

		$this->view->params = $params;

		if (!empty($params['company_logo'])) {
			$parameters = Zend_Registry::get('config')->parameters;
			$this->view->logoFolder = $parameters->logo->folder;
		}
	}

	function removelogoAction()
	{
		$result = null;
		$parameters = Zend_Registry::get('config')->parameters;
		$logoPath = $parameters->uploads_path . $parameters->logo->folder;

		$param = 'company_logo';
		$logoPath .= parent::getParameter($param);

		if (file_exists($logoPath) && unlink($logoPath))
		{
			$where['parameter_name = ?'] = $param;
			$this->db->update('parameters', array('parameter_value' => null), $where);
			$result = true;
		}

		echo $result;
	}

	private function save(array $company)
	{
		$parameters = Zend_Registry::get('config')->parameters;

		$uploadsPath = $parameters->uploads_path;
		$logoParams = $parameters->logo;

		$filename = $_FILES['company_logo']['name'];
		$logoPath = $uploadsPath . $logoParams->folder . $filename;
		if (move_uploaded_file($_FILES['company_logo']['tmp_name'], $logoPath))
		{
			$logoAspect = getimagesize($logoPath);

			$source_aspect_ratio = $logoAspect[0] / $logoAspect[1];
			$new_aspect_ratio = $logoParams->max_width / $logoParams->max_height;

			if ($logoAspect[0] <= $logoParams->max_width && $logoAspect[1] <= $logoParams->max_height) {
				$new_width = $logoAspect[0];
				$new_height = $logoAspect[1];
			}
			elseif ($new_aspect_ratio > $source_aspect_ratio) {
				$new_width = (int) ($logoParams->max_height * $source_aspect_ratio);
				$new_height = $logoParams->max_height;
			}
			else {
				$new_width = $logoParams->max_width;
				$new_height = (int) ($logoParams->max_width / $source_aspect_ratio);
			}

			$newImage = imagecreatetruecolor($new_width, $new_height);

			switch ($logoAspect[2]) {
				case IMAGETYPE_GIF:
					$sourceImage = imagecreatefromgif($logoPath);
					imagecolortransparent($sourceImage, imagecolorallocate($sourceImage, 0, 0, 0));
					break;
				case IMAGETYPE_JPEG:
					$sourceImage = imagecreatefromjpeg($logoPath);
					break;
				case IMAGETYPE_PNG:
					$sourceImage = imagecreatefrompng($logoPath);
					$blank = imagecreatefromstring(base64_decode($this->blankpng()));
					imagesavealpha($newImage, true);
					imagealphablending($newImage, false);
					imagecopyresized($newImage, $blank, 0, 0, 0, 0, $new_width, $new_height, $logoAspect[0], $logoAspect[1]);
					imagedestroy($blank);
					break;
				default:
					$sourceImage = null;
			}

			if ($sourceImage !== null) {
				imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0, $new_width, $new_height, $logoAspect[0], $logoAspect[1]);
				switch ($logoAspect[2])
				{
					case IMAGETYPE_GIF:
						imagegif($newImage, $logoPath);
						break;
					case IMAGETYPE_PNG:
						imagepng($newImage, $logoPath, 0);
						break;
					case IMAGETYPE_JPEG:
						imagejpeg($newImage, $logoPath, 100);
						break;
				}

				$company['company_logo'] = basename($logoPath);
			}
		}

		foreach ($company as $key => $value)
		{
			$where = array();
			$where['parameter_name = ?'] = $key;
			$this->db->update('parameters', array('parameter_value' => $value), $where);
		}
	}

	function saveAction()
	{
		if (isset($_POST['company_tax2'])) {
			$_POST['company_tax2'] = Zend_Registry::get('config')->parameters->tax2;
		}
		else $_POST['company_tax2'] = 0;

		$this->save($_POST);
		$this->_redirect('/company/update?saved=true');
	}

	function createfirstuserAction() {
		if ($this->db->fetchOne('SELECT COUNT(id) FROM users') > 0) {
			if (Zend_Auth::getInstance()->hasIdentity())
				$this->_redirect('/');
			else $this->_redirect('/login/login');
		}

		$parameters = Zend_Registry::get('config')->parameters;

		if ($this->getRequest()->isPost()) {
			$vat = $parameters->default_vat;

			$newVat = $_POST['company_default_vat'];
			if ($newVat > 0 && $newVat < 100) {
				$vat = $newVat;
			}

			$companyName = $_POST['company_name'];
			if (empty($companyName)) {
				$companyName = $_POST['surname'] . ' ' . $_POST['firstname'];
			}

			$company = array(
				'company_default_vat' => $vat,
				'company_name' => $companyName,
				'company_vat' => $_POST['company_vat'],
				'company_address' => $_POST['company_address'],
				'company_cap' => $_POST['company_cap'],
				'company_city' => $_POST['company_city'],
				'company_state' => $_POST['company_state'],
				'company_phone' => $_POST['company_phone'],
				'company_fax' => $_POST['company_fax'],
				'company_email' => $_POST['email'],
				'company_website' => $_POST['company_website'],
				'company_default_payment' => $_POST['company_default_payment'],
				'company_default_vat' => $_POST['company_default_vat'],
				'company_tax1' => $_POST['company_tax1'],
				'company_tax2' => (isset($_POST['company_tax2']))? 1:0,
				'company_heading' => $_POST['company_heading']
			);

			$user = array(
				'firstname' => $_POST['firstname'],
				'surname' => $_POST['surname'],
				'email' => $_POST['email'],
				'password' => md5($_POST['password']),
				'phone' => $_POST['company_phone'],
				'role' => UsersController::ROLE_ADMINISTRATOR
			);

			try
			{
				$this->db->beginTransaction();
				$this->db->save('users', $user);
				$this->save($company);
				$this->db->commit();

				$this->getRequest()->clearParams();
				$this->_forward('login', 'login', 'default', array(
					'identity' => $_POST['email'],
					'password' => $_POST['password']
				));
			}
			catch(Exception $e) {
				$this->db->rollBack();
				$this->applogger->info($e->getMessage());
			}
		}
		else $this->view->params = array(
			'company_default_vat' => $parameters->default_vat
		);
	}

	private function blankpng() {
		return 'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADqSURBVHjaYvz//z/DYAYAAcTEMMgBQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAgAEAMpcDTTQWJVEAAAAASUVORK5CYII=';
	}
}