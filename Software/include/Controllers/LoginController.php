<?php
class LoginController extends CustomControllerAction
{
	public function loginAction()
	{
		$params = $this->getRequest()->getParams();

		if(!$this->usersExist())
		{
			$this->_forward('createfirstuser', 'company', 'default');
		}
		else if (!empty($params['identity']) && !empty($params['password']))
		{
			$this->getAccountStatus();

			$identity = $params['identity'];
			$password = $params['password'];

			$login = Zend_Registry::get('config')->login;
			$authAdapter = new DexmaZend_Auth_Adapter_DbTable(null, $login, $identity, $password);
			$result = $authAdapter->authenticate();
			if ($result->isValid())
			{
				$this->_redirect(strtolower(trim($login->successRedirect)));
			}
			else
			{
				$this->view->identity = $identity;
				$this->view->errorCode = 'invalid_credential';
			}
		}
		else if ($this->getRequest()->isPost())
		{
			$this->view->identity = $params['identity'];
			$this->view->errorCode = 'invalid_credential';
		}
		else if (isset($_GET['identity']))
		{
			$this->view->operationResult = 'newPassword';
			$this->view->identity = $_GET['identity'];
		}
	}

	private function usersExist()
	{
		$count = 'SELECT COUNT(id) FROM users';
		return $this->db->fetchOne($count) > 0;
	}

	function lostpasswordAction()
	{
		$operationResult = 'tryAgain';

		if ($this->getRequest()->isPost())
		{
			$email = $_POST['email'];

			$userId =  'SELECT id
						FROM users
						WHERE email = ?';
			$userId = $this->db->fetchOne($userId, $email);

			if ($userId)
			{
				$data = array('userId'=>$userId);
				$activationUniqueId = $this->db->save("activations", $data);

				$this->view->recoveryLink = parent::getWebSiteURL() . "/login/newpassword?uid=$activationUniqueId";
				$htmlMessage = $this->view->render('emails/sendnewpassword.tpl');
				$smtpObject = Zend_Registry::get('config')->emails->sendNewPassword->subject;

				if (parent::sendEmailSmtpFromConfig($email, strip_tags($htmlMessage), $htmlMessage, $smtpObject) === 0) {
					$operationResult = 'emailSent';
				}
			}
			else
			{
				$operationResult = 'emailNotFound';
				$this->view->email = $email;
			}
		}
		else if (!empty($_REQUEST['invalidLink'])) {
				$operationResult = 'invalidLink';
		}

		$this->view->operationResult = $operationResult;
	}

	function newpasswordAction()
	{
		if ($this->getRequest()->isPost())
		{
			$uniqueid = $_POST['uniqueid'];
			$sql = 'SELECT userId FROM activations WHERE uniqueid = ?';

			$userId = $this->db->fetchOne($sql, $uniqueid);
			if ($userId)
			{
				$email = $this->db->fetchOne('SELECT email FROM users WHERE id = ?', $userId);

				if (!empty($_POST['password']) && $_POST['password'] == $_POST['passwordConfirm'])
				{
					$password = md5($_POST['password']);

					try {
						$this->db->beginTransaction();

						$where = array(
							'id = ?' => $userId
						);
						$this->db->update('users', array('password' => $password), $where);

						$where = array(
							'userId = ?' => $userId
						);
						$this->db->delete('activations', $where);

						$this->db->commit();
						$this->_redirect("/login/login?identity=$email");
					}
					catch (Exception $e) {
						$this->db->rollBack();
						$this->applogger->info($e->getMessage());
					}
				}
				else {
					$this->view->email = $email;
					$this->view->uniqueid = $uniqueid;
					$this->view->operationResult = 'wrongPassword';
				}
			}
			else $this->_redirect('/login/lostpassword?invalidLink=true');
		}
		else
		{
			$uniqueid = $_GET['uid'];
			$userId = $this->db->fetchOne('SELECT userId FROM activations WHERE uniqueid = ?', $uniqueid);

			if ($userId) {
				$email='SELECT email
						FROM users
						WHERE id = ?';
				$this->view->email = $this->db->fetchOne($email, $userId);
				$this->view->uniqueid = $uniqueid;
			}
			else $this->_redirect('/login/lostpassword?invalidLink=true');
		}
	}

	public function logoutAction()
	{
		$this->logout();
		$this->_redirect('/login/login');
	}

	private function logout()
	{
		Zend_Session::destroy();
	}

	private function getAccountStatus()
	{
		$customerUid = Zend_Registry::get('customerUid');
		$provisioningUrl = Zend_Registry::get('config')->parameters->provisioning_url;
		$url = $provisioningUrl . "/customers/checkaccountstatus?uid=$customerUid";

		$ch = curl_init($url);
		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// $output contains the output string
		$accountData = curl_exec($ch);
		// close curl resource to free up system resources
		curl_close($ch);

		$accountDataSplit = explode('*', $accountData);

		$days = $accountDataSplit[0];
		$tolerance = $accountDataSplit[1];
		$daysBefore = $accountDataSplit[2];
		$numberOfUsers = $accountDataSplit[3];
		$paymentType = $accountDataSplit[4];
		/**
		 *  0 = da attivare (ma non dovresti mai vederlo)
		 *  1 = attivo (il valore che troverai normalmente) 
		 * -2 = In attesa di cancellazione (il programma non deve aprirsi e magari deve mostrare un messaggio di utente cancellato)
		 * -3 = Utente cancellato (idem come sopra, ma questo pure lo troverai difficilmente, perché i record marcati a -3 vengono cancellati da un programma schedulato)
		 * -4 = Utente disabilitato (Devi mostrare il famoso messaggio di utente disabilitato, contattare gli amministratori).
		 */
		$status = $accountDataSplit[5];

		$account = parent::getAttribute(parent::$ACCOUNT);

		if($days == -999)
		{
			$account['status'] = 'notExist';
			parent::setAttribute(parent::$ACCOUNT, $account);
			$this->_redirect('/login/disabled');
		}
		if($status == -2 || $status == -3)
		{
			$account['status'] = 'deleted';
			parent::setAttribute(parent::$ACCOUNT, $account);
			$this->_redirect('/login/disabled');
		}
		else if($status == -4)
		{
			$account['status'] = 'disabled';
			parent::setAttribute(parent::$ACCOUNT, $account);
			$this->_redirect('/login/disabled');
		}
		else if($tolerance - $days < 0)
		{
			$account['status'] = 'expired';
			$account['paymentUrl'] = $provisioningUrl . "/customers/payment?uid=$customerUid";
			parent::setAttribute(parent::$ACCOUNT, $account);
			$this->_redirect('/login/disabled');
		}
		else 
		{
			$account['days'] = $days;
			$account['tolerance'] = $tolerance;
			$account['daysBefore'] = $daysBefore;
			$account['numberOfUsers'] = $numberOfUsers;
			/**
			 * 0 = Carta di credito
			 * 1 = Bonifico
			 */
			$account['type'] = $paymentType;
			$account['paymentUrl'] = $provisioningUrl . "/customers/payment?uid=$customerUid";
			parent::setAttribute(parent::$ACCOUNT, $account);
		}
	} 

	public function disabledAction()
	{
		/**
		 * 
		 * Parametro passato dal metodo getAccountStatus() della classe LoginController
		 * application_uid non presente = notExist
		 * -2 = deleted
		 * -4 = disabled
		 * tolerance - days = expired
		 * 
		 */
		$this->view->account = parent::getAttribute(parent::$ACCOUNT);
	}

	public function provisioningpaymentAction()
	{
		$account = parent::getAttribute(parent::$ACCOUNT);
		$this->logout();
		$this->_redirect($account['paymentUrl']);
	}
}