<?php
class TransfertsController extends CustomControllerAction
{
	private $startDateSearch = 'startDateSearch',
			$endDateSearch = 'endDateSearch';

	function init()
	{
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('customerName');
		parent::setDirectionDefault('ASC');
		parent::setSearchKey('searchTransfert');
		parent::setRedirect('/transferts/transferts');
	}

	public function transfertsAction() {
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$startDate = parent::getAttribute($this->startDateSearch);
		if ($startDate == NULL){
			$startDate = date('01/01/Y');
		}
		$endDate = parent::getAttribute($this->endDateSearch);
		if ($endDate == NULL){
			$endDate = date('31/12/Y');
		}

		$search = array(
			'startDateSearch' => $startDate,
			'startDateReset' => date('01/01/Y'),
			'endDateReset' => date('31/12/Y'),
			'endDateSearch' => $endDate,
			'searchTransfert' => parent::getAttribute(parent::getSearchKey())
		);
		$this->view->search = $search;

		$listParams = array_merge($listParams, $search);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	private function getFromCondition()
	{
		$fromCondition='FROM transferts t
						LEFT JOIN vehicles v ON v.id = t.vehicle_id
						LEFT JOIN users u ON u.id = t.user_id';
		return $fromCondition;
	}

	private function getWhereCondition($params)
	{
		$result = array();
		$whereCondition = 'WHERE 1=1 ';
		$data = array();
		$params = array_map('trim', $params);

		$startDate = $params['startDateSearch'];
		parent::setAttribute($this->startDateSearch, $startDate);

		if(!empty($startDate))
		{
			$startDate = parent::getDateInSQLFormat($startDate, '/');

			if(!empty($params['endDateSearch']))
			{
				$endDate = $params['endDateSearch'];
				parent::setAttribute($this->endDateSearch, $endDate);
				$endDate = parent::getDateInSQLFormat($endDate, '/');
				$whereCondition .= 'AND (t.date BETWEEN ? AND ?) ';
				array_push($data, $startDate, $endDate);
			}
			else
			{
				parent::setAttribute($this->endDateSearch, null);
				$whereCondition .= 'AND (t.date = ?) ';
				array_push($data, $startDate);
			}
		}
		else
		{
			parent::setAttribute($this->endDateSearch, null);
		}

		if(!empty($params['searchTransfert']))
		{
			$searchTransfert = $params['searchTransfert'];
			parent::setAttribute(parent::getSearchKey(), $searchTransfert);
			$whereCondition .= 'AND (t.customerName LIKE ? OR v.plate LIKE ? OR u.firstname LIKE ? OR u.surname LIKE ? OR t.kmTot LIKE ?) ';
			array_push($data, "%$searchTransfert%", "%$searchTransfert%", "%$searchTransfert%", "%$searchTransfert%", "%$searchTransfert%");
		}
		else parent::setAttribute(parent::getSearchKey(), null);

		$result['whereCondition'] = $whereCondition;
		$result['data'] = $data;
		return $result;
	}

	private function getOrderbyCondition($sortField, $sortDir)
	{
		$prefix = 't.';

		if($sortField == 'fullname')
		{
			$prefix = '';
		}
		else if($sortField == 'plate')
		{
			$prefix = 'v.';
		}

		return "ORDER BY $prefix$sortField $sortDir";
	}

	private function getTotal($fromCondition, $whereCondition, $orderbyCondition, $data)
	{
		$selectTotal = "SELECT SUM(CASE WHEN(diaria = 0) THEN (v.aciCoefficient * (t.kmTot * 2)) ELSE (v.aciCoefficient * (t.kmTot * 2)) + 46.48 END) AS total
		$fromCondition
		$whereCondition
		$orderbyCondition";
		return $this->db->fetchOne($selectTotal, $data);
	}

	private function getList($params)
	{
		$result = $this->getWhereCondition($params);
		$data = $result['data'];
		$whereCondition = $result['whereCondition'];
		$fromCondition = $this->getFromCondition();

		$sql = "SELECT COUNT(t.id)
				$fromCondition
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$orderbyCondition = $this->getOrderbyCondition($sortField, $sortDir);

		$sql = "SELECT DATE_FORMAT(t.date, '%d/%m/%Y') AS date, t.id, t.customerName, v.plate,
				CASE WHEN(diaria = 0) THEN (v.aciCoefficient * (t.kmTot * 2)) ELSE (v.aciCoefficient * (t.kmTot * 2)) + 46.48 END AS cost,
				CONCAT(u.surname, ' ', u.firstname) AS fullname, (t.kmTot * 2) AS kmTot, t.uniqueid
				$fromCondition
				$whereCondition
				$orderbyCondition
				LIMIT " . $page * $pageSize . ',' . $pageSize;
		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);

		$results['total'] = $this->getTotal($fromCondition, $whereCondition, $orderbyCondition, $data);

		return $results;
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	public function printlistAction()
	{
		$params = $_REQUEST;
		$fromCondition = $this->getFromCondition();
		$result = $this->getWhereCondition($params);

		$data = $result['data'];
		$whereCondition = $result['whereCondition'];

		$sortField = parent::getAttribute(parent::getActiveSortFieldKey());
		$sortDir = parent::getAttribute(parent::getActiveSortDirectionKey());

		$orderbyCondition = $this->getOrderbyCondition($sortField, $sortDir);

		$sql = "SELECT DATE_FORMAT(t.date, '%d/%m/%Y') AS date, t.customerName, v.plate,
				CONCAT(u.surname, ' ', u.firstname) AS fullname, (t.kmTot * 2),
				CASE WHEN(diaria = 0) THEN (v.aciCoefficient * (t.kmTot * 2)) ELSE (v.aciCoefficient * (t.kmTot * 2)) + 46.48 END AS cost,
				CASE WHEN(diaria = 1) THEN 46.48 ELSE 0 END AS diaria
				$fromCondition
				$whereCondition
				$orderbyCondition";
		$results = $this->db->fetchAll($sql, $data);

		$total = $this->getTotal($fromCondition, $whereCondition, $orderbyCondition, $data);

		$header = array('Data', 'Cliente', 'Veicolo', 'Utente', 'KmTot', 'Costo', 'Diaria');
		$footer = array('TOTALE', '', '', '', '', $total);
		$this->createCSV($results, $header, $footer, 'Trasferte');

		$this->_helper->viewRenderer->setNoRender();
	}

	public function updateAction()
	{
		$selectCustomers = 'SELECT id, company, address, zipcode, city, distanceKm
							FROM customers
							WHERE distanceKm > 0
							ORDER BY company ASC';
		$this->view->customers = $this->db->fetchAll($selectCustomers);

		$selectVehicle = 'SELECT id, plate, description
						  FROM vehicles
						  ORDER BY plate ASC';
		$this->view->vehicles = $this->db->fetchAll($selectVehicle);

		$selectUser = 'SELECT id, CONCAT(surname, " ", firstname) AS fullname
					   FROM users
					   ORDER BY fullname ASC';
		$this->view->users = $this->db->fetchAll($selectUser);

		if(!empty($_REQUEST['uid']))
		{
			$uniqueid = $_REQUEST['uid'];
			$selectTransfert = "SELECT t.id, DATE_FORMAT(t.date, '%d/%m/%Y') AS date, t.customerName, t.customerAddress, t.customerZipcode, 
								t.customerCity, t.vehicle_id AS currentVehicle, t.user_id AS currentUser, t.kmTot, t.uniqueid, t.diaria
								FROM transferts t
								WHERE uniqueid = ?";
			$transfert = $this->db->fetchRow($selectTransfert, $uniqueid);
		}
		else
		{
			$transfert['date'] = date('d/m/Y');
		}
		$this->view->transfert = array_map('htmlspecialchars', $transfert);
	}

	public function saveAction()
	{
		$transfert = $_POST;

		if(isset($transfert['diaria'])) {
			$transfert['diaria'] = 1;
		}

		$transfert['date'] = parent::getDateInSQLFormat($transfert['date'], '/');
		$this->db->save('transferts', $transfert);
		$this->_redirect(parent::getRedirect());
	}

	public function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$where['uniqueid = ?'] = $_REQUEST['uid'];
			$this->db->delete('transferts', $where);
			parent::updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}

	protected function createCSV($results, $header, $footer, $fileName)
	{
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Type: application/octet-stream charset=UTF-8');
		header('Content-Disposition: attachment; filename=' . $fileName . '.csv');
		header('Content-Transfer-Encoding: binary');

		$fp = fopen('php://output', 'w');
		fputcsv($fp, array_values($header), ';');
		foreach($results as $row)
		{
			fputcsv($fp, array_values($row), ';');
		}
		fputcsv($fp, array_values($footer), ';');
		fclose($fp);
	}
}