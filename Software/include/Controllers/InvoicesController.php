<?php
class InvoicesController extends ReferencesController
{
	function init()
	{
		parent::init();
		parent::setRedirect('/invoices/invoices');
		$this->folderPath = 'invoices/';
		$this->textMessage = 'send_invoice_text';
		$this->tableName = 'invoices';
		$this->tableMovements = 'movements';
		$this->columnId = 'invoice_id';
		$this->referredTo = 0;
		$this->titleReference = 'Fattura numero_';
		$this->destinationArray = array(
			'tableName' => 'creditnotes',
			'tableMovements' => 'movementscreditnotes',
			'columnName' => 'creditnote_id'
		);

		$request = $this->getRequest();
		$prefix = $request->getModuleName() . '-' . $request->getControllerName();
		$this->unpaid = $prefix . '-unpaid';
	}

	function invoicesAction() {
		$searchParams = array();
		if (parent::getAttribute($this->unpaid) == true) {
			$searchParams['searchUnpaid'] = true;
		}
		$this->reference($searchParams);
		$years = parent::getInvoicesYears();
		$this->view->years = $years;
	}

	protected function getList($params, $printList = false) {
		$whereCondition = 'WHERE (is_external = 0)';
		$data = array();
		$params = array_map('trim', $params);

		if (Zend_Auth::getInstance()->getIdentity()->role == 'user')
			$whereCondition .= ' AND (private <> 1) ';

		$startDate = $params['startDate'];
		parent::setAttribute($this->startDate, $startDate);

		if (!empty($startDate))
		{
			$startDate = parent::getDateInSQLFormat($startDate, '/');
			$data[] = $startDate;

			if (!empty($params['endDate']))
			{
				$endDate = $params['endDate'];
				parent::setAttribute($this->endDate, $endDate);
				$endDate = parent::getDateInSQLFormat($endDate, '/');
				$whereCondition .= ' AND (i.date BETWEEN ? AND ?)';
				$data[] = $endDate;
			}
			else
			{
				parent::setAttribute($this->endDate, null);
				$whereCondition .= ' AND (i.date >= ?)';
			}
		}
		else parent::setAttribute($this->endDate, null);

		if (isset($params['searchUnpaid']))
		{
			parent::setAttribute($this->unpaid, true);
			$whereCondition .= ' AND (i.paid_amount < i.amount)';
		}
		else parent::setAttribute($this->unpaid, false);

		if (!empty($params['searchSerialNumber']))
		{
			$serialNumber = trim($params['searchSerialNumber']);
			parent::setAttribute($this->serialNumber, $serialNumber);
			$whereCondition .= ' AND (i.serialNumber = ?)';
			$data[] = $serialNumber;
		}
		else parent::setAttribute($this->serialNumber, null);

		if (!empty($params['searchCustomer']))
		{
			$customer = $params['searchCustomer'];
			parent::setAttribute($this->customer, $customer);
			$whereCondition .= ' AND (i.customer_name LIKE ?)';
			$data[] = "%$customer%";
		}
		else parent::setAttribute($this->customer, null);

		$sql = "SELECT COUNT(i.id)
				FROM invoices i
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		if ($sortField == 'unpaid_amount')
			$sortField = '(i.amount - i.paid_amount)';
		else $sortField = 'i.' . $sortField;

		$sql = "SELECT DATE_FORMAT(i.date, '%d/%m/%Y') AS date, i.serialNumber, i.customer_name, i.amount, i.vat_amount, i.paid_amount,
				(i.amount - i.paid_amount) AS unpaid_amount, i.uniqueid, i.sent,
				CASE WHEN(i.paid_amount < i.amount) THEN 1 ELSE 0 END AS status, i.privatenotes, i.id
				FROM invoices i
				$whereCondition
				ORDER BY $sortField $sortDir, i.serialNumber DESC";

		if(!$printList)
		{
			$sql .= ' LIMIT ' . $page * $pageSize . ',' . $pageSize;
		}

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);

		$totals =  "SELECT
						IFNULL(SUM((i.amount - i.paid_amount)), 0) AS unpaid_amount,
						IFNULL(SUM(vat_amount), 0) AS vat_amount,
						IFNULL(SUM(amount), 0) AS amount
					FROM invoices i
					$whereCondition";
		$results['totals'] = $this->db->fetchRow($totals, $data);

		return $results;
	}

	protected function getSelectUpdate()
	{
		return parent::getSelectUpdate()
			. ', payments_term_description';
	}

	function updateAction()
	{
		$this->update(true);
	}

	function deleteAction() {
		if (!empty($_REQUEST['uid']))
		{
			$this->delete($_REQUEST['uid'], true);
			parent::updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}

	protected function getCheckReferenceFieldsResults($uniqueid, DateTime $date, $serialNumber, $invoiceON = false) {
		return parent::getCheckReferenceFieldsResults($uniqueid, $date, $serialNumber, true);
	}

	protected function getReference($uid)
	{
		$reference="SELECT id, uniqueid, DATE_FORMAT(date, '%d %M %Y') AS date,  DATE_FORMAT(expireDate, '%d %M %Y') AS expireDate, YEAR(date) AS year, serialNumber,
						amount, taxable, taxableFree, vat_amount, buyer, seller, notes, theme_id, customer_id, pdf, sent,
						payments_term_description, tax1, tax2, tax1amount, tax2amount, paid_amount, privatenotes, taxable + tax1amount AS totImponibile
					FROM $this->tableName
					WHERE uniqueid = ?";
		return $this->db->fetchRow($reference, $uid);
	}

	function savemovementAction() {
		if (isset($_POST['free'])) {
			$_POST['free'] = 1;
			$_POST['vat'] = 0;
		}
		else $_POST['free'] = 0;

		echo json_encode($this->savemovement(true));
	}
	
	function productpricesAction(){
		$this->applogger->info(print_r($_POST,1));
		
		$amount = $this->db->fetchOne('SELECT amount FROM product_prices WHERE id_product = ? AND id_list = ?', array($_POST['id_product'],$_POST['id_list']));
		
		$this->applogger->info($amount);
		
		if(empty($amount)){
			$amount = -1;
		}
		
		echo json_encode($amount);
	}
	
	function customerproductpricesAction(){
		$this->applogger->info(print_r($_POST,1));
		
		$idlist = $this->db->fetchOne('SELECT id_list FROM customers WHERE id = ? ', array($_POST['customer_id']));
		if($idlist){
			$amount = $this->db->fetchOne('SELECT amount FROM product_prices WHERE id_product = ? AND id_list = ?', array($_POST['id_product'],$idlist));
		}
		
		//$this->applogger->info($amount);
		
		if(empty($amount)){
			$amount = -1;
		}
		
		echo json_encode($amount);
	}

	function deletemovementAction() {
	    //aggiungere delete
	    $this->applogger->info(print_r($_POST,1));

	    $whereMov['uniqueid_invoices = ?'] = $_POST['referenceUid'];
	    $whereMov['uniqueid_movements = ?'] = $_POST['uid'];
    	$this->db->delete('productmovements', $whereMov);
	    //fine

		echo json_encode($this->deletemovement(true));
	}

	protected function view($noGetMsg = false) {
		parent::view($noGetMsg);
		$reference = array_map('htmlspecialchars', $this->getReference($_REQUEST['uid']));
		$paymentsSelect = 'SELECT p.description, p.amount, DATE_FORMAT(p.date, "%d/%m/%Y") AS date, (i.amount - i.paid_amount) AS unpaid_amount, p.uniqueid
						   FROM payments p LEFT JOIN explanations e ON e.payment_id = p.id INNER JOIN invoices i ON i.id = e.invoice_id
						   WHERE e.invoice_id = ?';
		$this->view->payments = $this->db->fetchAll($paymentsSelect, $reference['id']);
	}

	public function printlistAction()
	{
		$results = array();
		$parameters = Zend_Registry::get('config')->parameters;
		parent::initDomPdf();
		$search = $_REQUEST;
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());
		$params = array_merge($listParams, $search);

		$invoices = $this->getList($params, true);

		foreach ($invoices['currentPage'] as $invoice) {
			$selectExplanations = 'SELECT e.id, e.invoice_id, e.payment_id, e.amount, DATE_FORMAT(p.date, "%d/%m/%Y") AS date
								   FROM explanations e INNER JOIN payments p ON p.id = e.payment_id
								   WHERE e.invoice_id = ?';
			$explanations = $this->db->fetchAll($selectExplanations, $invoice['id']);
			$invoice['explanations'] = $explanations;
			array_push($results, $invoice);
		}
		$this->view->invoices = $results;

		$dompdf = new DOMPDF();
		$dompdf->load_html($this->view->render('invoices/printlist.tpl'));
		$dompdf->render();

		$pdfFilename = 'Fatture_emesse.pdf';
		$pdfPath = $parameters->uploads_path . $pdfFilename;
		$dompdf->stream($pdfFilename, array('Attachment' => 1));
		$this->_helper->viewRenderer->setNoRender();
	}

	function quickinvoiceaddAction()
	{
		$this->quickInvoiceAdd();
	}

	function quickinvoicesaveAction()
	{
		$this->quickInvoiceSave(0);
	}

	function generatezipAction()
	{
		// Chiudiamo la sessione per permettere al browser di eseguire un'altra request in parallelo
		Zend_Session::writeClose();
		$year = $_REQUEST['year'];
		$result = null;

		set_time_limit(0);

		$invoicesPath = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath;
		$filename = 'fatture_' . $year . '.zip';
		$zipName = $invoicesPath . '/' . $filename;

		try {
			$zip = new ZipArchive();
			$zip->open($zipName, ZIPARCHIVE::CREATE);

			$selectInvoices = 'SELECT uniqueid, pdf FROM invoices WHERE is_external = 0 AND YEAR(date) = ?';
			$invoices = $this->db->fetchAll($selectInvoices, $year);

			foreach ($invoices as $item)
			{
				if (empty($item['pdf'])) {
					$resultPdf = $this->preparePdfData($item['uniqueid']);
					$pdfFilename = $resultPdf['pdfFilename'];
				}
				else {
					$pdfFilename = $item['pdf'];
				}

				$zip->addFile($invoicesPath . $pdfFilename, $pdfFilename);
			}

			$zip->close();
			$result = array(
				'path' => HOME_DIRECTORY . self::UPLOAD_FOLDER . $this->folderPath . $filename,
				'filename' => $filename
			);
		}
		catch(Exception $e) {
			$this->applogger->info($e->getMessage());
		}

		echo json_encode($result);
	}

	function getzipstatusAction()
	{
		$year = $_REQUEST['year'];
		$tot = intval($this->db->fetchOne('SELECT COUNT(id) FROM invoices WHERE is_external = 0 AND YEAR(date) = ?', $year));
		$remain = intval($this->db->fetchOne('SELECT COUNT(id) FROM invoices WHERE is_external = 0 AND YEAR(date) = ? AND pdf IS NULL', $year));

		$result = array(
			'tot' => $tot,
			'remain' => $remain
		);

		echo json_encode($result);
	}

	protected function getSelectForMovementData() {
		return parent::getSelectForMovementData()
		. ', free, product_id';
	}
	
	function getzipAction()
    {
    	$pathtofile = $_REQUEST['href'];
    	$filename = $_REQUEST['filename'];
    	if (file_exists($pathtofile)) {
    		$finfo = finfo_open(FILEINFO_MIME_TYPE);
    		header("Cache-Control: no-cache");
    		header_remove("Pragma");
			header("Content-Description: File Transfer");
			header("Content-type:   ".finfo_file($finfo, $pathtofile));
			header('Content-Disposition: inline; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Content-length: '.filesize($pathtofile));
			header("X-Sendfile: $pathtofile");
			ob_clean();
			flush();
			readfile($pathtofile);
			exit;
    	}
		$this->_helper->viewRenderer->setNoRender();
	}
}