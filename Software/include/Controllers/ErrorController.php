<?php
class ErrorController extends Zend_Controller_Action
{
	public function errorAction()
	{
		if (APPLICATION_ENV === 'development')
		{
			$errors = $this->_getParam('error_handler');

			switch ($errors->type) {
				case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
				case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
					$this->getResponse()->setHttpResponseCode(404);
					$this->view->message = 'Pagina non trovata';
					break;
				default:
					$this->getResponse()->setHttpResponseCode(500);
					$this->view->message = 'Errore dell\'applicazione';
					break;
			}

			$this->view->exceptionMessage = $errors->exception->getMessage();
			$this->view->exceptionTrace = $errors->exception->getTraceAsString();
			$this->view->request = var_dump($errors->request, true);
		}
		else
		{
			echo 'Error';
			$this->_helper->viewRenderer->setNoRender();
		}
	}
}