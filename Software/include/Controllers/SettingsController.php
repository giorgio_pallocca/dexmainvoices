<?php
class SettingsController extends CustomControllerAction
{
	public function updateAction()
	{
		if (isset($_REQUEST['saved'])) {
			$this->view->confirmMessage = 'saved';
		}

		$parameters = array(
			'send_invoice_text',
			'send_reminder_text',
			'send_quotation_text',
			'send_transportdocument_text',
			'send_creditnote_text',
			'lists_rows'
		);

		foreach($parameters as &$item) {
			$parameters[$item] = parent::getParameter($item);
		}

		$this->view->params = $parameters;
	}

	public function saveAction()
	{
		$settings = $_POST;

		foreach ($settings as $key => $value) {
			$where = array();
			$where['parameter_name = ?'] = $key;
			$this->db->update('parameters', array('parameter_value' => $value), $where);
		}
		$this->_redirect('/settings/update?saved=true');
	}
}