<?php
class TransportdocumentsController extends ReferencesController
{
	function init()
	{
		parent::init();
		parent::setRedirect('/transportdocuments/transportdocuments');
		$this->folderPath = 'transportdocuments/';
		$this->textMessage = 'send_transportdocument_text';
		$this->tableName = 'transportdocuments';
		$this->tableMovements = 'movementstransportdocuments';
		$this->columnId = 'transportdocument_id';
		$this->referredTo = 2;
		$this->titleReference = 'Documento di trasporto numero_';
		$this->destinationArray = array(
			'tableName' => 'invoices',
			'tableMovements' => 'movements',
			'columnName' => 'invoice_id'
		);

		$this->isDDT = true;
	}

	protected function getSelectForMovementData() {
		return 'SELECT uniqueid, uniqueid AS movementUniqueid, quantity, unit_id, description AS descriptionMovement, price, vat, product_id';
	}

	protected function getReference($uid)
	{
		$reference="SELECT id, uniqueid, DATE_FORMAT(date, '%d %M %Y') AS date, DATE_FORMAT(expireDate, '%d %M %Y') AS expireDate, YEAR(date) AS year, serialNumber,
						amount, taxable, vat_amount, buyer, seller, customer_id, pdf, notes, theme_id, sent,
						transportBy, appearance, numberPackage, weight, vectorData, dateTimeTransport, taxable AS totImponibile
					FROM $this->tableName
					WHERE uniqueid = ?";
		return $this->db->fetchRow($reference, $uid);
	}

	function transportdocumentsAction() {
		$this->reference();
	}

	protected function getSelectUpdate()
	{
		return 'SELECT uniqueid, serialNumber, DATE_FORMAT(date, "%d/%m/%Y") AS date, customer_id,
				DATE_FORMAT(expireDate, "%d/%m/%Y") AS expireDate, notes, transportBy, appearance,
				numberPackage, weight, vectorData, DATE_FORMAT(dateTimeTransport, "%d/%m/%Y") AS dateTimeTransport,
				DATE_FORMAT(dateTimeTransport, "%H") AS hours, DATE_FORMAT(dateTimeTransport, "%i") AS minutes, private, privatenotes';
	}

	function updateAction()
	{
		$this->update(false, true);
	}

	function saveAction()
	{
		$reference = $_POST;

		if (!empty($reference['dateTimeTransport'])) {
			$reference['dateTimeTransport'] = parent::getDateInSQLFormat($reference['dateTimeTransport'], '/')
												. ' ' . $reference['hours'] . ':' . $reference['minutes'] . ':00';
		}
		else unset($reference['dateTimeTransport']);

		unset($reference['hours']);
		unset($reference['minutes']);
		$this->save($reference, false);
	}

	function savemovementAction() {
		echo json_encode($this->savemovement(false, true));
	}

	function deletemovementAction() {
		echo json_encode($this->deletemovement(false, true));
	}
}