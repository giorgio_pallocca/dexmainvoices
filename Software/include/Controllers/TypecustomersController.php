<?php
class TypecustomersController extends CustomControllerAction
{
	protected $searchName;

	function init()
	{
		parent::init();
		$this->setListSessionKeys();
		$this->setFieldDefault('id');
		$this->setDirectionDefault('ASC');

		$request = $this->getRequest();
		$prefix = $request->getModuleName() . '-' . $request->getControllerName();

		$this->searchName = "$prefix-name";
	}

	protected function typecustomersAction($searchParams = array())
	{
		$listParams = $this->initList($this->getActivePageKey(), $this->getActiveSortFieldKey(),
					  $this->getFieldDefault(), $this->getActiveSortDirectionKey(), $this->getDirectionDefault());

		$searchParams['searchName'] = $this->getAttribute($this->searchName);

		$this->view->search = $searchParams;

		$listParams = array_merge($listParams, $searchParams);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS | JSON_HEX_AMP);
	}

	protected function getList($params)
	{
		$data = array();
		$params = array_map('trim', $params);
		$whereCondition = $this->getWhereCondition($params, $data);
		$joins = $this->getJoinsForList();

		$totalRows = $this->db->fetchOne("SELECT COUNT(l.id) FROM list l $joins $whereCondition", $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		$this->saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		if ($sortField == 'id')
			$sortField = 'l.' . $sortField;

		$select = $this->getSelectForList();

		$sql = "$select
				FROM list l
				$joins
				$whereCondition
				ORDER BY $sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);
		return $results;
	}

	protected function getSelectForList()
	{
		return 'SELECT l.id, l.customer_type';
	}

	protected function getJoinsForList()
	{
		return '';
	}

	protected function getWhereCondition($params, &$data)
	{
		$whereCondition = 'WHERE 1=1';

		if (!empty($params['searchName'])) {
			$searchName = $params['searchName'];
			$this->setAttribute($this->searchName, $searchName);
			$whereCondition .= ' AND (l.customer_type LIKE ?)';
			array_push($data, "%$searchName%");
		}
		else {
			$this->setAttribute($this->searchName, null);
		}

		return $whereCondition;
	}

	function listAction()
	{
		echo json_encode($this->getList($_REQUEST));
	}

	function getrowdataAction()
	{
		$result = null;
		$this->applogger->info(print_r($_REQUEST,1));

		if (!empty($_REQUEST['uId'])) {
			$row = $this->db->fetchRow('SELECT l.id AS typecustomersId, l.customer_type AS typecustomersCustomer_type
								FROM list l
								WHERE id = ?', $_REQUEST['uId']);
			//unset($row['id']);
			//$this->applogger->info(print_r($row,1));
			$result['form'] = $row;
		}

		echo json_encode($result);
	}
	
	function saveAction()
	{
		$result = array(
				'error' => true
		);
		$typecustomersId = $_REQUEST['typecustomersId'];
	
		$list = array(
			'customer_type' => $_REQUEST['typecustomersCustomer_type'],
		);
	
		try {
			$this->db->beginTransaction();
	
			if (!empty($typecustomersId)) {
				$list['id'] = $typecustomersId;
			}
	
			if (!empty($typecustomersId)) {
				$where = array('id = ?' => $typecustomersId);
				$this->db->update('list', $list, $where);
			}else{
				$this->db->insert('list', $list);
			}
	
			$this->db->commit();
			$result['error'] = false;
		}
		catch(Exception $e) {
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
		}
	
		echo json_encode($result);
	}

	function deleteAction()
	{
		$result = null;

		if (!empty($_REQUEST['id'])) {
			try {
				$where = array('id = ?' => $_REQUEST['id']);
				$this->db->delete('list', $where);

				$this->updateActivePageAfterDelete();
				$result = $this->getAttribute($this->getActivePageKey());
			}
			catch(Exception $e) {
				$this->applogger->info($e->getMessage());
			}
		}
		echo json_encode($result);
	}
	
}