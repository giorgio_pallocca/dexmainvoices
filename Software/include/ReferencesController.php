<?php
abstract class ReferencesController extends CustomControllerAction
{
	protected $isDDT = false;

	function init() {
		parent::init();
		parent::setListSessionKeys();
		parent::setFieldDefault('date');
		parent::setDirectionDefault('DESC');

		$request = $this->getRequest();
		$prefix = $request->getModuleName() . '-' . $request->getControllerName();

		$this->startDate = $prefix . '-startDate';
		$this->endDate = $prefix . '-endDate';
		$this->serialNumber = $prefix . '-serialNumber';
		$this->customer = $prefix . '-customer';
		$this->code = $prefix . '-code';
		$this->status = $prefix . '-status';
		$this->controller = $request->getControllerName();
	}

	protected function reference($searchParams = array())
	{
		$listParams = parent::index(parent::getActivePageKey(), parent::getActiveSortFieldKey(),
					  parent::getFieldDefault(), parent::getActiveSortDirectionKey(), parent::getDirectionDefault());

		$startDateReset = date('01/01/Y');
		$endDateReset = date('31/12/Y');

		$searchParams['startDate'] = parent::getAttribute($this->startDate);
		if ($searchParams['startDate'] == NULL){
			$searchParams['startDate'] = $startDateReset;
		}
		$searchParams['endDate'] = parent::getAttribute($this->endDate);
		if ($searchParams['endDate'] == NULL){
			$searchParams['endDate'] = $endDateReset;
		}
		$searchParams['searchSerialNumber'] = parent::getAttribute($this->serialNumber);
		$searchParams['searchCustomer'] = parent::getAttribute($this->customer);
		$searchParams['searchCode'] = $this->getAttribute($this->code);
		$searchParams['startDateReset'] = $startDateReset;
		$searchParams['endDateReset'] = $endDateReset;

		$this->view->search = $searchParams;

		$listParams = array_merge($listParams, $searchParams);
		$this->view->list = json_encode($this->getList($listParams), JSON_HEX_APOS);
	}

	protected function getList($params) {
		$whereCondition = 'WHERE 1=1';
		$data = array();
		$params = array_map('trim', $params);

		if (Zend_Auth::getInstance()->getIdentity()->role == 'user')
			$whereCondition .= ' AND (private <> 1) ';

		$startDate = $params['startDate'];
		parent::setAttribute($this->startDate, $startDate);

		if (!empty($startDate))
		{
			$startDate = parent::getDateInSQLFormat($startDate, '/');
			$data[] = $startDate;

			if (!empty($params['endDate']))
			{
				$endDate = $params['endDate'];
				parent::setAttribute($this->endDate, $endDate);
				$endDate = parent::getDateInSQLFormat($endDate, '/');
				$whereCondition .= ' AND (ref.date BETWEEN ? AND ?)';
				$data[] = $endDate;
			}
			else
			{
				parent::setAttribute($this->endDate, null);
				$whereCondition .= ' AND (ref.date >= ?)';
			}
		}
		else parent::setAttribute($this->endDate, null);

		if (!empty($params['searchSerialNumber']))
		{
			$serialNumber = trim($params['searchSerialNumber']);
			parent::setAttribute($this->serialNumber, $serialNumber);
			$whereCondition .= ' AND (ref.serialNumber = ?)';
			$data[] = $serialNumber;
		}
		else parent::setAttribute($this->serialNumber, null);

		if (!empty($params['searchCustomer']))
		{
			$customer = $params['searchCustomer'];
			parent::setAttribute($this->customer, $customer);
			$whereCondition .= ' AND (ref.customer_name LIKE ?)';
			$data[] = "%$customer%";
		}
		else parent::setAttribute($this->customer, null);

		if (!empty($params['searchCode']))
		{
			$code = $params['searchCode'];
			$this->setAttribute($this->code, $code);
			$whereCondition .= ' AND (ref.code LIKE ?)';
			$data[] = "%$code%";
		}
		else $this->setAttribute($this->code, null);

		if (!empty($params['is_external'])) {
			$whereCondition .= ' AND (ref.is_external = 1)';
		}

		if (isset($params['searchStatus'])) {
			$status = $params['searchStatus'];
			parent::setAttribute($this->status, $status);
			if ($status > 0)
			{
				$whereCondition .= ' AND (ref.status = ?)';
				if($status == 1)
					$data[] = 'activated';
				else $data[] = 'draft';
			}
		}

		$sql = "SELECT COUNT(ref.id)
				FROM $this->tableName ref
				$whereCondition";
		$totalRows = $this->db->fetchOne($sql, $data);

		$page = $params['page'];
		$sortField = $params['sortColumn'];
		$sortDir = $params['sortOrder'];
		$pageSize = $params['pageSize'];
		parent::saveGridParameters($totalRows, $pageSize, $page, $sortField, $sortDir);

		$sql = $this->getSelectForList()
			. " FROM $this->tableName ref
				$whereCondition
				ORDER BY ref.$sortField $sortDir
				LIMIT " . $page * $pageSize . ',' . $pageSize;

		$results['totalRows'] = $totalRows;
		$results['currentPage'] = $this->db->fetchAll($sql, $data);

		if ('externalinvoices' == $this->controller) {
			$totals = "
				SELECT IFNULL(SUM((ref.amount - ref.paid_amount)), 0) AS unpaid_amount, IFNULL(SUM(vat_amount), 0) AS vat_amount, IFNULL(SUM(amount), 0) AS amount
				FROM $this->tableName ref
				$whereCondition";
			$results['totals'] = $this->db->fetchRow($totals, $data);			
		}
		
		return $results;
	}

	protected function getSelectForList() {
		return 'SELECT DATE_FORMAT(ref.date, "%d/%m/%Y") AS date, ref.serialNumber, ref.customer_name, ref.amount, ref.vat_amount, ref.uniqueid, ref.sent, ref.privatenotes, ref.id';
	}

	function listAction() {
		echo htmlspecialchars(json_encode($this->getList($_REQUEST)), ENT_NOQUOTES);
	}

	function viewAction() {
		$this->view();
		$params = Zend_Registry::get('config')->parameters;
		if ($filename = parent::getParameter('company_logo'))
		{
			$logoPath = $params->logo->folder . $filename;
			if (file_exists($params->uploads_path . $logoPath)) {
				$this->view->logoPath = $logoPath;
			}
		}
	}

	protected function view($noGetMsg = false)
	{
		$this->view->products = parent::getProductsList();
		$this->view->customertypes = parent::getcustomertype();
		$this->view->listPrice = parent::getlistPrice();
		$units = 'SELECT id, description, plural
				  FROM units';
		$this->view->units = $this->db->fetchAll($units);

		if (!empty($_REQUEST['uid'])) {
			$reference = array_map('htmlspecialchars', $this->getReference($_REQUEST['uid']));
			$this->view->reference = $reference;
			if ($noGetMsg == false) {
				//externalinvoices and recurringinvoices non hanno messaggi e $noGetMsg per loro è true, in tutti gli altri casi è false
				$this->view->messages = $this->getUserMessages($reference['customer_id'], $reference['id']);
			}
		}
	}

	function deleteAction()
	{
		if (!empty($_REQUEST['uid']))
		{
			$this->delete($_REQUEST['uid']);
			$this->updateActivePageAfterDelete();
		}
		$this->_redirect(parent::getRedirect());
	}

	protected function delete($uniqueid, $invoiceON = false)
	{
		$reference="SELECT id, serialNumber, YEAR(date) AS year, pdf
					FROM $this->tableName
					WHERE uniqueid = ?";
		$reference = $this->db->fetchRow($reference, $uniqueid);

		try {
			$this->applogger->info(print_r($uniqueid,1));
		    $this->db->beginTransaction();
			$this->db->delete($this->tableName, array('id = ?' => $reference['id']));
			$this->db->delete($this->tableMovements, array("$this->columnId = ?" => $reference['id']));
			$this->db->delete('productmovements', array("uniqueid_invoices = ?" => $uniqueid));
			if ($invoiceON) {
				$this->db->delete('explanations', array('invoice_id = ?' => $reference['id']));
			}
			$this->db->commit();

			$pdfFilename = $this->titleReference . $reference['serialNumber'] . '_' . $reference['year']. '.pdf';
			$referencePath = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath;
			$referencePath .= $pdfFilename;
			if (file_exists($referencePath)) {
				unlink($referencePath);
			}

// 			if (!empty($reference['pdf'])) {
// 				$referencePath = Zend_Registry::get('config')->parameters->uploads_path . $this->folderPath;
// 				$referencePath .= $reference['pdf'];
// 				if (file_exists($referencePath)) {
// 					unlink($referencePath);
// 				}
// 			}
		} catch (Exception $e) {
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
		}
	}

	function updateAction() {
		$this->update();
	}

	protected function getSelectUpdate()
	{
		return 'SELECT uniqueid, serialNumber, DATE_FORMAT(date, "%d/%m/%Y") AS date, customer_id,
				DATE_FORMAT(expireDate, "%d/%m/%Y") AS expireDate, notes, tax1, tax2, private, privatenotes';
	}

	protected function getAdditionalNotes()
	{
		return $this->db->fetchAll('SELECT id, title, description FROM additionalnotes WHERE referredto = ?', $this->referredTo);
	}

	protected function getDefaultNote()
	{
		return $this->db->fetchOne('SELECT description FROM additionalnotes WHERE referredto = ? AND isdefault = 1', $this->referredTo);
	}

	protected function update($invoiceON = false, $ddtON = false)
	{
		if(!empty($_REQUEST['customerId'])) {
			$this->view->customerId = $_REQUEST['customerId'];
		}
		$this->view->customers = parent::getCustomers();
		$this->view->additionalnotes = $this->getAdditionalNotes();

		$defaultPayment = parent::getParameter('company_default_payment');

		if (!empty($_REQUEST['uid']))
		{
			$uid = $_REQUEST['uid'];

			$select = $this->getSelectUpdate();
			$referenceSelect = "$select
								FROM $this->tableName
								WHERE uniqueid = ?";
			$reference = $this->db->fetchRow($referenceSelect, $uid);
		}
		else
		{
			$reference = array(
				'date' => date('d/m/Y')
			);

			$lastreference = $this->getlastreference($this->tableName, $invoiceON);

			if ($lastreference) {
				$reference['serialNumber'] = $lastreference['serialNumber'] + 1;
			}
			else $reference['serialNumber'] = 1;

			if ($invoiceON) {
				$reference['payments_term_description'] = $defaultPayment;
			}

			$reference['tax1'] = parent::getParameter('company_tax1');
			$reference['tax2'] = parent::getParameter('company_tax2');

			$reference['notes'] = $this->getDefaultNote();
		}

		if ($invoiceON) $reference['default_payments_term_description'] = $defaultPayment;

		if (!$ddtON) {
			$this->view->defaultTax1 = parent::getParameter('company_tax1');
			$this->view->defaultTax2 = parent::getParameter('company_tax2');
		}

		$this->view->reference = array_map('htmlspecialchars', $reference);
	}

	protected function getlastreference($tableName, $toInvoice = false)
	{
		$reference =   "SELECT serialNumber, DATE_FORMAT(r.date, '%d/%m/%Y') AS date
						FROM $tableName r
						WHERE ";
		if ($toInvoice) $reference .=  'is_external = 0 AND ';
		$reference .=  'YEAR(date) = YEAR(CURDATE())
						ORDER BY r.date DESC, serialNumber DESC
						LIMIT 1';
		return $this->db->fetchRow($reference);
	}

	public function exportreferenceAction()
	{
		$this->exportreference();
	}

	protected function exportreference()
	{
		$paymentDefault = parent::getParameter('company_default_payment');
		$toDDT = isset($_REQUEST['DDT']);

		//OLD reference
		$selectOldReference = 'SELECT id, customer_id, notes, customer_name, buyer, seller, amount, vat_amount,
								taxable, theme_id';

		if (!$this->isDDT && !$toDDT) $selectOldReference .= ', tax1amount, tax2amount, tax1, tax2, taxableFree';

		$selectOldReference .= " FROM $this->tableName WHERE uniqueid = ?";
		$oldReference = $this->db->fetchRow($selectOldReference, $_REQUEST['uniqueid']);

		$referenceId = $oldReference['id'];

		//OLD movements
		$oldMovements = $this->getMovements($referenceId);

		//NEW reference
		$referenceToSave = $oldReference;

		if ($toDDT) {
			$this->destinationArray = $this->destinationArrayDDT;
			$this->updateTotalAfterTransformation($referenceToSave, $oldMovements);
		}

		$referenceToSave['date'] = parent::getDateInSQLFormat(date('d/m/Y'), '/');

		$toInvoice = $this->destinationArray['tableName'] == 'invoices';

		$lastreference = $this->getlastreference($this->destinationArray['tableName'], $toInvoice);

		if ($lastreference) {
			$referenceToSave['serialNumber'] = $lastreference['serialNumber'] + 1;
		}
		else $referenceToSave['serialNumber'] = 1;

		if($toInvoice && $paymentDefault != '')
		{
			$referenceToSave['payments_term_description'] = $paymentDefault;
		}

		unset($referenceToSave['id']);
		$destinationTableName = $this->destinationArray['tableName'];

		try {
			$this->db->beginTransaction();
			$uid = $this->db->save($this->destinationArray['tableName'], $referenceToSave);
			$selectNewReferenceId ="SELECT id
									FROM $destinationTableName
									WHERE uniqueid = ?";
			$newReferenceId = $this->db->fetchOne($selectNewReferenceId, $uid);
			foreach ($oldMovements as $item) {
				$newMovement = array(
					'quantity' => $item['quantity'],
					'description' => $item['description'],
					'price' => $item['price'],
					'vat' => $item['vat'],
					$this->destinationArray['columnName'] => $newReferenceId
				);
				if (!$toDDT) {
					$newMovement['free'] = isset($item['free'])? $item['free'] : 0;
				}
				$this->db->save($this->destinationArray['tableMovements'], $newMovement);
			}
			$this->db->commit();
		} catch (Exception $e) {
			$this->db->rollBack();
			$this->applogger->info($e->getMessage());
		}

		$this->_redirect("/$destinationTableName/view?uid=$uid");
	}

	private function updateTotalAfterTransformation(&$reference, &$movements) {
		$amount = $vatAmount = $taxable = 0.00;
		foreach ($movements as $item) {
			$taxable += $item['quantity'] * $item['price'];
			$vatAmount += $item['quantity'] * $item['price'] * $item['vat'] / 100;
		}
		$reference['taxable'] = $taxable;
		$reference['vat_amount'] = $vatAmount;
		$reference['amount'] = $taxable + $vatAmount;
	}

	protected function save($reference = array(), $updateTotal = true, $quickInvoice = false)
	{
		$reference['date'] = parent::getDateInSQLFormat($reference['date'], '/');
		$reference['pdf'] = null;

		if (!empty($reference['expireDate']))
			$reference['expireDate'] = parent::getDateInSQLFormat($reference['expireDate'], '/');
		else $reference['expireDate'] = null;

		$customer ='SELECT c.company, c.address, c.zipcode, c.city, c.state, c.vatNumber, c.company, c.fiscalCode
					FROM customers c
					WHERE id = ?';
		$customer = $this->db->fetchRow($customer, $reference['customer_id']);

		$reference['customer_name'] = $customer['company'];

		$city = $customer['city'];
		if (!empty($customer['state']))
			$city .= ' (' . $customer['state'] . ') ';
		$city .= $customer['zipcode'];

		$buyer = array(
			$customer['company'],
			$customer['address'],
			$city
		);

		if (!empty($customer['vatNumber'])) {
			$buyer[] = 'PI: ' . $customer['vatNumber'];
		}

		if (!empty($customer['fiscalCode'])) {
			$buyer[] = 'CF: ' . $customer['fiscalCode'];
		}

		$phone = parent::getParameter('company_phone');
		if (!empty($phone)) {
			$phone = 'Tel: ' . $phone;
		}

		$heading = parent::getParameter('company_heading');
		if (!empty($heading)) {
			$seller = $heading;
		}
		else {
			$seller = array(
				parent::getParameter('company_name'),
				parent::getParameter('company_address'),
				parent::getParameter('company_city') . ' (' . parent::getParameter('company_state') . ') ' . parent::getParameter('company_cap'),
				'PI: ' . parent::getParameter('company_vat'),
				parent::getParameter('company_email'),
				$phone,
				parent::getParameter('company_website')
			);

			$seller = implode(PHP_EOL, array_filter($seller));
		}

		if (empty($reference['is_external']))
		{
			$reference['buyer'] = implode(PHP_EOL, array_filter($buyer));
			$reference['seller'] = $seller;
		}
		else
		{
			$reference['buyer'] = $seller;
			$reference['seller'] = implode(PHP_EOL, array_filter($buyer));
		}

		if ($updateTotal) {
			if (!isset($_POST['tax1'])) {
				$reference['tax1'] = null;
			}
			if (isset($_POST['tax2'])) {
				if ($_POST['tax2'] == 20.00) {
					$reference['tax2'] = Zend_Registry::get('config')->parameters->tax2_1;
				}
				else if($_POST['tax2'] == 11.50){
				    $reference['tax2'] = Zend_Registry::get('config')->parameters->tax2_2;
				}	
			} else $reference['tax2'] = 0;
		}

		if (isset($_POST['private'])) {
			$reference['private'] = 1;
		} else $reference['private'] = 0;

		$uid = $this->db->save($this->tableName, $reference);

		if ($updateTotal) {
			$this->updateTotal($uid);
		}

		if ($quickInvoice) {
			return $uid;
		}
		else {
			$controllerName = $this->getRequest()->getControllerName();
			$this->_redirect("/$controllerName/view?uid=$uid");
		}
	}

	function saveAction()
	{
		$this->save($_POST, true);
	}

	protected function calculateTaxable($referenceId, $movementUid = NULL, $free, $ddtON = false)
	{
		$select = "SELECT IFNULL(SUM(price * quantity), 0.00) FROM $this->tableMovements WHERE $this->columnId = ?";
		if (!$ddtON)
			$select .= " AND free = $free";

		$data = array($referenceId);

		if ($movementUid !== NULL) {
			$select .= ' AND uniqueid <> ?';
			$data[] = $movementUid;
		}
		return $this->db->fetchOne($select, $data);
	}

	protected function calculateVat($referenceId, $movementUid = NULL, $ddtON)
	{
		$select = "SELECT IFNULL(SUM((price * quantity) * (vat/100)), 0.00) FROM $this->tableMovements WHERE $this->columnId = ?";
		if (!$ddtON)
			$select .= ' AND free = 0';
		$data = array($referenceId);

		if ($movementUid !== NULL) {
			$select .= ' AND uniqueid <> ?';
			$data[] = $movementUid;
		}

		return $this->db->fetchOne($select, $data);
	}

	protected function calculateTotals($referenceUid, $taxable = NULL, $taxableFree = NULL, $vat = NULL, $ddtON = false)
	{
		$select = 'SELECT id';

		if (!$ddtON) {
			$select .= ', tax1, tax2';
		}

		$reference=$this->db->fetchRow("$select
										FROM $this->tableName
										WHERE uniqueid = ?", $referenceUid);

		if ($taxable == NULL) $taxable = $this->calculateTaxable($reference['id'], NULL, 0, $ddtON);
		if ($taxableFree == NULL && !$ddtON) $taxableFree = $this->calculateTaxable($reference['id'], NULL, 1, $ddtON);
		if ($vat == NULL) $vat = $this->calculateVat($reference['id'], NULL, $ddtON);

		$newTax1 = $newTax2 = $more_taxable = $perc = $perc_value = $extra = 0.00;

		// Calcoliamo la rivalsa previdenziale (con o senza cassa)
		if (!empty($reference['tax1'])) {
			list($taxdescription,$perc,$extra) = explode('---', $reference['tax1']);
			// Aumenta l'iva in forma proporzionale
			$vat = $vat + ($vat * $perc / 100);
			// Calcoliamo l'aumento di valore dell'imponibile per eventuale detrazione e calcolo versamento
			$more_taxable = $taxable * $perc / 100;
			// Calcolo importo tassa previdenziale
			$newTax1 = $taxable * $perc / 100;
		}

		// Calcoliamo la ritenuta in forma diversa se c'è la rivalsa o meno
		if (!empty($reference['tax2'])) {
			$newTax2 = ($taxable + ($more_taxable * $extra)) * $reference['tax2'] / 100;
		}

		$result = array(
			'vat_amount' => $vat,
			'taxable' => $taxable,
			'amount' => $taxable + $taxableFree + $vat + $newTax1 - $newTax2
		);

		if (!$ddtON) {
			$result['taxableFree'] = $taxableFree;
			$result['tax1amount'] = $newTax1;
			$result['tax2amount'] = $newTax2;
		}
		return $result;
	}

	protected function updateTotal($referenceUid, $ddtON = false)
	{
		$totals = $this->calculateTotals($referenceUid, NULL, NULL, NULL, $ddtON);
		$data = array(
			'pdf' => NULL,
			'uniqueid' => $referenceUid
		);

		$data = array_merge($data, $totals);

		$this->db->save($this->tableName, $data);
	}

	protected function getReference($uid)
	{
		$reference="SELECT id, uniqueid, DATE_FORMAT(date, '%d %M %Y') AS date, DATE_FORMAT(expireDate, '%d %M %Y') AS expireDate, YEAR(date) AS year, serialNumber,
						amount, taxable, taxableFree, vat_amount, buyer, seller, customer_id, pdf, notes, theme_id, sent, tax1, tax2, tax1amount, tax2amount, privatenotes, taxable + tax1amount AS totImponibile
					FROM $this->tableName
					WHERE uniqueid = ?";
		return $this->db->fetchRow($reference, $uid);
	}

	function getmovementdataAction() {
		echo json_encode($this->getmovementdata($_REQUEST['uId']));
	}

	protected function getSelectForMovementData() {
		return 'SELECT uniqueid, uniqueid AS movementUniqueid, quantity, unit_id, description AS descriptionMovement, price, vat, free';
	}

	protected function getmovementdata($uniqueid)
	{
		$result = null;
		if (!empty($uniqueid))
		{
			$select = $this->getSelectForMovementData();
			$movement = "$select
						 FROM $this->tableMovements
						 WHERE uniqueid = ?";
			$result['form'] = $this->db->fetchRow($movement, $uniqueid);
			$nameProduct = "SELECT product_name FROM products WHERE id = ?";
			$result['product_name_by_id'] = $this->db->fetchOne($nameProduct, $result['form']['product_id']);
			$result['product_id'] = $result['form']['product_id'];

			$uidProduct = "SELECT uniqueid FROM products WHERE id = ?";
			$result['uniqueid_product'] = $this->db->fetchOne($uidProduct, $result['form']['product_id']);

			$amountProduct = "SELECT amount FROM products WHERE id = ?";
			$result['amount'] = $this->db->fetchOne($amountProduct, $result['form']['product_id']);

			unset($result['form']['product_id']);
		}
		else
		{
			$result['form'] = array(
				'quantity' => 1,
				'vat' => parent::getParameter('company_default_vat')
			);
		}
		return $result;
	}

	function getmovementsAction()
	{
		$reference = $this->getReference($_REQUEST['uid']);
		$movements = $this->getMovements($reference['id']);
		unset($reference['id']);
		echo json_encode(array(
			'currentPage' => $movements,
			'totalRows' => count($movements),
			'reference' => $reference
		));
	}

	protected function getMovements($id)
	{
		$select = "SELECT m.uniqueid, m.quantity, m.description, m.price, m.vat, (m.quantity * m.price) AS taxable,
					(m.quantity * m.price * m.vat / 100) AS movementVat, m.unit_id,
					CASE WHEN m.unit_id > 0 THEN
						(CASE WHEN m.quantity > 1 THEN CONCAT(m.quantity, ' ', u.plural) ELSE CONCAT(m.quantity, ' ', u.description) END)
					ELSE m.quantity END AS tagQuantity";

		if (!$this->isDDT) {
			$select .= ' , m.free';
		}

		$movements="$select
					FROM $this->tableMovements m
					LEFT JOIN units u ON u.id = m.unit_id
					WHERE $this->columnId = ?";
		return $this->db->fetchAll($movements, $id);
	}

	protected function getCheckReferenceFieldsResults($uniqueid, DateTime $date, $serialNumber, $invoiceON = false)
	{
		$result = array(
			'errors' => array(),
			'year' => $date->format('Y'),
			'serialNumber' => $serialNumber
		);

		$select  = "SELECT COUNT(id)
					FROM $this->tableName
					WHERE serialNumber = ? AND YEAR(date) = ?";
		if ($invoiceON) {
			$select .= ' AND is_external = 0';
		}
		$params = array($serialNumber, $date->format('Y'));

		if (!empty($uniqueid))
		{
			$select .= ' AND uniqueid <> ?';
			$params[] = $uniqueid;
		}

		$serialExists = $this->db->fetchOne($select, $params) > 0;

		if ($serialExists)
		{
			$result['errors']['serialNumber'] = true;
		}
		else
		{
			$select =  "SELECT DATE(date)
						FROM $this->tableName ";

			$where = 'WHERE YEAR(date) = ?';
			if ($invoiceON) {
				$where .= ' AND is_external = 0';
			}
			$params = array($date->format('Y'));

			$query = implode(PHP_EOL, array(
				 $select,
				 $where,
				 'AND serialNumber < ?',
				 'ORDER BY date DESC, serialNumber DESC LIMIT 1'
			));

			$params[] = $serialNumber;

			$previousDate = $this->db->fetchOne($query, $params);
			if (empty($previousDate))
			{
				$previousDate = new DateTime($date->format('Y') . '-01-01');
			}
			else
			{
				$previousDate = new DateTime($previousDate);
			}

			$query = implode(PHP_EOL, array(
				$select,
				$where,
				'AND serialNumber > ?',
				'ORDER BY date ASC, serialNumber ASC LIMIT 1'
			));

			$nextDate = $this->db->fetchOne($query, $params);
			if (empty($nextDate))
			{
				$nextDate = new DateTime($date->format('Y') . '-12-31');
			}
			else
			{
				$nextDate = new DateTime($nextDate);
			}

			if ($date < $previousDate || $date > $nextDate)
			{
				$result['errors']['dates'] = true;
				$result['previousDate'] = $previousDate->format('d/m/Y');
				$result['nextDate'] = $nextDate->format('d/m/Y');
			}
			else
			{
				$result['errors'] = false;
			}
		}
		return $result;
	}

	function checkreferencefieldsAction()
	{
		$date = new DateTime(parent::getDateInSQLFormat($_POST['date'], '/'));
		$serialNumber = $_POST['serialNumber'];
		if (isset($_POST['uniqueid'])) {
			$uniqueid = $_POST['uniqueid'];
		}
		else $uniqueid = '';

		$result = $this->getCheckReferenceFieldsResults($uniqueid, $date, $serialNumber);
		echo json_encode($result);
	}

	function savemovementAction() {
		if (isset($_POST['free'])) {
			$_POST['free'] = 1;
			$_POST['vat'] = 0;
		}
		else $_POST['free'] = 0;

		echo json_encode($this->savemovement());
	}

	protected function savemovement($invoiceON = false, $ddtON = false, $oneMovement = false)
	{
        //$this->applogger->info($invoiceON.'aa');
        //$this->applogger->info($ddtON.'aa');
        //$this->applogger->info($oneMovement.'aa');
	    //salvataggio dati del movimento nel magazzino
	    $this->applogger->info(print_r($_POST,1));
	    $productmovementspost =  $_POST;
	    unset($_POST['id_prodotto']);
	    unset($_POST['amount']);
	    //$this->applogger->info(print_r($productmovements,1));
	    //fine

	    //ricerca se fattura emmessa o ricevuta
	    $selectIsExternal = 'SELECT is_external
								FROM invoices
								WHERE uniqueid = ?';
			$isExternal = $this->db->fetchOne($selectIsExternal, $_POST['referenceUniqueid']);
			$this->applogger->info($isExternal);
		//fine


		$taxableFree = null;
		$taxable = null;
		$vat = null;
		$result = array(
			'errors' => true
		);
		if ($oneMovement) {
			$movement = $oneMovement;
		}
		else {
			$movement = $_POST;
		}

		//aggiungo id_prodotto ad movements
		$movement['product_id'] = $productmovementspost['id_prodotto'];

		$movement['description'] = $movement['descriptionMovement'];
		unset($movement['descriptionMovement']);

		if(isset($movement['movementUniqueid']))
		{
			$movement['uniqueid'] = $movement['movementUniqueid'];
			unset($movement['movementUniqueid']);
		}

		if(isset($movement['unit_id']) && $movement['unit_id'] == -1)
		{
			$movement['unit_id'] = null;
		}

		$referenceUid = $movement['referenceUniqueid'];
		unset($movement['referenceUniqueid']);

		$reference = $this->getReference($referenceUid);

		if (!empty($movement['uniqueid']))
		{
			if ($invoiceON) {
				$movementTaxable = $movement['quantity'] * $movement['price'];
				$movementVat = $movementTaxable * $movement['vat'] / 100;

				if ($movement['free'] == 0) {
					$taxable = $this->calculateTaxable($reference['id'], $movement['uniqueid'], 0, $ddtON) + $movementTaxable;
					$vat = $this->calculateVat($reference['id'], $movement['uniqueid'], $ddtON) + $movementVat;
				}
				else {
					$taxableFree = $this->calculateTaxable($reference['id'], $movement['uniqueid'], 1, $ddtON) + $movementTaxable;
				}

				$totals = $this->calculateTotals($referenceUid, $taxable, $taxableFree, $vat);

				if ($totals['amount'] < $reference['paid_amount']) {
					$result['wrongTotal'] = true;
				}
			}
		}

		if (!isset($result['wrongTotal']))
		{
			$movement[$this->columnId] = $reference['id'];
			try
			{
				$this->db->beginTransaction();
				$movementUid = $this->db->save($this->tableMovements, $movement);

				//carica o scarica prodotto se fattura emessa o ricevuta
				if(!$isExternal && $invoiceON){
    				//registrazione movimento nel magazzino
    				if(empty($movement['uniqueid'])){
        				$productmovements = array('load' => 0, 'unload' => $productmovementspost['quantity'], 'prbuyunitary' => $productmovementspost['amount'],
        				                          'prsaleunitary' => $productmovementspost['price'], 'description' => "Fattura out",
        				                          'id_prodotto' => $productmovementspost['id_prodotto'], 'date' => date("Y-m-d"),
        				                          'uniqueid_invoices' => $productmovementspost['referenceUniqueid'], 'uniqueid_movements' => $movementUid);
        				$this->db->insert('productmovements', $productmovements);
    				}
    				else{
    				    $productmovements = array('load' => 0, 'unload' => $productmovementspost['quantity'], 'prbuyunitary' => $productmovementspost['amount'],
        				                          'prsaleunitary' => $productmovementspost['price'], 'description' => "Fattura out",
        				                          'id_prodotto' => $productmovementspost['id_prodotto'], 'date' => date("Y-m-d"),
        				                          'uniqueid_invoices' => $productmovementspost['referenceUniqueid'], 'uniqueid_movements' => $movementUid);
        				//$this->db->insert('productmovements', $productmovements);
        				$where = array(
    					    'uniqueid_movements = ?' => $movement['uniqueid']
    					);
        				$this->db->update('productmovements',$productmovements ,$where);

    				}

    				if(!empty($productmovementspost['id_prodotto'])){
    				    //verifica la quantità del prodotto fatturato
    				    $selectQuantity =  'SELECT quantity
						    	            FROM products
							                WHERE id = ?';
		                $quantityRes = $this->db->fetchOne($selectQuantity, $productmovementspost['id_prodotto']);
		                $quantityRes = $quantityRes - $productmovementspost['quantity'];
		                $result['quantityRes'] = $quantityRes;

		                //$this->view->quantityRes = $quantityRes;
		                $this->applogger->info($result['quantityRes']);
    				    //fine
    				}else{
    				    $result['quantityRes'] = 'empty';
    				}


				}
				else if($invoiceON){
				    //registrazione movimento nel magazzino
    				if(empty($movement['uniqueid'])){
        				$productmovements = array('load' => $productmovementspost['quantity'], 'unload' => 0, 'prbuyunitary' => $productmovementspost['amount'],
        				                          'prsaleunitary' => $productmovementspost['price'], 'description' => "Fattura in",
        				                          'id_prodotto' => $productmovementspost['id_prodotto'], 'date' => date("Y-m-d"),
        				                          'uniqueid_invoices' => $productmovementspost['referenceUniqueid'], 'uniqueid_movements' => $movementUid);
        				$this->db->insert('productmovements', $productmovements);
    				}
    				else{
    				    $productmovements = array('load' => $productmovementspost['quantity'], 'unload' => 0, 'prbuyunitary' => $productmovementspost['amount'],
        				                          'prsaleunitary' => $productmovementspost['price'], 'description' => "Fattura in",
        				                          'id_prodotto' => $productmovementspost['id_prodotto'], 'date' => date("Y-m-d"),
        				                          'uniqueid_invoices' => $productmovementspost['referenceUniqueid'], 'uniqueid_movements' => $movementUid);
        				//$this->db->insert('productmovements', $productmovements);
        				$where = array(
    					    'uniqueid_movements = ?' => $movement['uniqueid']
    					);
        				$this->db->update('productmovements',$productmovements ,$where);

    				}
				}

				$this->updateTotal($referenceUid, $ddtON);

				$this->db->commit();
				$result['errors'] = false;
			}
			catch (Exception $e)
			{
				$this->db->rollBack();
				$this->applogger->info($e->getMessage());
				$result['errors'] = true;
			}
		}
		return $result;
	}

	function deletemovementAction() {
		echo json_encode($this->deletemovement());
	}

	protected function deletemovement($invoiceON = false, $ddtON = false)
	{
		$result = array(
			'errors' => true
		);

		if (!empty($_POST['uid']) && !empty($_POST['referenceUid']))
		{
			$uid = $_POST['uid'];
			$referenceUid = $_POST['referenceUid'];

			$reference = $this->getReference($referenceUid);

			$movement ="SELECT (quantity * price) AS taxable, ((quantity * price) * (1 + vat/100)) AS amount,
							(quantity * price * vat / 100) AS vat_amount
						FROM $this->tableMovements
						WHERE uniqueid = ?";
			$movement = $this->db->fetchRow($movement, $uid);

			if ($invoiceON) {
				$taxable = $this->calculateTaxable($reference['id'], $uid, 0, $ddtON);
				$vat = $this->calculateVat($reference['id'], $uid, $ddtON);

				$totals = $this->calculateTotals($referenceUid, $taxable, NULL, $vat);

				if ($totals['amount'] < $reference['paid_amount']) {
					$result['wrongTotal'] = true;
				}
			}

			if (!isset($result['wrongTotal']))
			{
				try
				{
					$this->db->beginTransaction();

					$where['uniqueid = ?'] = $uid;

					if ($this->db->delete($this->tableMovements, $where))
					{
						$this->updateTotal($referenceUid, $ddtON);
						$result['errors'] = false;
					}

					$this->db->commit();
				}
				catch (Exception $e) {
					$this->db->rollBack();
					$this->applogger->info($e->getMessage());
					$result['errors'] = true;
				}
			}
		}
		return $result;
	}

	function sendbyemailAction() {
		$this->sendbyemail();
	}

	protected function sendbyemail()
	{
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		$this->view->currentUser = $loggedUser->id;

		$reference = $this->getReference($_REQUEST['uniqueid']);
		$this->view->reference = $reference;

		$receiverId = $reference['customer_id'];

		$selectReceiver =  'SELECT email
							FROM customers
							WHERE id = ?';
		$receiverEmail = $this->db->fetchOne($selectReceiver, $receiverId);
		$this->view->receiver = $receiverEmail;

		$selectSender= 'SELECT id, CONCAT(surname, " ", firstname) AS fullname
						FROM users
						ORDER BY fullname ASC';
		$sender = $this->db->fetchAll($selectSender);
		$this->view->sender = $sender;

		$text = parent::getParameter($this->textMessage);
		$messageReferenceText = str_replace('%NUMERO%', $reference['serialNumber'] . '/' . $reference['year'], $text);

		if (isset($_REQUEST['reminder'])) {
			$messageReferenceText =  str_replace('%NUMERO%', $reference['serialNumber'].'/'.$reference['year'], parent::getParameter('send_reminder_text'));
			$this->view->reminder = 'reminder';
		}

		$this->view->messageReferenceText = $messageReferenceText;

		$subject = $this->titleReference . $reference['serialNumber'] . '/' . $reference['year'];

		$this->view->subject = $subject;
	}

	protected function preparePdfData($uniqueid)
	{
		parent::initDomPdf();
		$parameters = Zend_Registry::get('config')->parameters;

		$reference = $this->getReference($uniqueid);

		if (!empty($reference['tax1'])) {
			$reference['tax1'] = explode('---', $reference['tax1']);
			//$reference['tax1'][1] = parent::getNumberInEuroFormat($reference['tax1'][1] * $reference['taxable'] / 100);
		}

		/*if (!empty($reference['tax2'])) {
			$reference['tax2'] = parent::getNumberInEuroFormat($reference['tax2'] * $reference['taxable'] / 100);
		}*/
		$movements = $this->getMovements($reference['id']);

		foreach ($movements as &$item)
		{
			$item['price'] = parent::getNumberInEuroFormat($item['price']);
			$item['taxable'] = parent::getNumberInEuroFormat($item['taxable']);
			$item['movementVat'] = parent::getNumberInEuroFormat($item['movementVat']);
		}

		if ($logo = parent::getParameter('company_logo'))
		{
			$reference['logo'] = $parameters->uploads_path . $parameters->logo->folder . $logo;
		}

		$reference['taxable'] = parent::getNumberInEuroFormat($reference['taxable']);
		$reference['vat_amount'] = parent::getNumberInEuroFormat($reference['vat_amount']);
		$reference['amount'] = parent::getNumberInEuroFormat($reference['amount']);

		$this->view->reference = $reference;
		$this->view->movements = $movements;

		$dompdf = new DOMPDF();
		$dompdf->load_html($this->view->render($this->getRequest()->getControllerName() . '/print.tpl'));
		$dompdf->render();

		$pdfFilename = $this->titleReference . $reference['serialNumber'] . '_' . $reference['year']. '.pdf';

		$pdfPath = $parameters->uploads_path . $this->folderPath . $pdfFilename;
		if (file_put_contents($pdfPath, $dompdf->output()) !== false) {
			$this->db->update($this->tableName, array('pdf'=>$pdfFilename), array('id = ?'=>$reference['id']));
		}

		$result = array();
		$result['pdfPath'] = $pdfPath;
		$result['pdfFilename'] = $pdfFilename;
		$result['dompdf'] = $dompdf;
		$result['reference'] = $reference;

		return $result;
	}

	function printAction() {
		if (!empty($_REQUEST['uniqueid'])) {
			$uniqueid = $_REQUEST['uniqueid'];
			$resultPdf = $this->preparePdfData($uniqueid);
			$this->printPdf($resultPdf);
		}
	}

	protected function getUserMessages($customerId, $documentId) {
		$messagesSelect =  "SELECT subject, text, DATE_FORMAT(creation_date, '%d/%m/%Y %H:%i') AS creation_date, customer_email
							FROM messages
							WHERE customer_id = ? AND document_type = $this->referredTo AND document_id = ?";
		return $this->db->fetchAll($messagesSelect, array($customerId, $documentId));
	}

	protected function printPdf($resultPdf)
	{
		$parameters = Zend_Registry::get('config')->parameters;
		$data = $_POST;
		$attachments = null;
		$result = 0;
		$pdfPath = $resultPdf['pdfPath'];
		$pdfFilename = $resultPdf['pdfFilename'];
		$dompdf = $resultPdf['dompdf'];
		$reference = $resultPdf['reference'];
		if (isset($data['toSend'])) {
			//send
			$sendTo = $data['receiver'];
			$recipients = explode(',', $sendTo);
			$htmlMessage = $data['messageText'];
			$date = date('d/m/Y H:i:s');
			$creationDate = parent::getDateWithHourInSQLFormat($date, '/');
			$fieldsList = array(
				'customer_id' => $reference['customer_id'],
				'sender_id' => $data['sender'],
				'customer_email' => $sendTo,
				'creation_date' => $creationDate,
				'subject' => $data['subject'],
				'text' => $htmlMessage,
				'document_type' => $this->referredTo,
				'document_id' => $reference['id']
			);
			$this->db->insert('messages', $fieldsList);
			//Invio mail
			$fromName = parent::getParameter('company_name');

			$selectFromEmail = 'SELECT email
								FROM users
								WHERE id = ?';
			$fromEmail = $this->db->fetchOne($selectFromEmail, $data['sender']);

			Zend_Mail::setDefaultFrom($fromEmail, $fromName);
			Zend_Mail::setDefaultReplyTo($fromEmail, $fromName);
			Zend_Mail::setDefaultTransport(new Zend_Mail_Transport_Smtp($parameters->smtp_server, array(
				'auth' => $parameters->smtp_auth,
				'username' => $parameters->smtp_user,
				'password' => $parameters->smtp_password
			)));

			foreach ($recipients as $item)
			{
				if (filter_var(trim($item), FILTER_VALIDATE_EMAIL))
				{
					try {
						$mail = new Zend_Mail('UTF-8');
						$mail->addTo($item, null);
						$mail->setSubject(
								$data['subject']
						);
						$mail->setBodyText($htmlMessage);
						if($data['type'] == null)
						{
							$fileContents = file_get_contents($pdfPath);
							$attachment = $mail->createAttachment($fileContents);
							$attachment->filename = $pdfFilename;
						}
						$mail->send();
					}
					catch (Exception $e) {
						$result = -1;
					}
				}
				else {
					$result = -1;
				}
				if ($result === 0){
					$where = array(
							'uniqueid = ?' => $data['uniqueid']
						);
						$this->db->update($this->tableName, array('sent' => 1), $where);
				}
			}
		}
		else
		{
			//print
			$dompdf->stream($pdfFilename, array('Attachment' => 1));
		}
		echo $result;
		$this->_helper->viewRenderer->setNoRender();
	}

	public function togglesentstatusAction()
	{
		$result = null;
		$sentStatus = $_POST['sentStatus'];
		$where = array(
				'uniqueid = ?' => $_POST['uniqueid']
			);
		$query = $this->db->update($this->tableName, array('sent' => !$sentStatus), $where);
		if ($query)
		{
			$result = !$sentStatus;
		}
		echo $result;
		$this->_helper->viewRenderer->setNoRender();
	}

	public function quickpaymentAction()
	{
	}

	protected function quickInvoiceAdd()
	{
		$lastinvoice = $this->getlastreference('invoices', true);
		$serialNumber = $lastinvoice['serialNumber'] + 1;

		$result = array(
			'dateInvoice' => date('d/m/Y'),
			'year' => date('Y'),
			'serialNumber' => $serialNumber,
			'defaultTax1' => parent::getParameter('company_tax1'),
			'defaultTax2' => parent::getParameter('company_tax2')
		);
		echo json_encode($result);
		$this->_helper->viewRenderer->setNoRender();
	}

	protected function quickInvoiceSave($invoiceType)
	{
		$post = $_POST;

		if (isset($post['free'])) {
			$post['free'] = 1;
			$post['vatQuick'] = 0;
		}
		else {
			$post['free'] = 0;
		}

		$invoice = array(
			'serialNumber' => $post['serialNumber'],
			'customer_id' => $post['customer_id'],
			'date' => $post['dateInvoice'],
			'is_external' => $invoiceType,
			'amount' => $post['priceQuick'],
			'vat_amount' => $post['vatQuick']
		);

		if (isset($post['tax1'])) {
			$invoice['tax1'] = $post['tax1'];
		}
		if (isset($post['tax2'])) {
			$invoice['tax2'] = $post['tax2'];
		}
		$invoiceUid = $this->save($invoice, true, true);

		$movement = array(
			'referenceUniqueid' => $invoiceUid,
			'quantity' => 1,
			'descriptionMovement' => $post['descriptionMovementQuick'],
			'price' => $post['priceQuick'],
			'vat' => $post['vatQuick'],
			'free' => $post['free']
		);

		$this->savemovement(true, false, $movement);

		$select = 'SELECT uniqueid, customer_name, serialNumber, (amount - paid_amount) AS unpaid_amount
				   FROM invoices
				   WHERE uniqueid = ?';
		$result = $this->db->fetchRow($select, $invoiceUid);
		$result['type'] = 'fees';
		echo json_encode($result);
		$this->_helper->viewRenderer->setNoRender();
	}
}