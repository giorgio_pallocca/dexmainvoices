{include file='header.tpl'}
<div class="container">
	<form class="form-signin login-form-height" action="/login/login" method="post">
		<img src="/images/piuiva.png" alt="Logo PiùIVA">
		<hr>
		<label for="identity">Email</label>
		<input autofocus class="input-block-level" id="identity" name="identity" placeholder="Email" type="email" value="{$identity}">
		<label for="password">Password</label>
		<input id="password" name="password" type="password" class="input-block-level" placeholder="Password">
		<div>
			<button class="btn btn-large btn-primary" type="submit">Login</button>
			<a class="pull-right login-minor-link" href="/login/lostpassword">Password dimenticata?</a>
		</div>
	</form>
	{if isset($errorCode)}
		<div class="form-signin-messages">
			<div class="alert alert-block alert-error">
				<h4 class="alert-heading">Attenzione!</h4>
				{if $errorCode == 'invalid_credential'}
					I dati inseriti non sono corretti.
				{/if}
			</div>
		</div>
	{elseif $operationResult == 'newPassword'}
		<div class="form-signin-messages">
			<div class="alert alert-block alert-success">
				<h4 class="alert-heading">Password impostata correttamente</h4>
				È ora possibile effettuare il login con la nuova password.
			</div>
		</div>
	{/if}
{include file='footer.tpl'}