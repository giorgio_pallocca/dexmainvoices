{include file='header.tpl'}
<div class="container">
	<form id="savePasswordForm" autocomplete="off" action="/login/newpassword" class="form-signin" method="post">
		<img src="/images/piuiva.png" alt="Logo PiùIVA">
		<hr>
		<input type="hidden" id="uniqueid" name="uniqueid" value="{$uniqueid}">
		<h3>Reset della password</h3>
		<p>Inserire la nuova password per l'utente <b>{$email}</b>.</p>
		<div class="control-group">
			<label class="control-label" for="password">Password</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="password" name="password" type="password">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="passwordConfirm">Conferma password</label>
			<div class="controls">
				<input class="input-xlarge" id="passwordConfirm" name="passwordConfirm" type="password">
			</div>
		</div>
		<button class="btn btn-large btn-primary">Conferma</button>
	</form>
	{if $operationResult == 'wrongPassword'}
		<div class="form-signin-messages">
			<div class="alert alert-block alert-error">
				<h4 class="alert-heading">Controlla i tuoi dati</h4>
				Inserire una password e la relativa conferma.
			</div>
		</div>
	{/if}
{include file='footer.tpl'}