{include file='header.tpl'}
<div class="container">
	<form class="form-signin login-form-height" action="/login/lostpassword" method="post" novalidate>
		<img src="/images/piuiva.png" alt="Logo PiùIVA">
		<hr>
		<p class="login-description-text">Inserire l'indirizzo email a cui verranno inviate le istruzioni per il recupero della password.</p>
		<label for="email">Email</label>
		<input autofocus class="input-block-level" id="email" name="email" placeholder="Email" type="email" value="{$email}">
		<button class="btn btn-large btn-primary" type="submit">Ricevi email</button>
		<a class="pull-right login-minor-link" href="/login/login">← Torna al login</a>
	</form>
	{if isset($operationResult)}
		<div class="form-signin-messages">
			{if $operationResult == 'emailNotFound'}
				<div class="alert alert-block alert-error">
					<h4 class="alert-heading">Attenzione!</h4>
					L'indirizzo email inserito non è corretto.
				</div>
			{elseif $operationResult == 'emailSent'}
				<div class="alert alert-block alert-success">
					<h4 class="alert-heading">Leggi la tua email</h4>
					Le istruzioni sono state inviate al tuo indirizzo email.
				</div>
			{elseif $operationResult == 'invalidLink'}
				<div class="alert alert-block alert-error">
					<h4 class="alert-heading">Attenzione</h4>
					Il link utilizzato per il reset della password non è valido.
				</div>
			{/if}
		</div>
	{/if}
{include file='footer.tpl'}