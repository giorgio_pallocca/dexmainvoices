{include file='header.tpl'}
{include file='menu.tpl' hideMenu=true}
<div class="container">
	<div class="row">
		<div class="span12">
			<div class="alert alert-block">
				<h4 class="alert-heading">Attenzione!</h4>
				{if $account.status == 'notExist'}
					L'accesso non è autorizzato
				{elseif $account.status == 'deleted'}
					Questa copia di PiùIVA non è più attiva.
				{elseif $account.status == 'disabled'}
					<div>Questa copia di PiùIVA è disabilitata.</div>
					<div>Per qualsiasi informazione la invitiamo a contattare la Dexma s.r.l. ai recapiti di seguito indicati.</div>
					<div><a href="http://www.dexma.it" target="_blank">Dexma s.r.l.</a> Via di ponente, 83 00049 - Velletri Roma</div>
					<div>Telefono: 06.9630367</div>
					<div>Email: <a href="mailto:info@dexma.it">info@dexma.it</a></div>
				{elseif $account.status == "expired"}
					Questa copia di PiùIVA è scaduta. Effettua il <a href="{$account.paymentUrl}">pagamento</a> adesso.
				{/if}
			</div>
			<div class="control-group pull-right">
				<input class="btn" type="button" value="Torna al login" onclick="location.href='/login/login'">
			</div>
		</div>
	</div>
{include file='footer.tpl'}