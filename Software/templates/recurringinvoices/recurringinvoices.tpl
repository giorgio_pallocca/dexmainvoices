{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/js/datePicker.js',
	'/public/js/dexmaUtils.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<h3 class="pull-left">Fatture ricorrenti</h3>
	<div class="pull-right control-group mainButtons">
		<a href="/{$request.controller}/update" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Nuova fattura</a>
	</div>
</div>
<form id="recurringinvoiceSearch" class="well well-small form-inline search-form">
	<label for="startDate" class="moreMarginBottom">Dal:</label>
	<div class="input-prepend input-append">
		<button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'startDate', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button><input class="jqueryCalendar" name="startDate" id="startDate" value="{$search.startDate}" title="Seleziona data" onfocus="this.blur()" data-autosearch  data-resetvalue="{$search.startDateReset}"><button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'startDate', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
	</div>
	<div class="inline-block">
		<label for="endDate" class="moreMarginBottom">al:</label>
		<div class="input-prepend input-append">
			<button id="substractDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'endDate', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button><input class="jqueryCalendar" name="endDate" id="endDate" value="{$search.endDate}" title="Seleziona data" onfocus="this.blur()" data-autosearch  data-resetvalue="{$search.endDateReset}"><button id="addDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'endDate', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
		</div>
	</div>
	<label>Cliente/Fornitore <input class="input-medium" name="searchCustomer" value="{$search.searchCustomer}" type="text" data-autosearch></label>
	<label>
		<select id="searchStatus" name="searchStatus" class="input-medium" data-autosearch>
			<option value="-1">Tutti gli stati</option>
			<option value="1"{if $search.searchStatus == 1} selected{/if}>Attiva</option>
			<option value="2"{if $search.searchStatus == 2} selected{/if}>Non attiva</option>
		</select>
	</label>
	<label>Codice <input class="input-mini" name="searchCode" value="{$search.searchCode}" type="text" data-autosearch></label>
	<button class="btn" id="recurringinvoiceSearchButton" type="button">{$words.search}</button>
	<button class="btn" id="recurringinvoiceShowAllButton" type="button">Mostra tutte</button>
</form>
<div id="recurringinvoicesGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
						 		data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/recurringinvoices/recurringinvoices.js?v={$appVersion}"></script>
{include file='footer.tpl'}