<div id="head">
	Pagamenti
</div>
<hr>
<table class="table table-condensed table-bordered">
	<thead class="well border-none">
		<tr>
			<th>Descrizione</th>
			<th class="pagination-right">Data</th>
			<th class="pagination-right">Importo</th>
		</tr>
	</thead>
	<tbody>
		{foreach from = $payments item = row}
			<tr>
				<td><b>{$row.description|truncate:200}</b>
				{foreach from = $row.explanations item = explanation}
					{if $explanation.invoice_id > 0}
						<br/>&nbsp;Fattura n° {$explanation.invoice_id} - {$explanation.amount}
					{else}
						<br/>&nbsp;{$explanation.description} - {$explanation.amount}
					{/if}
				{/foreach}
				</td>
				<td class="pagination-right"><b>{$row.dateLabel}</b></td>
				<td class="pagination-right"><b>{$row.amount}</b></td>
			</tr>
		{/foreach}
	</tbody>
</table>