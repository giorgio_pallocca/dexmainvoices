<div id="head">
	Fatture
</div>
<hr>
<table class="table table-condensed table-bordered">
	<thead class="well border-none">
		<tr>
			<th>Data</th>
			<th>Progr.</th>
			<th>Cliente</th>
			<th>Pagamenti</th>
			<th class="pagination-right">Pagato</th>
			<th class="pagination-right">Scoperto</th>
			<th class="pagination-right">IVA</th>
			<th class="pagination-right">Totale</th>
		</tr>
	</thead>
	<tbody>
		{foreach from = $invoices item = row}
			<tr>
				<td>{$row.date}</td>
				<td>{$row.serialNumber}</td>
				<td>{$row.customer_name}</td>
				<td>
					{foreach from = $row.explanations item = explanation}
						<br/>&nbsp;{$row.date} {$explanation.amount}
					{/foreach}
				</td>
				<td class="pagination-right">{$row.paid_amount}</td>
				<td class="pagination-right red">{$row.unpaid_amount}</td>
				<td class="pagination-right">{$row.vat_amount}</td>
				<td class="pagination-right"><b>{$row.amount}</b></td>
			</tr>
		{/foreach}
	</tbody>
</table>