<div id="head">
	{if $reference.logo}
		<img id="logo" class="pull-left" src="{$reference.logo}">
	{/if}
	<div id="seller" class="pull-right pagination-right">
		{$reference.seller|nl2br}
	</div>
</div>
<div class="clear-both"></div>
<hr>
<div id="buyer" class="pull-left">
	{$reference.buyer|nl2br}
</div>
<div class="pull-right pagination-right reference-title">
	<b id="serialNumberTitle">{$nameReference} {$reference.serialNumber}/{$reference.year}</b><br>
	data: <span class="referenceDate">{$reference.date}</span><br>
	{if $reference.expireDate} scadenza: <span class="referenceDate">{$reference.expireDate}</span>{/if}
</div>
<div class="clear-both"></div>
<table class="table">
	<thead class="well border-none">
		<tr>
			<th>Quantit&agrave;</th>
			<th>Descrizione</th>
			<th class="pagination-right">Prezzo unitario</th>
			<th class="pagination-right">IVA</th>
			<th class="pagination-right">Imponibile</th>
		</tr>
	</thead>
	<tbody>
		{foreach from = $movements item = row}
			<tr>
				<td>{$row.tagQuantity}</td>
				<td>{$row.description}</td>
				<td class="pagination-right">{$row.price}</td>
				<td class="pagination-right">{$row.vat}</td>
				<td class="pagination-right">{$row.taxable}</td>
			</tr>
		{/foreach}
	</tbody>
	<tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="pagination-right">Totale imponibile</td>
			<td class="pagination-right">{$reference.taxable}</td>
		</tr>
		{if $reference.tax1amount > 0}
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td class="pagination-right">{$reference.tax1[0]} {$reference.tax1[1]} %</td>
				<td class="pagination-right">{$reference.tax1amount}</td>
			</tr>
		{/if}
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="pagination-right">IVA</td>
			<td class="pagination-right">{$reference.vat_amount}</td>
		</tr>
		{if $reference.tax2amount > 0}
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td class="pagination-right">Ritenuta d'acconto IRPEF</td>
				<td class="pagination-right">- {$reference.tax2amount}</td>
			</tr>
		{/if}
	</tbody>
	<tfoot class="well border-none">
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="pagination-right"><b class="black">Totale</b></td>
			<td class="pagination-right"><b class="black">&euro; {$reference.amount}</b></td>
		</tr>
	</tfoot>
</table>
<div class="clear-both"></div>
<hr>
<div class="pull-left" style="margin-right: 20px; width: 300px;">
	{if $reference.payments_term_description}
		<div style="max-width: 300px; word-wrap: break-word;">
			<b>MODALIT&Agrave; DI PAGAMENTO</b><br>
			{$reference.payments_term_description|nl2br}
		</div>
	{/if}
</div>
<div class="pull-right pagination-right" style="margin-bottom: 0px; width: 400px;">
	{if $reference.notes}
		<div style="max-width: 400px; word-wrap: break-word;">
			<b>ALTRE INFORMAZIONI</b><br>
			{$reference.notes|nl2br}
		</div>
	{/if}
</div>