<div id="head">
	{if $invoice.logo}
		<img id="logo" class="pull-left" src="{$invoice.logo}">
	{/if}
	<div id="seller" class="pull-right pagination-right">
		{$invoice.seller|nl2br}
	</div>
</div>
<div class="clear-both"></div>
<hr>
<div id="buyer" class="pull-left">
	{$invoice.buyer|nl2br}
</div>
<div class="pull-right pagination-right reference-title">
	<b id="serialNumberTitle">FATTURA {$invoice.serialNumber}</b><br>
	del {$invoice.date}<br>
	{if $invoice.payments_term_description}
		Termine di pagamento: {$invoice.payments_term_description}
	{/if}
</div>
<div class="clear-both"></div>
<table class="table table-condensed">
	<thead class="well border-none">
		<tr>
			<th>Quantit&agrave;</th>
			<th>Descrizione</th>
			<th class="pagination-right">Prezzo unitario</th>
			<th class="pagination-right">IVA</th>
			<th class="pagination-right">Imponibile</th>
		</tr>
	</thead>
	<tbody>
		{foreach from = $movements item = row}
			<tr>
				<td>{$row.quantity}</td>
				<td>{$row.description}</td>
				<td class="pagination-right">{$row.price}</td>
				<td class="pagination-right">{$row.movementVat}</td>
				<td class="pagination-right">{$row.taxable}</td>
			</tr>
		{/foreach}
	</tbody>
	<tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="pagination-right">Totale imponibile</td>
			<td class="pagination-right">{$reference.taxable}</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="pagination-right">IVA</td>
			<td class="pagination-right">{$reference.vat_amount}</td>
		</tr>
	</tbody>
	<tfoot class="well border-none">
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="pagination-right"><b class="black">Totale</b></td>
			<td class="pagination-right"><b class="black">&euro; {$reference.amount}</b></td>
		</tr>
	</tfoot>
</table>
<div class="clear-both"></div>
{if $reference.payments_term_description}
	<div class="pull-left pagination-left">
		<b>MODALIT&Agrave; DI PAGAMENTO</b><br>
		{$reference.payments_term_description|nl2br}
	</div>
{/if}
<div class="clear-both"></div>
{if $invoice.notes}
	<div class="pull-right pagination-right">
		<b>ALTRE INFORMAZIONI</b><br>
		{$invoice.notes|nl2br}
	</div>
{/if}