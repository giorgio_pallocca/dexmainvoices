{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/public/js/dexmaUtils.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<div class="pull-left"><h3>Unità di misura</h3></div>
	<div class="pull-right control-group mainButtons">
		<a href="/units/update" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Nuova unità di misura</a>
	</div>
</div>
<form id="unitsSearch" class="search-form well well-small form-inline">
	<label>Ricerca: <input autofocus class="input-large" name="searchUnit" value="{$search.searchUnit}" type="text" data-autosearch></label>
	<button class="btn" id="searchButton" type="button">Cerca</button>
	<button class="btn" id="showAllButton" type="button">Mostra tutte</button>
</form>
<div id="unitsGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
					data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/units/units.js?v={$appVersion}"></script>
{include file='footer.tpl'}