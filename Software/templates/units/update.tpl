{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/units/update.js'
] index='js'}
{include file='template.tpl'}
{if $unit.uniqueid}
	{include file='breadcrumb.tpl' page='Unità di misura' pageUrl='/units/units' inner_page=$unit.description}
{else}
	{include file='breadcrumb.tpl' page='Unità di misura' pageUrl='/units/units' inner_page='Nuova unità di misura'}
{/if}
<form id="unitsForm" class="form-horizontal" method="post" action="/units/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$unit.uniqueid}">
	<fieldset>
		<legend>
			{if $unit.id}
				Unità di misura: {$unit.description}
			{else}
				Nuova unità di misura
			{/if}
		</legend>
		<div id="descriptionContainer" class="control-group">
			<label class="control-label" for="description">Descrizione singolare</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="description" name="description" type="text" value="{$unit.description}" onblur="validateDescription()">
				<span id="descriptionError" class="help-inline"></span>
			</div>
		</div>
		<div id="pluralContainer" class="control-group">
			<label class="control-label" for="plural">Descrizione plurale</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="plural" name="plural" type="text" value="{$unit.plural}" onblur="validatePlural()">
				<span id="pluralError" class="help-inline"></span>
			</div>
		</div>
		<div class="form-actions">
			<a class="btn" href="/units/units">Annulla</a>
			{if $unit.uniqueid}
				<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteUnit()">
			{/if}
			<input id="saveUnitBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveUnit()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}