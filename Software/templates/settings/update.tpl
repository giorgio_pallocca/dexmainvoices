{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/settings/update.js'
] index='js'}
{include file='template.tpl'}
{if $confirmMessage}
	<div class="span5">
		<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<h4 class="alert-heading">Operazione riuscita</h4>
			Le modifiche sono state salvate con successo.
		</div>
	</div>
{/if}
<form id="settingsForm" action="/settings/save" autocomplete="off" class="form-horizontal form-settings" method="post">
	<fieldset>
		<legend>Impostazioni</legend>
		<div class="row">
			<div id="legend-settings" class="control-group pull-left">
				<i>La stringa %NUMERO% nei messaggi viene automaticamente sostituita al momento dell'invio del messaggio dal numero progressivo del documento.</i>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="send_invoice_text">Messaggio di invio copia fattura</label>
			<div class="controls">
				<textarea autofocus class="input-xxlarge" name="send_invoice_text" id="send_invoice_text" rows=5>{$params.send_invoice_text}</textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="send_quotation_text">Messaggio di invio copia preventivo</label>
			<div class="controls">
				<textarea class="input-xxlarge" name="send_quotation_text" id="send_quotation_text" rows=5>{$params.send_quotation_text}</textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="send_transportdocument_text">Messaggio di invio copia documento di trasporto</label>
			<div class="controls">
				<textarea class="input-xxlarge" name="send_transportdocument_text" id="send_transportdocument_text" rows=5>{$params.send_transportdocument_text}</textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="send_creditnote_text">Messaggio di invio copia nota di credito</label>
			<div class="controls">
				<textarea class="input-xxlarge" name="send_creditnote_text" id="send_creditnote_text" rows=5>{$params.send_creditnote_text}</textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="send_reminder_text">Messaggio di sollecito pagamento</label>
			<div class="controls">
				<textarea class="input-xxlarge" name="send_reminder_text" id="send_reminder_text" rows=5>{$params.send_reminder_text}</textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="lists_rows">Numero di righe nelle liste</label>
			<div class="controls">
				<input id="lists_rows" name="lists_rows" class="input-mini" type="text" value="{$params.lists_rows}">
			</div>
		</div>
		<div class="form-actions">
			<button id="saveSettingsBtn" class="btn btn-primary" type="button" onclick="saveSettings()">Conferma</button>
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}