{append var=includes value=[
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/js/datePicker.js',
	'/public/js/dexmaUtils.js',
	'/js/transferts/update.js'
] index='js'}
{include file='template.tpl'}
{if $transfert.uniqueid}
	{include file='breadcrumb.tpl' page='Trasferte' pageUrl='/transferts/transferts' inner_page='Modifica trasferta'}
{else}
	{include file='breadcrumb.tpl' page='Trasferte' pageUrl='/transferts/transferts' inner_page='Nuova trasferta'}
{/if}
<form id="transfertsForm" class="form-horizontal" method="post" action="/transferts/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$transfert.uniqueid}">
	<fieldset>
		<legend>
			{if $transfert.id}
				Modifica trasferta
			{else}
				Nuova trasferta
			{/if}
		</legend>

		<div id="dateContainer" class="control-group">
			<label class="control-label" for="date">Data</label>
			<div class="controls">
				<input class="jqueryCalendar" type="text" name="date" id="date" value="{$transfert.date}" title="Seleziona data" onfocus="this.blur()" />
				<span id="dateError" class="help-inline"></span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="customerName">Denominazione</label>
			<div class="controls">
				<input class="input-xlarge" id="customerName" name="customerName" type="text" value="{$transfert.customerName}">
				<div class="btn-group align-top">
					<button class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown" type="button">
						Aggiungi da lista clienti
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown-transferts">
						{foreach from = $customers item = row}
							<li><a onclick="selectFromCustomers('{$row.company|escape:'javascript'}','{$row.address|escape:'javascript'}','{$row.zipcode}','{$row.city|escape:'javascript'}','{$row.distanceKm}')">{$row.company}</a></li>
						{/foreach}
					</ul>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="customerAddress">Indirizzo e CAP</label>
			<div class="controls">
				<input class="input" id="customerAddress" name="customerAddress" type="text" value="{$transfert.customerAddress}">
				<input class="input-xmini" id="customerZipcode" name="customerZipcode" type="text" value="{$transfert.customerZipcode}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="customerCity">Città</label>
			<div class="controls">
				<input class="input-xlarge" id="customerCity" name="customerCity" type="text" value="{$transfert.customerCity}">
			</div>
		</div>
		<div id="vehicle_idContainer" class="control-group">
			<label class="control-label" for="vehicle_id">Veicolo</label>
			<div class="controls">
				<select id="vehicle_id" name="vehicle_id" class="input-xlarge">
					{foreach from = $vehicles item = vehicle}
						<option{if $transfert.currentVehicle == $vehicle.id} selected{/if} value="{$vehicle.id}">{$vehicle.plate} - {$vehicle.description}</option>
					{/foreach}
				</select>
				<span id="vehicle_idError" class="help-inline"></span>
			</div>
		</div>
		<div id="user_idContainer" class="control-group">
			<label class="control-label" for="user_id">Utente</label>
			<div class="controls">
				<select id="user_id" name="user_id" class="input-xlarge">
					{foreach from = $users item = user}
						<option{if $transfert.currentUser == $user.id} selected{/if} value="{$user.id}">{$user.fullname}</option>
					{/foreach}
				</select>
				<span id="user_idError" class="help-inline"></span>
			</div>
		</div>
		<div id="kmTotContainer" class="control-group">
			<label class="control-label" for="kmTot">Distanza in km</label>
			<div class="controls">
				<input class="input-medium" id="kmTot" name="kmTot" type="text" value="{$transfert.kmTot}" onblur="validateKmTot()" onkeydown="return isKeyboardNumber(event, true)">
				<span id="kmTotError" class="help-inline"></span>
			</div>
		</div>
		<div id="diariaContainer" class="control-group">
			<div class="controls">
				<label class="checkbox">
					<input id="diaria" name="diaria" type="checkbox" {if !$transfert.id || $transfert.diaria == 1} checked{/if}>Applica diaria
				</label>
			</div>
		</div>
		<div class="form-actions">
			<a class="btn" href="/transferts/transferts">Annulla</a>
			{if $transfert.uniqueid}
				<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteTransfert()">
			{/if}
			<input id="saveTransfertBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveTransfert()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}