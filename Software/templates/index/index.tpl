{append var=includes value=[
	'/public/js/raphael-2.0.2.min.js',
	'/public/js/morris.min.js',
	'/js/index/index.js'
] index='js'}
{include file='template.tpl'}
<script>
{literal}
$(function(){
	// annual data
	Morris.Line({
		element: 'annual',
		data: {/literal}{$graph1}{literal},
		xkey: 'ym',
		ykeys: ['a'],
		labels: ['Fatturato'],
		dateFormat: PIUIVA.morris.dateFormat,
		xLabelFormat: PIUIVA.morris.xLabelFormat
	});
	// Trendy
	Morris.Area({
		element: 'trend',
		data: {/literal}{$graph2}{literal},
		xkey: 'ym',
		ykeys: ['a', 'b', 'c'],
		labels: ['IVA', 'Imponibile','Scoperto'],
		dateFormat: PIUIVA.morris.dateFormat,
		xLabelFormat: PIUIVA.morris.xLabelFormat
	});
	// Best
	Morris.Donut({
	  element: 'best',
	  data: [{/literal}{$graph3}{literal}]
	});
});
{/literal}
</script>
<div class="row-fluid">
	<img id="graphBarIcon" class="pull-left" src="/public/images/metroicons/48/appbar.graph.bar.png" alt>
	<h3 class="pull-left">{$words.view}</h3>
	<div class="pull-right mainButtons">
		{if $years}
			<form id="indexFilter" name="indexFilter" method="post" action="/index/index">
				<select id="yearSelected" name="yearSelected" class="input-mini" onchange="searchIndex()">
					{foreach from=$years item=row}
						<option value="{$row}"{if $row == $yearSelected} selected{/if}>{$row}</option>
					{/foreach}
				</select>
			</form>
		{/if}
		<div class="btn-group">
			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				{$words.shortcuts}&nbsp;
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu pull-right">
				<li><a href="/customers/update">Nuovo contatto</a></li>
				<li><a href="/invoices/update">{$words.new_invoice}</a></li>
				<li><a href="/quotations/update">{$words.new_estimate}</a></li>
				<li><a href="/transportdocuments/update">{$words.new_ddt}</a></li>
				<li><a href="/creditnotes/update">{$words.new_creditnote}</a></li>
				<li><a href="/products/update">{$words.new_product}</a></li>
				<li><a href="/payments/payments">{$words.to_payments}</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span6 well">
		<h4>{$words.best_customers}</h4>
		{if $graph3}
			<div class="graph" id="best"></div>
		{else}
			<img src="/images/grafico_3.png" alt="Grafico clienti">
		{/if}
	</div>
	<div class="span6 well">
		<h4>{$words.turnover}</h4>
		{if $graph2}
			<div class="graph" id="annual"></div>
		{else}
			<img src="/images/grafico_2.png" alt="Grafico fatturato">
		{/if}
	</div>
</div>
<div class="row-fluid">
	<div class="span6 well">
		<h4>{$words.economic_perf}</h4>
		{if $graph1}
			<div class="graph" id="trend"></div>
		{else}
			<img src="/images/grafico_1.png" alt="Grafico andamento">
		{/if}
	</div>
	<div class="span6 well">
		<h4>{$words.turnover} {$yearSelected}</h4>
		<table class="table">
			<tbody>
				<tr>
					<td>{$words.taxable}</td>
					<td>€ {$result.taxable}</td>
				</tr>
				<tr>
					<td>{$words.taxable} {$words.vat|upper}</td>
					<td>€ {$result.vat_amount}</td>
				</tr>
				<tr>
					<td>Totale</td>
					<td>€ {$result.total}</td>
				</tr>
				<tr>
					<td>{$words.paid}</td>
					<td>€ {$result.paid_amount}</td>
				</tr>
				<tr class="text-error">
					<td>{$words.unpaid}</td>
					<td>€ {$result.unpaid_amount}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
{include file='footer.tpl'}