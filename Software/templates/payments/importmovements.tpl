{include file='template.tpl'}
{include file='breadcrumb.tpl' page='Pagamenti' pageUrl='/payments/payments' inner_page='Importa movimenti bancari'}
{*<form id="bankMovementsForm" action="/payments/loadfromfile" autocomplete="off" class="form-horizontal span12" enctype="multipart/form-data" method="post">*}
<form id="bankMovementsForm" action="/payments/loadfromfile" class="form-horizontal" enctype="multipart/form-data" method="post">
	<fieldset>
		<legend>
			Importa movimenti bancari
		</legend>
		<div class="row">
			<div id="legend-settings" class="control-group pull-left">
				<i>E' possibile caricare file in formato <strong>".OFX"</strong> oppure <strong>".OFC"</strong></i>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="bankMovements">Carica file</label>
			<div class="controls">
				<input class="input-xlarge" id="bankMovements" name="bankMovements" type="file">
			</div>
		</div>
		<div class="form-actions">
			<a href="/payments/payments" class="btn">Annulla</a>
			<input id="saveBankMovementsBtn" type="submit" class="btn btn-primary" value="Conferma">
		</div>
	</fieldset>
</form>
{if $errorMsg}
	<div class="form-bankMovements-messages">
		<div class="alert alert-block alert-error">
			<h4 class="alert-heading">Errore</h4>
			Si è verificato un errore durante il caricamento del file, si prega di riprovare.
		</div>
	</div>
{elseif $confirmMsg}
	<div class="form-bankMovements-messages">
		<div class="alert alert-block alert-success">
			<h4 class="alert-heading">Operazione effettuata</h4>
			{$countInsert}
			{if $countInsert == 1}
				movimento presente sul file è stato inserito correttamente
			{else}
				movimenti presenti sul file sono stati inseriti correttamente
			{/if}
			<br>
			{$countSkipped}
			{if $countSkipped == 1}
				movimento presente sul file era già presente ed è stato ignorato
			{else}
				movimenti presenti sul file erano già presenti e sono stati ignorati
			{/if}
			<br> <br>
			<a href="/payments/payments">Torna alla lista dei pagamenti</a>
		</div>
	</div>
{/if}
{include file='footer.tpl'}