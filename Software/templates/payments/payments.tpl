{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/js/datePicker.js',
	'/public/js/dexmaUtils.js',
	'/public/js/modalDialog.js',
	'/js/payments/update.js'
] index='js'}
{include file='template.tpl'}
{include file='payments/header.tpl'}
<div class="row-fluid">
	<div class="pull-left"><h3>Pagamenti</h3></div>
	<div class="pull-right control-group mainButtons">
		<a href="/payments/importmovements" class="btn">Importa movimenti bancari</a>
		<button class="btn" onclick="printPayments()">Stampa</button>
		<div class="btn-group">
			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				<i class="btn-icon-plus-sign icon-plus-sign icon-white"></i> Nuovo pagamento
				<span class="caret"></span>
			</button>
			<ul id="archiveNewFeeBtn" class="dropdown-menu">
				<li><a href="#" onclick="paymentDetail('', 0, 0)">Incasso</a></li>
				<li><a href="#" onclick="paymentDetail('', 1, 0)">Spesa</a></li>
			</ul>
		</div>
	</div>
</div>
<form id="paymentsSearch" class="search-form well well-small form-inline">
	<label>Ricerca: <input autofocus id="paymentSearchField" class="input-medium" name="paymentSearchField" value="{$search.paymentSearchField}" type="text" data-autosearch></label>
	<div class="inline-block">
		<label for="startDateSearch" class="moreMarginBottom">Dal:</label>
		<div class="input-prepend input-append">
			<button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'startDateSearch', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button>
			<input class="jqueryCalendar" name="startDateSearch" id="startDateSearch" value="{$search.startDateSearch}" title="Seleziona data" onfocus="this.blur()" data-autosearch data-resetvalue="{$search.startDateReset}">
			<button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'startDateSearch', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
		</div>
	</div>
	<div class="inline-block">
		<label for="endDateSearch" class="moreMarginBottom">al:</label>
		<div class="input-prepend input-append">
			<button id="substractDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'endDateSearch', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button>
			<input class="jqueryCalendar" name="endDateSearch" id="endDateSearch" value="{$search.endDateSearch}" title="Seleziona data" onfocus="this.blur()" data-autosearch data-resetvalue="{$search.endDateReset}">
			<button id="addDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'endDateSearch', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
		</div>
	</div>
	<label>
		<select id="typologySearch" name="typologySearch" class="input-medium-large" data-autosearch>
			<option value="-1">Tutte le tipologie</option>
			<option value="0" {if $currentTypology == '0'}selected="selected"{/if}>Banca</option>
			<option value="1" {if $currentTypology == '1'}selected="selected"{/if}>Cassa</option>
		</select>
	</label>
	<label class="checkbox">
		<input{if $search.statusSearch} checked{/if} type="checkbox" id="statusSearch" name="statusSearch" data-autosearch> Aperti
	</label>
	<button class="btn" id="searchButton" type="button">Cerca</button>
	<button class="btn" id="showAllButton" type="button">Mostra tutti</button><br>
	<i>B = Banca, C = Cassa</i>
</form>
<div id="paymentsGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
					   data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/payments/payments.js?v={$appVersion}"></script>
{include file='payments/update.tpl'}
{include file='footer.tpl'}