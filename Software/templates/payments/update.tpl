<div id="paymentDIV" class="modal custom-modal hide">
	<div id="paymentModalBody" class="modal-body">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<ul id="paymentTabs" class="nav nav-tabs">
			<li id="payment-tabs-1Tab"><a href="#payment-tabs-1" data-toggle="tab">Scheda pagamento</a></li>
			<li id="payment-tabs-2Tab" class="hide"><a href="#payment-tabs-2" data-toggle="tab">Inserimento rapido fattura</a></li>
		</ul>
		<div class="tab-content">
			<div id="payment-tabs-1" class="tab-pane">
				<form id="paymentForm" class="form-horizontal custom-form" method="post" action="/payments/save">
					<input type="hidden" id="uniqueid" name="uniqueid" value="" />
					<input type="hidden" id="type" name="type" value="" />
					<div id="descriptionContainer" class="control-group">
						<label class="control-label" for="description">Descrizione</label>
						<div class="controls">
							<input class="input-xlarge" id="description" name="description" type="text" value="" onblur="validateDescription()" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="date">Data</label>
						<div class="controls">
							<input class="jqueryCalendar" type="text" name="date" id="date" value="" title="Seleziona data" onfocus="this.blur()" />
						</div>
					</div>
					<div id="amountContainer" class="control-group">
						<label class="control-label" for="amount">Importo</label>
						<div class="controls">
							<input class="paymentAmount" id="amount" name="amount" type="text" value="" onblur="if(validateAmount())hidePopover();" onfocus="showPopover(this)" onkeydown="return isKeyboardNumber(event, 1)"/>
							<select class="input-medium" id="typology" name="typology">
								<option value="0">Banca</option>
								<option value="1">Cassa</option>
							</select>
							<div class="displayInlineBlock" id="paymentDirectionLabel"></div>
							<span id="amountError" class="help-inline"></span>
						</div>
					</div>
					{if $quickPayment}
						<input type="hidden" id="paymentDetail" name="paymentDetail" value="{$reference.uniqueid}" />
						<input type="hidden" id="quickDetail" name="quickDetail" value="" />
					{else}
						{include file="{$smarty.const.DEXMA_COMMONS}/public/PaymentsManager/update.tpl"}
					{/if}
					<div class="form-actions pagination-right">
						{if $request.controller == 'payments'}
							<a id="quickInvoiceButton" name="quickInvoiceButton" href="#" class="btn btn-primary" onclick="showQuickInvoiceTab()"><i class="icon-star icon-white"></i> Inserisci fattura</a>
						{/if}
						<input id="goBackButton" class="btn" type="button" value="Annulla" data-dismiss="modal" />
						<input id="deletePaymentButton" class="btn btn-danger" type="button" value="Elimina" onclick="deletePayment()" />
						<input id="savePaymentButton" class="btn btn-primary" type="button" value="Conferma" {if $quickPayment} onclick="savePayment(1, '{$reference.uniqueid}')"{else} onclick="savePayment(0, '')"{/if} />
					</div>
				</form>
			</div>
			<div id="payment-tabs-2" class="tab-pane">
				{if $request.controller == 'payments'}
					<form id="quickInvoiceForm" class="form-horizontal quickInvoice" method="post">
						<fieldset>
							<div id="serialNumberContainer" class="control-group">
								<label class="control-label" for="serialNumber">N° progressivo</label>
								<div class="controls">
									<div class="input-append">
										<input class="input-mini" id="serialNumber" name="serialNumber" type="text" value="" onkeydown="return isKeyboardNumber(event, false)"><span id="year" class="add-on"></span>
									</div>
									<span id="serialNumberError" class="help-inline"></span>
								</div>
							</div>
							<div id="dateInvoiceContainer" class="control-group">
								<label class="control-label" for="dateInvoice">Data</label>
								<div class="controls">
									<input class="jqueryCalendar" id="dateInvoice" name="dateInvoice" type="text">
									<span id="dateInvoiceError" class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label id="customer_idLabel" class="control-label" for="customer_id"></label>
								<div class="controls">
									<select class="input-xlarge" id="customer_id" name="customer_id">
										{foreach from = $customers item = customer}
											<option value="{$customer.id}">{$customer.company}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div id="tax1Container" class="control-group hide">
								<label class="control-label" for="tax1">Rivalsa previdenza</label>
								<div class="controls">
									<select class="input-xlarge" id="tax1" name="tax1">
										{include file='references/tax1options.tpl'}
									</select>
								</div>
							</div>
							<div id="tax2Container" class="control-group hide">
								<label class="control-label" for="tax2">Ritenuta di acconto</label>
								<div class="controls">
									<input id="tax2" name="tax2" type="checkbox">
								</div>
							</div>
							<div id="descriptionMovementQuickContainer" class="control-group">
								<label for="descriptionMovementQuick" class="control-label">Descrizione</label>
								<div class="controls">
									<textarea id="descriptionMovementQuick" name="descriptionMovementQuick" onblur="checkDescription(this.id)" rows=4 tabindex=10 class="input-large"></textarea>
									<div class="btn-group align-top">
										<button class="btn btn-mini btn-primary dropdown-toggle" data-toggle="dropdown" type="button">
											Aggiungi da lista prodotti
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-products" id="productsList">
											{foreach from = $products item = product}
												<li><a onclick="selectFromProduct('{$product.description}', '{$product.amount}', '{$product.vat}')">{$product.description|substr:0:36}</a></li>
											{/foreach}
										</ul>
									</div>
									<span id="descriptionMovementQuickError" class="help-inline align-top"></span>
								</div>
							</div>
							<div id="freeContainer" class="control-group">
								<label class="control-label" for="free">Spesa esente</label>
								<div class="controls">
									<input id="free" name="free" type="checkbox" onclick="isFree('vatQuick')">
								</div>
							</div>
							<div id="priceQuickContainer" class="control-group">
								<label for="priceQuick" class="control-label">Imponibile (€)</label>
								<div class="controls">
									<input class="input-mini" id="priceQuick" name="priceQuick" type="text" onkeydown="return isKeyboardNumber(event, true)" onblur="checkTaxable()" tabindex=10>
									<span id="priceQuickError" class="help-inline"></span>
								</div>
							</div>
							<div id="vatQuickContainer" class="control-group">
								<label for="vatQuick" class="control-label">IVA (%)</label>
								<div class="controls">
									<input class="input-mini" id="vatQuick" name="vatQuick" type="text" onkeydown="return isKeyboardNumber(event, false)" onblur="checkVat(this.id)" tabindex=10>
									<span id="vatQuickError" class="help-inline"></span>
								</div>
							</div>
							<div class="form-actions pagination-right">
								<input class="btn" type="button" value="Annulla" onclick="hideQuickInvoiceTab()">
								<input id="saveQuickInvoiceBtn" class="btn btn-primary" type="button" value="Conferma" onclick="checkFields()">
							</div>
						</fieldset>
					</form>
				{/if}
			</div>
		</div>
	</div>
</div>