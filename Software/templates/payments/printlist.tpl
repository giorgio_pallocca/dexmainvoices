<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="{$smarty.const.DEXMA_COMMONS}/public/bootstrap/2.3.1/css/bootstrap.min.css">
		<style>{literal}
			.clear-both {clear:both}
			.border-none {border:0 none}
		{/literal}</style>
		<link rel="stylesheet" href="css/pdf/printlist.css">
	</head>
	<body>
		{literal}
			<script type="text/php">
				if (isset($pdf))
				{
					$w = $pdf->get_width();
					$h = $pdf->get_height();
					$font = Font_Metrics::get_font('helvetica');
					$pdf->page_text($w - 80, $h - 40, "Pagina {PAGE_NUM} di {PAGE_COUNT}", $font, 8, array(51/255,51/255,51/255));
				}
			</script>
		{/literal}
		{include file="themes/theme-paym.tpl"}
	</body>
</html>