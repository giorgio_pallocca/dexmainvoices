<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="{$smarty.const.DEXMA_COMMONS}/public/bootstrap/2.3.1/css/bootstrap.min.css">
		<style>{literal}
			.clear-both {clear:both}
			.border-none {border:0 none}
			.reference-title {margin-bottom:20px}
		{/literal}</style>
		<link rel="stylesheet" href="css/pdf/invoice-{$reference.theme_id}.css">
	</head>
	<body>
		{literal}
			<script type="text/php">
				if (isset($pdf))
				{
					$w = $pdf->get_width();
					$h = $pdf->get_height();
					$font = Font_Metrics::get_font('helvetica');
					$pdf->page_text($w - 80, $h - 40, "Pagina {PAGE_NUM} di {PAGE_COUNT}", $font, 8, array(51/255,51/255,51/255));
				}
			</script>
		{/literal}
		{include file="themes/theme-{$reference.theme_id}.tpl"}
		{if $request.controller == 'transportdocuments'}
			<div class="clear-both"></div>
			<hr>
			<table class="table">
				<thead class="well">
					<tr>
						<th>Trasporto a cura del</th>
						<th class="pagination-right">Aspetto esteriore dei beni</th>
						<th class="pagination-right">N° colli</th>
						<th class="pagination-right">Firma del conducente</th>
					</tr>
				</thead>
				<tbody class="well">
					<tr>
						<td>{$reference.transportBy}</td>
						<td class="pagination-right">{$reference.appearance}</td>
						<td class="pagination-right">{$reference.numberPackage}</td>
						<td class="pagination-right"></td>
					</tr>
				</tbody>
				<thead class="well">
					<tr>
						<th>Data e ora del ritiro/trasporto</th>
						<th class="pagination-right">Dati del vettore</th>
						<th class="pagination-right">Peso in Kg</th>
						<th class="pagination-right">Firma del cessionario</th>
					</tr>
				</thead>
				<tbody class="well">
					<tr>
						<td>{$reference.dateTimeTransport}</td>
						<td class="pagination-right">{$reference.vectorData}</td>
						<td class="pagination-right">{$reference.weight}</td>
						<td class="pagination-right"></td>
					</tr>
				</tbody>
			</table>
		{else if $request.controller == 'quotations'}
			<div class="clear-both"></div>
			<hr>
			<div class="pagination-right"><b>Firma per accettazione</b></div>
		{/if}
	</body>
</html>