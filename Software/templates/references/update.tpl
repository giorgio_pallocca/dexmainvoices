{append var=includes value=[
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/js/datePicker.js',
	'/public/js/dexmaUtils.js',
	'/js/references/update.js',
	'/public/js/modalDialog.js'
] index='js'}
{include file='template.tpl'}
{if $reference.uniqueid}
	{include file='breadcrumb.tpl' page=$breadcrumbPage pageUrl=$breadcrumbPageUrl inner_page="Modifica $nameInsertReference"}
{else}
	{include file='breadcrumb.tpl' page=$breadcrumbPage pageUrl=$breadcrumbPageUrl inner_page="Inserimento $nameInsertReference"}
{/if}
<form id="referencesForm" class="form-horizontal{if $request.controller == 'transportdocuments'} ddt{/if}" enctype="multipart/form-data" method="post" action="/{$request.controller}/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$reference.uniqueid}">
	<fieldset>
		<legend>
			{if $reference.uniqueid}
				{$nameNewReference} N° {$reference.serialNumber}
			{else}
				Inserimento {$nameInsertReference}
			{/if}
		</legend>
		{if $request.controller != 'recurringinvoices'}
			<div id="serialNumberContainer" class="control-group">
				<label class="control-label" for="serialNumber">N° progressivo</label> 
				<div class="controls">
					<div class="input-append">
						<input autofocus class="input-mini" id="serialNumber" name="serialNumber" type="text" value="{$reference.serialNumber}" onkeydown="return isKeyboardNumber(event, false)" onblur="{if $request.controller != 'externalinvoices'}checkReferenceFields('date', '{$request.controller}'){else}checkSerialNumber(){/if}"><span id="year" class="add-on"></span>
					</div>
					<span id="serialNumberError" class="help-inline"></span>
				</div>
			</div>
		{/if}
		<div id="dateContainer" class="control-group">
			<label class="control-label" for="date">{if $request.controller != 'recurringinvoices'}Data{else}Data inizio{/if}</label>
			<div class="controls">
				<input class="jqueryCalendar" id="date" name="date" type="text" value="{$reference.date}"{if $request.controller != 'externalinvoices'} onchange="checkReferenceFields('date', '{$request.controller}')"{/if}>
				<span id="dateError" class="help-inline"></span>
			</div>
		</div>
		{if $request.controller == 'recurringinvoices'}
			<div class="control-group">
				<label class="control-label" for="frequency">Frequenza</label>
				<div class="controls">
					<select id="frequency" name="frequency" class="input-medium-large">
						<option {if $reference.frequency == 1_week} selected{/if} value="1_week">Settimanale</option>
						<option {if $reference.frequency == 2_week} selected{/if} value="2_week">Quindicinale</option>
						<option {if $reference.frequency == 1_month} selected{/if} value="1_month">Mensile</option>
						<option {if $reference.frequency == 2_month} selected{/if} value="2_month">Bimestrale</option>
						<option {if $reference.frequency == 3_month} selected{/if} value="3_month">Trimestrale</option>
						<option {if $reference.frequency == 4_month} selected{/if} value="4_month">Quadrimestrale</option>
						<option {if $reference.frequency == 6_month} selected{/if} value="6_month">Semestrale</option>
						<option {if $reference.frequency == 1_year} selected{/if} value="1_year">Annuale</option>
						<option {if $reference.frequency == 2_year} selected{/if} value="2_year">Biennale</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="payments_term">Termine di pagamento</label>
				<div class="controls">
					<div class="input-append">
						<input class="input-xmini" id="payments_term" name="payments_term" type="text" value="{if !$reference.payments_term}30{else}{$reference.payments_term}{/if}"><span class="add-on">giorni</span>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="status">Attiva</label>
				<div class="controls">
					<input id="status" name="status" type="checkbox"{if $reference.status == 'activated'} checked{/if}>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="code">Codice identificativo</label> 
				<div class="controls">
					<input class="input-mini" id="code" name="code" type="text" value="{$reference.code}">
				</div>
			</div>
		{/if}
		{if $request.controller != 'recurringinvoices'}
			<div class="control-group">
				<label class="control-label" for="expireDate">Data di scadenza</label>
				<div class="controls">
					<input class="jqueryCalendar" id="expireDate" name="expireDate" type="text" value="{$reference.expireDate}">
				</div>
			</div>
		{/if}
		<div id="customer_idContainer" class="control-group">
			<label class="control-label" for="customer_id">{if $request.controller != 'externalinvoices'}Cliente{else}Fornitore{/if}</label>
			<div class="controls">
				<select class="input-xlarge" id="customer_id" name="customer_id">
					{foreach from = $customers item = customer}
						<option{if $reference.customer_id == $customer.id || $customerId == $customer.id} selected{/if} value="{$customer.id}">{$customer.company}</option>
					{/foreach}
				</select>
				<div class="btn-group">
					<a class="btn btn-small btn-primary"  onclick="addCustomer()">
						Aggiungi {if $request.controller != 'externalinvoices'}cliente{else}fornitore{/if}
					</a>
				</div>
				<span id="customer_idError" class="help-inline"></span>
			</div>
		</div>
		{if $request.controller == 'externalinvoices' || $request.controller == 'invoices' || $request.controller == 'recurringinvoices'}
			<div class="control-group">
				<label class="control-label" for="payments_term_description">Modalità di pagamento</label>
				<div class="controls">
					<textarea class="input-xlarge" id="payments_term_description" name="payments_term_description" rows="6">{$reference.payments_term_description}</textarea>
					{if $reference.default_payments_term_description}
						<div class="btn-group align-top">
							<button type="button" class="btn btn-small btn-primary" onclick="$('#payments_term_description').val('{$reference.default_payments_term_description}')">Imposta standard</button>
						</div>
					{/if}
				</div>
			</div>
		{/if}
		{if $defaultTax1 || $request.controller == 'externalinvoices'}
			<div class="control-group">
				<label class="control-label" for="tax1">Rivalsa previdenza</label>
				<div class="controls">
					<select class="input-xlarge" id="tax1" name="tax1">
						{include file='references/tax1options.tpl'}
					</select>
				</div>
			</div>
		{/if}
		{if $defaultTax2 || $request.controller == 'externalinvoices'}
            <div class="control-group">
                <label class="control-label" for="tax2">Ritenuta di acconto</label>
                <div class="controls">
                    <select class="input-xlarge" id="tax2" name="tax2">
                        {include file='references/tax2options.tpl'}
                    </select>
                </div>
            </div>
		{/if}
		{if $request.controller != 'externalinvoices'}
			<div class="control-group">
				<label class="control-label" for="notes">Note</label>
				<div class="controls">
					<textarea class="input-xlarge" id="notes" name="notes" rows="6">{$reference.notes}</textarea>
					<div class="btn-group align-top">
						<button class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown" type="button">
							Aggiungi da lista note aggiuntive
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-additionalnotes">
							{foreach from = $additionalnotes item = row}
								<li><a onclick="selectFromAdditionalNotes('{$row.description|escape:'javascript'}')">{$row.title}</a></li>
							{/foreach}
						</ul>
					</div>
				</div>
			</div>
		{/if}
		{if $request.controller == 'transportdocuments'}
			<div class="control-group">
				<label class="control-label" for="transportBy">Trasporto a cura del</label>
				<div class="controls">
					<input class="input-xlarge" id="transportBy" name="transportBy" type="text" value="{$reference.transportBy}">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="appearance">Aspetto esteriore dei beni</label>
				<div class="controls">
					<input class="input-xlarge" id="appearance" name="appearance" type="text" value="{$reference.appearance}">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="vectorData">Dati del vettore</label>
				<div class="controls">
					<input class="input-xlarge" id="vectorData" name="vectorData" type="text" value="{$reference.vectorData}">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="numberPackage">N° colli</label>
				<div class="controls">
					<input class="input-mini" id="numberPackage" name="numberPackage" type="text" value="{$reference.numberPackage}">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="weight">Peso in Kg</label>
				<div class="controls">
					<input class="input-mini" id="weight" name="weight" type="text" value="{$reference.weight}">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="dateTimeTransport">Data e ora del ritiro/trasporto</label>
				<div class="controls">
					<input class="jqueryCalendar" id="dateTimeTransport" name="dateTimeTransport" type="text" value="{$reference.dateTimeTransport}">
					<select id="hours" name="hours" class="input-mini">
					{section name=ora loop=24}
						<option{if $reference.hours == {$smarty.section.ora.index|string_format:'%\'02u'}} selected{/if} value="{$smarty.section.ora.index|string_format:'%\'02u'}">{$smarty.section.ora.index|string_format:"%'02u"}</option>
					{/section}
					</select>
					<select id="minutes" name="minutes" class="input-mini">
						{section name=minuti loop=60}
							<option{if $reference.minutes == {$smarty.section.minuti.index|string_format:'%\'02u'}} selected{/if} value="{$smarty.section.minuti.index|string_format:'%\'02u'}">{$smarty.section.minuti.index|string_format:"%'02u"}</option>
						{/section}
					</select>
				</div>
			</div>
		{/if}
		{if $request.controller != 'recurringinvoices' && $request.controller != 'externalinvoices'}
			<div class="control-group">
				<label class="control-label" for="privatenotes">Note private</label>
				<div class="controls">
					<textarea class="input-xlarge" id="privatenotes" name="privatenotes" rows="6">{$reference.privatenotes}</textarea>
				</div>
			</div>
		{/if}
		<div id="otherOptions" class="hide">
			<div class="control-group">
				<div class="controls">
					<label class="checkbox">
						<input id="private" name="private" type="checkbox"{if $reference.private == 1} checked{/if}>Visibile solo agli amministratori
					</label>
				</div>
			</div>
		</div>
		{if $request.controller == 'externalinvoices'}
			<div class="control-group">
				<label class="control-label" for="attachment">Allegato</label>
				<div class="controls invoiceAttachment">
					{if $reference.attachment}
						<div id="attachmentName">
							<a href="/uploads/externalinvoicesUploads/{$reference.attachment}" target="_blank" alt="Allegato">{$reference.attachment}</a>
							<button class="btn btn-mini btn-danger" type="button" onclick="removeAttachment('{$reference.attachment}')" title="Rimuovi allegato"><i class="icon-remove-sign icon-white"></i> Rimuovi allegato</button>
						</div>
					{/if}
					<input class="input-xlarge{if $reference.attachment} hide{/if}" id="attachment" name="attachment" type="file">
				</div>
			</div>
		{/if}
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<button type="button" id="showOptions" name="showOptions" class="btn btn-primary" onclick="showOptionsDiv()">Mostra altre opzioni</button>
				<button type="button" id="hideOptions" name="hideOptions" class="btn btn-primary" onclick="hideOptionsDiv()">Nascondi altre opzioni</button>
			</div>
		</div>
		<div class="form-actions">
			<a href="/{$request.controller}/{if $reference.uniqueid}view?uid={$reference.uniqueid}{else}{$request.controller}{/if}" class="btn">Annulla</a>
			<input id="saveReferencesBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveReference()">
		</div>
	</fieldset>
</form>
{include file='customers/addnewcustomer.tpl'}
{include file='footer.tpl'}