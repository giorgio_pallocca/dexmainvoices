{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/js/datePicker.js',
	'/public/js/dexmaUtils.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<h3 class="pull-left">{$titleList}</h3>
	<div class="pull-right control-group mainButtons">
		{if $request.controller == 'invoices'}
			<button class="btn" onclick="printListInvoices()">Stampa</button>
			<div class="btn-group">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-download-alt"></i> Scarica fatture <span class="caret"></span></a>
				<ul class="dropdown-menu pull-right">
					{foreach from=$years item=row}
						<li><a href="#" onclick="generateZip('{$row}')">Anno {$row}</a></li>
					{/foreach}
				</ul>
			</div>
		{/if}
		<a href="/{$request.controller}/update" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> {$buttonText}</a>
	</div>
</div>
<form id="referenceSearch" class="well well-small form-inline search-form">
	<label for="startDate" class="moreMarginBottom">Dal:</label>
	<div class="input-prepend input-append">
		<button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'startDate', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button>
		<input class="jqueryCalendar" name="startDate" id="startDate" value="{$search.startDate}" title="Seleziona data" onfocus="this.blur()" data-autosearch data-resetvalue="{$search.startDateReset}">
		<button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'startDate', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
	</div>
	<div class="inline-block">
		<label for="endDate" class="moreMarginBottom">al:</label>
		<div class="input-prepend input-append">
			<button id="substractDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'endDate', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button>
			<input class="jqueryCalendar" name="endDate" id="endDate" value="{$search.endDate}" title="Seleziona data" onfocus="this.blur()" data-autosearch data-resetvalue="{$search.endDateReset}">
			<button id="addDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'endDate', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
		</div>
	</div>
	<label>N° Progressivo: <input class="input-xmini" name="searchSerialNumber" value="{$search.searchSerialNumber}" type="text" data-autosearch></label>
	<label>{if $request.controller != 'externalinvoices'}{$words.customer}{else}Fornitore{/if}: <input class="input-medium" name="searchCustomer" value="{$search.searchCustomer}" type="text" data-autosearch></label>
	{if $request.controller == 'invoices'}
		<label class="checkbox"><input{if $search.searchUnpaid} checked{/if} type="checkbox" name="searchUnpaid" data-autosearch> {$words.unpaid_a}</label>
	{/if}
	<button class="btn" id="referenceSearchButton" type="button">{$words.search}</button>
	<button class="btn" id="referenceShowAllButton" type="button">{$showAllText}</button>
</form>
<div id="referencesGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
						 data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/references/common.js?v={$appVersion}"></script>
{if $request.controller == 'invoices'}
	<script src="/js/invoices/invoices.js?v={$appVersion}"></script>
{elseif $request.controller == 'externalinvoices'}
	<script src="/js/externalinvoices/externalinvoices.js?v={$appVersion}"></script>
{else}
	<script src="/js/references/references.js?v={$appVersion}"></script>
{/if}
{if $request.controller == 'invoices'}
	<div id="generateZipModal" class="modal hide">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Download fatture <span id="generateYear"></span></h3>
		</div>
		<div class="modal-body">
			<div id="generatingZipBox">
				Operazione in corso, si prega di attendere.<hr>
				<div class="progress progress-striped active">
					<div id="generatingBar" class="bar" style="width:0"></div>
				</div>
			</div>
			<div id="errorZipBox" class="hide">
				Si è verificato un errore, si prega di riprovare.
			</div>
			<div id="resultZipBox" class="hide">
				<p>Operazione conclusa con successo</p>
				<a href="#" class="btn btn-primary btn-large">Scarica fatture</a>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Chiudi</button>
		</div>
	</div>
{/if}
{include file='footer.tpl'}