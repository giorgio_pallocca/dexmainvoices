<option value="0">Nessuna</option>
<option{if $reference.tax1 == 'INPS Gestione separata---4---1'} selected{/if} value="INPS Gestione separata---4---1">INPS Gestione separata 4% (Professionisti senza cassa)</option>
<option{if $reference.tax1 == 'C.N.P.A.I.A.---4---0'} selected{/if} value="C.N.P.A.I.A.---4---0">C.N.P.A.I.A. 4% (Architetti e ingegneri)</option>
<option{if $reference.tax1 == 'C.N.G.---4---0'} selected{/if} value="C.N.G.---4---0">C.N.G. 4% (Geometri)</option>
<option{if $reference.tax1 == 'Cassa avvocati---4---0'} selected{/if} value="Cassa avvocati---4---0">Cassa avvocati 4%</option>
<option{if $reference.tax1 == 'C.P.D.C.---4---0'} selected{/if} value="C.P.D.C.---4---0">C.P.D.C. 4% (Commercialisti)</option>
<option{if $reference.tax1 == 'ENPAM---4---0'} selected{/if} value="ENPAM---4---0">ENPAM 4% (Medici)</option>
<option{if $reference.tax1 == 'C.N.N.---4---0'} selected{/if} value="C.N.N.---4---0">C.N.N. 4% (Notai)</option>
<option{if $reference.tax1 == 'ENASARCO---7.325---0'} selected{/if} value="ENASARCO---7.325---0">ENASARCO 7,325%</option>