<script src="/js/references/editmovement.js?v={$appVersion}"></script>
<div id="editMovementDIV" class="modal custom-modal movements-modal hide">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h4>Scheda movimento</h4>
	</div>
	<div class="modal-body">
		<form id="movementForm" class="form-horizontal custom-form" method="post">
			<input type="hidden" name="movementUniqueid" id="movementUniqueid">
			<input type="hidden" id="referenceUniqueid" name="referenceUniqueid" value="{$reference.uniqueid}" noreset>
			<div id="quantityContainer" class="control-group">
				<label class="control-label" for="quantity">Quantità</label>
				<div class="controls">
					<input class="input-mini" id="quantity" name="quantity" type="text" onkeydown="return isKeyboardNumber(event, false)" onblur="checkQuantity()" tabindex=10>
					<select name="unit_id" id="unit_id" class="input-medium">
						<option value="-1">-</option>
						{foreach from=$units item=row}
							<option value="{$row.id}">{$row.description}</option>
						{/foreach}
					</select>
					<span id="quantityError" class="help-inline"></span>
				</div>
			</div>
      
      <div id="nomeContainer" class="control-group">
                <label class="control-label">Nome prod. magazzino</label>
                <div class="controls">
                    <a onclick="schedaProdotto()"><input class="btn btn-info hide" id="nomeProdotto" name="nomeProdotto" type="button" data-uniqueid='' ></a>
                </div>
            </div>
      
      
			<div id="descriptionMovementContainer" class="control-group">
				<label for="descriptionMovement" class="control-label">Nome prodotto fattura</label>
				<div class="controls">
					<textarea id="descriptionMovement" name="descriptionMovement" onblur="checkDescription(this.id)" rows=4 tabindex=10 class="input-large"></textarea>
					<div class="btn-group align-top">
						<button class="btn btn-mini btn-primary dropdown-toggle" data-toggle="dropdown" type="button">
							Aggiungi da lista prodotti
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-products">
							{foreach from=$products item=row}
								<li><a onclick="selectFromProduct('{$row.product_name}', {$row.amountsale}, {$row.vat}, {$row.id}, {$row.amount}, '{$row.uniqueid}','{$reference.customer_id}')">{$row.product_name|truncate:38}</a></li>
							{/foreach}
						</ul>
					</div>
					<span id="descriptionMovementError" class="help-inline align-top"></span>
				</div>
			</div>
			{if $request.controller != 'transportdocuments'}
				<div id="freeContainer" class="control-group">
					<label class="control-label" for="free">Spesa esente</label>
					<div class="controls">
						<input id="free" name="free" type="checkbox"{if $movement.free == 1} checked{/if} onclick="isFree('vat')">
					</div>
				</div>
			{/if}
			<div id="priceContainer" class="control-group">
				<label for="price" class="control-label">Prezzo unitario (€)</label>
				<div class="controls">
					<input class="input-mini" id="price" name="price" type="text" onkeydown="return isKeyboardNumber(event, true)" onblur="checkPrice()" tabindex=10>
                    <input type="hidden" id="id_prodotto" name="id_prodotto" noreset>
                    <input type="hidden" id="amount" name="amount" noreset>
					<span id="priceError" class="help-inline"></span>
				</div>
			</div>
			<div id="selectContainer" class="control-group hide">
            <label class="control-label" for="amountsale">Variazione prezzo</label>
            <div class="controls">
                <div class="btn-group align-top">
					<button class="btn btn-mini btn-primary dropdown-toggle " data-toggle="dropdown" type="button">
						Seleziona Tipo Cliente
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown-customertypes" id="customertypesList">
						{foreach from = $customertypes item = customertype}
							<li><a onclick="selectFromCustomertype('{$customertype.id}')" id="list{$customertype.id}">{$customertype.customer_type|substr:0:36}</a></li>
						{/foreach}
					</ul>
				</div>
            </div>
        </div>
			<div id="vatContainer" class="control-group">
				<label for="vat" class="control-label">IVA (%)</label>
				<div class="controls">
					<input class="input-mini" id="vat" name="vat" type="text" onkeydown="return isKeyboardNumber(event, false)" onblur="checkVat(this.id)" tabindex=10>
					<span id="vatError" class="help-inline"></span>
				</div>
			</div>
		</form>
		<div class="form-actions pagination-right">
			<input id="cancelButton" type="button" class="btn" value="Annulla" data-dismiss="modal">
			<input id="deleteMovementBtn" type="button" class="btn btn-danger hide" value="Elimina" onclick="deleteMovement()">
			<input type="button" class="btn btn-info" value="Conferma e continua" onclick="saveMovement(true)" tabindex=10>
			<input id="saveMovementBtn" type="button" class="btn btn-primary" value="Conferma" onclick="saveMovement()">
		</div>
	</div>
</div>