{append var=includes value=['/public/ListsManager/css/simplePagingGrid-0.2.css'] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/js/datePicker.js',
	'/public/js/dexmaUtils.js',
	'/public/js/modalDialog.js',
	'/js/payments/update.js'
] index='js'}
{include file='template.tpl'}
{include file='breadcrumb.tpl' page=$breadcrumbPage pageUrl=$breadcrumbPageUrl inner_page="{if {$reference.serialNumber}}N° {$reference.serialNumber}{else}Fattura ricorrente{/if}"}
<div class="row-fluid">
	<div class="pull-left">
		<h3>{$nameReference} {$reference.serialNumber}</h3>
	</div>
	<div class="pull-right mainButtons">
		<a href="{$breadcrumbPageUrl}" class="btn hidden-phone">Torna alla lista</a>
		<button class="btn btn-danger" onclick="deleteReference('{$reference.uniqueid}')">Elimina</button>
		<a href="/{$request.controller}/update?uid={$reference.uniqueid}" class="btn btn-primary">Modifica</a>
		{if $request.controller != 'externalinvoices' && $request.controller != 'recurringinvoices'}
			<div class="btn-group">
				<button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
					Strumenti
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<li><a href="/{$request.controller}/print?uniqueid={$reference.uniqueid}">Scarica come PDF</a></li>
					<li><a href="/{$request.controller}/sendbyemail?uniqueid={$reference.uniqueid}">Invia via email</a></li>
					{if $request.controller == 'invoices'}
						<li><a href="/{$request.controller}/sendbyemail?uniqueid={$reference.uniqueid}&amp;reminder=true">Invia sollecito</a></li>
					{/if}
					{if $request.controller != 'creditnotes'}
						{if $request.controller == 'quotations'}
							<li><a href="/{$request.controller}/exportreference?uniqueid={$reference.uniqueid}&amp;DDT=true">Converti in {$destinationNameDDT}</a></li>
						{/if}
						<li><a href="/{$request.controller}/exportreference?uniqueid={$reference.uniqueid}">Converti in {$destinationName}</a></li>
					{/if}
					<li id="sentStatusToggler" value="{$reference.sent}">
						<a href="#" onclick="toggleSentStatus('{$reference.uniqueid}')">
							Segna come inviato
							<span id="sentStatus">
								{if $reference.sent == 1}
									<i class="icon-ok"></i>
								{/if}
							</span>
						</a>
					</li>
					{if $request.controller == 'invoices'}
						<li><a href="#" onclick="paymentDetail('', 0, 1)">Associa un pagamento</a></li>
					{/if}
				</ul>
			</div>
		{/if}
	</div>
</div>
<hr>
<div id="referencePreview" class="span9 offset1">
	<div class="row-fluid">
		{if $logoPath}
			<div class="pull-left">
				<img src="/index/getlogo" alt="Company logo">
			</div>
		{/if}
		<div class="pull-right pagination-right">
			{$reference.seller|nl2br}
		</div>
	</div>
	<hr>
	<div class="row-fluid">
		<div class="pull-left">
			{$reference.buyer|nl2br}
		</div>
		<div class="pull-right pagination-right">
			<h4 class="referenceTitle">{$nameReference} {$reference.serialNumber}{if $reference.year}/{$reference.year}{/if}</h4>
			data: <span class="referenceDate">{$reference.date}</span><br>
			{if $reference.expireDate} scadenza: <span class="referenceDate">{$reference.expireDate}</span>{/if}
		</div>
	</div>
	<div class="row">
		<div id="editMovementsBtn" class="control-group pull-right">
			<button class="btn btn-primary" onclick="movementDetail('')">
				<i class="icon-plus-sign icon-white"></i>
				Aggiungi movimenti
			</button>
		</div>
	</div>
	<div id="referenceMovementsGrid" data-uid="{$reference.uniqueid}"></div>
	<script src="/js/references/view.js?v={$appVersion}"></script>
	<div class="row-fluid break-word">
		{if $reference.payments_term_description}
			<div class="pull-left span6">
				<b>MODALITÀ DI PAGAMENTO</b><br>
				{$reference.payments_term_description|nl2br}
			</div>
		{/if}
		{if $reference.notes}
			<div class="pull-right span6 pagination-right">
				<b>ALTRE INFORMAZIONI</b><br>
				{$reference.notes|nl2br}
			</div>
		{/if}
	</div>
	<div class="row-fluid break-word">
		{if $reference.transportBy}
			<div class="span3">
				<b>Trasporto a cura del</b><br>
				{$reference.transportBy}
			</div>
		{/if}
		{if $reference.appearance}
			<div class="span3">
				<b>Aspetto esteriore dei beni</b><br>
				{$reference.appearance}
			</div>
		{/if}
		{if $reference.numberPackage}
			<div class="span2">
				<b>N° colli</b><br>
				{$reference.numberPackage}
			</div>
		{/if}
	</div>
	<div class="row-fluid break-word">
		{if $reference.dateTimeTransport}
			<div class="span3 ddtDetailMargin">
				<b>Data e ora del ritiro/trasporto</b><br>
				{$reference.dateTimeTransport}
			</div>
		{/if}
		{if $reference.vectorData}
			<div class="span3 ddtDetailMargin">
				<b>Dati del vettore</b><br>
				{$reference.vectorData}
			</div>
		{/if}
		{if $reference.weight}
			<div class="span2 ddtDetailMargin">
				<b>Peso in Kg</b><br>
				{$reference.weight}
			</div>
		{/if}
	</div>
	{include file='references/editmovement.tpl'}
</div>
{if $request.controller != 'externalinvoices' && $request.controller != 'recurringinvoices'}
	<div class="row-fluid span12">
	    <ul id="referenceTabs" class="nav nav-tabs">
		    {if $request.controller == 'invoices'}<li><a href="#payments" data-toggle="tab">Pagamenti</a></li>{/if}
		    <li><a href="#messages" data-toggle="tab">Messaggi</a></li>
		    <li><a href="#privatenotes" data-toggle="tab">Note private</a></li>
		</ul>
		<div class="tab-content">
		  {if $request.controller == 'invoices'}
		       <div class="tab-pane active" id="payments">
				     <div class="well">
				         {if $payments}
				          <table class="table table-condensed cursor-default">
				              <thead>
				                  <tr>
				                      <th class="span4">Descrizione</th>
				                      <th class="span2">Data</th>
				                      <th class="span3 pagination-right">Importo</th>
				                      <th class="span3 pagination-right">Scoperto</th>
				                  </tr>
				              </thead>
				              <tbody>
				                  {foreach from=$payments item=row}
				                      <tr>
				                          <td class="span4">{$row.description}</td>
				                          <td class="span2">{$row.date}</td>
				                          <td class="span3"><div class="pagination-right">&euro; {$row.amount}</div></td>
				                          <td class="span3"><div class="text-error pagination-right">&euro; {$row.unpaid_amount}</div></td>
				                      </tr>
				                  {/foreach}
				              </tbody>
				          </table>
				       {else}
				         Non ci sono pagamenti
				       {/if}
				     </div>
				</div>
		   {/if}
	       <div class="tab-pane active" id="messages">
	            <div class="well">
	                {if $messages}
						<table class="table table-condensed cursor-default" id="summaryMessages">
						    <thead>
						        <tr>
						            <th class="span3">Oggetto</th>
						            <th class="span5">Testo</th>
						            <th class="span2">Destinatario</th>
						            <th class="span2">Data e ora invio</th>
						        </tr>
						    </thead>
						    <tbody>
						        {foreach from=$messages item=msg}
						            <tr>
						                <td class="span3">{$msg.subject}</td>
						                <td class="span5"><p>{$msg.text|nl2br}</p></td>
						                <td class="span2"><p>{$msg.customer_email}</p></td>
						                <td class="span2">{$msg.creation_date}</td>
						            </tr>
						        {/foreach}
						    </tbody>
						</table>
					{else}
					    Non ci sono messaggi
					{/if}
				</div>
	       </div>
		   <div class="tab-pane active" id="privatenotes">
		       <div class="well">
		           {if $reference.privatenotes}
		               {$reference.privatenotes}
	               {else}
	                    Non ci sono note private
	               {/if}
		       </div>
		   </div>
		</div>
	</div>
{/if}
{assign var=quickPayment value=true}
<script src="/public/PaymentsManager/js/update.js"></script>
<script src="/js/payments/payments.js?v={$appVersion}"></script>
{include file='payments/update.tpl'}
{include file='footer.tpl'}
