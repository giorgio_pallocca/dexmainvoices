{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/references/sendbyemail.js'
] index='js'}
{include file='template.tpl'}
<div class="span5 hide" id="errorMsg">
	<div class="alert alert-block alert-error">
		<button type="button" class="close" onclick="closeMsg()">×</button>
		<h4 class="alert-heading">Errore</h4>
		Si è verificato un errore durante l'invio del messaggio. <br>
		Si prega di riprovare.
	</div>
</div>
<form id="sendbyemailForm" autocomplete="off" class="form-horizontal span12" method="post">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$reference.uniqueid}">
	<fieldset>
		{if $reminder == 'reminder'}
			<legend>Invio sollecito di pagamento</legend>
		{else}
			<legend>Invio {$sendReference}</legend>
		{/if}
		<div class="row">
			<div id="legend-settings" class="control-group pull-left">
				<i>Si possono inserire destinatari multipli utilizzando la virgola (,) per separare gli indirizzi.</i>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="sender">Da</label>
			<div class="controls">
				<select class="select-xlarge" id="sender" name="sender">
					{foreach from = $sender item = row}
						<option value="{$row.id}" {if $row.id == $currentUser}selected{/if}>{$row.fullname}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div id="receiverContainer" class="control-group">
			<label class="control-label" for="receiver">A</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="receiver" name="receiver" type="text" value="{$receiver}" onblur="checkField('receiver', 'Inserire almeno un destinatario')">
				<span id="receiverError" class="help-inline"></span>
			</div>
		</div>
		{if !$reminder}
			<div class="control-group">
				<label class="control-label">Allegato</label>
				<div class="controls controls-text">
					<i class="icon-file"></i> {$subject|replace:'/':'_'}
				</div>
			</div>
		{/if}
		<div class="control-group">
			<label class="control-label" for="subject">Oggetto</label>
			<div class="controls">
				<input class="input-xlarge" id="subject" name="subject" type="text"{if $messageReferenceText} value="{$subject}"{/if}>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="messageText">Messaggio</label>
			<div class="controls">
				<textarea class="input-xlarge textarea-medium" id="messageText" name="messageText">{if $messageReferenceText}{$messageReferenceText}{else}{$messageReminderText}{/if}</textarea>
			</div>
		</div>
		<div class="form-actions">
			<a href="/{$request.controller}/view?uid={$reference.uniqueid}" class="btn">Annulla</a>
			{if $reminder == 'reminder'}
				<input id="sendbyemailButton" type="button" class="btn btn-primary" value="Invia" onclick="sendByEmail('{$reference.uniqueid}','reminder')">
			{else}
				<input id="sendbyemailButton" type="button" class="btn btn-primary" value="Invia" onclick="sendByEmail('{$reference.uniqueid}', null)">
			{/if}
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}