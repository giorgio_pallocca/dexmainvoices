{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/additionalnotes/update.js'
] index='js'}
{include file='template.tpl'}
{if $note.uniqueid}
	{include file='breadcrumb.tpl' page='Note aggiuntive' pageUrl='/additionalnotes/additionalnotes' inner_page=$note.title}
{else}
	{include file='breadcrumb.tpl' page='Note aggiuntive' pageUrl='/additionalnotes/additionalnotes' inner_page='Nuova nota aggiuntiva'}
{/if}
<form id="additionalnotesForm" class="form-horizontal" method="post" action="/additionalnotes/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$note.uniqueid}">
	<fieldset>
		<legend>
			{if $note.id}
				Nota aggiuntiva: {$note.title}
			{else}
				Nuova nota aggiuntiva
			{/if}
		</legend>
		<div id="titleContainer" class="control-group">
			<label class="control-label" for="title">Titolo</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="title" name="title" type="text" value="{$note.title}" onblur="validateTitle()">
				<span id="titleError" class="help-inline"></span>
			</div>
		</div>
		<div id="descriptionContainer" class="control-group">
			<label class="control-label" for="description">Descrizione</label>
			<div class="controls">
				<textarea class="input-xlarge" id="description" name="description" rows="6" onblur="validateDescription()">{$note.description}</textarea>
				<span id="descriptionError" class="help-inline"></span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="referredto">Riferimento</label>
			<div class="controls">
				<select id="referredto" name="referredto" class="input-xlarge">
					<option {if $note.referredto == 0}selected{/if} value="0">Fatture</option>
					<option {if $note.referredto == 1}selected{/if} value="1">Preventivi</option>
					<option {if $note.referredto == 2}selected{/if} value="2">Documenti di trasporto</option>
					<option {if $note.referredto == 3}selected{/if} value="3">Note di credito</option>
				</select>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<label class="checkbox">
					<input id="isdefault" name="isdefault" type="checkbox"{if $note.isdefault == 1} checked{/if}>Seleziona come nota di default
				</label>
			</div>
		</div>
		<div class="form-actions">
			<a class="btn" href="/additionalnotes/additionalnotes">Annulla</a>
			{if $note.uniqueid}
				<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteNote()">
			{/if}
			<input id="saveNoteBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveNote()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}