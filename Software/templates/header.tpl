<!DOCTYPE html>
<html lang="it">
	<head>
		<title>PiùIVA</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="/public/bootstrap/2.3.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="/public/bootstrap/2.3.2/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="/jquery/css/smoothness/jquery-ui-1.9.2.custom.min.css">
		{section name=styles loop=$includes.css}
			<link rel="stylesheet" href="{$includes.css[styles]}?v={$appVersion}">
		{/section}
		<link rel="stylesheet" href="/css/style.css?v={$appVersion}">
		<link rel="icon" type="image/ico" href="//www.piuiva.it/wp-content/uploads/2012/12/favicon.ico">
		<script src="/public/js/jquery/jquery-1.9.1.min.js"></script>
		<script src="/public/bootstrap/2.3.2/js/bootstrap.min.js"></script>
		<script src="/js/template.js?v={$appVersion}"></script>
		{section name=scripts loop=$includes.js}
			<script src="{$includes.js[scripts]}?v={$appVersion}"></script>
		{/section}
	</head>
	{assign var='accountCheck' value=0 scope=parent}
	{if empty($account.type) || $account.type == 1}
		{if $account.days > 0}
			{assign var='accountCheck' value=1 scope=parent}
		{elseif $account.daysBefore != '' && $account.days != '' && $account.daysBefore >= -$account.days}
			{assign var='accountCheck' value=2 scope=parent}
		{/if}
	{/if}
	<body id="{$request.module}-{$request.controller}-{$request.action}"{if $accountCheck != 0} class="bodyPayment"{/if} data-module="{$request.module}" data-controller="{$request.controller}" data-action="{$request.action}"{if $pageSize} data-pagesize="{$pageSize}"{/if}>
