<div class="navbar navbar-fixed-top navbar-inverse">
	{if $accountCheck == 1}
		<div id="expiredAccountMsg" class="alert alert-error">
			<!-- <strong>Attenzione!</strong> La tua copia di PiùIVA è scaduta da {$account.days} {if $account.days == 1} giorno{else} giorni{/if}. -->
			<strong>Attenzione!</strong> La tua copia di PiùIVA non è attiva contatta subito il tuo rivenditore.
			<!-- Hai ancora
				{if $account.tolerance-$account.days == 0}
					oggi
				{else}
					{$account.tolerance-$account.days}
					{if $account.tolerance-$account.days == 1}
						giorno
					{else}
						giorni
					{/if}
				{/if} 
			per effettuare il <a href="/login/provisioningpayment">pagamento</a>.-->
		</div>
	{elseif $accountCheck == 2}
		{assign var="accountDays" value=$account.days*-1}
		<div id="expiredAccountMsg" class="alert alert-error">
			<!-- <strong>Attenzione!</strong> La tua copia di PiùIVA scadrà -->
			<strong>Attenzione!</strong> La tua copia di PiùIVA non è attiva contatta subito il tuo rivenditore.
			<!-- {if $accountDays == 0}
				oggi.
			{else}
				tra {$accountDays}
				{if $accountDays == 1}
					giorno.
				{else}
					giorni.
				{/if}
			{/if}
			Effettua il <a href="/login/provisioningpayment">pagamento</a> adesso.-->
		</div>
	{/if}
	<div class="navbar-inner">
		<div id="menuContainer" class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target="#collapseMenu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="/"><img src="/images/piuiva38.png" alt="Logo PiùIVA"></a>
			{if !$hideMenu}
				<div id="collapseMenu" class="nav-collapse collapse">
					<ul class="nav">
						<li{if $request.controller == 'customers'} class="active"{/if}><a href="/customers/customers">{$words.contacts}</a></li>
						{if $loggedUser->menu == 0}
							<li class="dropdown{if $request.controller == 'invoices' || $request.controller == 'externalinvoices'} active{/if}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Fatture
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li{if $request.controller == 'invoices'} class="active"{/if}><a href="/invoices/invoices">{$words.invoices}</a></li>
									<li{if $request.controller == 'externalinvoices'} class="active"{/if}><a href="/externalinvoices/externalinvoices">{$words.external_invoices}</a></li>
									<li{if $request.controller == 'recurringinvoices'} class="active"{/if}><a href="/recurringinvoices/recurringinvoices">Fatture ricorrenti</a></li>
								</ul>
							</li>
							<li class="dropdown{if $request.controller == 'quotations' || $request.controller == 'transportdocuments' || $request.controller == 'creditnotes'} active{/if}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									{$words.documents}
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li{if $request.controller == 'quotations'} class="active"{/if}><a href="/quotations/quotations">{$words.estimates}</a></li>
									<li{if $request.controller == 'transportdocuments'} class="active"{/if}><a href="/transportdocuments/transportdocuments" title="Documenti di trasporto">{$words.ddt}</a></li>
									<li{if $request.controller == 'creditnotes'} class="active"{/if}><a href="/creditnotes/creditnotes">{$words.creditnotes}</a></li>
								</ul>
							</li>
                            <li{if $request.controller == 'products'} class="active"{/if}><a href="/products/products">{$words.products}</a></li>
							<li{if $request.controller == 'payments'} class="active"{/if}><a href="/payments/payments">{$words.payments}</a></li>
							<li{if $request.controller == 'transferts'} class="active"{/if}><a href="/transferts/transferts">{$words.transferts}</a></li>
							<li{if $request.controller == 'reports'} class="active"{/if}><a href="/reports/invoicesbymonth">Reports</a></li>
							{*
							<li class="dropdown{if $request.controller == 'reports'} active{/if}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Report
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li{if $request.action == 'customersreport'} class="active"{/if}><a href="/reports/customersreport">Contatti</a></li>
								</ul>
							</li>
							*}
						{else}
							<li class="dropdown{if $request.controller == 'quotations' || $request.controller == 'transportdocuments' || $request.controller == 'creditnotes'} active{/if}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									{$words.documents}
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li{if $request.controller == 'quotations'} class="active"{/if}><a href="/quotations/quotations">{$words.estimates}</a></li>
									<li{if $request.controller == 'invoices'} class="active"{/if}><a href="/invoices/invoices">{$words.invoices}</a></li>
									<li{if $request.controller == 'creditnotes'} class="active"{/if}><a href="/creditnotes/creditnotes">{$words.creditnotes}</a></li>
								</ul>
							</li>
							<li{if $request.controller == 'payments'} class="active"{/if}><a href="/payments/payments">{$words.payments}</a></li>
							<li{if $request.controller == 'transferts'} class="active"{/if}><a href="/transferts/transferts">{$words.transferts}</a></li>
							<li{if $request.controller == 'reports'} class="active"{/if}><a href="/reports/invoicesbymonth">Reports</a></li>
						{/if}	
					</ul>
					<ul class="nav pull-right">
						{if $loggedUser->role == 'administrator'}
							<li class="dropdown{if $request.controller == 'users' || $request.controller == 'company' || $request.controller == 'settings' || $request.controller == 'additionalnotes'
													|| $request.controller == 'vehicles'} active{/if}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									{$words.options}
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li{if $request.controller == 'users'} class="active"{/if}><a href="/users/users">{$words.users}</a></li>
									<li{if $request.controller == 'company'} class="active"{/if}><a href="/company/update">{$words.company_data}</a></li>
									<li{if $request.controller == 'typecustomers'} class="active"{/if}><a href="/typecustomers/typecustomers">Listini Clienti</a></li>
									<li{if $request.controller == 'vehicles'} class="active"{/if}><a href="/vehicles/vehicles">{$words.vehicles}</a></li>
									<li{if $request.controller == 'settings'} class="active"{/if}><a href="/settings/update">{$words.settings}</a></li>
									<li{if $request.controller == 'additionalnotes'} class="active"{/if}><a href="/additionalnotes/additionalnotes">{$words.customize_notes}</a></li>
									<li{if $request.controller == 'units'} class="active"{/if}><a href="/units/units">Personalizza unità di misura</a></li>
								</ul>
							</li>
						{/if}
						<li><a href="/login/logout">Logout {$loggedUser->firstname}</a></li>
					</ul>
				</div><!--nav-collapse-->
			{/if}
		</div>
	</div>
</div>
