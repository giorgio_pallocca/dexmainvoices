{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/company/update.js'
] index='js'}
{include file='template.tpl'}

<div class="row-fluid">
	<img id="graphBarIcon" class="pull-left" src="/public/images/metroicons/48/appbar.graph.bar.png" alt>
	<h3 class="pull-left">Reports</h3>
	<table class="table table-striped table-bordered table-condensed table-hover">
		<tr>
		<th>Anno</th>
		<th>Mese</th>
		<th>Imponibile</th>
		<th>Totale</th>
		{foreach from=$invoicesbymonth item=row}
			<tr>
				<td>{$row.y}</td>
				<td>{$row.m}</td>
				<td>{$row.taxable}</td>			
				<td>{$row.amount}</td>			
			</tr>
		{/foreach}			
	</table>
</div>

{include file='footer.tpl'}