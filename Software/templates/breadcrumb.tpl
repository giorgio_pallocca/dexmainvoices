<ul class="breadcrumb">
	<li>
		{if $pageUrl}
			<a href="{$pageUrl}">{$page}</a>
		{else}
			{$page}
		{/if}
	</li>
	{if $inner_page}
		<li class="active"><span class="divider">/</span> {$inner_page}</li>
	{/if}
</ul>