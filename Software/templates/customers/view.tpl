{include file='template.tpl'}
{include file='breadcrumb.tpl' page='Contatti' pageUrl='/customers/customers' inner_page='Vista contatto'}
<div class="row-fluid">
	<div class="pull-left">
		<h3>{$customer.company}</h3>
	</div>
	<div class="pull-right mainButtons">
		<a class="btn" href="/customers/customers">Vai alla lista contatti</a>
		<a href="/invoices/update?customerId={$customer.id}" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Nuova fattura</a>
	</div>
</div>
<hr>
<ul id="customerTabs" class="nav nav-tabs">
	<li><a href="#data" data-toggle="tab">Dati contatto</a></li>
	<li><a href="#invoices" data-toggle="tab">Fatture</a></li>
	<li><a href="#quotations" data-toggle="tab">Preventivi</a></li>
	<li><a href="#ddt" data-toggle="tab">DDT</a></li>
	<li><a href="#creditnote" data-toggle="tab">Note di credito</a></li>
	<li><a href="#messages" data-toggle="tab">Messaggi</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="data">
		<div class="well">
			<div class="row-fluid">
				<div class="pull-left">
					<dl class="dl-horizontal customerDetails">
						<dt>Denominazione</dt>
						<dd>{$customer.company}</dd>
						{if $customer.firstname}
							<dt>Nome</dt>
							<dd>{$customer.firstname}</dd>
						{/if}
						{if $customer.surname}
							<dt>Cognome</dt>
							<dd>{$customer.surname}</dd>
						{/if}
						{if $customer.email}
							<dt>Email</dt>
							<dd>{$customer.email}</dd>
						{/if}
						{if $customer.vatNumber}
							<dt>Partita IVA</dt>
							<dd>{$customer.vatNumber}</dd>
						{/if}
						{if $customer.fiscalCode}
							<dt>Codice Fiscale</dt>
							<dd>{$customer.fiscalCode}</dd>
						{/if}
						{if $customer.address}
							<dt>Indirizzo</dt>
							<dd>{$customer.address}</dd>
						{/if}
						{if $customer.zipcode}
							<dt>CAP</dt>
							<dd>{$customer.zipcode}</dd>
						{/if}
						{if $customer.city}
							<dt>Citt&agrave;</dt>
							<dd>{$customer.city}</dd>
						{/if}
						{if $customer.state}
							<dt>Provincia</dt>
							<dd>{$customer.state}</dd>
						{/if}
						{if $customer.type}
							<dt>Tipologia</dt>
							<dd>{$customer.type}</dd>
						{/if}
						{if $customer.distanceKm}
							<dt>Distanza in km</dt>
							<dd>{$customer.distanceKm}</dd>
						{/if}
						{if $customer.phone}
							<dt>Telefono</dt>
							<dd>{$customer.phone}</dd>
						{/if}
						{if $customer.fax}
							<dt>Fax</dt>
							<dd>{$customer.fax}</dd>
						{/if}
						{if $customer.notes}
							<dt>Note</dt>
							<dd class="justified" style="max-width:840px;">{$customer.notes|nl2br}</dd>
						{/if}
					</dl>
				</div>
				<div class="pull-right control-group mainButtons">
					<a class="btn btn-primary" href="/customers/update?uid={$customer.uniqueid}">Modifica dati</a>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="invoices">
		<div class="well">
			{if $invoice}
				<h4>FATTURE</h4>
				<table class="table table-condensed customer-view-table">
					<thead>
						<tr>
							<th>Progressivo</th>
							<th class="pagination-right">Importo</th>
							<th class="pagination-right">Scoperto</th>
						</tr>
					</thead>
					<tbody>
						{foreach from = $invoice item = row}
							<tr>
								{if $row.is_external == 'emessa'}
									<td><a href="/invoices/view?uid={$row.uniqueid}">{$row.serialNumber} ({$row.is_external})</a></td>
								{else}
									<td><a href="/externalinvoices/view?uid={$row.uniqueid}">{$row.serialNumber} ({$row.is_external})</a></td>
								{/if}
								<td><div class="pagination-right">{$row.amount|number_format:2:',':'.'}</div></td>
								<td><div class="text-error pagination-right">{$row.unpaid_amount|number_format:2:',':'.'}</div></td>
							</tr>
						{/foreach}
					</tbody>
				</table>
				<table class="table table-condensed customer-view-table">
					<thead>
						<th>FATTURE TOTALI</th>
						<th class="pagination-right">TOTALE DA PAGARE</th>
						<th class="pagination-right">TOTALE SCOPERTO</th>
					</thead>
					<tbody>
						{foreach from = $totalAmount item = row}
							<tr>
								<td>{$row.totalInvoices}</td>
								<td><div class="pagination-right">{$row.debtAmount|number_format:2:',':'.'}</div></td>
								<td><div class="text-error pagination-right"><strong>{$row.totalUnpaid|number_format:2:',':'.'}</strong></div></td>
							</tr>
						{/foreach}
					</tbody>
				</table>
			{else}
				Non ci sono fatture
			{/if}
		</div>
	</div>
	<div class="tab-pane" id="quotations">
		{include file='customers/reftable.tpl' reference=$quotations refTitle="PREVENTIVI" refname="quotations"}
	</div>
	<div class="tab-pane" id="ddt">
		{include file='customers/reftable.tpl' reference=$ddt refTitle="DDT" refname="transportdocuments"}
	</div>
	<div class="tab-pane" id="creditnote">
		{include file='customers/reftable.tpl' reference=$creditnotes refTitle="NOTE DI CREDITO" refname="creditnotes"}
	</div>
	<div class="tab-pane" id="messages">
		<div class="well">
			{if $messages}
				<h4>MESSAGGI</h4>
				<table class="table table-condensed" id="customersMessages">
					<thead>
						<tr>
							<th class="span1">Oggetto</th>
							<th class="span11">Testo</th>
						</tr>
					</thead>
					<tbody>
						{foreach from = $messages item = row}
							<tr>
								<td class="span4">{$row.subject}</td>
								<td class="span8"><p>{$row.text}</p></td>
							</tr>
						{/foreach}
					</tbody>
				</table>
			{else}
				Non ci sono messaggi
			{/if}
		</div>
	</div>
</div>
{literal}<script>
$(function(){
	$('#customerTabs a:first').tab('show');
});
</script>{/literal}
{include file='footer.tpl'}