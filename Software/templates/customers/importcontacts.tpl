{append var=includes value=[
	'/js/customers/importcontacts.js'
] index='js'}
{include file='template.tpl'}
{include file='breadcrumb.tpl' page='Contatti' pageUrl='/customers/customers' inner_page='Importa contatti'}
<form id="importContactsForm" action="/customers/uploadcontacts" class="form-horizontal" enctype="multipart/form-data" method="post">
	<fieldset>
		<legend>Importa contatti</legend>
		{if $errorUploadFile}
			<div class="alert alert-block alert-error">
				<h4 class="alert-heading">Errore</h4>
				Si è verificato un errore durante il caricamento del file, si prega di riprovare.
			</div>
		{elseif $uploadedFile}
			<div class="alert alert-block alert-success">
				<h4 class="alert-heading">Operazione effettuata</h4>
				Il file "{$uploadedFile}" è stato caricato correttamente.
			</div>
			<h4>Anteprima file:</h4>
		{elseif $insertedCustomers}
			<div class="alert alert-block alert-success">
				<h4 class="alert-heading">Operazione effettuata</h4>
				I tuoi contatti sono stati correttamente inseriti all'interno di PiùIva!<br>
				<a href="/customers/customers" class="text-success"><h5>vai ai contatti</h5></a>
			</div>
		{/if}
		{if !$uploadedFile}
			<div class="row">
				<div id="legend-settings" class="control-group pull-left">
					<i>E' possibile caricare file in formato <strong>".csv"</strong> in cui sia stato utilizzato come separatore il carattere <strong>" ; "</strong>.<br>
						Il file dovrebbe avere le seguenti colonne: DENOMINAZIONE, NOME, COGNOME, EMAIL, PARTITA IVA, CODICE FISCALE, INDIRIZZO, CAP, CITTA', PROVINCIA, TELEFONO, FAX,
						DISTANZA IN KM.<br>
						Una volta caricato il file si vedrà un'anteprima in cui sarà possibile modificare l'associazione tra ogni colonna ed il corrispondente campo della scheda contatto.
					</i>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="contacts">Carica file</label>
				<div class="controls">
					<input class="input-xlarge" id="contacts" name="contacts" type="file">
				</div>
			</div>
		{/if}
		<div id="errorInSelect" class="text-error hide spacer20bottom">Attenzione! Il campo "Denominazione" deve essere scelto obbligatoriamente per una colonna ed ogni colonna deve avere un campo diverso dalle altre.</div>
		<div id="contactsPreview" class="hide" style="font-size: 11px;">
			<table id="contactsPreviewTable" class="table table-bordered" data-contacts='{$contactsJSON}'>
				<thead>
					<tr id="titleRow" class="text-primary"></tr>
				</thead>
				<tbody class="preview-body">
				</tbody>
			</table>
		</div>
		<div class="form-actions" {if $uploadedFile}style="padding-left: 10px"{/if}>
			<a {if $uploadedFile}href="/customers/importcontacts?removeFile=true"{else}href="/customers/customers"{/if} class="btn">Annulla</a>
			{if !$uploadedFile}<input type="submit" class="btn btn-primary" value="Carica file">{/if}
			{if $uploadedFile}<input id="confirmBtn" type="button" class="btn btn-success" value="Conferma">{/if}
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}