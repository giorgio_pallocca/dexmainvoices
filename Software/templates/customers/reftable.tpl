<div class="well">
	{if $reference}
		<h4>{$refTitle}</h4>
		<table class="table table-condensed customer-view-table">
			<thead>
				<tr>
					<th>Progressivo</th>
					<th>Data</th>
					<th class="pagination-right">Importo</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$reference item=row}
					<tr>
						<td><a href="/{$refname}/view?uid={$row.uniqueid}">{$row.serialNumber}</a></td>
						<td>{$row.date}</td>
						<td><div class="pagination-right">{$row.amount|number_format:2:',':'.'}</div></td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{else}
		Non ci sono documenti
	{/if}
</div>