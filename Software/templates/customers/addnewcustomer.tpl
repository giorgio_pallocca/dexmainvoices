<script src="/js/customers/update.js?v={$appVersion}"></script>
<div id="addCustomerDIV" class="modal custom-modal hide">
	<div class="modal-header">
		<legend>
			Inserimento {if $request.controller != 'externalinvoices'}cliente{else}fornitore{/if}
			<button type="button" class="close" data-dismiss="modal">×</button>
		</legend>
	</div>
	<div id="addCustomerModalBody" class="modal-body">
		<form id="addCustomerForm" class="form-horizontal custom-form" autocomplete="off" method="post" action="/customers/save">
			<input type="hidden" id="addCustomer" name="addCustomer" value="addCustomer">
			<div id="companyContainer" class="control-group">
				<label class="control-label" for="company">Denominazione *</label>
				<div class="controls">
					<input class="input-xlarge" id="company" name="company" type="text" onblur="validateCompany()">
					<span id="companyError" class="help-inline"></span>
				</div>
			</div>
			<div id="emailContainer" class="control-group">
				<label class="control-label" for="email">Email</label>
				<div class="controls">
					<input class="input-xlarge" id="email" name="email" type="text" value="{$customer.email}" onblur="validateEmail()">
					<span id="emailError" class="help-inline"></span>
				</div>
			</div>
			<div id="vatNumberContainer" class="control-group">
				<label class="control-label" for="vatNumber">Partita IVA *</label>
				<div class="controls">
					<input class="input-xlarge" id="vatNumber" name="vatNumber" type="text" onblur="validateVatNumberFiscalCode()">
					<span id="vatNumberError" class="help-inline"></span>
				</div>
			</div>
			<div id="fiscalCodeContainer" class="control-group">
				<label class="control-label" for="fiscalCode">Codice Fiscale *</label>
				<div class="controls">
					<input class="input-xlarge" id="fiscalCode" name="fiscalCode" type="text" value="{$customer.fiscalCode}" onblur="validateVatNumberFiscalCode()">
					<span id="fiscalCodeError" class="help-inline"></span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="address">Indirizzo e CAP</label>
				<div class="controls">
					<input class="input" id="address" name="address" type="text">
					<input class="input-xmini" id="zipcode" name="zipcode" type="text">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="city">Città e Provincia</label>
				<div class="controls">
					<input class="input" id="city" name="city" type="text">
					<input class="input-xmini" id="state" name="state" type="text">
				</div>
			</div>
			<div class="form-actions pagination-right">
				<input id="goBackBtn" class="btn" type="button" value="Annulla" data-dismiss="modal">
				<input id="saveCustomerBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveCustomer('addFromInvoice')">
			</div>
		</form>
	</div>
</div>