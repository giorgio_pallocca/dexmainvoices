{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/public/js/dexmaUtils.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<div class="pull-left"><h3>{$words.contacts}</h3></div>
	{if $loggedUser->role == 'administrator'}
		<div class="pull-right control-group mainButtons">
			<a href="/customers/importcontacts" class="btn btn-default"><i class="icon-upload"></i> Importa contatti</a>
			<a href="/customers/update" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Nuovo contatto</a>
		</div>
	{/if}
</div>
<form id="customersSearch" class="well well-small form-inline search-form">
	<label>Ricerca: <input autofocus class="input-large" name="searchCustomer" value="{$search.searchCustomer}" type="text" data-autosearch></label>
	<label>
		<select name="searchCustomerType" data-autosearch>
			<option value="-1">Tutte le tipologie</option>
			<option{if $search.searchCustomerType == "0"} selected{/if} value="0">Clienti</option>
			<option{if $search.searchCustomerType == 1} selected{/if} value="1">Fornitori</option>
			<option{if $search.searchCustomerType == 2} selected{/if} value="2">Clienti/Fornitori</option>
		</select>
	</label>
	<button class="btn" id="searchButton" type="button">Cerca</button>
	<button class="btn" id="showAllButton" type="button">Mostra tutti</button>
</form>
<div id="customersGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
						data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/customers/customers.js?v={$appVersion}"></script>
{include file='footer.tpl'}