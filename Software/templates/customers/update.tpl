{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/customers/update.js'
] index='js'}
{include file='template.tpl'}
{if $customer.uniqueid}
	{include file='breadcrumb.tpl' page='Contatti' pageUrl='/customers/customers' inner_page=$customer.company}
{else}
	{include file='breadcrumb.tpl' page='Contatti' pageUrl='/customers/customers' inner_page='Nuovo contatto'}
{/if}
<form id="customersForm" autocomplete="off" class="form-horizontal" method="post" action="/customers/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$customer.uniqueid}">
	<fieldset>
		<legend>{$customer.company|default:'Nuovo contatto'}</legend>
		<div class="control-group">
			<label class="control-label" for="firstname">Nome</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="firstname" name="firstname" type="text" value="{$customer.firstname}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="surname">Cognome</label>
			<div class="controls">
				<input class="input-xlarge" id="surname" name="surname" type="text" value="{$customer.surname}">
			</div>
      
        
        
		</div>
	<label class="radio-inline">
            <div class="control-group controls">
                <input class="customerType" type="radio" name="inlineRadioOptions" value="azienda"> Azienda
                <input class="customerType" type="radio" name="inlineRadioOptions" value="privato"> Privato
            </div>
        </label>	
    <div id="companyContainer" class="control-group">
            <label class="control-label" for="company">Denominazione *</label>
            <div class="controls">
                <input class="input-xlarge" id="company" name="company" type="text" value="{$customer.company}" onblur="validateCompany()">
                <span id="companyError" class="help-inline"></span>
            </div>
        </div>
        <div id="emailContainer" class="control-group">
			<label class="control-label" for="email">Email</label>
			<div class="controls">
				<input class="input-xlarge" id="email" name="email" type="text" value="{$customer.email}" onblur="validateEmail()">
				<span id="emailError" class="help-inline"></span>
			</div>
		</div>
		<div id="vatNumberContainer" class="control-group">
			<label class="control-label" for="vatNumber">Partita IVA</label>
			<div class="controls">
				<input class="input-xlarge" id="vatNumber" name="vatNumber" type="text" value="{$customer.vatNumber}" onblur="{literal}checkNumber('vatNumber', 'Partita IVA NON conforme', /^[0-9]{11}$/){/literal}">
				<span id="vatNumberError" class="help-inline"></span>
			</div>
		</div>
		<div id="fiscalCodeContainer" class="control-group">
			<label class="control-label" for="fiscalCode">Codice Fiscale</label>
			<div class="controls">
				<input class="input-xlarge" id="fiscalCode" name="fiscalCode" type="text" value="{$customer.fiscalCode}" onblur="{literal}checkNumber('fiscalCode', 'Codice Fiscale NON conforme', /^[A-Za-z0-9]{11,16}$/){/literal}" >
				<span id="fiscalCodeError" class="help-inline"></span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="address">Indirizzo e CAP</label>
			<div class="controls">
				<input class="input" id="address" name="address" type="text" value="{$customer.address}">
				<input class="input-xmini" id="zipcode" name="zipcode" type="text" value="{$customer.zipcode}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="city">Citt&agrave; e Provincia</label>
			<div class="controls">
				<input class="input" id="city" name="city" type="text" value="{$customer.city}">
				<input class="input-xmini" id="state" name="state" type="text" value="{$customer.state}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="phone">Telefono e Fax</label>
			<div class="controls">
				<input class="input-medium" id="phone" name="phone" type="text" value="{$customer.phone}">
				<input class="input-medium" id="fax" name="fax" type="text" value="{$customer.fax}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="type">Tipologia</label>
			<div class="controls">
				<select id="type" name="type">
					<option value="0">Cliente</option>
					<option{if $customer.type == 1} selected{/if} value="1">Fornitore</option>
					<option{if $customer.type == 2} selected{/if} value="2">Cliente/Fornitore</option>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="type">Listino</label>
			<div class="controls">
				<select id="id_list" name="id_list">
					<option value="0">Standard</option>
					{foreach from = $customertypes item = customertype}
							<option{if $customertype.id == $customer.id_list} selected{/if} value="{$customertype.id}">{$customertype.customer_type|substr:0:36}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div id="distanceKmContainer" class="control-group">
			<label class="control-label" for="distanceKm">Distanza in km</label>
			<div class="controls">
				<input class="input-medium" id="distanceKm" name="distanceKm" type="text" value="{$customer.distanceKm}" onkeydown="return isKeyboardNumber(event, true)">
				<span id="distanceKmError" class="help-inline"></span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="notes">Note</label>
			<div class="controls">
				<textarea class="input-xlarge" id="notes" name="notes" rows="6">{$customer.notes}</textarea>
			</div>
		</div>
		<div class="form-actions">
			{if $customer.uniqueid}
				<a class="btn" href="/customers/view?uid={$customer.uniqueid}">Annulla</a>
			{else}
				<a class="btn" href="/customers/customers">Annulla</a>
			{/if}	
			<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteCustomer()">
			<input id="saveCustomerBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveCustomer()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}