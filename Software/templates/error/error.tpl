<!DOCTYPE html>
<html>
	<head>
		<title>PiuIVA Error</title>
	</head>
	<body>
		<h1>Si &egrave; verificato un errore</h1>
		<h2>{$message}</h2>
		<h3>Informazioni sull'eccezione:</h3>
		<p>
			<b>Messaggio:</b>
			{$exceptionMessage}
		</p>
		<h3>Stack trace:</h3>
		<pre>{$exceptionTrace}</pre>
		<h3>Parametri della Request:</h3>
		<pre>{$request}</pre>
	</body>
</html>