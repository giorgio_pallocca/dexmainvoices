{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/products/update.js'
] index='js'}
{include file='template.tpl'}
{if $product.uniqueid}
	{include file='breadcrumb.tpl' page='Prodotti' pageUrl='/products/products' inner_page=$product.description}
{else}
	{include file='breadcrumb.tpl' page='Prodotti' pageUrl='/products/products' inner_page='Nuovo prodotto'}
{/if}
<form id="productsForm" class="form-horizontal" method="post" action="/products/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$product.uniqueid}">
	<fieldset>
		<legend>
			{if $product.id}
				Prodotto: {$product.description}
			{else}
				Nuovo prodotto
			{/if}
		</legend>
        <div id="product_nameContainer" class="control-group">
            <label class="control-label" for="product_name">Nome Prodotto</label>
            <div class="controls">
                <input autofocus class="input-xlarge" id="product_name" name="product_name" type="text" value="{$product.product_name}" onblur="validateProduct_name()">
                <span id="product_nameError" class="help-inline"></span>
            </div>
        </div>
		<div id="descriptionContainer" class="control-group">
			<label class="control-label" for="description">Descrizione</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="description" name="description" type="text" value="{$product.description}" onblur="validateDescription()">
				<span id="descriptionError" class="help-inline"></span>
			</div>
		</div>
        <div id="quantityContainer" class="control-group">
            <label class="control-label" for="quantity">Quantità</label>
            <div class="controls">
                <input class="input-mini" id="quantity" name="quantity" type="text" value="{$product.quantity}" onblur="validateQuantity()">
                <span id="quantityError" class="help-inline"></span>
            </div>
        </div>
		<div id="amountContainer" class="control-group">
			<label class="control-label" for="amount">Importo Acquisto</label>
			<div class="controls">
				<input class="input-mini" id="amount" name="amount" type="text" value="{$product.amount}" onblur="validateAmount()">
				<span id="amountError" class="help-inline"></span>
			</div>
		</div>
        <div id="amountsaleContainer" class="control-group">
            <label class="control-label" for="amountsale">Importo Vendita</label>
            <div class="controls">
                <input class="input-mini" id="amountsale" name="amountsale" type="text" value="{$product.amountsale}" onblur="validateAmountSale()">
                <span id="amountsaleError" class="help-inline"></span>
            </div>
        </div>
        <p id="amountsaleAddContainer"></p>

        <div id="selectContainer" class="control-group">
            <label class="control-label" for="amountsale">Variazione prezzo</label>
            <div class="controls">
                <div class="btn-group align-top">
					<button class="btn btn-mini btn-primary dropdown-toggle" data-toggle="dropdown" type="button">
						Seleziona Tipo Cliente
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown-customertypes" id="customertypesList">
						{foreach from = $customertypes item = customertype}
							<li><a onclick="selectFromCustomertype('{$customertype.customer_type}', '{$customertype.id}')" id="list{$customertype.id}">{$customertype.customer_type|substr:0:36}</a></li>
						{/foreach}
					</ul>
				</div>
            </div>
        </div>
		<div id="vatContainer" class="control-group">
			<label class="control-label" for="vat">IVA</label>
			<div class="controls">
				<div class="input-append">
					<input class="input-mini" id="vat" name="vat" type="text" value="{$product.vat}" onblur="validateVat()"><span class="add-on">%</span>
				</div>
				<span id="vatError" class="help-inline"></span>
			</div>
		</div>
		<div class="form-actions">
			<a class="btn" href="/products/products">Annulla</a>
			{if $product.uniqueid}
				<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteProduct()">
			{/if}
			<input id="saveProductBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveProduct()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}