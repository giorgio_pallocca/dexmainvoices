{append var=includes value=[
    '/js/products/view.js',
    '/jquery/js/jquery-ui-1.9.2.custom.min.js',
    '/js/datePicker.js'
] index='js'}
{include file='template.tpl'}
{include file='breadcrumb.tpl' page='Prodotti' pageUrl='/products/products' inner_page='Vista Prodotto'}
<input id="tab-name" value="{$tabname}" type="hidden">
<div class="row-fluid">
    <div class="pull-left">
        <h3>{$product.product_name}</h3>
    </div>
    <div class="pull-right mainButtons">
        <a class="btn" href="/products/products">Vai alla lista Prodotti</a>
        <a href="/products/update" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Nuovo prodotto</a>
    </div>
</div>
<hr>
<ul id="productTabs" class="nav nav-tabs">
    <li><a href="#data" data-toggle="tab">Dati prodotto</a></li>
    <li><a href="#movements" data-toggle="tab">Movimenti magazzino</a></li>
    <!--<li><a href="#quotations" data-toggle="tab">Preventivi</a></li> 
    <li><a href="#ddt" data-toggle="tab">DDT</a></li>
    <li><a href="#creditnote" data-toggle="tab">Note di credito</a></li>
    <li><a href="#messages" data-toggle="tab">Messaggi</a></li>-->
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="data">
        <div class="well">
            <div class="row-fluid">
                <div class="pull-left">
                    <dl class="dl-horizontal customerDetails">
                        <dt>Nome prodotto</dt>
                        <dd>{$product.product_name}</dd>
                        {if $product.description}
                            <dt>Descrizione</dt>
                            <dd>{$product.description}</dd>
                        {/if}
                        {if $product.quantity}
                            <dt>Quantità</dt>
                            <dd>{$product.quantity}</dd>
                        {/if}
                        {if $product.vat}
                            <dt>Iva</dt>
                            <dd>{$product.vat}</dd>
                        {/if}
                        {if $product.amount}
                            <dt>Importo acquisto</dt>
                            <dd>{$product.amount}<span class="help-inline" style="font-size: 11px">Il prezzo visualizzato è quello del movimento del magazzino inserito per ultimo</span></dd>
                        {/if}
                        {if $product.amountsale}
                            <dt>Importo vendita</dt>
                            <dd>{$product.amountsale}<span class="help-inline" style="font-size: 11px">Il prezzo visualizzato è quello del movimento del magazzino inserito per ultimo</span></dd>
                        {/if}
                    </dl>
                </div>
                <div class="pull-right control-group mainButtons">
                    <a class="btn btn-primary" href="/products/update?uid={$product.uniqueid}">Modifica dati prodotto</a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="movements">
        <div class="well">
            <h4>Movimenti magazzino</h4>
            <table class="table table-condensed product-view-table">
                <thead>
                    <tr>
                        <th>Carico</th>
                        <th class="pagination-right">Scarico</th>
                        <th class="pagination-right">Prezzo Acquisto</th>
                        <th class="pagination-right">Prezzo Vendita</th>
                        <th class="pagination-right">Descrizione</th>
                        <th class="pagination-right">Data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="product-row-up product-id" data-id="{$product.id}">
                            <td><input class="input-mini product-load pro-enab product-load-err margin-zero-size" name="load" type="text" value="0"></td>
                            <td><input class="input-mini product-unload pro-enab product-unload-err margin-zero-size" name="unload" type="text" value="0" ></td>
                            <td><input class="input-mini product-prbuyunitary pro-enab product-prbuyunitary-err margin-zero-size" name="prbuyunitary" type="text" value="{0|number_format:2:',':'.'}" ></td>
                            <td><input class="input-mini product-prsaleunitary pro-enab product-prsaleunitary-err margin-zero-size" name="prsaleunitary" type="text" value="{0|number_format:2:',':'.'}" ></td>
                            <td><input class="input-small product-description pro-enab margin-zero-size" name="description" type="text" value="" ></td>
                            <td><input class="jqueryCalendar product-date margin-zero-size" name="date" value="{$smarty.now|date_format:'%d/%m/%Y'}" title="Seleziona data" onfocus="this.blur()" ></td>
                            <td><button class="btn btn-mini btn-primary add-movement-btn" >Aggiungi</button>
                            </td>

                        </tr>
                    {foreach from = $productmovements item = row}
                        <tr class="product-row" data-id="{$row.id}">
                                <td><input class="input-mini product-load pro-enab product-load-err margin-zero-size" name="load" type="text" value="{$row.load}" disabled></td>
                                <td><input class="input-mini product-unload pro-enab product-unload-err margin-zero-size" name="unload" type="text" value="{$row.unload}" disabled></td>
                                <td><input class="input-mini product-prbuyunitary pro-enab product-prbuyunitary-err margin-zero-size" name="prbuyunitary" type="text" value="{$row.prbuyunitary|number_format:2:',':'.'}" disabled></td>
                                <td><input class="input-mini product-prsaleunitary pro-enab product-prsaleunitary-err margin-zero-size" name="prsaleunitary" type="text" value="{$row.prsaleunitary|number_format:2:',':'.'}" disabled></td>
                                <td><input class="input-small product-description pro-enab margin-zero-size" name="description" type="text" value="{$row.description}" disabled></td>
                                <td><input class="input-small product-date pro-enab jqueryCalendar margin-zero-size" name="date" type="text" value="{$row.date}" disabled></td>
                        {if $row.uniqueid_invoices == ''}
                                <td><button class="btn btn-mini modify-movement-btn" >Modifica</button>
                                    <button class="btn btn-primary btn-mini apply-movement-btn hide" >Conferma</button>
                                <button class="btn btn-mini btn-danger delete-movement-btn"><i class="icon-remove icon-white" style="vertical-align:middle"></i></button></td>
                            </tr>
                        {else}
                                <td><button class="btn btn-mini view-invoice-btn" value="{$row.uniqueid_invoices}" data-tipe="{$row.description}" >Fattura</button></td>
                            </tr>
                                
                        {/if}
                    {/foreach}
                </tbody>
            </table>
            <table class="table table-condensed customer-view-table">
                <thead>
                    <th>GIACENZA</th>
                    <th class="pagination-right">Media prezzo acquisto</th>
                    <th class="pagination-right">Media prezzo vendita</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{$totproducts}</td>
                        <td><div class="pagination-right">{$medbuy}</div></td>
                        <td><div class="pagination-right"><strong>{$medsale}</strong></div></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane" id="quotations">
        {include file='customers/reftable.tpl' reference=$quotations refTitle="PREVENTIVI" refname="quotations"}
    </div>
    <div class="tab-pane" id="ddt">
        {include file='customers/reftable.tpl' reference=$ddt refTitle="DDT" refname="transportdocuments"}
    </div>
    <div class="tab-pane" id="creditnote">
        {include file='customers/reftable.tpl' reference=$creditnotes refTitle="NOTE DI CREDITO" refname="creditnotes"}
    </div>
    <div class="tab-pane" id="messages">
        <div class="well">
            {if $messages}
                <h4>MESSAGGI</h4>
                <table class="table table-condensed" id="customersMessages">
                    <thead>
                        <tr>
                            <th class="span1">Oggetto</th>
                            <th class="span11">Testo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from = $messages item = row}
                            <tr>
                                <td class="span4">{$row.subject}</td>
                                <td class="span8"><p>{$row.text}</p></td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            {else}
                Non ci sono messaggi
            {/if}
        </div>
    </div>
</div>
{include file='footer.tpl'}