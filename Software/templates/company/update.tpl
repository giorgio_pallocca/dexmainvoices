{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/company/update.js'
] index='js'}
{include file='template.tpl'}
{if $confirmMessage}
	<div class="span5">
		<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<h4 class="alert-heading">Operazione riuscita</h4>
			Le modifiche sono state salvate con successo.
		</div>
	</div>
{/if}
<form id="companyForm" action="/company/save" autocomplete="off" class="form-horizontal span12" enctype="multipart/form-data" method="post">
	<fieldset>
		<legend>Dati azienda</legend>
		{include file='company/formfields.tpl'}
		<div class="form-actions">
			<input id="saveCompanyBtn" type="button" class="btn btn-primary" onclick="saveCompany()" value="Conferma">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}