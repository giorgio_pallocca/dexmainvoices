<div class="control-group">
	<label class="control-label" for="company_name">Denominazione</label>
	<div class="controls">
		<input{if !$isWizard} autofocus{/if} class="input-xlarge" id="company_name" name="company_name" type="text" value="{$params.company_name}">
	</div>
</div>
<div id="company_vatContainer" class="control-group">
	<label class="control-label" for="company_vat">Partita IVA *</label>
	<div class="controls">
		<input class="input-xlarge" id="company_vat" name="company_vat" type="text" value="{$params.company_vat}" onblur="validateVat()">
		<span id="company_vatError" class="help-inline"></span>
	</div>
</div>
<div id="company_addressContainer" class="control-group">
	<label class="control-label" for="company_address">Indirizzo *</label>
	<div class="controls">
		<input class="input-xlarge" id="company_address" name="company_address" type="text" value="{$params.company_address}" onblur="checkCompanyAddress()">
		<span id="company_addressError" class="help-inline"></span>
	</div>
</div>
<div id="company_capContainer" class="control-group">
	<label class="control-label" for="company_cap">CAP *</label>
	<div class="controls">
		<input class="input-xlarge" id="company_cap" name="company_cap" type="text" value="{$params.company_cap}" onblur="checkCompanyCAP()">
		<span id="company_capError" class="help-inline"></span>
	</div>
</div>
<div id="company_cityContainer" class="control-group">
	<label class="control-label" for="company_city">Città *</label>
	<div class="controls">
		<input class="input-xlarge" id="company_city" name="company_city" type="text" value="{$params.company_city}" onblur="checkCompanyCity()">
		<span id="company_cityError" class="help-inline"></span>
	</div>
</div>
<div id="company_stateContainer" class="control-group">
	<label class="control-label" for="company_state">Provincia *</label>
	<div class="controls">
		<input class="input-xlarge" id="company_state" name="company_state" type="text" value="{$params.company_state}" onblur="checkCompanyState()">
		<span id="company_stateError" class="help-inline"></span>
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="company_phone">Telefono e Fax</label>
	<div class="controls">
		<input class="input-medium" id="company_phone" name="company_phone" type="text" value="{$params.company_phone}">
		<input class="input-medium" id="company_fax" name="company_fax" type="text" value="{$params.company_fax}">
	</div>
</div>
{if !$isWizard}
	<div id="company_emailContainer" class="control-group">
		<label class="control-label" for="company_email">Email *</label>
		<div class="controls">
			<input class="input-xlarge" id="company_email" name="company_email" type="text" value="{$params.company_email}" onblur="checkCompanyEmail()">
			<span id="company_emailError" class="help-inline"></span>
		</div>
	</div>
{/if}
<div class="control-group">
	<label class="control-label" for="company_website">Sito web</label>
	<div class="controls">
		<input class="input-xlarge" id="company_website" name="company_website" type="text" value="{$params.company_website}">
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="company_heading">Intestazione fattura personalizzata</label>
	<div class="controls">
		<textarea class="input-xlarge" id="company_heading" name="company_heading" rows="6">{$params.company_heading}</textarea>
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="company_default_payment">Modalità di pagamento standard</label>
	<div class="controls">
		<textarea class="input-xlarge" id="company_default_payment" name="company_default_payment" rows="6">{$params.company_default_payment}</textarea>
	</div>
</div>
<div id="company_default_vatContainer" class="control-group">
	<label class="control-label" for="company_default_vat">IVA predefinita</label>
	<div class="controls">
		<div class="input-append">
			<input id="company_default_vat" class="input-xmini" name="company_default_vat" type="text" value="{$params.company_default_vat}" onkeydown="return isKeyboardNumber(event, false)" onblur="validateDefaultVat()"><span class="add-on">%</span>
		</div>
		<span id="company_default_vatError" class="help-inline"></span>
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="company_tax1">Rivalsa previdenza</label>
	<div class="controls">
		<select class="input-xlarge" id="company_tax1" name="company_tax1">
			{include file='references/tax1options.tpl'}
		</select>
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="company_tax2">Ritenuta di acconto</label>
	<div class="controls">
		<input id="company_tax2" name="company_tax2" type="checkbox"{if $params.company_tax2} checked{/if}>
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="company_logo">Logo</label>
	<div class="controls">
		{if $params.company_logo}
			<div id="logo-preview">
				<img class="company_logo" src="index/getlogo" alt="Logo">
				<input class="btn btn-small btn-danger" type="button" onclick="removeLogo()" value="Rimuovi logo">
			</div>
		{/if}
		<input class="input-xlarge{if $params.company_logo} hide{/if}" id="company_logo" name="company_logo" type="file">
	</div>
</div>
