{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/company/update.js'
] index='js'}
{include file='header.tpl'}
{include file='menu.tpl' hideMenu=TRUE}
<div class="container">
	<div class="well">
		<h1>Benvenuto su PiùIVA!</h1>
		<form id="companyForm" action="/company/createfirstuser" autocomplete="off" class="form-horizontal" enctype="multipart/form-data" method="post">
			<fieldset>
				<legend>Crea il tuo profilo utente</legend>
				<div id="firstnameContainer" class="control-group">
					<label class="control-label" for="firstname">Nome *</label>
					<div class="controls">
						<input autofocus class="input-xlarge" id="firstname" name="firstname" type="text" onblur="checkUserFirstname()">
						<span id="firstnameError" class="help-inline"></span>
					</div>
				</div>
				<div id="surnameContainer" class="control-group">
					<label class="control-label" for="surname">Cognome *</label>
					<div class="controls">
						<input class="input-xlarge" id="surname" name="surname" type="text" onblur="checkUserSurname()">
						<span id="surnameError" class="help-inline"></span>
					</div>
				</div>
				<div id="emailContainer" class="control-group">
					<label class="control-label" for="email">Email *</label>
					<div class="controls">
						<input class="input-xlarge" id="email" name="email" type="text" onblur="checkUserEmail()">
						<span id="emailError" class="help-inline"></span>
					</div>
				</div>
				<div id="passwordContainer" class="control-group">
					<label class="control-label" for="password">Password *</label>
					<div class="controls">
						<input class="input-xlarge" id="password" name="password" type="password" onblur="checkUserPassword()">
						<span id="passwordError" class="help-inline"></span>
					</div>
				</div>
				<div id="passwordConfirmContainer" class="control-group">
					<label class="control-label" for="passwordConfirm">Conferma password *</label>
					<div class="controls">
						<input class="input-xlarge" id="passwordConfirm" name="passwordConfirm" type="password" onblur="checkUserPassword()">
						<span id="passwordConfirmError" class="help-inline"></span>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Dati azienda</legend>
				{include file='company/formfields.tpl' isWizard=TRUE}
				<div class="form-actions">
					<button id="saveCompanyBtn" class="btn btn-primary btn-large" type="button" onclick="saveCompany()">Conferma</button>
				</div>
			</fieldset>
		</form>
	</div>