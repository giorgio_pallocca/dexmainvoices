{append var=includes value=[
    '/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
    '/public/js/handlebars-1.0.js',
    '/public/ListsManager/js/simplePagingGrid-0.2.js',
    '/jquery/js/jquery-ui-1.9.2.custom.min.js',
    '/js/datePicker.js',
    '/public/js/dexmaUtils.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
    <div class="pull-left"><h3>Movimenti magazzino</h3></div>
    <div class="pull-right control-group mainButtons">
        <button class="btn" onclick="printProductmovements()">Stampa</button>
    </div>
</div>
<form id="productmovementsSearch" class="search-form well well-small form-inline">
    <label>Ricerca: <input autofocus class="input-medium" name="searchProductmovements" id="searchProductmovements" value="{$search.searchProductmovements}" type="text" data-autosearch></label>
    <div class="inline-block">
        <label for="startDateSearch" class="moreMarginBottom">Dal:</label>
        <div class="input-prepend input-append">
            <button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'startDateSearch', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button>
            <input class="jqueryCalendar" name="startDateSearch" id="startDateSearch" value="{$search.startDateSearch}" title="Seleziona data" onfocus="this.blur()" data-autosearch data-resetvalue="{$search.startDateReset}">
            <button class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'startDateSearch', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
        </div>
    </div>
    <div class="inline-block">
        <label for="endDateSearch" class="moreMarginBottom">al:</label>
        <div class="input-prepend input-append">
            <button id="substractDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(false, 'endDateSearch', true)" title="Giorno precedente"><i class="icon-chevron-left"></i></button>
            <input class="jqueryCalendar" name="endDateSearch" id="endDateSearch" value="{$search.endDateSearch}" title="Seleziona data" onfocus="this.blur()" data-autosearch data-resetvalue="{$search.endDateReset}">
            <button id="addDayEndButton" class="btn addSubstractArrow" type="button" onclick="addSubstractDay(true, 'endDateSearch', true)" title="Giorno successivo"><i class="icon-chevron-right"></i></button>
        </div>
    </div>
    <button class="btn" id="searchButton" type="button">Cerca</button>
    <button class="btn" id="showAllButton" type="button">Mostra tutte</button>
</form>
<div id="productmovementsGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
                         data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/productmovements/productmovements.js?v={$appVersion}"></script>
{include file='footer.tpl'}