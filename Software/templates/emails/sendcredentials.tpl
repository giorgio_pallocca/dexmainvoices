<p>
	Gentile Utente,<br>
	la informiamo che le &egrave; stato abilitato l'accesso al software di fatturazione <b>Pi&ugrave;IVA</b>.
</p>
<br>
<p>Il suo profilo &egrave; stato attivato con le seguenti credenziali di accesso:</p>
<p>
	Email: <b>{$email}</b><br>
	Password: <b>{$password}</b>
</p>
<br>
<p>Le ricordiamo che potr&agrave; accedere a Pi&ugrave;IVA al seguente indirizzo <a href="{$websiteurl}">{$websiteurl}</a></p>
<p>Cordiali Saluti</p>