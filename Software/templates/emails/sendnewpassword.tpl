<p>
	Gentile Utente,<br>
	abbiamo ricevuto una richiesta per resettare la password del suo account su <b>Pi&ugrave;IVA</b>.
</p>
<p>Se vuole resettare la password clicchi sul link seguente:<br>
	<a href="{$recoveryLink}">{$recoveryLink}</a>
	<br>
	<br>	
	Se non vuole resettare la password, le chiediamo cortesemente di ignorare questo messaggio.
</p>
<p>Cordiali Saluti</p>