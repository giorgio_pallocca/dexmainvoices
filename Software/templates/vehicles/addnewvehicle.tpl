<div id="descriptionContainer" class="control-group">
	<label class="control-label" for="description">Descrizione</label>
	<div class="controls">
		<input autofocus class="input-xlarge" id="description" name="description" type="text" value="{$vehicle.description}" onblur="validateDescription()">
		<span id="descriptionError" class="help-inline"></span>
	</div>
</div>
<div id="plateContainer" class="control-group">
	<label class="control-label" for="plate">Targa</label>
	<div class="controls">
		<input class="input-xlarge" id="plate" name="plate" type="text" value="{$vehicle.plate}" onblur="validatePlate()">
		<span id="plateError" class="help-inline"></span>
	</div>
</div>
<div id="aciCoefficientContainer" class="control-group">
	<label class="control-label" for="aciCoefficient">Coefficiente ACI</label>
	<div class="controls">
		<input class="input-mini" id="aciCoefficient" name="aciCoefficient" type="text" value="{$vehicle.aciCoefficient}" onkeydown="return isKeyboardNumber(event, true)" onblur="validateAciCoefficient()">
		<span id="aciCoefficientError" class="help-inline"></span>
	</div>
</div>