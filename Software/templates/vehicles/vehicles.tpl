{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/public/js/dexmaUtils.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<div class="pull-left"><h3>Veicoli</h3></div>
	<div class="pull-right control-group mainButtons">
		<a href="/vehicles/update" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> Nuovo veicolo</a>
	</div>
</div>
<form id="vehiclesSearch" class="search-form well well-small form-inline">
	<label>Ricerca: <input autofocus class="input-large" name="searchVehicle" value="{$search.searchVehicle}" type="text" data-autosearch></label>
	<button class="btn" id="searchButton" type="button">Cerca</button>
	<button class="btn" id="showAllButton" type="button">Mostra tutti</button>
</form>
<div id="vehiclesGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
					   data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/vehicles/vehicles.js?v={$appVersion}"></script>
{include file='footer.tpl'}