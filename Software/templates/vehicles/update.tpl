{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/vehicles/update.js'
] index='js'}
{include file='template.tpl'}
{if $vehicle.uniqueid}
	{include file='breadcrumb.tpl' page='Veicoli' pageUrl='/vehicles/vehicles' inner_page=$vehicle.plate}
{else}
	{include file='breadcrumb.tpl' page='Veicoli' pageUrl='/vehicles/vehicles' inner_page='Nuovo veicolo'}
{/if}
<form id="vehiclesForm" class="form-horizontal" method="post" action="/vehicles/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$vehicle.uniqueid}">
	<fieldset>
		<legend>
			{if $vehicle.id}
				Veicolo: {$vehicle.plate}
			{else}
				Nuovo veicolo
			{/if}
		</legend>
		<div id="descriptionContainer" class="control-group">
			<label class="control-label" for="description">Descrizione</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="description" name="description" type="text" value="{$vehicle.description}" onblur="validateDescription()">
				<span id="descriptionError" class="help-inline"></span>
			</div>
		</div>
		<div id="plateContainer" class="control-group">
			<label class="control-label" for="plate">Targa</label>
			<div class="controls">
				<input class="input-xlarge" id="plate" name="plate" type="text" value="{$vehicle.plate}" onblur="validatePlate()">
				<span id="plateError" class="help-inline"></span>
			</div>
		</div>
		<div id="aciCoefficientContainer" class="control-group">
			<label class="control-label" for="aciCoefficient">Coefficiente ACI</label>
			<div class="controls">
				<input class="input-mini" id="aciCoefficient" name="aciCoefficient" type="text" value="{$vehicle.aciCoefficient}" onkeydown="return isKeyboardNumber(event, true)" onblur="validateAciCoefficient()">
				<span id="aciCoefficientError" class="help-inline"></span>
			</div>
		</div>
		<div class="form-actions">
			<a class="btn" href="/vehicles/vehicles">Annulla</a>
			{if $vehicle.uniqueid}
				<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteVehicle()">
			{/if}
			<input id="saveVehicleBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveVehicle()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}