{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/public/js/modalDialog.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<div class="pull-left"><h3>Utenti</h3></div>
	<div class="pull-right control-group mainButtons">
		<button class="btn btn-primary" onclick="userDetail('')"><i class="icon-plus-sign icon-white"></i> Nuovo utente</button>
	</div>
</div>
<form id="userSearch" class="well well-small search-form form-inline">
	<label>Nome: <input autofocus class="input-medium" name="searchName" value="{$search.searchName}" type="text" data-autosearch></label>
	<label>Email: <input class="input-medium" name="searchEmail" value="{$search.searchEmail}" type="text" data-autosearch></label>
	<label>
		<select name="searchUserRole" class="input-medium" data-autosearch>
			<option value="-1">Tutti i ruoli</option>
			<option{if $search.searchUserRole == 'user'} selected{/if} value="user">Utenti</option>
			<option{if $search.searchUserRole == 'administrator'} selected{/if} value="administrator">Amministratori</option>
		</select>
	</label>
	<button class="btn" id="searchButton" type="button">Cerca</button>
	<button class="btn" id="showAllButton" type="button">Mostra tutti</button>
</form>
<div id="usersGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}"
					data-activepage="{$activePage}" data-list='{$list}'>
</div>
<script src="/js/users/users.js?v={$appVersion}"></script>
{include file='footer.tpl'}