{append var=includes value=[
	'/public/js/dexmaUtils.js',
	'/js/users/update.js'
] index='js'}
{include file='template.tpl'}
{if $user.uniqueid}
	{include file='breadcrumb.tpl' page='Utenti' pageUrl='/users/users' inner_page="{$user.firstname} {$user.surname}"}
{else}
	{include file='breadcrumb.tpl' page='Utenti' pageUrl='/users/users' inner_page='Nuovo utente'}
{/if}
<form autocomplete="off" id="userForm" class="form-horizontal" method="post" action="/users/save">
	<input type="hidden" id="uniqueid" name="uniqueid" value="{$user.uniqueid}">
	<fieldset>
		<legend>
			{if $user.uniqueid}
				{$user.firstname} {$user.surname}
			{else}
				Nuovo utente
			{/if}
		</legend>
		<div id="firstnameContainer" class="control-group">
			<label class="control-label" for="firstname">Nome *</label>
			<div class="controls">
				<input autofocus class="input-xlarge" id="firstname" name="firstname" type="text" value="{$user.firstname}" onblur="checkField(this.id, 'Nome obbligatorio')">
				<span id="firstnameError" class="help-inline"></span>
			</div>
		</div>
		<div id="surnameContainer" class="control-group">
			<label class="control-label" for="surname">Cognome *</label>
			<div class="controls">
				<input class="input-xlarge" id="surname" name="surname" type="text" value="{$user.surname}" onblur="checkField(this.id, 'Cognome obbligatorio')">
				<span id="surnameError" class="help-inline"></span>
			</div>
		</div>
		<div id="emailContainer" class="control-group">
			<label class="control-label" for="email">Email *</label>
			<div class="controls">
				<input class="input-xlarge" id="email" name="email" type="text" value="{$user.email}" onblur="validateEmail()">
				<span id="emailError" class="help-inline"></span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="phone">Telefono</label>
			<div class="controls">
				<input class="input-xlarge" id="phone" name="phone" type="text" value="{$user.phone}">
			</div>
		</div>
		<div id="passwordContainer" class="control-group">
			<label class="control-label" for="password">Password</label>
			<div class="controls">
				<input class="input-xlarge" id="password" name="password" type="password" onblur="validatePassword()">
				<span id="passwordError" class="help-inline"></span>
			</div>
		</div>
		<div id="passwordConfirmContainer" class="control-group">
			<label class="control-label" for="passwordConfirm">Conferma password</label>
			<div class="controls">
				<input class="input-xlarge" id="passwordConfirm" name="passwordConfirm" type="password" value="" onblur="validatePassword()">
				<span id="passwordConfirmError" class="help-inline"></span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="role">Ruolo</label>
			<div class="controls">
				<select id="role" name="role">
					<option value="user">Utente</option>
					<option value="administrator"{if $user.role == 'administrator'} selected{/if}>Amministratore</option>
				</select>
			</div>
		</div>
		<div id="sendCredentialsContainer" class="control-group">
			<div class="controls">
				<label class="checkbox">
					<input id="sendCredentials" name="sendCredentials" type="checkbox"{if !$user.uniqueid} checked{/if}>Invia credenziali via email
				</label>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<label class="checkbox">
					<input id="dashboard" name="dashboard" type="checkbox"{if $user.dashboard == 1} checked{/if}>Abilita dashboard
				</label>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="menu">Tipologia menu</label>
			<div class="controls">
				<select id="menu" name="menu">
					<option value="0">Default</option>
					<option value="1"{if $user.menu == 1} selected{/if}>Professionista</option>
				</select>
			</div>
		</div>
		<div class="form-actions">
			<a href="/users/users" class="btn">Annulla</a>
			{if $user.uniqueid && $user.uniqueid != $loggedUser->uniqueid}
				<input class="btn btn-danger" type="button" value="Elimina" onclick="deleteUser()">
			{/if}
			<input id="saveUserBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveUser()">
		</div>
	</fieldset>
</form>
{include file='footer.tpl'}