<div id="typecustomersModal" class="modal custom-modal hide">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Tipo Cliente</h3>
	</div>
	<div class="modal-body">
		<form id="typecustomersForm" autocomplete="off" class="form-horizontal" method="post" action="/typecustomers/save">
			<input type="hidden" id="typecustomersId" name="typecustomersId">
			<div id="typecustomersNameContainer" class="control-group">
				<label class="control-label" for="typecustomersCustomer_type">Tipo cliente</label>
				<div class="controls">
					<input class="input-large" id="typecustomersCustomer_type" name="typecustomersCustomer_type" type="text">
					<span id="typecustomersError" class="help-inline"></span>
				</div>
			</div>
			<div class="form-actions">
				<input class="btn" id="typecustomersCancelButton" data-dismiss="modal" type="button" value="Annulla">
				<input id="deleteTypecustomersBtn" class="btn btn-danger hide" type="button" value="Elimina" onclick="deleteTypecustomers()">
				<input id="saveTypecustomersBtn" class="btn btn-primary" type="button" value="Conferma" onclick="saveTypecustomers()">
			</div>
		</form>
	</div>
</div>