{append var=includes value=[
	'/public/ListsManager/css/simplePagingGrid-0.2.css'
] index='css'}
{append var=includes value=[
	'/public/js/handlebars-1.0.js',
	'/jquery/js/jquery-ui-1.9.2.custom.min.js',
	'/public/ListsManager/js/simplePagingGrid-0.2.js',
	'/public/js/dexmaUtils.js',
	'/js/datePicker.js',
	'/public/js/modalDialog.js',
	'/js/typecustomers/update.js'
] index='js'}
{include file='template.tpl'}
<div class="row-fluid">
	<div class="pull-left"><h3>Listini Clienti</h3></div>
	<div class="pull-right control-group mainButtons">
		<button class="btn btn-primary" onclick="typecustomersDetail('')"><i class="icon-plus-sign icon-white"></i>Nuovo tipo cliente</button>
	</div>
</div>
<form id="typecustomersSearch" class="well well-small form-inline search-form">
	<label>Ricerca: <input autofocus class="input-medium" name="searchName" value="{$search.searchName}" type="text" data-autosearch></label>
	<button class="btn" id="searchButton" type="button">Cerca</button>
	<button class="btn" id="showAllButton" type="button">Mostra tutti</button>
</form>
<div id="typecustomersGrid" data-activesortfield="{$activeSortField}" data-activesortdirection="{$activeSortDirection}" data-activepage="{$activePage}" data-list='{$list}'></div>
<script src="{dexma_asset url='/js/typecustomers/typecustomers.js'}"></script>
{include file='typecustomers/typecustomersmodal.tpl'}
{include file='footer.tpl'}